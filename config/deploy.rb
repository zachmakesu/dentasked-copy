set :application, "dentasked"
set :repo_url, "git@bitbucket.org:gorated/dentasked.git"

# Project-specific overrides go here.
# For list of variables that can be customized, see:
# https://github.com/mattbrictson/capistrano-mb/blob/master/lib/capistrano/tasks/defaults.rake

fetch(:mb_recipes) << "sidekiq"
fetch(:mb_aptitude_packages).merge!(
  "redis-server@ppa:rwky/redis" => :redis
)

set :deploy_to, -> { "/home/#{fetch(:user)}/apps/#{fetch(:application)}" }

set :mb_recipes, %w(
  user
  aptitude
  dotenv
  logrotate
  migrate
  seed
  nginx
  postgresql
  rake
  rbenv
  ufw
  unicorn
  version
)

set :mb_dotenv_keys, %w(
  android_patient_download_url
  app_download_url
  aws_sns_access_key
  aws_sns_secret_key
  aws_sns_region
  birthdate_secret_key
  bitly_api_key
  consumer_fcm_key
  checkmobi_secret
  docs_username
  docs_password
  facebook_app_id
  facebook_app_secret
  gmail_username
  gmail_password
  google_client_id
  google_client_secret
  google_map_api
  hmac_secret
  ios_patient_download_url
  license_number_secret_key
  mailgun_domain
  mailgun_username
  mailgun_password
  mobile_number_secret_key
  partner_fcm_key
  pusher_app_id
  pusher_app_key
  pusher_app_secret
  rails_secret_key_base
  sidekiq_web_username
  sidekiq_web_password
  tmpi_cms_username
  tmpi_cms_password
  firebase_dynamic_link_domain
  dynamic_link_base
  dynamic_link_apn
  dynamic_link_dfl
)

set :linked_files, -> {
  [fetch(:mb_dotenv_filename)] +
  %w(
      config/database.yml
      config/unicorn.rb
      config/client_secrets.json
  ) +
    [
      "config/consumer_apn_#{fetch(:stage)}.pem",
      "config/partner_apn_#{fetch(:stage)}.pem"
  ]
}

set :db_local_clean, true
set :db_remote_clean, true
set :locals_rails_env, 'production'
set :assets_dir, 'public/system'

set :whenever_roles, ->{ :app }
set :whenever_identifier, ->{ "Whenever: #{fetch(:application)}_#{fetch(:stage)}" }

namespace :deploy do
  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/#{fetch(:branch)}`
        puts 'WARNING: HEAD is not the same as origin/master'
        puts 'Run `git push` to sync changes.'
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'mb:unicorn:start', 'mb:sidekiq:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'mb:unicorn:restart'
      invoke 'mb:sidekiq:restart'
    end
  end

  before :starting,  :check_revision
  after :finishing,  :compile_assets
  after :finishing,  :cleanup
  after :finishing,  :restart
end
