Rails.application.routes.draw do

  mount API::Root => "/"
  mount Sidekiq::Web => "/sidekiq" # monitoring console

  scope '/tmpi' do
    get "/", to: "tmpi#index", as: "tmpi_index"
    post "/:id/confirm", to: "tmpi#confirm", as: "tmpi_confirm"
    post "/:id/decline", to: "tmpi#decline", as: "tmpi_decline"
  end
  get "support", to: "support#index"
  get "funfacts", to: "home#funfacts"
  post "support", to: "support#feedback", as: "submit_feedback"
  get "successful_password_change", to: "home#successful_password_change"

  scope "/cms" do
    unauthenticated :user do
      devise_scope :user do
        root to: "devise/sessions#new", as: "unauthenticated_root"
      end
    end

    authenticated :user do
      root to: "admin#index"
    end

    post "admin/dentists/:id/assignment",
      to: "admin/dentists#assignment",
      as: "admin_dentist_assignment"
    post "admin/dentists/:id/unassignment",
      to: "admin/dentists#unassignment",
      as: "admin_dentist_unassignment"
    delete "admin/dentists/:id/assignment/:clinic_assignment_id/schedule",
      to: "admin/dentists#delete_schedule",
      as: "admin_dentist_delete_schedule"
    post "admin/users/:id/toggle_status",
      to: "admin/users#toggle_status",
      as: "admin_toggle_user_status"

    get "admin/users/graph", to: "admin/users#graph"
    get "admin/appointments/graph", to: "admin/appointments#graph"

    get "admin/patients/appointments/graph", to: "admin/patients#graph"
    get "admin/patients/graph", to: "admin/patients#patients_graph"

    get "admin/secretaries/appointments/graph", to: "admin/secretaries#graph"
    get "admin/secretaries/graph", to: "admin/secretaries#secretaries_graph"

    get "admin/location/provinces", to: "admin/cities#provinces"

    get "admin/clinics/map", to: "admin/clinics#map"

    get "admin/rally_exports",
      to: "admin/rally_exports#registered_users",
      as: "admin_rally_exports"

    post "admin/users/:id/resend_code",
      to: "admin/users#resend_code",
      as: "admin_resend_user_code"

    get "admin/sms", to: "admin/sms#index", as: "admin_sms"
    post "admin/sms", to: "admin/sms#change", as: "admin_sms_change"

    authenticate :user do
      namespace :admin do
        resources :appointments, only: [:index, :show, :new, :create]
        resources :cities, only: [:index, :update]
        resources :clinics, except: :show
        resources :dentists
        resources :drafts, only: [:index, :update]
        resources :faqs, except: :show
        resources :feedbacks
        resources :hmos
        resources :patients
        resources :partner_signups, only: [:index, :update]
        resources :photos, only: [:destroy]
        resources :ratings, only: [:index, :show]
        resources :rating_suggestions, except: :show
        resources :secretaries
        resources :services
        resources :specialties
        resources :symptoms
        resources :users, only: [:index, :show, :edit, :update]
        resources :app_versions, except: [:new]
      end
    end
    devise_for :users, controllers: { passwords: "passwords" }
  end

  get "ios", to: "home#ios", as: "ios"
  get "download", to: "home#download", as: "mobile_download"
  # TODO : Uncomment on development
  # get '/rails/mailers' => "rails/mailers#index"
  # get '/rails/mailers/*path' => "rails/mailers#preview"
  get "docs", to: 'docs#index', as: 'docs'

  # Redirect all unknown path
  # and root to 404
  return404 = proc { [404, {}, [""]] }
  get "/", to: return404
  match "*path", to: return404, via: :get
end
