# Staging configuration is identical to production, with some overrides
# for hostname, etc.

require_relative "./production"

Rails.application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    authentication:  :plain,
    address: "smtp.mailgun.org",
    port: 587,
    domain: ENV["MAILGUN_DOMAIN"],
    user_name: ENV["MAILGUN_USERNAME"],
    password: ENV["MAILGUN_PASSWORD"]
  }

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = {
    host: "staging.dentasked.xyz"
  }

  config.action_mailer.asset_host = "https://staging.dentasked.xyz"
  config.action_controller.asset_host = "https://staging.dentasked.xyz"

  Rails.application.routes.default_url_options[:host] = "staging.dentasked.xyz"

  # Configure lograge for staging
  config.lograge.enabled = true
  config.lograge.custom_options = lambda do |event|
    {
      host: event.payload[:host],
      time: event.time
    }
  end
end
