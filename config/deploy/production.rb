set :branch, ENV.fetch("CAPISTRANO_BRANCH", "master")

set :user, "ubuntu"
set :mb_privileged_user, "ubuntu"
set :mb_sidekiq_concurrency, 10
set :mb_nginx_redirect_hosts, %w(dentasked.xyz)
set :whenever_environment,  ->{ fetch :rails_env, fetch(:stage, "production") }

server "ec2-52-77-121-90.ap-southeast-1.compute.amazonaws.com",
  user:  "ubuntu",
  roles:  %w(app web db redis sidekiq web)
