set :branch, ENV.fetch("CAPISTRANO_BRANCH", "development")

set :user, "ubuntu"

set :mb_privileged_user, "ubuntu"

set :mb_sidekiq_concurrency, 10

set :mb_nginx_redirect_hosts, %w(dentasked-staging-api.com)

set :whenever_environment,  ->{ fetch :rails_env, fetch(:stage, "staging") }

server "ubuntu@ec2-52-76-158-229.ap-southeast-1.compute.amazonaws.com",
       :user => "ubuntu",
       :roles => %w(app web db redis sidekiq web)
