# https://github.com/thoughtbot/paperclip/wiki/Hashing
Paperclip::Attachment.default_options.update(
  url: "/system/:class/:attachment/:id_partition/:style/:hash.:extension",
  hash_secret: ENV.fetch("RAILS_SECRET_KEY_BASE")
)
