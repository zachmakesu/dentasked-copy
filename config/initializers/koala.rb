Koala.configure do |config|
  config.app_id = ENV.fetch("FACEBOOK_APP_ID")
  config.app_secret = ENV.fetch("FACEBOOK_APP_SECRET")
  config.api_version = "v2.10"
end
