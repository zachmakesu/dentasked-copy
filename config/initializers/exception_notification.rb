require "exception_notification/rails"

ExceptionNotification.configure do |config|
  [:production, :staging].each do |env|
    if Rails.env.send("#{env}?".to_sym)
      config.add_notifier :email, {
        email_prefix:          "[ERROR] DentaSKED (#{Rails.env})",
        sender_address:        %("Notifier" <notifier@dentasked.ph>),
        exception_recipients:  %w(ragde@gorated.ph kevinc@gorated.ph mcdave@gorated.ph jaime@gorated.ph zhiena@gorated.ph)
      }

      config.add_notifier :slack, {
        webhook_url:  "https://hooks.slack.com/services/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",
        username:  "DentaSKED Bot (#{Rails.env})",
        channel:  "#dentasked-di9it",
        additional_parameters:  {
          icon_emoji:  ":boom:"
        }
      }
    end
  end
end
