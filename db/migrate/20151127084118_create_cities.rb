class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name, null: false
      t.string :psgc_code, null: false
      t.references :province, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :cities, :psgc_code, unique: true
  end
end
