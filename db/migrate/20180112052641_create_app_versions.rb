class CreateAppVersions < ActiveRecord::Migration
  def change
    create_table :app_versions do |t|
      t.integer :category
      t.string :create_by
      t.integer :platform
      t.string :version_code

      t.timestamps null: false
    end
  end
end
