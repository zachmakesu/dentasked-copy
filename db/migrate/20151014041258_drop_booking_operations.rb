class DropBookingOperations < ActiveRecord::Migration
  def up
    drop_table :booking_operations
  end

  def down
    create_table :booking_operations do |t|
      t.string :status
      t.integer :dentist_id, index: true
      t.references :clinic, index: true, foreign_key: true, null: false
      t.references :booking, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
