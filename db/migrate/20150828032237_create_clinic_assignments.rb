class CreateClinicAssignments < ActiveRecord::Migration
  def change
    create_table :clinic_assignments do |t|
      t.integer :clinic_id, :null => false, index: true
      t.integer :dentist_id, :null => false, index: true

      t.timestamps null: false
    end
    add_index :clinic_assignments, [:clinic_id, :dentist_id], unique: true
  end
end
