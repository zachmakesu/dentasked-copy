class AddContextToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :context_type, :string, null: false
    add_column :ratings, :context_id, :int, null: false
    add_index :ratings, :context_id
  end
end
