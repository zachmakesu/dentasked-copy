class CreateDrafts < ActiveRecord::Migration
  def change
    create_table :drafts do |t|
      t.string    :classification
      t.text      :object
      t.integer   :status, default: 0
      t.datetime  :reviewed_at
      t.integer   :reviewer_id
      t.integer   :owner_id
      t.integer   :dentist_id

      t.timestamps null: false
    end

    add_index :drafts, :reviewer_id
    add_index :drafts, :owner_id
    add_index :drafts, :dentist_id
  end
end
