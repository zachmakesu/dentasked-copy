class CreateSecretaryAssignments < ActiveRecord::Migration
  def change
    create_table :secretary_assignments do |t|
      t.integer :dentist_id, index: true
      t.integer :secretary_id, index: true

      t.timestamps null: false
    end
  end
end
