class AddDentistToBookingOperations < ActiveRecord::Migration
  def change
    add_column :booking_operations, :dentist_id, :integer, index: true
  end
end
