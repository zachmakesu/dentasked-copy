class CreateTMPILogs < ActiveRecord::Migration
  def change
    create_table :tmpi_logs do |t|
      t.string :username, null: false
      t.string :status, null: false
      t.integer :appointment_id, null: false

      t.timestamps null: false
    end
  end
end
