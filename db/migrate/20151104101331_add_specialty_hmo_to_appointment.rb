class AddSpecialtyHMOToAppointment < ActiveRecord::Migration
  def change
    add_reference :appointments, :hmo, index: true, foreign_key: true
    add_reference :appointments, :specialty, index: true, foreign_key: true
  end
end
