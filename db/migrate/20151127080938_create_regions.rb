class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :psgc_code, null: false
      t.string :name, null: false

      t.timestamps null: false
    end
    add_index :regions, :psgc_code, unique: true
  end
end
