class CleanupRoleMigration < ActiveRecord::Migration
  def up
    remove_column :users, :role
    remove_column :users, :profile_id
    remove_column :users, :profile_type
  end

  def down
    add_column :users, :role, :string, null: false, default: "Patient"
    add_column :users, :profile_id, :int, :null => false, default: 0
    add_column :users, :profile_type, :string, :null => false, default: "PatientProfile"
    add_index :users, :profile_id
  end
end
