class CreateBookingOperations < ActiveRecord::Migration
  def change
    create_table :booking_operations do |t|
      t.string :status
      t.references :clinic, index: true, foreign_key: true, null: false
      t.references :booking, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
