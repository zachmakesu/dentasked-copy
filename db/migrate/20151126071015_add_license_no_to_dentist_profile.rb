class AddLicenseNoToDentistProfile < ActiveRecord::Migration
  def change
    add_column :dentist_profiles, :license_number, :string
  end
end
