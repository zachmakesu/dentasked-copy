class AddNotNullPropertiesOnAppVersions < ActiveRecord::Migration
  def change
    change_column_null :app_versions, :create_by, false
  end
end
