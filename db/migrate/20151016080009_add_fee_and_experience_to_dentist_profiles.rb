class AddFeeAndExperienceToDentistProfiles < ActiveRecord::Migration
  def change
    add_column :dentist_profiles, :fee, :decimal, precision: 10, scale: 2
    add_column :dentist_profiles, :experience, :int
  end
end
