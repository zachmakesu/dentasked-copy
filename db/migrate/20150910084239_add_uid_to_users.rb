class AddUidToUsers < ActiveRecord::Migration
  def up
    add_column :users, :uid, :string, null: false, default: ""
    add_index :users, :uid, unique: true
    User.all.each do |user|
      user.update_attribute :uid, SecureRandom.hex(16)
    end
  end
  def down
    remove_column :users, :uid
  end
end
