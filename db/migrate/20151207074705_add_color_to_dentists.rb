class AddColorToDentists < ActiveRecord::Migration
  def change
    add_column :dentist_profiles, :color, :string, default: '#2AA6A4', null: false
  end
end
