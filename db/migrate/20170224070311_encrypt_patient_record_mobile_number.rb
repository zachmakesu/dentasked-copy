class EncryptPatientRecordMobileNumber < ActiveRecord::Migration
  def up
    rename_column :patient_records, :mobile_number, :raw_mobile_number
    add_column :patient_records, :encrypted_mobile_number, :string
    add_column :patient_records, :encrypted_mobile_number_iv, :string
  end

  def down
    rename_column :patient_records, :raw_mobile_number, :mobile_number
    remove_column :patient_records, :encrypted_mobile_number
    remove_column :patient_records, :encrypted_mobile_number_iv
  end
end
