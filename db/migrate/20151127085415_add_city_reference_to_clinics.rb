class AddCityReferenceToClinics < ActiveRecord::Migration
  def change
    add_column :clinics, :city_id, :integer, index: true
  end
end
