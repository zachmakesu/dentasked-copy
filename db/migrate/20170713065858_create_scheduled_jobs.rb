class CreateScheduledJobs < ActiveRecord::Migration
  def change
    create_table :scheduled_jobs do |t|
      t.string :jid, null: false
      t.integer :schedulable_id
      t.string :schedulable_type
      t.timestamps null: false
      t.index :jid, unique: true
      t.index :schedulable_id
    end
  end
end
