class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.references :patient, index: true, null: false
      t.references :clinic_assignment, index: true, foreign_key: true, null: false
      t.datetime :schedule, null: false
      t.references :service, index: true, foreign_key: true, null: false
      t.text :note
      t.integer :status, null: false

      t.timestamps null: false
    end
    add_foreign_key :appointments, :users, column: :patient_id
  end
end
