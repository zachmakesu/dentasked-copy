class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.float :lat
      t.float :lng
      t.string :base_address
      t.string :name

      t.timestamps null: false
    end
  end
end
