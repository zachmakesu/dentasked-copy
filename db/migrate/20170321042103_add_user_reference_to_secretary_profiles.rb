class AddUserReferenceToSecretaryProfiles < ActiveRecord::Migration
  def change
    add_reference :secretary_profiles, :user, index: true, foreign_key: true
  end
end
