class CreateAppointmentHistories < ActiveRecord::Migration
  def change
    create_table :appointment_histories do |t|
      t.references :appointment, index: true, foreign_key: true
      t.integer :processor_id
      t.string  :status_from
      t.string  :status_changed_to
      t.boolean :rescheduled, default: false

      t.timestamps null: false
    end
  end
end
