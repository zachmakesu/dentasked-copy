class AddUserReferenceToDentistProfiles < ActiveRecord::Migration
  def change
    add_reference :dentist_profiles, :user, index: true, foreign_key: true
  end
end
