class EncryptBirthdate < ActiveRecord::Migration
  def up
    rename_column :users, :birthdate, :raw_birthdate
    add_column :users, :encrypted_birthdate, :string
    add_column :users, :encrypted_birthdate_iv, :string
  end

  def down
    rename_column :users, :raw_birthdate, :birthdate
    remove_column :users, :encrypted_birthdate
    remove_column :users, :encrypted_birthdate_iv
  end
end
