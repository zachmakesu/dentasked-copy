class RemoveNotNullConstraintInPatientReferenceFromAppointment < ActiveRecord::Migration
  def change
    change_column_null :appointments, :patient_id, true
  end
end
