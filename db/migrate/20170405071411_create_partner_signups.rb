class CreatePartnerSignups < ActiveRecord::Migration
  def change
    create_table :partner_signups do |t|
      t.text :clinic_object
      t.text :secretary_object
      t.text :dentist_object
      t.attachment :clinic_logo
      t.attachment :dentist_avatar
      t.attachment :secretary_avatar
      t.integer :status, default: 0
      t.integer   :reviewer_id
      t.datetime  :reviewed_at

      t.timestamps null: false
    end
    add_index :partner_signups, :reviewer_id
  end
end
