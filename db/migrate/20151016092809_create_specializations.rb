class CreateSpecializations < ActiveRecord::Migration
  def change
    create_table :specializations do |t|
      t.references :specialty, index: true, foreign_key: true
      t.integer :dentist_id, index: true

      t.timestamps null: false
    end
  end
end
