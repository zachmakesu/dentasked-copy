class RemoveRawPiiColumns < ActiveRecord::Migration
  def up
    remove_column :users, :raw_mobile_number
    remove_column :users, :raw_birthdate
    remove_column :patient_records, :raw_mobile_number
    remove_column :patient_records, :raw_birthdate
    remove_column :dentist_profiles, :raw_license_number
  end

  def down
    add_column :users, :raw_mobile_number, :string
    add_column :users, :raw_birthdate, :date
    add_column :patient_records, :raw_mobile_number, :string
    add_column :patient_records, :raw_birthdate, :date
    add_column :dentist_profiles, :raw_license_number, :string
  end
end
