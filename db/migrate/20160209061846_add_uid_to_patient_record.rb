class AddUidToPatientRecord < ActiveRecord::Migration
  def change
    add_column :patient_records, :uid, :string
  end
end
