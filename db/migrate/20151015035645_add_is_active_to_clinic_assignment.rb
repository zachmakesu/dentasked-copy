class AddIsActiveToClinicAssignment < ActiveRecord::Migration
  def change
    add_column :clinic_assignments, :is_active, :boolean, default: true
  end
end
