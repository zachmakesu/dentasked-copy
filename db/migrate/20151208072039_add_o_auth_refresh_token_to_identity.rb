class AddOAuthRefreshTokenToIdentity < ActiveRecord::Migration
  def change
    add_column :identities, :oauth_refresh_token, :text
  end
end
