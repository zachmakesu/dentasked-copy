class CreateDentalServices < ActiveRecord::Migration
  def change
    create_table :dental_services do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
