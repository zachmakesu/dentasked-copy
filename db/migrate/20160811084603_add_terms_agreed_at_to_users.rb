class AddTermsAgreedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :terms_agreed_at, :datetime
  end
end
