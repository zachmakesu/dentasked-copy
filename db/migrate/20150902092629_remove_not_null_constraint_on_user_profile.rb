class RemoveNotNullConstraintOnUserProfile < ActiveRecord::Migration
  def up
    remove_column :users, :profile_id
    remove_column :users, :profile_type
    add_column :users, :profile_id, :int
    add_column :users, :profile_type, :string
    add_index :users, :profile_id
  end

  def down
    remove_column :users, :profile_id
    remove_column :users, :profile_type
    add_column :users, :profile_id, :int, :null => false
    add_column :users, :profile_type, :string, :null => false
    add_index :users, :profile_id
  end
end
