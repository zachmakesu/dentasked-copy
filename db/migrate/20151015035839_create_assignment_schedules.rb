class CreateAssignmentSchedules < ActiveRecord::Migration
  def change
    create_table :assignment_schedules do |t|
      t.references :clinic_assignment, index: true, foreign_key: true
      t.integer :day, null: false
      t.time :start_time, null: false
      t.time :end_time, null: false
      t.boolean :is_active, null: false, default: true

      t.timestamps null: false
    end
  end
end
