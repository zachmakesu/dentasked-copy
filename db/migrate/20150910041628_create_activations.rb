class CreateActivations < ActiveRecord::Migration
  def change
    create_table :activations do |t|
      t.datetime :activated_at
      t.string :code, null: false
      t.datetime :expires_at
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
