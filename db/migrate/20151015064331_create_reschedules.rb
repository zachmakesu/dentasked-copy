class CreateReschedules < ActiveRecord::Migration
  def change
    create_table :reschedules do |t|
      t.references :appointment, index: true, foreign_key: true
      t.datetime :schedule, null: false

      t.timestamps null: false
    end
  end
end
