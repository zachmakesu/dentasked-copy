class CreateFAQs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.string :question
      t.text :answer
      t.string :app_type

      t.timestamps null: false
    end
  end
end
