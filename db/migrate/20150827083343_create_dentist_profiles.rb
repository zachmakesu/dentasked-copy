class CreateDentistProfiles < ActiveRecord::Migration
  def change
    create_table :dentist_profiles do |t|
      t.datetime :activated_at
      t.string :activation_code, null: false

      t.timestamps null: false
    end
    add_index :dentist_profiles, :activation_code, unique: true
  end
end
