class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.references :reportable, polymorphic: true, index: true
      t.references :reporter, index: true

      t.timestamps null: false
    end
    add_foreign_key :reports, :users, column: :reporter_id
  end
end
