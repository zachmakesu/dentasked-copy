class CreateServiceAssignments < ActiveRecord::Migration
  def change
    create_table :service_assignments do |t|
      t.references :service, index: true, foreign_key: true
      t.references :assigned, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
