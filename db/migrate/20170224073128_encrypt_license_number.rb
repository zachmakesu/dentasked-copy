class EncryptLicenseNumber < ActiveRecord::Migration
  def up
    rename_column :dentist_profiles, :license_number, :raw_license_number
    add_column :dentist_profiles, :encrypted_license_number, :string
    add_column :dentist_profiles, :encrypted_license_number_iv, :string
  end

  def down
    rename_column :dentist_profiles, :raw_license_number, :license_number
    remove_column :dentist_profiles, :encrypted_license_number
    remove_column :dentist_profiles, :encrypted_license_number_iv
  end
end
