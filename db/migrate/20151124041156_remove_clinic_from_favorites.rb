class RemoveClinicFromFavorites < ActiveRecord::Migration
  def change
    remove_reference :favorites, :clinic, index: true, foreign_key: true
  end
end
