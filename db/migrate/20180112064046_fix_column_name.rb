class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :app_versions, :create_by, :created_by
  end
end
