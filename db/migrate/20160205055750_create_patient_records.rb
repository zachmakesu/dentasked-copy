class CreatePatientRecords < ActiveRecord::Migration
  def change
    create_table :patient_records do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :mobile_number
      t.date :birthdate

      t.timestamps null: false
    end
  end
end
