class ModifyUserForeignKeys < ActiveRecord::Migration
  def up
    reset_user_foreign_keys
    add_foreign_key :admin_profiles, :users, on_delete: :cascade
    add_foreign_key :appointments, :users, column: :patient_id, on_delete: :cascade
    add_foreign_key :dentist_profiles, :users, on_delete: :cascade
    add_foreign_key :feedbacks, :users, on_delete: :cascade
    add_foreign_key :patient_profiles, :users, on_delete: :cascade
    add_foreign_key :secretary_profiles, :users, on_delete: :cascade
  end

  def down
    reset_user_foreign_keys
    add_foreign_key :admin_profiles, :users
    add_foreign_key :appointments, :users, column: :patient_id
    add_foreign_key :dentist_profiles, :users
    add_foreign_key :feedbacks, :users
    add_foreign_key :patient_profiles, :users
    add_foreign_key :secretary_profiles, :users
  end

  private

  def reset_user_foreign_keys
    remove_foreign_key :admin_profiles, :users
    remove_foreign_key :appointments, column: :patient_id
    remove_foreign_key :dentist_profiles, :users
    remove_foreign_key :feedbacks, column: :user_id
    remove_foreign_key :patient_profiles, :users
    remove_foreign_key :secretary_profiles, :users
  end
end
