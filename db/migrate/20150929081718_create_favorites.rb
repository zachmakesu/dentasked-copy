class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :clinic, index: true, foreign_key: true
      t.integer :patient_id, null: false

      t.timestamps null: false
    end
    add_index :favorites, :patient_id
  end
end
