class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.references :user, index: true, foreign_key: true
      t.string :platform, null: false
      t.string :token, null: false
      t.boolean :enabled, null: false, default: true
      t.string :name, null: false

      t.timestamps null: false
    end
    add_index :devices, :token, unique: true
  end
end
