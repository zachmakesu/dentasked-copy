class AddAttachmentAvatarToPatientRecords < ActiveRecord::Migration
  def self.up
    change_table :patient_records do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :patient_records, :avatar
  end
end
