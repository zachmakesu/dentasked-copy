class AddOfflinePatientRecordIdToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :offline, :boolean, default: false
    add_reference :appointments, :patient_record, index: true, foreign_key: true
  end
end
