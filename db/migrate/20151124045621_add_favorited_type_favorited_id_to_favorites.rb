class AddFavoritedTypeFavoritedIdToFavorites < ActiveRecord::Migration
  def change
    add_reference :favorites, :favorited, polymorphic: true, index: true
  end
end
