class AddGenderWorkToPatientProfile < ActiveRecord::Migration
  def change
    add_column :patient_profiles, :gender, :integer
    add_column :patient_profiles, :work, :string
  end
end
