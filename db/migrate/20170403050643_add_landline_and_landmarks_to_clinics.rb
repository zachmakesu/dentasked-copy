class AddLandlineAndLandmarksToClinics < ActiveRecord::Migration
  def change
    add_column :clinics, :landline, :string
    add_column :clinics, :landmarks, :string
  end
end
