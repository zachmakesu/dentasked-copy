class DropDentalServices < ActiveRecord::Migration
  def up
    drop_table :dental_services
  end

  def down
    create_table :dental_services do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
