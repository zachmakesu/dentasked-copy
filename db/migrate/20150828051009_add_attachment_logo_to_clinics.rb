class AddAttachmentLogoToClinics < ActiveRecord::Migration
  def self.up
    change_table :clinics do |t|
      t.attachment :logo
    end
  end

  def self.down
    remove_attachment :clinics, :logo
  end
end
