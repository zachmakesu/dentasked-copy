class AddCodeToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :code, :string
    add_index :appointments, :code
  end
end
