class EncryptPatientRecordBirthdate < ActiveRecord::Migration
  def up
    rename_column :patient_records, :birthdate, :raw_birthdate
    add_column :patient_records, :encrypted_birthdate, :string
    add_column :patient_records, :encrypted_birthdate_iv, :string
  end

  def down
    rename_column :patient_records, :raw_birthdate, :birthdate
    remove_column :patient_records, :encrypted_birthdate
    remove_column :patient_records, :encrypted_birthdate_iv
  end
end
