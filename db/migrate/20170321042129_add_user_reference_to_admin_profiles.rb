class AddUserReferenceToAdminProfiles < ActiveRecord::Migration
  def change
    add_reference :admin_profiles, :user, index: true, foreign_key: true
  end
end
