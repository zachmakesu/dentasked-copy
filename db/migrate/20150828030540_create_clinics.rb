class CreateClinics < ActiveRecord::Migration
  def change
    create_table :clinics do |t|
      t.string :name
      t.string :address
      t.float :lat
      t.float :lng
      t.string :mobile_number

      t.timestamps null: false
    end
  end
end
