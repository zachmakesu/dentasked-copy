class AddNotNullInPlatform < ActiveRecord::Migration
  def change
    change_column_null :app_versions, :platform, false
  end
end
