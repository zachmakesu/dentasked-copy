class RemoveBookingRelationshipFromRatings < ActiveRecord::Migration
  def up
    remove_column :ratings, :booking_id
  end

  def down
    add_column :ratings, :booking_id, :int, null: false
    add_index :ratings, :booking_id
  end
end
