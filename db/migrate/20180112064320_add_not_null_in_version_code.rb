class AddNotNullInVersionCode < ActiveRecord::Migration
  def change
    change_column_null :app_versions, :version_code, false
  end
end
