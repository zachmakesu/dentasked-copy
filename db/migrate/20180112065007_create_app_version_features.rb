class CreateAppVersionFeatures < ActiveRecord::Migration
  def change
    create_table :app_version_features do |t|
      t.integer :app_version_id
      t.text :details
      t.string :title

      t.timestamps null: false
    end
  end
end
