class EncryptMobileNumber < ActiveRecord::Migration
  def up
    rename_column :users, :mobile_number, :raw_mobile_number
    add_column :users, :encrypted_mobile_number, :string
    add_column :users, :encrypted_mobile_number_iv, :string
  end

  def down
    rename_column :users, :raw_mobile_number, :mobile_number
    remove_column :users, :encrypted_mobile_number
    remove_column :users, :encrypted_mobile_number_iv
  end
end
