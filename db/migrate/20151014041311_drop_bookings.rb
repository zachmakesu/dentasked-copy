class DropBookings < ActiveRecord::Migration
  def up
    drop_table :bookings
  end

  def down
    create_table :bookings do |t|
      t.datetime :schedule
      t.text :note
      t.boolean :confirmed
      t.integer :patient_id, null: false
      t.references :dental_service, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
    add_index :bookings, :patient_id
  end
end
