class CreateProvinces < ActiveRecord::Migration
  def change
    create_table :provinces do |t|
      t.string :name, null: false
      t.string :psgc_code, null: false
      t.references :region, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :provinces, :psgc_code, unique: true
  end
end
