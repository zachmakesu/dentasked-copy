class CreateHMOAccreditations < ActiveRecord::Migration
  def change
    create_table :hmo_accreditations do |t|
      t.references :hmo, index: true, foreign_key: true
      t.references :accredited, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
