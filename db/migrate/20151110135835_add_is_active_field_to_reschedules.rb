class AddIsActiveFieldToReschedules < ActiveRecord::Migration
  def change
    add_column :reschedules, :is_active, :boolean, default: false
  end
end
