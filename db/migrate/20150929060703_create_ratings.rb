class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :booking_id, index: true, null: false
      t.float :rating, default: 0.0
      t.integer :rater_id, index: true, null: false
      t.string :rater_type, null: false
      t.integer :ratee_id, index: true, null: false
      t.string :ratee_type, null: false
      t.text :description

      t.timestamps null: false
    end
  end
end
