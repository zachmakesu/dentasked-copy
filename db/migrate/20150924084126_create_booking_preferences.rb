class CreateBookingPreferences < ActiveRecord::Migration
  def change
    create_table :booking_preferences do |t|
      t.references :symptom, index: true, foreign_key: true
      t.references :specialty, index: true, foreign_key: true

      t.integer :patient_id, null: false

      t.timestamps null: false
    end

    add_index :booking_preferences, :patient_id, unique: true

  end
end
