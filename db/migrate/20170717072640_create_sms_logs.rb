class CreateSMSLogs < ActiveRecord::Migration
  def change
    create_table :sms_logs do |t|
      t.text :message
      t.integer :client
      t.string :recipient
      t.integer :status

      t.timestamps null: false
    end
  end
end
