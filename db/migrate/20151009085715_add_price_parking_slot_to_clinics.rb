class AddPriceParkingSlotToClinics < ActiveRecord::Migration
  def change
    add_column :clinics, :price, :string
    add_column :clinics, :parking_slot, :string
  end
end
