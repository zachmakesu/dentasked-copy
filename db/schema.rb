# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180112065007) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activations", force: :cascade do |t|
    t.datetime "activated_at"
    t.string   "code",         null: false
    t.datetime "expires_at"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "activations", ["user_id"], name: "index_activations_on_user_id", using: :btree

  create_table "admin_profiles", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "admin_profiles", ["user_id"], name: "index_admin_profiles_on_user_id", using: :btree

  create_table "api_keys", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "encrypted_access_token", default: "", null: false
    t.datetime "expires_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "api_keys", ["user_id"], name: "index_api_keys_on_user_id", using: :btree

  create_table "app_version_features", force: :cascade do |t|
    t.integer  "app_version_id"
    t.text     "details"
    t.string   "title"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "app_versions", force: :cascade do |t|
    t.integer  "category"
    t.string   "created_by",   null: false
    t.integer  "platform",     null: false
    t.string   "version_code", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "appointment_histories", force: :cascade do |t|
    t.integer  "appointment_id"
    t.integer  "processor_id"
    t.string   "status_from"
    t.string   "status_changed_to"
    t.boolean  "rescheduled",       default: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "appointment_histories", ["appointment_id"], name: "index_appointment_histories_on_appointment_id", using: :btree

  create_table "appointments", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "clinic_assignment_id",                 null: false
    t.datetime "schedule",                             null: false
    t.integer  "service_id",                           null: false
    t.text     "note"
    t.integer  "status",                               null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "hmo_id"
    t.integer  "specialty_id"
    t.boolean  "offline",              default: false
    t.integer  "patient_record_id"
    t.string   "code"
  end

  add_index "appointments", ["clinic_assignment_id"], name: "index_appointments_on_clinic_assignment_id", using: :btree
  add_index "appointments", ["code"], name: "index_appointments_on_code", using: :btree
  add_index "appointments", ["hmo_id"], name: "index_appointments_on_hmo_id", using: :btree
  add_index "appointments", ["patient_id"], name: "index_appointments_on_patient_id", using: :btree
  add_index "appointments", ["patient_record_id"], name: "index_appointments_on_patient_record_id", using: :btree
  add_index "appointments", ["service_id"], name: "index_appointments_on_service_id", using: :btree
  add_index "appointments", ["specialty_id"], name: "index_appointments_on_specialty_id", using: :btree

  create_table "areas", force: :cascade do |t|
    t.float    "lat"
    t.float    "lng"
    t.string   "base_address"
    t.string   "name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "assignment_schedules", force: :cascade do |t|
    t.integer  "clinic_assignment_id"
    t.integer  "day",                                 null: false
    t.time     "start_time",                          null: false
    t.time     "end_time",                            null: false
    t.boolean  "is_active",            default: true, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "assignment_schedules", ["clinic_assignment_id"], name: "index_assignment_schedules_on_clinic_assignment_id", using: :btree

  create_table "booking_preferences", force: :cascade do |t|
    t.integer  "symptom_id"
    t.integer  "specialty_id"
    t.integer  "patient_id",   null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "booking_preferences", ["patient_id"], name: "index_booking_preferences_on_patient_id", unique: true, using: :btree
  add_index "booking_preferences", ["specialty_id"], name: "index_booking_preferences_on_specialty_id", using: :btree
  add_index "booking_preferences", ["symptom_id"], name: "index_booking_preferences_on_symptom_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "psgc_code",   null: false
    t.integer  "province_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "cities", ["province_id"], name: "index_cities_on_province_id", using: :btree
  add_index "cities", ["psgc_code"], name: "index_cities_on_psgc_code", unique: true, using: :btree

  create_table "clinic_assignments", force: :cascade do |t|
    t.integer  "clinic_id",                 null: false
    t.integer  "dentist_id",                null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "is_active",  default: true
  end

  add_index "clinic_assignments", ["clinic_id", "dentist_id"], name: "index_clinic_assignments_on_clinic_id_and_dentist_id", unique: true, using: :btree
  add_index "clinic_assignments", ["clinic_id"], name: "index_clinic_assignments_on_clinic_id", using: :btree
  add_index "clinic_assignments", ["dentist_id"], name: "index_clinic_assignments_on_dentist_id", using: :btree

  create_table "clinics", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.string   "mobile_number"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "price"
    t.string   "parking_slot"
    t.integer  "city_id"
    t.string   "landline"
    t.string   "landmarks"
  end

  create_table "dentist_profiles", force: :cascade do |t|
    t.datetime "activated_at"
    t.string   "activation_code",                                                          null: false
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.decimal  "fee",                         precision: 10, scale: 2
    t.integer  "experience"
    t.string   "color",                                                default: "#2AA6A4", null: false
    t.string   "encrypted_license_number"
    t.string   "encrypted_license_number_iv"
    t.integer  "user_id"
  end

  add_index "dentist_profiles", ["activation_code"], name: "index_dentist_profiles_on_activation_code", unique: true, using: :btree
  add_index "dentist_profiles", ["user_id"], name: "index_dentist_profiles_on_user_id", using: :btree

  create_table "devices", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "platform",                  null: false
    t.string   "token",                     null: false
    t.boolean  "enabled",    default: true, null: false
    t.string   "name",                      null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "devices", ["token"], name: "index_devices_on_token", unique: true, using: :btree
  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "drafts", force: :cascade do |t|
    t.string   "classification"
    t.text     "object"
    t.integer  "status",         default: 0
    t.datetime "reviewed_at"
    t.integer  "reviewer_id"
    t.integer  "owner_id"
    t.integer  "dentist_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "drafts", ["dentist_id"], name: "index_drafts_on_dentist_id", using: :btree
  add_index "drafts", ["owner_id"], name: "index_drafts_on_owner_id", using: :btree
  add_index "drafts", ["reviewer_id"], name: "index_drafts_on_reviewer_id", using: :btree

  create_table "faqs", force: :cascade do |t|
    t.string   "question"
    t.text     "answer"
    t.string   "app_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "patient_id",     null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "favorited_id"
    t.string   "favorited_type"
  end

  add_index "favorites", ["favorited_type", "favorited_id"], name: "index_favorites_on_favorited_type_and_favorited_id", using: :btree
  add_index "favorites", ["patient_id"], name: "index_favorites_on_patient_id", using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.string   "subject"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "feedbacks", ["user_id"], name: "index_feedbacks_on_user_id", using: :btree

  create_table "hmo_accreditations", force: :cascade do |t|
    t.integer  "hmo_id"
    t.integer  "accredited_id"
    t.string   "accredited_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "hmo_accreditations", ["accredited_type", "accredited_id"], name: "index_hmo_accreditations_on_accredited_type_and_accredited_id", using: :btree
  add_index "hmo_accreditations", ["hmo_id"], name: "index_hmo_accreditations_on_hmo_id", using: :btree

  create_table "hmos", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "uid"
    t.string   "provider"
    t.text     "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.text     "oauth_refresh_token"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "partner_signups", force: :cascade do |t|
    t.text     "clinic_object"
    t.text     "secretary_object"
    t.text     "dentist_object"
    t.string   "clinic_logo_file_name"
    t.string   "clinic_logo_content_type"
    t.integer  "clinic_logo_file_size"
    t.datetime "clinic_logo_updated_at"
    t.string   "dentist_avatar_file_name"
    t.string   "dentist_avatar_content_type"
    t.integer  "dentist_avatar_file_size"
    t.datetime "dentist_avatar_updated_at"
    t.string   "secretary_avatar_file_name"
    t.string   "secretary_avatar_content_type"
    t.integer  "secretary_avatar_file_size"
    t.datetime "secretary_avatar_updated_at"
    t.integer  "status",                        default: 0
    t.integer  "reviewer_id"
    t.datetime "reviewed_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "partner_signups", ["reviewer_id"], name: "index_partner_signups_on_reviewer_id", using: :btree

  create_table "patient_profiles", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "gender"
    t.string   "work"
    t.integer  "user_id"
  end

  add_index "patient_profiles", ["user_id"], name: "index_patient_profiles_on_user_id", using: :btree

  create_table "patient_records", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "uid"
    t.string   "encrypted_mobile_number"
    t.string   "encrypted_mobile_number_iv"
    t.string   "encrypted_birthdate"
    t.string   "encrypted_birthdate_iv"
  end

  create_table "photos", force: :cascade do |t|
    t.string   "name"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "photos", ["imageable_type", "imageable_id"], name: "index_photos_on_imageable_type_and_imageable_id", using: :btree

  create_table "provinces", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "psgc_code",  null: false
    t.integer  "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "provinces", ["psgc_code"], name: "index_provinces_on_psgc_code", unique: true, using: :btree
  add_index "provinces", ["region_id"], name: "index_provinces_on_region_id", using: :btree

  create_table "rating_suggestions", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "enabled",    default: true
    t.integer  "user_id",                   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "rating_suggestions", ["user_id"], name: "index_rating_suggestions_on_user_id", using: :btree

  create_table "ratings", force: :cascade do |t|
    t.float    "rating",       default: 0.0
    t.integer  "rater_id",                   null: false
    t.string   "rater_type",                 null: false
    t.integer  "ratee_id",                   null: false
    t.string   "ratee_type",                 null: false
    t.text     "description"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "context_type",               null: false
    t.integer  "context_id",                 null: false
  end

  add_index "ratings", ["context_id"], name: "index_ratings_on_context_id", using: :btree
  add_index "ratings", ["ratee_id"], name: "index_ratings_on_ratee_id", using: :btree
  add_index "ratings", ["rater_id"], name: "index_ratings_on_rater_id", using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "psgc_code",  null: false
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "regions", ["psgc_code"], name: "index_regions_on_psgc_code", unique: true, using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "reportable_id"
    t.string   "reportable_type"
    t.integer  "reporter_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "reports", ["reportable_type", "reportable_id"], name: "index_reports_on_reportable_type_and_reportable_id", using: :btree
  add_index "reports", ["reporter_id"], name: "index_reports_on_reporter_id", using: :btree

  create_table "reschedules", force: :cascade do |t|
    t.integer  "appointment_id"
    t.datetime "schedule",                       null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "is_active",      default: false
  end

  add_index "reschedules", ["appointment_id"], name: "index_reschedules_on_appointment_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "roles", ["name"], name: "index_roles_on_name", unique: true, using: :btree

  create_table "scheduled_jobs", force: :cascade do |t|
    t.string   "jid",              null: false
    t.integer  "schedulable_id"
    t.string   "schedulable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "scheduled_jobs", ["jid"], name: "index_scheduled_jobs_on_jid", unique: true, using: :btree
  add_index "scheduled_jobs", ["schedulable_id"], name: "index_scheduled_jobs_on_schedulable_id", using: :btree

  create_table "secretary_assignments", force: :cascade do |t|
    t.integer  "dentist_id"
    t.integer  "secretary_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "secretary_assignments", ["dentist_id"], name: "index_secretary_assignments_on_dentist_id", using: :btree
  add_index "secretary_assignments", ["secretary_id"], name: "index_secretary_assignments_on_secretary_id", using: :btree

  create_table "secretary_profiles", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "secretary_profiles", ["user_id"], name: "index_secretary_profiles_on_user_id", using: :btree

  create_table "service_assignments", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "assigned_id"
    t.string   "assigned_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "service_assignments", ["assigned_type", "assigned_id"], name: "index_service_assignments_on_assigned_type_and_assigned_id", using: :btree
  add_index "service_assignments", ["service_id"], name: "index_service_assignments_on_service_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "duration"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "var",                   null: false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "sms_logs", force: :cascade do |t|
    t.text     "message"
    t.integer  "client"
    t.string   "recipient"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specializations", force: :cascade do |t|
    t.integer  "specialty_id"
    t.integer  "dentist_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "specializations", ["dentist_id"], name: "index_specializations_on_dentist_id", using: :btree
  add_index "specializations", ["specialty_id"], name: "index_specializations_on_specialty_id", using: :btree

  create_table "specialties", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "symptoms", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tmpi_logs", force: :cascade do |t|
    t.string   "username",       null: false
    t.string   "status",         null: false
    t.integer  "appointment_id", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "role_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id", using: :btree
  add_index "user_roles", ["user_id", "role_id"], name: "index_user_roles_on_user_id_and_role_id", unique: true, using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                      default: "", null: false
    t.string   "encrypted_password",         default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",              default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "uid",                        default: "", null: false
    t.datetime "disabled_at"
    t.datetime "terms_agreed_at"
    t.string   "encrypted_mobile_number"
    t.string   "encrypted_mobile_number_iv"
    t.string   "encrypted_birthdate"
    t.string   "encrypted_birthdate_iv"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", unique: true, using: :btree

  add_foreign_key "activations", "users"
  add_foreign_key "admin_profiles", "users", on_delete: :cascade
  add_foreign_key "api_keys", "users"
  add_foreign_key "appointment_histories", "appointments"
  add_foreign_key "appointments", "clinic_assignments"
  add_foreign_key "appointments", "hmos"
  add_foreign_key "appointments", "patient_records"
  add_foreign_key "appointments", "services"
  add_foreign_key "appointments", "specialties"
  add_foreign_key "appointments", "users", column: "patient_id", on_delete: :cascade
  add_foreign_key "assignment_schedules", "clinic_assignments"
  add_foreign_key "booking_preferences", "specialties"
  add_foreign_key "booking_preferences", "symptoms"
  add_foreign_key "cities", "provinces"
  add_foreign_key "dentist_profiles", "users", on_delete: :cascade
  add_foreign_key "devices", "users"
  add_foreign_key "feedbacks", "users", on_delete: :cascade
  add_foreign_key "hmo_accreditations", "hmos"
  add_foreign_key "identities", "users"
  add_foreign_key "patient_profiles", "users", on_delete: :cascade
  add_foreign_key "provinces", "regions"
  add_foreign_key "rating_suggestions", "users"
  add_foreign_key "reports", "users", column: "reporter_id"
  add_foreign_key "reschedules", "appointments"
  add_foreign_key "secretary_profiles", "users", on_delete: :cascade
  add_foreign_key "service_assignments", "services"
  add_foreign_key "specializations", "specialties"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
end
