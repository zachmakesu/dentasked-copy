# Add seed file for all envs here
models = %w{roles services specialties symptoms admin dentists patients regions provinces cities}

models.each do |model|
  puts '#' * 80
  puts "Table: #{model.upcase}"
  require_relative "models/#{model}.rb"
end
