json = ActiveSupport::JSON.decode(File.read('vendor/seed/json/provinces.json'))
provinces = json["RECORDS"]

ActiveRecord::Base.transaction do
  if Province.all.empty?
    provinces.each do |province|
      next unless Province.where(id: province["provCode"].to_i).empty?
      new_province = Province.new
      new_province.id = province["provCode"].to_i
      new_province.name = province["provDesc"].titleize
      new_province.psgc_code = province["psgcCode"]
      new_province.region_id = province["regCode"].to_i
      if new_province.save
        print '✓'
      else
        puts new_province.errors.inspect
        break
      end
    end
    print "\nTotal : #{Province.all.count}\n"
  else
    print "Skipped seeding Province table.\n"
  end
end
