symptoms = [
 "Bad breath that won't go away",
 "Red or swollen gums",
 "Tender or bleeding gums",
 "Painful chewing",
 "Loose teeth",
 "Sensitive teeth"
]

ActiveRecord::Base.transaction do
  if Symptom.all.empty?
    symptoms.each do |symptom|
      new_symptom = Symptom.new
      new_symptom.name = symptom
      new_symptom.description = ""
      if new_symptom.save
        print '✓'
      else
        puts new_symptom.errors.inspect
        break
      end
    end
    print "\nTotal : #{Symptom.all.count}\n"
  else
    print "Skipped seeding Symptom table.\n"
  end
end
