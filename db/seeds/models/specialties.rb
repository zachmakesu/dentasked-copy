specialties = [
 "Dental Public Health",
 "Endodontics",
 "Oral and Maxillofacial Pathology",
 "Oral and Maxillofacial Radiology",
 "Orthodontics and Dentofacial Orthopedics",
 "Pediatric Dentistry",
 "Periodontics",
 "Prosthodontics"
]

ActiveRecord::Base.transaction do
  if Specialty.all.empty?
    specialties.each do |specialty|
      new_specialty = Specialty.new
      new_specialty.name = specialty
      new_specialty.description = ""
      if new_specialty.save
        print '✓'
      else
        puts new_specialty.errors.inspect
        break
      end
    end
    print "\nTotal : #{Specialty.all.count}\n"
  else
    print "Skipped seeding Specialty table.\n"
  end
end
