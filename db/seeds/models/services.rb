services = [
  "Dental Cleaning",
  "Whitening",
  "Extraction",
  "Surgical Extraction",
  "Endodontic Treatment",
  "Dental Fillings",
  "Fixed Prosthetics",
  "Implantology",
  "Removal Dentures"
]

ActiveRecord::Base.transaction do
  if Service.all.empty?
    services.each do |service|
      new_service = Service.new
      new_service.name = service
      new_service.duration = 60
      if new_service.save
        print '✓'
      else
        puts new_service.errors.inspect
        break
      end
    end
    print "\nTotal : #{Service.all.count}\n"
  else
    print "Skipped seeding Service table.\n"
  end
end
