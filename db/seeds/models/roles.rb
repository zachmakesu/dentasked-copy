ActiveRecord::Base.transaction do
  if Role.all.empty?
    User::ROLES.map(&:downcase).each do |role|
      new_role = Role.new
      new_role.name = role
      new_role.description = role.titleize
      if new_role.save
        print "✓"
      else
        puts new_role.errors.inspect
        break
      end
    end
    print "\nTotal : #{Role.all.count}\n"
  else
    print "Skipped seeding Role table.\n"
  end
end
