dentist_list = [
  {
    :first_name => 'Dean',
    :last_name => 'Canlas',
  },
  {
    :first_name => 'Meng',
    :last_name => 'David',
  },
  {
    :first_name => 'Dennis',
    :last_name => 'Punzalan',
  },
]

ActiveRecord::Base.transaction do
  if Dentist.all.empty?
    dentist_list.each do |dentist|
      new_dentist = Dentist.new
      new_dentist.first_name = dentist[:first_name]
      new_dentist.last_name = dentist[:last_name]
      new_dentist.email = "#{dentist[:first_name].downcase}@dentasked.ph"
      new_dentist.password = 'Dentasked2016'
      new_dentist.avatar = File.new("#{Rails.root}/app/assets/images/icon.png")
      new_dentist.skip_confirmation!

      if new_dentist.save
        new_dentist.confirm
        print '✓'
      else
        puts new_dentist.errors.inspect
        break
      end
    end
    print "\nTotal : #{Dentist.all.count}\n"
  else
    print "Skipped seeding dentist users.\n"
  end
end
