patient_list = [
  {
    :first_name => 'Kevin',
    :last_name => 'Candelaria',
  },
  {
    :first_name => 'McDave',
    :last_name => 'Valdecantos',
  }
]

ActiveRecord::Base.transaction do
  if Patient.all.empty?
    patient_list.each do |patient|
      new_patient = Patient.new
      new_patient.first_name = patient[:first_name]
      new_patient.last_name = patient[:last_name]
      new_patient.email = "#{patient[:first_name].downcase}@dentasked.ph"
      new_patient.password = 'Dentasked2016'
      new_patient.avatar = File.new("#{Rails.root}/app/assets/images/icon.png")
      new_patient.skip_confirmation!

      if new_patient.save
        new_patient.confirm
        print '✓'
      else
        puts new_patient.errors.inspect
        break
      end
    end
    print "\nTotal : #{Patient.all.count}\n"
  else
    print "Skipped seeding patient users.\n"
  end
end
