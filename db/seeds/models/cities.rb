json = ActiveSupport::JSON.decode(File.read('vendor/seed/json/cities.json'))
cities = json["RECORDS"]

ActiveRecord::Base.transaction do
  if City.all.empty?
    cities.each do |city|
      next unless City.where(id: city["citymunCode"].to_i).empty?
      new_city = City.new
      new_city.id = city["citymunCode"].to_i
      new_city.name = city["citymunDesc"].titleize
      new_city.psgc_code = city["psgcCode"]
      new_city.province_id = city["provCode"].to_i
      if new_city.save
        print '✓'
      else
        puts new_city.errors.inspect
        break
      end
    end
    print "\nTotal : #{City.all.count}\n"
  else
    print "Skipped seeding City table.\n"
  end
end
