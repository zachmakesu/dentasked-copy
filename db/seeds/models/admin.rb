admin_list = [
  {
    :first_name => 'Dominique',
    :last_name => 'De Leon',
  },
  {
    :first_name => 'Ragde',
    :last_name => 'Falcis',
  },
  {
    :first_name => 'Admin',
    :last_name => '',
  },
]

ActiveRecord::Base.transaction do
  if Admin.all.empty?
    admin_list.each do |admin|
      new_admin = Admin.new
      new_admin.first_name = admin[:first_name]
      new_admin.last_name = admin[:last_name]
      new_admin.email = "#{admin[:first_name].downcase}@dentasked.ph"
      new_admin.password = 'Dentasked2016'
      new_admin.avatar = File.new("#{Rails.root}/app/assets/images/icon.png")
      new_admin.skip_confirmation!

      if new_admin.save
        new_admin.confirm!
        print '✓'
      else
        puts new_admin.errors.inspect
        break
      end
    end
    print "\nTotal : #{Admin.all.count}\n"
  else
    print "Skipped seeding Admin users.\n"
  end
end
