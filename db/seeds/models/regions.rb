json = ActiveSupport::JSON.decode(File.read('vendor/seed/json/regions.json'))
regions = json["RECORDS"]

ActiveRecord::Base.transaction do
  if Region.all.empty?
    regions.each do |region|
      new_region = Region.new
      new_region.id = region["regCode"].to_i
      new_region.name = region["regDesc"]
      new_region.psgc_code = region["psgcCode"]
      if new_region.save
        print '✓'
      else
        puts new_region.errors.inspect
        break
      end
    end
    print "\nTotal : #{Region.all.count}\n"
  else
    print "Skipped seeding Region table.\n"
  end
end
