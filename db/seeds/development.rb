# Add seed file for development environment here
models = %w{}

models.each do |model|
  puts '#' * 80
  puts "Table: #{model.upcase}"
  require_relative "models/#{model}.rb"
end
