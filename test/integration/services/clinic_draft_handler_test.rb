require "test_helper"

class ClinicDraftHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @region = regions(:test_region)
    @city = cities(:test_city)
    @handler = ClinicDraftHandler.new(object_params)
  end

  def object_params
    {
      name: "Clinic Name",
      address: "A. Bonifacio Avenu",
      city_id: @city.id,
      landline: "1234567",
      mobile_number: "09191234567",
      landmarks: "McDonalds",
      logo: default_photo_attachment
    }
  end

  test "ClinicDraftHandler#validate" do
    VCR.use_cassette "clinics/draft" do
      assert @handler.validate, "should be a valid clinic"
    end
  end

  test "ClinicDraftHandler#object" do
    assert_equal @handler.object.class, Clinic, "should be a clinic instance"
  end

  test "save valid clinic object" do
    VCR.use_cassette "clinics/draft" do
      @handler.object.save
    end
    assert @handler.object.logo.present?, "should have referenced logo"
  end
end
