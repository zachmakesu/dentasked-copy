require "test_helper"

class AndroidHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @recipient = users(:patient)
    @alert = "Test alert"
    @payload = { sample: "data", alert: @alert }
    @handler = AndroidHandler.new(@recipient, @alert, @payload)
  end

  test "active android devices" do
    assert_equal @recipient.devices.android.active, @handler.send(:devices), "should return only active android devices"
    refute_includes @handler.send(:devices), devices(:inactive_xiaomi), "should not include inactive devices"
  end

  test "deliver notification to user with no device" do
    recipient = users(:dentist)
    handler = AndroidHandler.new(recipient, @alert, @payload)
    assert handler.deliver, "should be successful even if user has no device"
  end

  test "namespace of notification payload" do
    assert_equal @handler.send(:options), { data: @payload }, "should be 'data' namespaced"
  end

  test "registration ids of recipient" do
    assert_equal @recipient.devices.android.active.pluck(:token), @handler.send(:registration_ids), "should be android only"
  end

  test "disable not registered ids" do
    assert_difference "Device.active.android.count", -1 do
      @handler.send(:disable, @recipient.devices.android.active.map(&:token))
    end
  end
end
