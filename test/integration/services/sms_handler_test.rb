require "test_helper"

class SMSHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @message = "Message"
    @number = "9201234567"
    @prefix = "+63"
    @formatted_number = "#{@prefix}#{@number}"
    @notification = SMS::Notification::Activation.new(activation_id: activations(:patient).id, mobile_number: @number)
    @sms_handler = SMSHandler.new(notification: @notification)
  end

  test "number should be correctly formatted to +63920123456" do
    assert_equal @sms_handler.send(:formatted_number), @formatted_number
  end

  test "message should be the same as notification message" do
    assert_equal @sms_handler.message, @notification.message
  end

  test "client should be the app setting client if set" do
    preferred_client = "twilio"
    AppSetting.sms_client = preferred_client
    notification = SMS::Notification::Activation.new(activation_id: activations(:patient).id, mobile_number: @number)
    sms_handler = SMSHandler.new(notification: notification)
    assert_equal preferred_client, sms_handler.client.name
  end
end
