require "test_helper"
class PartnerSignupHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @handler = PartnerSignupHandler.new(clinic_params, secretary_params, dentist_params)
    @partner_signup = partner_signups(:partner_signup)
    @pending = partner_signups(:pending)
    @admin = adminify(users(:admin))
  end

  def clinic_params
    {
      name: "Clinic Name",
      address: "A. Bonifacio Avenu",
      city_id: cities(:test_city).id,
      landline: "1234567",
      mobile_number: "09191234567",
      landmarks: "McDonalds",
      logo: default_photo_attachment
    }
  end

  def secretary_params
    {
      first_name: "Rintarou",
      last_name: "Okabe",
      birthdate: "08/08/2008",
      email: "okabe@rintarou.com",
      mobile_number: "09191234567",
      avatar: default_photo_attachment
    }
  end

  def dentist_params
    {
      first_name: "Rintarou",
      last_name: "Okabe",
      email: "okabe@rintarou.com",
      mobile_number: "09191234567",
      birthdate: "08/08/2008",
      license_number: "PRC123",
      fee: 1337,
      experience: 1,
      hmo_ids: HMO.all.map(&:id),
      service_ids: Service.all.map(&:id),
      specialization_ids: Specialty.all.map(&:id),
      avatar: default_photo_attachment
    }
  end

  test "PartnerSignupHandler#validate" do
    VCR.use_cassette "clinics/draft" do
      assert @handler.validate, "should be a valid partner signup"
    end
  end

  test "PartnerSignupHandler#errors" do
    handler = PartnerSignupHandler.new
    VCR.use_cassette "clinics/draft" do
      assert_equal handler.errors, PartnerSignupHandler::ERROR_MESSAGE
    end
  end

  test "PartnerSignupHandler#dentist_is_secretary with same email" do
    assert @handler.send(:dentist_is_secretary), "should be true"
  end

  test "PartnerSignupHandler#dentist_is_secretary with different email" do
    different_dentist = dentist_params
    different_dentist[:email] = "new_email@email.com"
    handler = PartnerSignupHandler.new(clinic_params, secretary_params, different_dentist)
    refute handler.send(:dentist_is_secretary), "should be false"
  end

  test "PartnerSignupHandler#approve! if dentist is secretary" do
    VCR.use_cassette "partners/draft" do
      assert_difference ["Dentist.count",
                         "Secretary.count",
                         "Clinic.count",
                         "User.count"], 1 do
                           PartnerSignupHandler.approve!(@pending, @admin)
                         end
    end
  end

  test "PartnerSignupHandler#approve if dentist is not secretary" do
    VCR.use_cassette "partners/draft" do
      assert_difference "User.count", 2 do
        PartnerSignupHandler.approve!(@partner_signup, @admin)
      end
    end
  end

  test "PartnerSignupHandler#create valid partner signup" do
    handler = PartnerSignupHandler.new(clinic_params, secretary_params, dentist_params)
    VCR.use_cassette "partners/draft" do
      assert_difference "PartnerSignup.count", 1 do
        handler.create
      end
    end
  end
end
