require "test_helper"

class IOSHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @recipient = users(:patient)
    @alert = "This is a sample alert!"
    @payload = { sample: "payload" }
    @client = ConsumerAPN.new
    @handler = IOSHandler.new(@recipient, @alert, @payload, @client)
  end

  test "active ios devices" do
    assert_equal @recipient.devices.ios.active, @handler.send(:devices), "should return only active ios devices"
    refute_includes @handler.send(:devices), devices(:inactive_iphone_5), "should not include inactive devices"
  end

  test "building notification for device" do
    device = @recipient.devices.ios.active.first
    notification = Houston::Notification.new(device: device.token)
    notification.alert = @alert
    notification.custom_data = @payload
    assert_kind_of Houston::Notification, @handler.send(:create_notification, device), "should a notification instance"
    assert_equal notification.alert, @handler.send(:create_notification, device).alert, "should have same alert"
  end

  test "exposure of errors in handler, if any" do
    @handler.deliver
    assert_empty @handler.errors, "shouldn't have errors"
  end
end
