require "test_helper"

class GoogleOauthHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @patient = users(:patient)
    @oauth_handler = GoogleOauthHandler.new(@patient, "", "https://www.googleapis.com/auth/calendar", "#{Rails.root}/test/support/config/client_secrets.json")
  end

  test "handler should have response" do
    VCR.use_cassette "google/oauth" do
      @oauth_handler.call
    end
    assert_instance_of OpenStruct, @oauth_handler.response
  end

  test "handler should destroy google identity if call fails" do
    assert_difference "Identity.count", -1 do
      VCR.use_cassette "google/oauth" do
        @oauth_handler.call
      end
    end
  end
end
