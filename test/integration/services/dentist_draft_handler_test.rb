require "test_helper"
class DentistDraftHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @handler = DentistDraftHandler.new(object_params)
  end

  def object_params
    {
      avatar: default_photo_attachment,
      first_name: "Rintarou",
      last_name: "Okabe",
      email: "okabe@rintarou.com",
      mobile_number: "09191234567",
      birthdate: "08/08/2008",
      license_number: "PRC123",
      fee: 1337,
      experience: 1,
      hmo_ids: HMO.all.map(&:id),
      service_ids: Service.all.map(&:id),
      specialization_ids: Specialty.all.map(&:id)
    }
  end

  test "DentistDraftHandler#validate" do
    assert @handler.validate, "should be a valid dentist"
  end

  test "DentistDraftHandler#object" do
    assert_equal @handler.object.class, Dentist, "should be a dentist instance"
  end

  test "save valid dentist object" do
    @handler.object.save
    assert @handler.object.avatar.present?, "should have referenced avatar"
    assert @handler.object.profile.present?, "should have referenced profile"
  end
end
