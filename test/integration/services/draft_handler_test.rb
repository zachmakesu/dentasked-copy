require "test_helper"

class DraftHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @secretary  = secretarify(users(:secretary))
    @dentist    = dentistify(users(:assigned_dentist))
    @admin      = adminify(users(:admin))
    @handler = ::DraftHandler.new(@secretary, object_params, classification: "Edit dentist profile and basic information").create
    @draft = Draft.last
  end

  def object_params
    {
      license_number: "2345678909",
      experience: 20,
      fee: 2000,
      specialties: Specialty.all.map(&:id).join(";"),
      hmos: HMO.all.map(&:id).join(";"),
      services: Service.all.map(&:id).join(";"),
      clinic_assignment_id: clinics(:prestige).clinic_assignments.last.id,
      schedules: 'NTs5OjAwOzIwOjAwfDI7MTA6MDA7MTU6MDA=\n',
      uid: @dentist.uid
    }
  end

  test 'creating draft with valid parameters' do
    assert_equal 3, @secretary.drafts.count
    assert_equal 1, @dentist.drafts.count
    assert_equal true, @handler.response[:success]
    assert_equal @dentist, @handler.response[:details]
  end

  test 'approving drafts' do
    handler = DraftHandler.approve!(@draft, @admin)

    assert_equal true, handler[:success], 'should return true'
    assert_equal true, @draft.approved?, 'should update status to approved'
    assert_equal @draft.reviewer, @admin, 'should reference reviewer'
  end
  
  # Since unfortunately the handler doesn't return the draft object?
  # We resort to referencing the generated draft object
  # via Draft.last
  test 'adding dentist hmos via drafts' do
    DraftHandler.approve!(@draft, @admin)
    assert_equal object_params[:hmos].split(';').count, @dentist.hmos.count, 'should equal draft hmos count'
  end

  test 'removing all dentist hmos via drafts' do
    dup_params = object_params.dup
    dup_params[:hmos] = ""
    DraftHandler.new(@secretary, dup_params, classification: "Edit dentist profile and basic information").create
    recent_draft = Draft.last
    DraftHandler.approve!(recent_draft, @admin)
    assert_equal dup_params[:hmos].split(';').count, @dentist.hmos.count, 'should equal draft hmos count'
  end

  test 'removing certain dentist hmos via drafts' do
    only_hmo = HMO.first
    dup_params = object_params.dup
    dup_params[:hmos] = only_hmo.id.to_s
    DraftHandler.new(@secretary, dup_params, classification: "Edit dentist profile and basic information").create
    recent_draft = Draft.last
    DraftHandler.approve!(recent_draft, @admin)
    assert_equal dup_params[:hmos].split(';').count, @dentist.hmos.count, 'should equal draft hmos count'
    assert_equal @dentist.hmos.map(&:id), [only_hmo.id], 'should equal assigned hmos'
  end

  test 'adding dentist services via drafts' do
    DraftHandler.approve!(@draft, @admin)
    assert_equal object_params[:services].split(';').count, @dentist.services.count, 'should equal draft services count'
  end

  test 'removing all dentist services via drafts' do
    dup_params = object_params.dup
    dup_params[:services] = ""
    DraftHandler.new(@secretary, dup_params, classification: "Edit dentist profile and basic information").create
    recent_draft = Draft.last
    DraftHandler.approve!(recent_draft, @admin)
    assert_equal dup_params[:services].split(';').count, @dentist.services.count, 'should equal draft services count'
  end

  test 'removing certain dentist services via drafts' do
    only_service = Service.first
    dup_params = object_params.dup
    dup_params[:services] = only_service.id.to_s
    DraftHandler.new(@secretary, dup_params, classification: "Edit dentist profile and basic information").create
    recent_draft = Draft.last
    DraftHandler.approve!(recent_draft, @admin)
    assert_equal dup_params[:services].split(';').count, @dentist.services.count, 'should equal draft services count'
    assert_equal @dentist.services.map(&:id), [only_service.id], 'should equal assigned services'
  end

  test 'adding dentist specialties via drafts' do
    DraftHandler.approve!(@draft, @admin)
    assert_equal object_params[:specialties].split(';').count, @dentist.specialties.count, 'should equal draft specialties count'
  end

  test 'removing all dentist specialties via drafts' do
    dup_params = object_params.dup
    dup_params[:specialties] = ""
    DraftHandler.new(@secretary, dup_params, classification: "Edit dentist profile and basic information").create
    recent_draft = Draft.last
    DraftHandler.approve!(recent_draft, @admin)
    assert_equal dup_params[:specialties].split(';').count, @dentist.specialties.count, 'should equal draft specialties count'
  end

  test 'removing certain dentist specialties via drafts' do
    only_specialty = Specialty.first
    dup_params = object_params.dup
    dup_params[:specialties] = only_specialty.id.to_s
    DraftHandler.new(@secretary, dup_params, classification: "Edit dentist profile and basic information").create
    recent_draft = Draft.last
    DraftHandler.approve!(recent_draft, @admin)
    assert_equal dup_params[:specialties].split(';').count, @dentist.specialties.count, 'should equal draft specialties count'
    assert_equal @dentist.specialties.map(&:id), [only_specialty.id], 'should equal assigned specialties'
  end

  test 'declining drafts' do
    DraftHandler.new(@secretary, object_params, classification: "Edit dentist profile and basic information").create
    handler = DraftHandler.decline!(@draft, @admin)

    assert_equal true, handler[:success], 'should return true since decline succeed'
    assert_equal true, @draft.declined?, 'should update status to declined'
    assert_equal @draft.reviewer, @admin, 'should reference reviewer'
  end
end
