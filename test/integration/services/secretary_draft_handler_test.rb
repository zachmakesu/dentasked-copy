require "test_helper"

class SecretaryDraftHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @handler = SecretaryDraftHandler.new(object_params)
  end

  def object_params
    {
      first_name: "Rintarou",
      last_name: "Okabe",
      birthdate: "08/08/2008",
      email: "okabe@rintarou.com",
      mobile_number: "09191234567",
      avatar: default_photo_attachment
    }
  end

  test "SecretaryDraftHandler#validate" do
    assert @handler.validate, "should be a valid secretary"
  end

  test "SecretaryDraftHandler#object" do
    assert_equal @handler.object.class, Secretary, "should be a secretary instance"
  end

  test "save valid secretary object" do
    @handler.object.save
    assert @handler.object.avatar.present?, "should have referenced avatar"
    assert @handler.object.profile.present?, "should have referenced profile"
  end
end
