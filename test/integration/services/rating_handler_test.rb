require "test_helper"

class RatingHandlerTest < ActionDispatch::IntegrationTest
  setup do
    @appointment = appointments(:completed_appointment)
    @current_user = users(:patient)
    @params = { rating: 5 }
    @handler = RatingHandler.new(@appointment, @current_user, @params)
  end

  test "initialize handler with valid params" do
    assert_equal @handler.send(:context), @appointment, "should have appointment as context"
    assert_equal @handler.send(:rater), @current_user, "should have curent user as rater"
    assert_equal @handler.send(:ratee), @appointment.clinic_assignment.clinic, "should have clinic as ratee"
  end

  test "create valid rating" do
    assert_difference "Rating.count", 1 do
      @handler.call
      assert_equal @handler.rating.rater, @current_user, "should have correct rater"
      assert_equal @handler.rating.ratee, @appointment.clinic_assignment.clinic, "should have correct ratee"
    end
  end
end
