require "test_helper"

module Notifications
  class RatingHandlerTest < ActionDispatch::IntegrationTest
    setup do
      @appointment = appointments(:confirmed_appointment)
      @handler = Notifications::RatingHandler.new(@appointment)
      @alert = "Thank you for booking with DentaSked. Please take a moment to leave a rating to help improve our services!"
      @fcm_key = ENV['CONSUMER_FCM_KEY']
      @payload = {
        notification_type: 4,
        appointment_id: @appointment.id,
        ratee_name: @appointment.clinic_assignment.clinic.name
      }
    end
    test "notification client" do
      assert_instance_of ConsumerAPN, @handler.send(:client), "should use PartnerAPN client"
    end
    test "notification fcm key" do
      assert_equal @handler.send(:fcm_key), @fcm_key, "should return correct fcm key"
    end
    test "notification alert" do
      assert_equal @handler.send(:alert), @alert, "should return correct alert"
    end
    test "notification payload" do
      assert_equal @handler.send(:payload), @payload, "should have the same payload"
    end
    test "notification recipient" do
      assert_equal @handler.send(:recipient), @appointment.patient, "should be the appointment's patient"
    end
    test "notification type" do
      assert_equal Notifications::RatingHandler::NOTIFICATION_TYPE, 4, "should have notification type"
    end
  end
end
