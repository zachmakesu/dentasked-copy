require "test_helper"

module Notifications
  class PatientAppointmentHandlerTest < ActionDispatch::IntegrationTest
    setup do
      @appointment = appointments(:confirmed_appointment)
      @handler = PatientAppointmentHandler.new(@appointment)
      @fcm_key = ENV['CONSUMER_FCM_KEY']
    end
    test "notification client" do
      assert_instance_of ConsumerAPN, @handler.send(:client), "should use ConsumerAPN client"
    end
    test "notification fcm key" do
      assert_equal @handler.send(:fcm_key), @fcm_key, "should return correct fcm key"
    end
    test "notification recipient" do
      assert_equal @handler.send(:recipient), @appointment.patient, "should be the appointment's patient"
    end
    test "notification type" do
      assert_equal PatientAppointmentHandler::NOTIFICATION_TYPE, 2, "should have notification type"
    end

    # confirmed?
    test "confirmed appointments" do
      appointment = appointments(:confirmed_appointment)
      handler = PatientAppointmentHandler.new(appointment)
      dentist_name = appointment.dentist.name_with_salutation
      date = appointment.schedule.strftime("%a %b %e, %Y at %I:%M %p")
      alert = "Your appointment with #{dentist_name} is confirmed."
      payload = {
        id: appointment.id,
        name: dentist_name,
        avatar_url: URI.join(ActionController::Base.asset_host.to_s, appointment.dentist.avatar.url).to_s,
        schedule: date,
        status_id: appointment[:status]
      }
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end

    # confirmed? && rescheduled?
    test "confirmed and rescheduled appointments" do
      appointment = appointments(:rescheduled_appointment)
      handler = PatientAppointmentHandler.new(appointment)
      dentist_name = appointment.dentist.name_with_salutation
      date = appointment.schedule.strftime("%a %b %e, %Y at %I:%M %p")
      old_date = appointment.read_attribute(:schedule).strftime("%a %b %e, %Y at %I:%M %p")
      alert = "Your appointment #{dentist_name} has been rescheduled from #{old_date} to #{date}."
      payload = { none: nil }
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end

    # cancelled?
    test "cancelled appointments" do
      appointment = appointments(:pending_appointment)
      appointment.cancelled!
      handler = PatientAppointmentHandler.new(appointment)
      dentist_name = appointment.dentist.name_with_salutation
      date = appointment.schedule.strftime("%a %b %e, %Y at %I:%M %p")
      alert = "#{dentist_name} cancelled your appointment on #{date}"
      payload = {
        id: appointment.id,
        status_id: appointment[:status]
      }
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end

    # failed?
    test "failed appointments" do
      appointment = appointments(:pending_appointment)
      appointment.failed!
      handler = PatientAppointmentHandler.new(appointment)
      dentist_name = appointment.dentist.name_with_salutation
      alert = "Sorry, #{dentist_name} is currently not available. Here are our other options."
      required_data = {
        id: appointment.id,
        name: dentist_name,
        avatar_url: URI.join(ActionController::Base.asset_host.to_s, appointment.dentist.avatar.url).to_s,
        schedule: appointment.schedule.iso8601,
        service_id: appointment.service.id,
        clinic_assignment_id: appointment.clinic_assignment_id,
        status_id: appointment[:status]
      }
      unless appointment.hmo.nil?
        required_data["hmo_id"] = appointment.hmo.id
        required_data
      end
      payload = required_data
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end

    # declined?
    test "declined appointments" do
      appointment = appointments(:pending_appointment)
      appointment.declined!
      handler = PatientAppointmentHandler.new(appointment)
      dentist_name = appointment.dentist.name_with_salutation
      alert = "Sorry, #{dentist_name} is currently not available. Here are our other options."
      required_data = {
        id: appointment.id,
        name: dentist_name,
        avatar_url: URI.join(ActionController::Base.asset_host.to_s, appointment.dentist.avatar.url).to_s,
        schedule: appointment.schedule.iso8601,
        service_id: appointment.service.id,
        clinic_assignment_id: appointment.clinic_assignment_id,
        status_id: appointment[:status]
      }
      unless appointment.hmo.nil?
        required_data["hmo_id"] = appointment.hmo.id
        required_data
      end
      payload = required_data
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end

    # complete?
    test "complete appointments" do
      appointment = appointments(:pending_appointment)
      appointment.complete!
      handler = PatientAppointmentHandler.new(appointment)
      dentist_name = appointment.dentist.name_with_salutation
      alert = "Your appointment with #{dentist_name} is done."
      payload = { none: nil }
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end
  end
end
