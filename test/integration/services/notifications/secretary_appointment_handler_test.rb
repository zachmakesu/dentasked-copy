require "test_helper"

module Notifications
  class SecretaryAppointmentHandlerTest < ActionDispatch::IntegrationTest
    setup do
      @appointment = appointments(:confirmed_appointment)
      @handler = Notifications::SecretaryAppointmentHandler.new(@appointment)
      patient_name = @appointment.patient.name
      dentist_name = @appointment.dentist.name_with_salutation
      @alert = "#{patient_name} made an appointment with #{dentist_name}"
      @fcm_key = ENV['PARTNER_FCM_KEY']
      data = {
        id: @appointment.id,
        avatar_url: URI.join(ActionController::Base.asset_host.to_s, @appointment.dentist.avatar.url).to_s,
        schedule: @appointment.schedule.iso8601,
        service_id: @appointment.service.id,
        clinic_assignment_id: @appointment.clinic_assignment_id,
        status_id: @appointment[:status]
      }

      unless @appointment.hmo.nil?
        data[:hmo_id] = @appointment.hmo.id
      end
      @payload = data
    end
    test "notification alert" do
      assert_equal @handler.send(:alert), @alert, "should return correct alert"
    end
    test "notification client" do
      assert_instance_of PartnerAPN, @handler.send(:client), "should use PartnerAPN client"
    end
    test "notification fcm key" do
      assert_equal @handler.send(:fcm_key), @fcm_key, "should return correct fcm key"
    end
    test "notification payload" do
      assert_equal @handler.send(:payload), @payload, "should have the same payload"
    end
    test "notification recipient" do
      assert_equal @handler.send(:recipient), @appointment.dentist.secretaries, "should be the appointment's patient"
    end
    test "notification type" do
      assert_equal Notifications::SecretaryAppointmentHandler::NOTIFICATION_TYPE, 3, "should have notification type"
    end

    test "patient cancelled appointment notification" do
      appointment = appointments(:pending_appointment)
      appointment.cancelled!
      handler = SecretaryAppointmentHandler.new(appointment)
      patient_name = appointment.patient.name
      dentist_name = appointment.dentist.name_with_salutation
      date = appointment.schedule.strftime("%a %b %e, %Y at %I:%M %p")
      alert ="#{patient_name} cancelled their appointment request for #{dentist_name} on #{date}"
      payload = {
        id: appointment.id,
        status_id: appointment[:status]
      }
      assert_equal handler.send(:alert), alert, "should return correct alert"
      assert_equal handler.send(:payload), payload, "should have the same payload"
    end
  end
end
