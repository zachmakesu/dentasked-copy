require "test_helper"

module Notifications
  class DayReminderHandlerTest < ActionDispatch::IntegrationTest
    setup do
      @appointment = appointments(:confirmed_appointment)
      @handler = Notifications::DayReminderHandler.new(@appointment)
      @alert = "Thank you for booking with DentaSked. Please take a moment to leave a rating to help improve our services!"
      @date = @appointment.schedule.strftime("%d %b %Y, %l:%M %p")
      @alert = "Reminder: Don't forget: #{@appointment.service.name} with #{@appointment.dentist.name_with_salutation}, Tomorrow, #{@date}"
      @fcm_key = ENV['CONSUMER_FCM_KEY']
      @payload = {
        notification_type: Notifications::DayReminderHandler::NOTIFICATION_TYPE,
        appointment_id: @appointment.id,
      }
    end
    test "notification client" do
      assert_instance_of ConsumerAPN, @handler.send(:client), "should use ConsumerAPN client"
    end
    test "notification fcm key" do
      assert_equal @handler.send(:fcm_key), @fcm_key, "should return correct fcm key"
    end
    test "notification alert" do
      assert_equal @handler.send(:alert), @alert, "should return correct alert"
    end
    test "notification payload" do
      assert_equal @handler.send(:payload), @payload, "should have the same payload"
    end
    test "notification recipient" do
      assert_equal @handler.send(:recipient), @appointment.patient, "should be the appointment's patient"
    end
    test "notification type" do
      assert_equal Notifications::DayReminderHandler::NOTIFICATION_TYPE, 5, "should have notification type"
    end
    test "#valid? if schedule is more than a day from now" do
      assert @handler.valid?, 'should be valid'
    end
    test "#valid? if schedule is less than a day" do
      appointment = @appointment.dup
      appointment.schedule = 23.hours.from_now
      appointment.save
      handler = Notifications::DayReminderHandler.new(appointment)
      refute handler.valid?, 'should not be valid'
    end
  end
end
