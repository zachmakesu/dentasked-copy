require "test_helper"

module Notifications
  class DraftHandlerTest < ActionDispatch::IntegrationTest
    setup do
      @draft = drafts(:draft)
      @handler = Notifications::DraftHandler.new(@draft)
      @alert = "Your recent Dentist info. update request was #{@draft.status} by Dentasked Admin."
      @fcm_key = ENV['PARTNER_FCM_KEY']
      @payload = {
        notification_type: 1,
        owner_uid: @draft.owner.uid
      }
    end
    test "notification client" do
      assert_instance_of PartnerAPN, @handler.send(:client), "should use PartnerAPN client"
    end
    test "notification fcm key" do
      assert_equal @handler.send(:fcm_key), @fcm_key, "should return correct fcm key"
    end
    test "notification alert" do
      assert_equal @handler.send(:alert), @alert, "should return correct alert"
    end
    test "notification payload" do
      assert_equal @handler.send(:payload), @payload, "should have the same payload"
    end
    test "notification recipient" do
      assert_equal @handler.send(:recipient), @draft.owner, "should be the draft's owner"
    end
    test "notification type" do
      assert_equal Notifications::DraftHandler::NOTIFICATION_TYPE, 1, "should have notification type"
    end
  end
end
