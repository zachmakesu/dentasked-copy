require "test_helper"

module API
  module V1
    class VerificationTest < ActionDispatch::IntegrationTest
      setup do
        @patient = users(:no_mobile_patient)
      end
      test "send existing verification code to a patient's mobile number" do
        params = { mobile_number: "639191122233", uid: @patient.uid  }
        assert_difference "Activation.count", 0 do
          post "/api/v1/verification/link", params
        end
        assert_response :success, "should be successful"
        @response = JSON.parse(response.body)
        assert @response.key?("message"), "should have message key"
        assert_equal "Activation code sent!", @response["message"]
      end

      test "send new verification code to a patient's mobile number" do
        activation = activations(:no_mobile_patient)
        activation.update_column(:expires_at, 1.hour.ago)
        params = { mobile_number: "639191132233", uid: @patient.uid  }
        assert_difference "Activation.count", 1 do
          post "/api/v1/verification/link", params
        end
        assert_response :success, "should be successful"
        @response = JSON.parse(response.body)
        assert @response.key?("message"), "should have message key"
        assert_equal "New activation code sent!", @response["message"]
      end

      test "should have successful verification" do
        params = { code: users(:patient).activations.first.code }
        post "/api/v1/verification", params
        @response = JSON.parse(response.body)
        assert @response.key?("message"), "should have message key"
        assert_equal "Verification successful! You can now log in.", @response["message"], "should have correct expected message"
      end

      test "verifying and linking mobile number" do
        mobile_number = "+639191142233"
        activation = activations(:no_mobile_patient)
        params = { code: activation.code,  mobile_number: mobile_number, uid: @patient.uid }
        post "/api/v1/verification", params
        res = JSON.parse(response.body)
        assert res.key?("message"), "should have message key"
        assert_equal "Verification successful! You can now log in.", res["message"], "should have correct expected message"
        # @patient doesn't work since it's not aware of the changes
        # in the mobile_number attribute
        assert_equal User.find_by!(uid: @patient.uid).mobile_number, mobile_number, "should correctly update mobile_number"
      end

      test "should have sucessful resending of activation code via mobile number" do
        patient = users(:patient)
        mobile_number = "09191244567"
        patient.update(mobile_number: mobile_number)
        assert_difference("Activation.count") do
          post "/api/v1/resend", mobile_number: mobile_number
          @response = JSON.parse(response.body)
        end
        assert @response.key?("message")
        assert_equal "New activation code sent!", @response["message"]
      end

      test "should not resend activation code for already active user" do
        patient = users(:patient)
        mobile_number = "09191214567"
        patient.update(mobile_number: mobile_number)
        patient.activations.last.activate!
        post "/api/v1/resend", mobile_number: mobile_number
        @response = JSON.parse(response.body)
        assert_equal "User already activated", @response["error"]
      end

      test "secretary verification with valid code" do
        post "/api/v1/secretary/verification", code: activations(:secretary_activation).code
        @response = JSON.parse(response.body)
        assert @response.key?("data"), "should return secretary data"
        refute users(:secretary).activated?, "should not reflect activated status for secretary until password has been changed"
      end

      test "secretary verification with invalid code" do
        post "/api/v1/secretary/verification", code: "invalid_code"
        @response = JSON.parse(response.body)
        refute response.success?
      end
    end
  end
end
