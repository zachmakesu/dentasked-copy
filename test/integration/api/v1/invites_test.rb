require "test_helper"

class API::V1::InvitesTest < ActionDispatch::IntegrationTest
  setup do
    @patient = patientify(users(:patient))
    @secretary = secretarify(users(:secretary))
  end

  test "patient invitation endpoint with valid invites" do
    contact_names = [
      "Test"
    ]
    params = {
      access_token: "PatientAccessToken",
      mobile_numbers: [
        "09191234567"
      ],
      contact_names: contact_names
    }
    endpoint = "/api/v1/p/invites"
    post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
    assert response.success?, "should be successful"
    message = JSON.parse(response.body)["message"]
    assert_equal "Successfully invited #{contact_names.count} #{"contact".pluralize(contact_names.count)}", message, "should have success message"
  end

  test "patient invitation endpoint with invalid invites count" do
    params = {
      access_token: "PatientAccessToken",
      mobile_numbers: [
        "09191234567"
      ],
      contact_names: [
        "Test",
        "Test2"
      ]
    }
    endpoint = "/api/v1/p/invites"
    post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
    refute response.success?, "should not be successful"
    error = JSON.parse(response.body)["error"]
    assert_equal "Invalid number of invites", error, "should have error message"
  end
end
