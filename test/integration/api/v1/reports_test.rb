require 'test_helper'

class API::V1::ReportsTest < ActionDispatch::IntegrationTest
  setup do
    @patient = users(:patient)
    @secretary = users(:secretary)
  end

  test 'report a user' do
    params = { access_token: 'PatientAccessToken', user_id: @secretary.uid }
    endpoint = '/api/v1/reports'
    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert response.success?, 'should be successful'
  end

  test 'report same user the next day' do
    report = @patient.report!(@secretary)
    report.update_attribute :created_at, (report.created_at - 1.day)
    params = { access_token: 'PatientAccessToken', user_id: @secretary.uid }
    endpoint = '/api/v1/reports'
    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert response.success?, 'should be successful'
  end

  test 'report a recently reported user' do
    @patient.report!(@secretary)
    params = { access_token: 'PatientAccessToken', user_id: @secretary.uid }
    endpoint = '/api/v1/reports'
    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    refute response.success?, 'should not be successful'
  end

  test 'report yourself' do
    params = { access_token: 'PatientAccessToken', user_id: @patient.uid }
    endpoint = '/api/v1/reports'
    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    refute response.success?, 'should not be successful'
  end
end
