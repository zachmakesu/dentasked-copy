require 'test_helper'

class API::V1::BookingsTest < ActionDispatch::IntegrationTest

  setup do
    @patient = users(:patient)
  end

  test 'return all areas' do
    params = { access_token: 'PatientAccessToken' }
    endpoint = '/api/v1/areas'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert response.success?, 'should be successful'
    @areas = JSON.parse(response.body)
    assert_equal Area.count, @areas.count, 'should return all areas'
  end

  test 'return areas based on query string' do
    params = { query: 'P', access_token: 'PatientAccessToken' }
    endpoint = '/api/v1/areas'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert response.success?, 'should be successful'
    @areas = JSON.parse(response.body)
    assert_equal Area.search('P').count, @areas.count, 'should return all areas matching query'
  end

end
