require "test_helper"

module API
  module V1
    class PartnersTest < ActionDispatch::IntegrationTest
      setup do
        @city = cities(:test_city)
      end
      def clinic_params
        {
          name: "Clinic Name",
          address: "A. Bonifacio Avenu",
          city_id: @city.id,
          landline: "1234567",
          mobile_number: "09191234567",
          landmarks: "McDonalds",
          logo: default_blank_photo
        }
      end
      test "validate valid clinic information" do
        VCR.use_cassette "clinics/draft" do
          post "/api/v1/partners/clinic", clinic_params
          assert_response :success, "should be successful"
        end
      end
      test "validate invalid clinic information" do
        invalid_params = {}
        VCR.use_cassette "clinics/draft" do
          post "/api/v1/partners/clinic", invalid_params
        end
        res = JSON.parse(response.body)
        VCR.use_cassette "clinics/draft" do
          clinic = ClinicDraftHandler.new(invalid_params).object
          clinic.valid?
          assert_equal res["error"], clinic.errors.full_messages.join('\n')
        end
      end
      def secretary_params
        {
          first_name: "Rintarou",
          last_name: "Okabe",
          birthdate: "08/08/2008",
          email: "okabe@rintarou.com",
          mobile_number: "09191234567",
          avatar: default_blank_photo
        }
      end
      test "validate valid secretary information" do
        post "/api/v1/partners/secretary", secretary_params
        assert_response :success, "should be successful"
      end
      test "validate invalid secretary information" do
        invalid_params = {}
        post "/api/v1/partners/secretary", invalid_params
        res = JSON.parse(response.body)
        secretary = SecretaryDraftHandler.new(invalid_params).object
        secretary.valid?
        assert_equal res["error"], secretary.errors.full_messages.join('\n')
        assert_response 400, "should be unsuccessful"
      end
      def dentist_params
        {
          first_name: "Rintarou",
          last_name: "Okabe",
          email: "okabe@rintarou.com",
          mobile_number: "09191234567",
          birthdate: "08/08/2008",
          license_number: "PRC123",
          fee: 1337,
          experience: 1,
          hmo_ids: ::HMO.all.map(&:id),
          service_ids: ::Service.all.map(&:id),
          specialization_ids: ::Specialty.all.map(&:id),
          avatar: default_blank_photo
        }
      end
      test "validate valid dentist information" do
        post "/api/v1/partners/dentist", dentist_params
        assert_response :success, "should be successful"
      end
      test "validate invalid dentist information" do
        invalid_params = {}
        post "/api/v1/partners/dentist", invalid_params
        res = JSON.parse(response.body)
        dentist = DentistDraftHandler.new(invalid_params).object
        dentist.valid?
        assert_equal res["error"], dentist.errors.full_messages.join('\n')
        assert_response 400, "should be unsuccessful"
      end
      test "get all available hmos" do
        get "/api/v1/partners/hmo"
        assert_response :success, "should be successful"
        res = JSON.parse(response.body)
        assert_equal res["data"].count, ::HMO.count
      end
      test "get all available services" do
        get "/api/v1/partners/services"
        assert_response :success, "should be successful"
        res = JSON.parse(response.body)
        assert_equal res["data"].count, ::Service.count
      end
      test "get all available specialties" do
        get "/api/v1/partners/specialties"
        assert_response :success, "should be successful"
        res = JSON.parse(response.body)
        assert_equal res["data"].count, ::Specialty.count
      end
      test "submit invalid partner application" do
        VCR.use_cassette "clinics/draft" do
          assert_difference "PartnerSignup.count", 0 do
          post "/api/v1/partners", {
            clinic: {},
            dentist: {},
            secretary: {} 
          }
          assert_response 400, "should be unsuccessful"
          res = JSON.parse(response.body)
          assert_equal res["error"], PartnerSignupHandler::ERROR_MESSAGE
          end
        end
      end
      test "submit valid partner application" do
        VCR.use_cassette "clinics/draft" do
          assert_difference "PartnerSignup.count", 1 do
            post "/api/v1/partners", {
              clinic: clinic_params,
              dentist: dentist_params,
              secretary: secretary_params
            }
            assert_response :success, "should be successful"
            res = JSON.parse(response.body)
            assert_equal res["message"], PartnerSignupHandler::SUCCESS_MESSAGE
            secretary_signup = PartnerSignup.last
            assert secretary_signup.clinic_logo.present?, "should have clinic logo reference"
            assert secretary_signup.secretary_avatar.present?, "should have secretary avatar reference"
            assert secretary_signup.dentist_avatar.present?, "should have dentist avatar reference"
          end
        end
      end
    end
  end
end
