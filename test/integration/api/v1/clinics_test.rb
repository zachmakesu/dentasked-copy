require 'test_helper'

class API::V1::ClinicsTest < ActionDispatch::IntegrationTest

  setup do
    @patient = users(:patient)
    @clinic = clinics(:adajar)
  end

  test 'should return list of clinics' do
    params = { access_token: 'PatientAccessToken' }
    endpoint = '/api/v1/clinics'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert_equal Clinic.count, json['data'].count, 'should return all clinics'
  end

  test 'should return results of clinics search' do
    params = { query: 'prestige', access_token: 'PatientAccessToken'}
    endpoint = '/api/v1/clinics/search'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert json.has_key?('data'), 'should have key data'
    assert_equal Clinic.search('prestige').count, json['data'].count, 'should return Prestige Clinic from clinics search'
  end

  test 'should return list of nearby clinics' do
    params = { lat: 14.6082573, lng: 121.0923022, access_token: 'PatientAccessToken' }
    endpoint = '/api/v1/clinics/nearby'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert json.has_key?('data'), 'should have key data'
    assert_equal  Clinic.near([params[:lat], params[:lng]], 10).count(:all), json['data'].count, 'should have same number of clinics'
  end

  test 'should return clinic data' do
    params = { id: @clinic.id,access_token: 'PatientAccessToken' }
    endpoint = "/api/v1/clinic/#{params[:id]}"
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert json.has_key?('data'), 'should have key data'
  end

end
