require 'test_helper'
class API::V1::DentistsTest < ActionDispatch::IntegrationTest
  setup do
    @secretary = secretarify(users(:secretary))
    @appointment = appointments(:confirmed_appointment)
    @dentist = @appointment.clinic_assignment.dentist
  end

  test 'get /api/v1/s/dentists with no search param' do
    params = { access_token: 'SecretaryAccessToken' }
    endpoint = '/api/v1/s/dentists'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert_equal @secretary.dentists.count, json['data'].count, 'should return all dentists'
  end

  test 'get /api/v1/s/dentists with search param' do
    params = { query: 'alderson', access_token: 'SecretaryAccessToken'}
    endpoint = '/api/v1/s/dentists'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert json.key?('data'), 'should have key data'
    assert_equal Secretary.search('alderson').count, json['data'].count, 'should return \'alderson\' dentists from dentists search'
  end

  test 'get /api/v1/s/dentists/uid with valid activation code' do
    params = { access_token: 'SecretaryAccessToken', activation_code: '123450' }
    endpoint = '/api/v1/s/dentists/uid'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 200, 'should have 200 response status'
  end

  test 'get /api/v1/s/dentists/uid with invalid activation code' do
    params = { access_token: 'SecretaryAccessToken', activation_code: 'invalid' }
    endpoint = '/api/v1/s/dentists/uid'
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 404, 'should have 404 response status'
  end

  test 'get /api/v1/s/dentist/:uid/history with valid uid' do
    params = { uid: "#{@dentist.uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{@dentist.uid}/history"
    (1...9).each do |x|
      appointment = @appointment.dup
      appointment.schedule = x.days.from_now
      appointment.code = Appointment.generate_code
      appointment.save
      appointment.confirmed!
    end
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert json.key?('data'), 'should have key data'
    #.page(1).per(5) => paginate per_page: 5 first page by default.
    appointments = Kaminari.paginate_array(@dentist.clinic_assignments.map{|c| c.appointments.confirmed_appointments.by_latest}.flatten.group_by{|a| a.schedule.to_date }.to_a).page(1).per(5)
    assert_equal appointments.count, json['data'].count, 'should return correct number of paginated appointments'
    # Get the "date" which the first element of a grouped appointments array
    expected_first_date = appointments.first.first
    actual_first_date = json["data"].first["date"].to_date
    assert_equal expected_first_date, actual_first_date, 'should be sorted according to schedule'
  end

  test 'get /api/v1/s/dentist/:uid' do
    params = { uid: "#{@dentist.uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{@dentist.uid}"

    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 200, 'should have 200 response status'
    json = JSON.parse(response.body)
    assert_equal json['data']['uid'], @dentist.uid, 'should return same uid of dentist you\'re requesting'
  end

  test 'post /api/v1/s/dentist/:uid/add with nonexistent dentist' do
    nonexistent_uid = "thisisnotexisting"
    params = { uid: "#{nonexistent_uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{nonexistent_uid}/add"

    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 404, 'should have 404 response status'
  end

  test 'post /api/v1/s/dentist/:uid/add with already dentist' do
    params = { uid: "#{@dentist.uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{@dentist.uid}/add"

    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 400, 'should have 400 response status'
  end

  test 'post /api/v1/s/dentist/:uid/add with unassigned dentist' do
    @unassigned_dentist = users(:dentist)
    params = { uid: "#{@unassigned_dentist.uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{@unassigned_dentist.uid}/add"

    post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response :success, 'response should be successful'
  end

  test 'delete /api/v1/s/dentist/:uid/delete with dentist you manage' do
    params = { uid: "#{@dentist.uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{@dentist.uid}/delete"

    delete endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response :success, 'response should be successful'
  end

  test 'delete /api/v1/s/dentist/:uid/delete with invalid dentist' do
    @unassigned_dentist = users(:dentist)
    params = { uid: "#{@unassigned_dentist.uid}", access_token: 'SecretaryAccessToken'}
    endpoint = "/api/v1/s/dentist/#{@unassigned_dentist.uid}/delete"

    delete endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    assert_response 404, 'should have 404 response status'
  end

  test 'post /api/v1/s/dentist/:uid/edit with params' do
    assert_difference 'Draft.count', 1, 'should increase draft count' do
      params = {
        uid: "#{@dentist.uid}",
        access_token: 'SecretaryAccessToken',
        fee: 1000,
        license_number: 'A007'
      }
      endpoint = "/api/v1/s/dentist/#{@dentist.uid}/edit"
      post endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @secretary.uid) }
    end
  end
end
