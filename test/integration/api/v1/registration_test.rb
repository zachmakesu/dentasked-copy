require "test_helper"
module API
  module V1
    class RegistrationTest < ActionDispatch::IntegrationTest
      setup do
        @reset_password_changes = [
          "SMS::ResetPasswordInstructionsSender.jobs.count",
          "ActionMailer::Base.deliveries.count"
        ]
        @params = {
          first_name:      "Dean",
          last_name:       "Canlas",
          mobile_number:   "639471603482",
          email:           "dean@gorated.ph",
          password:        default_api_password
        }
        @user = users(:patient)
      end

      test "have successful patient registration" do
        assert_difference("Patient.count") do
          post "/api/v1/registration", @params
          assert_response 201, "have 201 response code"
        end
      end

      test "have successful patient registration with birthdate" do
        date = Date.new(1991, 8, 19)
        params = @params.merge({
          birthday: "(08) - (19) - (1991)"
        })
        assert_difference("Patient.count") do
          post "/api/v1/registration", params
          assert_response 201, "have 201 response code"
          assert_equal User.find_by(email: 'dean@gorated.ph').birthdate, date.iso8601
        end
      end

      test "have unsuccessful patient registration for invalid email" do
        @params[:email] = "dean"
        post "/api/v1/registration", @params
        assert_no_difference("Patient.count") do
          post "/api/v1/registration", @params
          assert_response 500, "have 500 response code"
          @error = JSON.parse(response.body)
        end
        assert @error.key?("error"), "have error message"
      end

      test "have unsuccessful patient registration for blank password" do
        @params[:password] = nil
        post "/api/v1/registration", @params
        assert_no_difference("Patient.count") do
          post "/api/v1/registration", @params
          assert_response 500, "have 500 response code"
          @error = JSON.parse(response.body)
        end
        assert @error.key?("error"), "have error message"
      end

      test "have unsuccessful patient registration for invalid password" do
        @params[:password] = "1234567"
        post "/api/v1/registration", @params
        assert_no_difference("Patient.count") do
          post "/api/v1/registration", @params
          assert_response 500, "have 500 response code"
          @error = JSON.parse(response.body)
        end
        assert @error.key?("error"), "have error message"
      end

      test "reset password delivers reset password email and sms" do
        assert_difference(@reset_password_changes, 1) do
          post "/api/v1/password_recovery", email: @user.email
        end
      end

      test "reset password for nonexisting user with email" do
        assert_difference(@reset_password_changes, 0) do
          post "/api/v1/password_recovery", email: "nonexistent@email.com"
          assert_response 404, 'response should be 404'
        end
      end

      test "update password of already activated secretary" do
        activations(:secretary_activation).activate!
        post "/api/v1/secretary/registration", code: activations(:secretary_activation).code, password: "thisismynewpassword"
        refute response.success?, "have unsuccessful response"
        refute users(:secretary).valid_password?("thisismynewpassword"), "should not update existing password"
      end

      test "update secretary password after code verification" do
        post "/api/v1/secretary/registration", code: activations(:secretary_activation).code, password: default_api_password
        assert response.success?, "have successful response"
        assert users(:secretary).valid_password?(default_api_password), "successfully update password"
        assert users(:secretary).activated?, "reflect activated status for secretary activations"
      end

      test "update secretary password with invalid password" do
        post "/api/v1/secretary/registration", code: activations(:secretary_activation).code, password: ""
        refute response.success?, "have unsuccessful response"
        refute users(:secretary).valid_password?(""), "not update password"
        refute users(:secretary).activated?, "not reflect activated status for secretary activations"
      end

      test "update secretary password with invalid code" do
        post "/api/v1/secretary/registration", code: "invalidcode", password: ""
        refute response.success?, "have unsuccessful response"
        refute users(:secretary).activated?, "not reflect activated status for secretary activations"
      end
    end
  end
end
