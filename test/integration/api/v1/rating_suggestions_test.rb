require "test_helper"
class API::V1::RatingSuggestionTest < ActionDispatch::IntegrationTest
  setup do
    @patient = users(:patient)
  end
  test "should return list of rating suggestions" do
    endpoint = "/api/v1/rating_suggestions"
    params = { access_token: "PatientAccessToken" }
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert response.successful?, "should have successful status"
    json = JSON.parse(response.body)
    assert_equal RatingSuggestion.active.count, json["data"].count, "should return enabled suggestions"
  end
end
