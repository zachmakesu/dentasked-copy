require 'test_helper'
class API::V1::HMOTest < ActionDispatch::IntegrationTest
  setup do
    @patient = users(:patient)
  end
  test 'should return list of HMO' do
    endpoint = '/api/v1/hmo'
    params = { access_token: 'PatientAccessToken' }
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, @patient.uid) }
    assert response.successful?, 'should have successful status'
    json = JSON.parse(response.body)
    assert_equal HMO.count, json['data'].count, 'should return all hmo'
  end
end
