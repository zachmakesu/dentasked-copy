require "test_helper"
class API::V1::LocationTest < ActionDispatch::IntegrationTest
  setup do
    @region = regions(:test_region)
    @patient = users(:patient)
  end
  test "get regions from location regions endpoint" do
    endpoint = "/api/v1/location/regions"
    get endpoint
    assert response.successful?, "should have successful status"
    json = JSON.parse(response.body)
    assert_equal Region.count, json["data"].count, "should return all regions"
  end
  test "get cities from location cities endpoint" do
    endpoint = "/api/v1/location/cities"
    params = { region_id: @region.id }
    get endpoint, params
    assert response.successful?, "should have successful status"
    json = JSON.parse(response.body)
    assert_equal Region.find(@region.id).provinces.map(&:cities).flatten.count, json["data"].count, "should return all cities under a region"
  end
end
