require "test_helper"

class API::V1::PatientsTest < ActionDispatch::IntegrationTest
  setup do
    @patient = patientify(users(:patient))
    @secretary = secretarify(users(:secretary))
  end

  test "should have patient profile data" do
    hmac_params = { uid: @patient.uid, access_token: "PatientAccessToken" }
    params = { access_token: "PatientAccessToken" }
    endpoint = "/api/v1/patient/#{hmac_params[:uid]}"
    get endpoint, params, "Authorization" => valid_authorization_header(endpoint, hmac_params, @patient.uid)
    assert_response 200, "should have 200 response status"
    json = JSON.parse(response.body)
    assert json.key?("data")
  end

  test "update of patient booking preferences" do
    params = { access_token: "PatientAccessToken", symptom_id: symptoms(:loose_teeth).id, specialty_id: specialties(:endodontics).id }
    endpoint = "/api/v1/patients/preferences"
    put endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
    assert_response 200, "should have 200 response status"
    json = JSON.parse(response.body)
    assert_equal json["notice"], "Successfully updated preferences", "should return all patients"
    assert_equal @patient.preference.symptom_id, symptoms(:loose_teeth).id, "should update changed symptom"
  end

  # secretary namespace
  # /s/endpoint...
  test "should return list of all dentists' patients" do
    endpoint = "/api/v1/s/patients"
    params = { access_token: "SecretaryAccessToken" }
    get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
    json = JSON.parse(response.body)
    assert_response 200, "should have 200 response status"
    assert json.key?("data")
    dentist_ids = @secretary.dentist_ids
    clinic_assignments = ClinicAssignment.where(dentist_id: dentist_ids)
    appointments = clinic_assignments.map(&:appointments).flatten
    patients = appointments.map(&:patient).uniq
    assert_equal patients.count, json["data"].count
  end

  test "should return patient profile" do
    params = { uid: @patient.uid, access_token: "SecretaryAccessToken" }
    endpoint = "/api/v1/s/patient/#{params[:uid]}"
    get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
    assert_response :success, "should have 200 response status"
    json = JSON.parse(response.body)
    assert json.key?("data")
  end
end
