require "test_helper"

module API
  module V1
    class MeTest < ActionDispatch::IntegrationTest
      setup do
        @patient = patientify(users(:patient))
        @secretary = secretarify(users(:secretary))
        @clinic = clinics(:adajar)
      end

      test "return patient's profile" do
        endpoint = "/api/v1/p/me"
        params = { access_token: "PatientAccessToken" }
        get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
        json = JSON.parse(response.body)
        assert_response 200, "request should be successful"
        assert json.key?("data"), "response should be json with 'data' key"
      end

      test "return secretary's profile" do
        endpoint = "/api/v1/s/me"
        params = { access_token: "SecretaryAccessToken" }
        get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
        json = JSON.parse(response.body)
        assert_response 200, "request should be successful"
        assert json.key?("data"), "response should be json with 'data' key"
      end

      test "should create new favorite clinic" do
        assert_difference("Favorite.count", 1, "No new favorite") do
          endpoint = "/api/v1/p/me/favorites/new"
          params = { clinic_id: @clinic.id, access_token: "PatientAccessToken" }
          post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
          json = JSON.parse(response.body)
          assert_response 201, "should have 201 response status"
          assert json.key?("message")
          assert_equal "Clinic added to favorites", json["message"]
        end
      end

      test "should return list of favorite clinics and dentists" do
        endpoint = "/api/v1/p/me/favorites"
        params = { access_token: "PatientAccessToken" }
        get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
        json = JSON.parse(response.body)
        assert_response :success, "should have 200 response status"
        assert json.key?("data")
        assert_equal @patient.favorite_clinics.count, json["data"]["clinics"].count
        assert_equal @patient.favorite_dentists.count, json["data"]["dentists"].count
      end

      test "should allow device registration" do
        endpoint = "/api/v1/me/devices"
        params = {
          access_token: "PatientAccessToken",
          name: "iPhone 6s Plus",
          token: devices(:iphone_5).token,
          platform: "ios"
        }
        post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
        assert response.success?, "should have successful response status"
      end

      test "expired access token response" do
        api_keys(:patient_api_key).update(expires_at: 1.week.ago)
        endpoint = "/api/v1/p/me"
        params = { access_token: "PatientAccessToken" }
        get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
        refute response.success?
        assert_equal response.code, "401"
        json = JSON.parse(response.body)
        assert_equal json["code"], 104
        assert_equal json["error"], "Access Token is expired"
      end
    end
  end
end
