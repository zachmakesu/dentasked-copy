require "test_helper"

class API::V1::AppointmentsTest < ActionDispatch::IntegrationTest
  setup do
    @secretary = users(:secretary)
    @patient = users(:patient)
    @pending = appointments(:pending_appointment)
    @confirmed = appointments(:confirmed_appointment)
  end

  test "create a new appointment" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "PatientAccessToken",
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/appointments"
    assert_difference "Appointment.count", 1 do
      post endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
      assert response.success?, "should be successful"
      appointment = JSON.parse(response.body)
      assert_equal schedule, appointment["data"]["schedule"], "appointment schedule should be equal"
    end
  end

  test "creating a new appointment with ios user agent" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "PatientAccessToken",
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/appointments"
      ios_ua = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1'
      post endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid), 'User-Agent': ios_ua }
      assert response.success?, "should be successful"
      assert Appointment.last.code.include?('IOS'), 'should have ios related code'
  end

  test "creating a new appointment with android user agent" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "PatientAccessToken",
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/appointments"
    ios_ua = 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36'
      post endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid), 'User-Agent': ios_ua }
      assert response.success?, "should be successful"
      assert Appointment.last.code.include?('ANDROID'), 'should have ios related code'
  end

  test "confirming a pending appointment" do
    params = { id: @pending.id, access_token: "PatientAccessToken" }
    endpoint = "/api/v1/s/appointments/pending/#{params[:id]}/confirm"
    put endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    message = JSON.parse(response.body)
    assert_response 200, "should have 200 response status"
    assert_equal "Appointment successfully confirmed.", message["message"]
    assert @pending.pending?, "should change status to pending"
  end

  test "ignoring a pending appointment" do
    params = { id: @pending.id, access_token: "PatientAccessToken" }
    endpoint = "/api/v1/s/appointments/pending/#{params[:id]}/ignore"
    put endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    message = JSON.parse(response.body)
    assert_response 200, "should have 200 response status"
    assert_equal "Appointment ignored.", message["message"]
    assert @pending.declined!, "should change status to declined"
  end

  # Patient
  test "getting an appointment of a patient" do
    params = { id: @confirmed.id, access_token: "PatientAccessToken" }
    endpoint = "/api/v1/p/appointment/#{params[:id]}/show"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response 200, "should have 200 response status"
  end

  test "rating an appointment of a patient with valid params" do
    params = { id: @confirmed.id, access_token: "PatientAccessToken", rating: 3 }
    endpoint = "/api/v1/p/appointment/#{params[:id]}/rate"
    assert_difference "Rating.count", 1 do
      post endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    end
  end

!  test "rating an appointment of a patient with invalid params" do
    params = { id: @confirmed.id, access_token: "PatientAccessToken", rating: 7 }
    endpoint = "/api/v1/p/appointment/#{params[:id]}/rate"
    assert_difference "Rating.count", 0 do
      post endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
      json = JSON.parse(response.body)
      assert_equal RatingHandler::FAILED_RATING_MESSAGE, json["error"], "should have designated error message"
    end
  end

  # Secretary
  test "walk in appointment with nonexistent patient" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "SecretaryAccessToken",
      patient_uid: "nonexistent",
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/s/appointments/new"
    post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
    assert_response 404, "response should be 404"
  end

  test "walk in appointment with valid and existing patient" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "SecretaryAccessToken",
      patient_uid: @patient.uid,
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/s/appointments/new"
    assert_difference "Appointment.count", 1 do
      post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
      assert_response :success, "response should be successful"
    end
  end

  test "walk in appointment with valid mobile number" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "SecretaryAccessToken",
      mobile_number: "09191234567",
      first_name: "Okabe",
      last_name: "Rintarou",
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/s/appointments/new"
    assert_difference "Appointment.count", 1 do
      post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
      assert_response :success, "response should be successful"
    end
  end

  test "walk in appointment with existing mobile number" do
    schedule = 24.hours.from_now.iso8601
    params = {
      access_token: "SecretaryAccessToken",
      mobile_number: "09191234567",
      first_name: "Okabe",
      last_name: "Rintarou",
      clinic_assignment_id: clinic_assignments(:assigned_dentist_prestige).id,
      service_id: services(:whitening).id,
      schedule: schedule
    }
    endpoint = "/api/v1/s/appointments/new"
    # We're creating the existing patient record with
    # said mobile number since there should be a service
    # object handling the appointment creation process.
    # But since there's not, we send the same request twice
    # and assert on the second request
    # TODO: Add service object for managing appointments
    post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
    assert_difference "Appointment.count", 1 do
      post endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @secretary.uid)
      assert_response :success, "response should be successful"
    end
  end

  test "offline scope" do
    offline = appointments(:offline_appointment)
    online = appointments(:pending_appointment)
    assert_includes Appointment.offline, offline
    refute_includes Appointment.offline, online
  end

  test "online scope" do
    offline = appointments(:offline_appointment)
    online = appointments(:pending_appointment)
    refute_includes Appointment.online, offline
    assert_includes Appointment.online, online
  end
end
