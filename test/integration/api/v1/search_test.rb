require "test_helper"

class API::V1::SearchTest < ActionDispatch::IntegrationTest
  setup do
    @patient = users(:patient)
    @dentist = users(:dentist)
  end

  test "should return success with valid params for universal search" do
    params = { access_token: "PatientAccessToken",
               page: 2
    }
    endpoint = "/api/v1/p/search/universal"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
  end

  test "successful search with city params" do
    params = { access_token: "PatientAccessToken",
               schedule: DateTime.now,
               keyword_id: @dentist.id,
               keyword_type: "dentist",
               page: 1,
               city_id: cities(:test_city).id
    }
    endpoint = "/api/v1/p/search"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
  end

  test "should return success with valid params for search" do
    params = { access_token: "PatientAccessToken",
               schedule: DateTime.now,
               keyword_id: @dentist.id,
               keyword_type: "dentist",
               page: 2
    }
    endpoint = "/api/v1/p/search"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
  end

  test "search keywords with valid search query" do
    params = {
      access_token: "PatientAccessToken",
      q: "q",
      page: 1
    }
    endpoint = "/api/v1/p/search/keywords"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
  end

  test "search keywords with empty query" do
    params = {
      access_token: "PatientAccessToken",
      q: "",
      page: 1
    }
    endpoint = "/api/v1/p/search/keywords"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
  end

  test "search keywords with city_id and empty query" do
    params = {
      access_token: "PatientAccessToken",
      q: "",
      page: 1,
      city_id: cities(:test_city).id
    }
    endpoint = "/api/v1/p/search/keywords"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
  end

  test "search keywords with city_id and valid search query" do
    params = {
      access_token: "PatientAccessToken",
      q: "Edw",
      page: 1,
      city_id: cities(:test_city).id
    }
    endpoint = "/api/v1/p/search/keywords"
    get endpoint, params, { "Authorization" => valid_authorization_header(endpoint, params, @patient.uid) }
    assert_response :success, "should be successful"
    response_object = JSON.parse(response.body)
    assert_not_empty response_object["data"]
  end
end
