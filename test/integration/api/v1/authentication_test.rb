require "test_helper"

module API
  module V1
    class AuthenticationTest < ActionDispatch::IntegrationTest
      setup do
        @patient = users(:patient)
        @patient.activations.first.activate!
      end

      test "should have successful login with access token" do
        endpoint = "/api/v1/login"
        params = { email: @patient.email, password: default_api_password, type: "Patient" }
        post endpoint, params
        assert_response :success, "should have sucessful response"
        json = JSON.parse(response.body)
        assert json.key?("token"), "should have token key in response body"
      end

      test "should have unsucessful login if logging in from invalid app" do
        endpoint = "/api/v1/login"
        params = { email: @patient.email, password: "itshappening", type: "Secretary" }
        post endpoint, params
        assert_response 401, "should have unsuccessful response"
      end
      test "get /api/v1/symptoms without proper header" do
        get "/api/v1/symptoms", {}, "Authorization" => "Dentasked"
        refute response.ok?, "should not be ok without valid auth header and access token"
      end
      test "get /api/v1/symptoms with proper header and valid access token" do
        endpoint = "/api/v1/symptoms"
        params = { access_token: "PatientAccessToken" }
        get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
        assert response.ok?, "should be ok with valid auth header"
      end

      test "get /api/v1/symptoms with proper header but invalid access token" do
        endpoint = "/api/v1/symptoms"
        params = { access_token: "InvalidAccessToken" }
        get endpoint, params, "Authorization" => valid_authorization_header(endpoint, params, @patient.uid)
        refute response.ok?, "should not be ok with invalid access token"
      end
    end
  end
end
