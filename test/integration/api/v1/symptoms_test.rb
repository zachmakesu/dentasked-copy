require 'test_helper'

class API::V1::SymptomsTest < ActionDispatch::IntegrationTest

  test 'get /api/v1/symptoms' do
    patient = users(:patient)
    endpoint = '/api/v1/symptoms'
    params = { access_token: 'PatientAccessToken' }
    get endpoint, params, { 'Authorization' => valid_authorization_header(endpoint, params, patient.uid) }
    assert response.ok?, 'request response should be ok'
    json = JSON.parse(response.body)
    assert_equal json['data'].length, Symptom.count, 'all symptoms should be returned'
  end

end
