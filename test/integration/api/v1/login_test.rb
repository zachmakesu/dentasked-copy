require "test_helper"

class API::V1::LoginTest < ActionDispatch::IntegrationTest
  setup do
    @token = File.read("#{Rails.root}/test/support/config/valid_fb_access_token").chomp
    users(:patient).activations.first.activate!
    @patient_params = {
      email: users(:patient).email,
      password: default_api_password,
      type: "Patient"
    }
    users(:secretary).activations.first.activate!
    @secretary_params = {
      email: users(:secretary).email,
      password: default_api_password,
      type: "Secretary"
    }
  end
  test "patient login with valid credentials" do
    post "/api/v1/login", @patient_params
    assert_response :success, "should return successful response"
  end
  test "patient login with missing mobile number" do
    post "/api/v1/login", @patient_params
    res = JSON.parse(response.body)
    assert res["missing_mobile_number"], "should have missing mobile number key"
  end
  test "patient login with activated mobile number" do
    patient = users(:patient)
    patient.update_attribute(:mobile_number, "639191234567")
    patient.activations.last.activate!
    post "/api/v1/login", @patient_params
    res = JSON.parse(response.body)
    refute res["missing_mobile_number"], "should have missing mobile number key"
  end
  test "patient login with valid credentials but invalid type" do
    params = @patient_params.dup
    params[:type] = "Secretary"
    post "/api/v1/login", params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response"
  end
  test "patient login with invalid password" do
    @patient_params[:password] = "invalidpassword"
    post "/api/v1/login", @patient_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Incorrect password supplied", "should not allow login if invalid password"
  end
  test "patient login with nonexistent account" do
    @patient_params[:email] = "rem@rezero.com"
    post "/api/v1/login", @patient_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Invalid email, you must create an account first", "should not allow login if user does not exist"
  end
  test "patient login with disabled account" do
    users(:patient).disable!
    post "/api/v1/login", @patient_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Account is disabled, please contact an admin for more info", "should not allow login if user is disabled"
  end
  test "patient login with inactive account" do
    users(:patient).activations.first.update(activated_at: nil)
    post "/api/v1/login", @patient_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Account is not yet activated, please activate your account", "should not allow login if user is inactive"
    assert @error.key?("mobile_number"), "should return mobile number of patient"
  end

  test "dentist login with valid credentials" do
    dentist_params = {
      email: users(:dentist).email,
      password: default_api_password,
      type: "Patient"
    }
    post "/api/v1/login", dentist_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return successful response"
    assert_response 401, "should return 401 response code"
  end
  test "independent dentist login with valid credentials" do
    users(:independent).activations.first.activate!
    dentist_params = {
      email: users(:independent).email,
      password: default_api_password,
      type: "Secretary"
    }
    post "/api/v1/login", dentist_params
    assert_response :success, "should return successful response"
  end
  test "secretary login with valid credentials" do
    post "/api/v1/login", @secretary_params
    @error = JSON.parse(response.body)
    assert_response :success, "should return successful response"
  end
  test "secretary login with valid credentials but invalid type" do
    params = @secretary_params.dup
    params[:type] = "Patient"
    post "/api/v1/login", params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response"
  end
  test "secretary login with invalid password" do
    @secretary_params[:password] = "invalidpassword"
    post "/api/v1/login", @secretary_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Incorrect password supplied", "should not allow login if invalid password"
  end
  test "secretary login with nonexistent account" do
    @secretary_params[:email] = "rem@rezero.com"
    post "/api/v1/login", @secretary_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Invalid email, you must create an account first", "should not allow login if user does not exist"
  end
  test "secretary login with disabled account" do
    users(:secretary).disable!
    post "/api/v1/login", @secretary_params
    @error = JSON.parse(response.body)
    assert_response 401, "should return 401 response code"
    assert_equal @error["error"], "Account is disabled, please contact an admin for more info", "should not allow login if user is disabled"
  end
  test "patient facebook login with valid token" do
    VCR.use_cassette "facebook/authenticate_fb_user" do
      assert_difference ['User.count', 'Identity.count', 'UserRole.count', 'APIKey.count'], 1 do
        post "/api/v1/login/facebook", {
          token: @token,
          profile: 'patient'
        }
        assert response.success?, 'should be successful'
      end
    end
  end
  test "patient facebook login with invalid token" do
    VCR.use_cassette "facebook/authenticate_invalid_fb_user" do
      post "/api/v1/login/facebook", {
        token: "invalidToken",
        profile: 'patient'
      }
      @error = JSON.parse(response.body)
      refute response.success?, 'should be successful'
      assert_equal @error["error"], "Facebook access token is invalid"
    end
  end
end
