ENV["RAILS_ENV"] ||= "test"
# require 'simplecov'
# SimpleCov.start 'rails'

require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"

# Disable net requests
require 'webmock/minitest'
WebMock.disable_net_connect!(allow_localhost: true)

# Use vcr for simulating net requests
require 'vcr'
require 'kaminari'
require 'grape'
require 'grape-kaminari'
require 'minitest/mock'

VCR.configure do |c|
  # Ignore requests to Slanger
  c.ignore_request do |request|
    URI(request.uri).port == 4567
  end
  c.cassette_library_dir = 'test/cassettes'
  c.hook_into :webmock
end

# Mocha provides mocking and stubbing helpers
require "mocha/mini_test"
Mocha::Configuration.warn_when(:stubbing_non_existent_method)
Mocha::Configuration.warn_when(:stubbing_non_public_method)

# Minitest::Reporters adds color and progress bar to the test runner
require "minitest/reporters"
Minitest::Reporters.use!(
  [
    Minitest::Reporters::SpecReporter.new,
    Minitest::Reporters::JUnitReporter.new
  ],
  ENV,
  Minitest.backtrace_filter)

# Capybara + poltergeist allow JS testing via headless webkit
require "capybara/rails"
require "capybara/poltergeist"
Capybara.javascript_driver = :poltergeist

# Use Sidekiq's test fake that pushes all jobs into a jobs array
require "sidekiq/testing"
Sidekiq::Testing.fake!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def default_api_password
    "1t'$h@PP3n1nG!?"
  end

  def dentistify(user)
    user.becomes(Dentist)
  end

  def patientify(user)
    user.becomes(Patient)
  end

  def secretarify(user)
    user.becomes(Secretary)
  end

  def adminify(user)
    user.becomes(Admin)
  end

  def default_blank_photo
    fixture_file_upload("test/support/images/test.jpg", "image/jpg")
  end

  def default_photo_attachment
    {
      filename: "test.jpg",
      tempfile: default_blank_photo,
      type: "image/jpg"
    }
  end

  # Add more helper methods to be used by all tests here...
  # http://stackoverflow.com/a/29726865
  module FixtureFileHelpers

    def default_blank_photo
      fixture_file_upload("test/support/images/test.jpg", "image/jpg")
    end

    def encrypted_password(password = "1t'$h@PP3n1nG!?")
      User.new.send(:password_digest, password)
    end
    def encrypted_access_token(token = 'ThisIsAValidAccessTokenOf32Len')
      APIKey.new.send(:access_token=, token)
    end
  end

  ActiveRecord::FixtureSet.context_class.send :include, FixtureFileHelpers
  setup { ActionMailer::Base.deliveries.clear }
end

class ActionDispatch::IntegrationTest
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL

  def login_via_api(email, password)
    params = { email: email, password: password }
    post 'api/v1/login', params
    @response = response
  end

  def valid_authorization_header(endpoint, params, uid)
    sig = HMAC.signature_from(endpoint, params)
    return "Dentasked #{uid}:#{sig}"
  end
end

# Monkey patch so that AR shares a single DB connection among all threads.
# This ensures data consistency between the test thread and poltergeist thread.
class ActiveRecord::Base
  mattr_accessor :shared_connection
  @@shared_connection = nil

  def self.connection
    @@shared_connection || ConnectionPool::Wrapper.new(:size => 1) { retrieve_connection }
  end
end
ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

class ActionController::TestCase
  include Devise::TestHelpers

  def login user
    @admin = user
    @request.env["devise.mapping"] = Devise.mappings[:users]
    @admin.confirm
    sign_in @admin
  end
end
