# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def welcome_email
    @user = User.first
    UserMailer.welcome_email(@user.id)
  end
  def welcome_patient
    @user = Patient.first
    UserMailer.welcome_patient(@user.id)
  end
end
