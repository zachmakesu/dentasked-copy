# Preview all emails at http://localhost:3000/rails/mailers/notification_mailer
class NotificationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/daily_appointments
  def daily_appointments
    NotificationMailer.daily_appointments
  end

end
