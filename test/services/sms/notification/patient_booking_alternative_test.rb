require "test_helper"
module SMS
  module Notification
    class PatientBookingAlternativeTest < ActiveSupport::TestCase
      setup do
        @appointment = appointments(:pending_appointment)
        @notification = SMS::Notification::PatientBookingAlternative.new(appointment_id: @appointment.id)
      end

      test "message should contain actual appointment message" do
        short_url = 'https://bit.ly/2u82IM7'
        @notification.stub(:dynamic_link_url, short_url) do
          message = <<~EOS.strip_heredoc
          Sorry, #{@appointment.dentist.name_with_salutation} is currently not available. Here are your options:

          #{alternatives}

          EOS
          complete_msg = message + short_url
          assert_equal @notification.message, complete_msg
        end
      end

      test "recipient should be the mobile number of appointment user" do
        recipient = @appointment.patient.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "client should be checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end

      private

      def alternatives
        @appointment.alternatives.map{|d| "- #{d.name_with_salutation}"}.join("\n")
      end
    end
  end
end
