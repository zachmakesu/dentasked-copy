require "test_helper"
module SMS
  module Notification
    class PartnerBookingReminderTest < ActiveSupport::TestCase
      setup do
        @appointment = appointments(:pending_appointment)
        @notification = SMS::Notification::PartnerBookingReminder.new(appointment_id: @appointment.id)
        mobile_number = "09191112222"
        @secretary = @appointment.clinic_assignment.dentist.secretaries.first
        @secretary.update(mobile_number: mobile_number)
      end

      test "message should contain actual reminder message" do
        message = <<-EOS.strip_heredoc
          An appointment request from #{@appointment.patient.name} is about to expire soon. Respond to the request by logging in to your DentaSked Partner's app.
        EOS
        assert_equal @notification.message, message
      end

      test "recipient should be the mobile number of secretary" do
        recipient = @secretary.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "client should be checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end
    end
  end
end
