require "test_helper"
module SMS
  module Notification
    class PatientBookingDayReminderTest < ActiveSupport::TestCase
      setup do
        @appointment = appointments(:pending_appointment)
        @notification = SMS::Notification::PatientBookingDayReminder.new(appointment_id: @appointment.id)
        mobile_number = "09191112222"
        @patient = @appointment.patient
        @patient.update(mobile_number: mobile_number)
      end

      test "message should contain actual reminder message" do
        message = <<-EOS.strip_heredoc
          DentaSked Reminder: Hi, #{@patient.first_name}! Don't forget, you have a
          #{@appointment.service.name} with #{@appointment.dentist.name_with_salutation} Tomorrow at #{@appointment.formatted_time}
        EOS
        assert_equal @notification.message, message
      end

      test "recipient should be the mobile number of patient" do
        recipient = @patient.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "client should be checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end
    end
  end
end
