require "test_helper"

module SMS
  module Notification
    class BaseTest < ActiveSupport::TestCase
      setup do
        @notification = SMS::Notification::Base.new
      end

      test "valid? should raise not implemented error" do
        assert_raise NotImplementedError do
          @notification.valid?
        end
      end

      test "message should raise not implemented error" do
        assert_raise NotImplementedError do
          @notification.message
        end
      end

      test "recipient should raise not implemented error" do
        assert_raise NotImplementedError do
          @notification.recipient
        end
      end

      test "client should raise not implemented error" do
        assert_raise NotImplementedError do
          @notification.client
        end
      end
    end
  end
end
