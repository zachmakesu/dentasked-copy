require "test_helper"
module SMS
  module Notification
    class PatientBookingConfirmationTest < ActiveSupport::TestCase
      setup do
        @appointment = appointments(:confirmed_appointment)
        @notification = SMS::Notification::PatientBookingConfirmation.new(appointment_id: @appointment.id)
      end

      test "message should contain actual appointment message" do
        message = <<-EOS.strip_heredoc
        Your appointment with #{@appointment.dentist.name_with_salutation} is confirmed.

        #{@appointment.clinic_assignment.clinic.name}
        #{@appointment.service.name}
        #{@appointment.formatted_schedule}
        EOS
        assert_equal @notification.message, message
      end

      test "recipient should be the mobile number of appointment user" do
        recipient = @appointment.patient.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "client should be checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end
    end
  end
end
