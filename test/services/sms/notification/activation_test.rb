require "test_helper"

module SMS
  module Notification
    class ActivationTest < ActiveSupport::TestCase
      setup do
        @activation = activations(:patient)
        @notification = SMS::Notification::Activation.new(activation_id: @activation.id)
      end

      test "message should contain actual activation message" do
        message = "Your Dentasked Activation Code: #{@activation.code}"
        assert_equal @notification.message, message
      end

      test "recipient should be the mobile number of activation user" do
        recipient = @activation.user.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "recipient can be modified by passing custom mobile number" do
        mobile_number = "09201234567"
        notification = SMS::Notification::Activation.new(activation_id: @activation.id, mobile_number: mobile_number)
        assert_equal notification.recipient, mobile_number
      end

      test "client should default to checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end

      test "valid? should return true for valid activation" do
        assert @notification.valid?
      end

      test "valid? should return false for invalid activation" do
        notification = SMS::Notification::Activation.new(activation_id: nil, mobile_number: "")
        refute notification.valid?
      end
    end
  end
end
