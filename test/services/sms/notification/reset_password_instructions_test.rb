require "test_helper"

module SMS
  module Notification
    class ResetPasswordInstructionsTest < ActionDispatch::IntegrationTest
      setup do
        @user = users(:patient)
        @token = 'token'
        @notification = SMS::Notification::ResetPasswordInstructions.new(user: @user, token: @token)
      end
      test "recipient should be the mobile number of user" do
        assert_equal @notification.recipient, @user.mobile_number
      end

      test "client should default to checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end

      test "message should contain reset password message" do
        short_url = 'https://bit.ly/2ez4rtz'
        notification = SMS::Notification::ResetPasswordInstructions.new(user: @user, token: @token)
        notification.stub(:reset_password_url, short_url) do
          message = <<-EOS.strip_heredoc
          Someone has requested a link to change your password. You can do this through the link below. If you didn't request this, please ignore this message. Your password won't change until you access the link below and create a new one. Don't forget to share the app to your friends! If you have comments on how we can improve the service, feel free to email us at dentaskedph@gmail.com

          Change my password: #{short_url}
        EOS
        assert_equal notification.message, message
        end
      end
    end
  end
end
