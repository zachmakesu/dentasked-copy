require "test_helper"
module SMS
  module Notification
    class PatientBookingRequestTest < ActiveSupport::TestCase
      setup do
        @appointment = appointments(:pending_appointment)
        @notification = SMS::Notification::PatientBookingRequest.new(appointment_id: @appointment.id)
        @walkin_notification = SMS::Notification::PatientBookingRequest.new(appointment_id: @appointment.id, walkin: true)
      end

      test "message should contain actual appointment message" do
        message = <<-EOS.strip_heredoc
          Hi #{@appointment.patient.first_name}, Thanks for booking through Dentasked. Here are your appointment request details:

          #{@appointment.clinic_assignment.clinic.name}
          #{@appointment.service.name}
          #{@appointment.dentist.name_with_salutation}
          #{@appointment.formatted_schedule}

          We'll notify you within 48 hours before the dentist can confirm the schedule.
        EOS
        assert_equal @notification.message, message
      end

      test "message should contain actual walkin appointment message" do
        download_link= ENV.fetch("APP_DOWNLOAD_URL")
        message = <<-EOS.strip_heredoc
          Hi #{@appointment.patient.first_name}, Here are your appointment request details:

          #{@appointment.clinic_assignment.clinic.name}
          #{@appointment.service.name}
          #{@appointment.dentist.name_with_salutation}
          #{@appointment.formatted_schedule}

          Scheduling and keeping track of your dentist appointments just got easier. Just download the Dentasked app here #{download_link}
        EOS
        assert_equal @walkin_notification.message, message
      end

      test "recipient should be the mobile number of appointment user" do
        recipient = @appointment.patient.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "client should be checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end
    end
  end
end
