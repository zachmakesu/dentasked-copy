require "test_helper"
module SMS
  module Notification
    class PartnerBookingRequestTest < ActiveSupport::TestCase
      setup do
        @appointment = appointments(:pending_appointment)
        @notification = SMS::Notification::PartnerBookingRequest.new(appointment_id: @appointment.id)
        mobile_number = "09191112222"
        @secretary = @appointment.clinic_assignment.dentist.secretaries.first
        @secretary.update(mobile_number: mobile_number)
      end

      test "message should contain actual notification message" do
        message = <<-EOS.strip_heredoc
          Hello #{@secretary.first_name}, You received an appointment request from #{@appointment.patient.name}

          Log in to your Dentasked Partner's App Account to view the booking details and confirm it.
        EOS
        assert_equal @notification.message, message
      end

      test "recipient should be the mobile number of secretary" do
        recipient = @secretary.mobile_number
        assert_equal @notification.recipient, recipient
      end

      test "client should be checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end
    end
  end
end
