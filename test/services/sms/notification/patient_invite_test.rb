require "test_helper"

module SMS
  module Notification
    class PatientInviteTest < ActionDispatch::IntegrationTest
      setup do
        @secretary = secretarify(users(:independent))
        @name = "Lauren"
        @number = "09191234567"
        @download_url = ENV.fetch("APP_DOWNLOAD_URL")
        @notification = SMS::Notification::PatientInvite.new(name: @name, number: @number, inviter: @secretary)
      end

      test "message should contain invitation message" do
        message = "Hi #{@name}, You have been invited by #{@secretary.name} to book your next dental appointment using the DentaSked App. Download DentaSked here. #{@download_url}"
        assert_equal @notification.message, message
      end

      test "message should contain invitation message from clinic" do
        secretary = secretarify(users(:secretary))
        notification = SMS::Notification::PatientInvite.new(name: @name, number: @number, inviter: secretary)
        clinic = clinics(:prestige)
        message = "#{clinic.name} invites you to book your next appointment using the Dentasked app. Download Dentasked here #{@download_url}"
        assert_equal notification.message, message
      end

      test "recipient should be the mobile number used to initialize notification" do
        assert_equal @notification.recipient, @number
      end

      test "client should default to checkmobi" do
        assert_equal @notification.client, SMS::CheckmobiClient
      end

      test "notification with invalid mobile number" do
        notification = SMS::Notification::PatientInvite.new(name: "Lauren", number: "1234567", inviter: @secretary)
        refute notification.valid?, "should not be valid?"
      end
    end
  end
end
