require 'test_helper'

module Actions
  class ValidatesAuthorizationHeaderTest < ActiveSupport::TestCase
    setup do
      @user = users(:patient)
    end
    test "presence validation for authorization_header" do
      authorization_header = ""
      data = { authorization_header: authorization_header }
      context = ::LightService::Context.make(data)
      result = ValidatesAuthorizationHeader.execute(context)
      assert result.failure?, "should fail if authorization_header header is missing"
      assert_equal result.message, "Missing authorization header"
    end

    test "valid format for authorization_header" do
      authorization_header = "Dentasked UID:Signature"
      data = { authorization_header: authorization_header }
      context = ::LightService::Context.make(data)
      result = ValidatesAuthorizationHeader.execute(context)
      assert result.success?, "should succeed"
      assert_equal result.uid, "UID"
      assert_equal result.signature, "Signature"
    end

    test "invalid format for authorization header" do
      authorization_header = "Invalid AuthorizationHeader-Format"
      data = { authorization_header: authorization_header }
      context = ::LightService::Context.make(data)
      result = ValidatesAuthorizationHeader.execute(context)
      assert result.failure?, "should fail"
      assert_equal result.message, "Invalid authorization header"
    end
  end
end
