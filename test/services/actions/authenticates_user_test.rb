require 'test_helper'

module Actions
  class AuthenticatesUserTest < ActiveSupport::TestCase
    setup do
      @patient = users(:patient)
      @email = @patient.email
      @password = default_api_password
      @valid_data = {
        user:     @patient,
        password: @password,
        role:     'Patient'
      }
      @invalid_data = {
        user:     @patient,
        password: 'InvalidPassword',
        role:     'Patient'
      }
    end
    test "valid credentials" do
      @patient.activations.last.activate!
      context = ::LightService::Context.make(@valid_data)
      result = AuthenticatesUser.execute(context)
      assert result.success?, 'should be successful'
    end
    test "valid credentials but disabled" do
      @patient.disable!
      context = ::LightService::Context.make(@valid_data)
      result = AuthenticatesUser.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, ::Errors::DisabledAccount.message
      assert_equal result.error_code, ::Errors::DisabledAccount.code
    end
    test "valid credentials but inactive" do
      @patient.activations.destroy_all
      context = ::LightService::Context.make(@valid_data)
      result = AuthenticatesUser.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, ::Errors::InactiveAccount.message
      assert_equal result.error_code, ::Errors::InactiveAccount.code
    end
    test "invalid credentials" do
      context = ::LightService::Context.make(@invalid_data)
      result = AuthenticatesUser.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, ::Errors::InvalidPassword.message
      assert_equal result.error_code, ::Errors::InvalidPassword.code
    end
    test "valid credentials but invalid role" do
      @valid_data[:role] = 'Secretary'
      @patient.activations.last.activate!
      context = ::LightService::Context.make(@valid_data)
      result = AuthenticatesUser.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, ::Errors::InvalidPassword.message
      assert_equal result.error_code, ::Errors::InvalidPassword.code
    end
  end
end
