require 'test_helper'

module Actions
  class AssignsFbUserRoleTest < ActiveSupport::TestCase
    setup do
      @role = "patient"
      @user = users(:no_mobile_patient)
    end

    test "new role for user" do
      data = {
        role: @role,
        user: @user
      }
      context = ::LightService::Context.make(data)
      assert_difference 'UserRole.count', 1 do
        result = AssignsFbUserRole.execute(context)
        assert result.success?, 'should be successful'
        assert result.user.patient?, 'should be a patient'
        refute result.user.dentist?, 'should not be another role'
      end
    end
  end
end

