require 'test_helper'

module Actions
  class ValidatesAccessTokenTest < ActiveSupport::TestCase
    setup do
      @uid = users(:patient).uid
      @token = "PatientAccessToken"
    end
    test "valid access token for user" do
      data = {
        uid: @uid,
        token: @token
      }
      context = ::LightService::Context.make(data)
      result = ValidatesAccessToken.execute(context)
      assert result.success?, 'should be succesful'
    end

    test "invalid user for access token" do
      data = {
        uid: "InvalidUID",
        token: @token
      }
      context = ::LightService::Context.make(data)
      result = ValidatesAccessToken.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, 'Access Token is invalid'
    end

    test "invalid access token for user" do
      token = "InvalidToken"
      data = {
        uid: @uid,
        token: token
      }
      context = ::LightService::Context.make(data)
      result = ValidatesAccessToken.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, 'Access Token is invalid'
    end
    
    test "expired access token for user" do
      api_keys(:patient_api_key).update(expires_at: 1.week.ago)
      data = {
        uid: @uid,
        token: @token
      }
      context = ::LightService::Context.make(data)
      result = ValidatesAccessToken.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, 'Access Token is expired'
    end
  end
end
