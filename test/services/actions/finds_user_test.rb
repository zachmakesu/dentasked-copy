require 'test_helper'

module Actions
  class FindsUserTest < ActiveSupport::TestCase
    setup do
      @email = users(:patient).email
    end

    test 'existing email' do
      data = {
        email: @email
      }
      context = ::LightService::Context.make(data)
      result = FindsUser.execute(context)
      assert result.success?, 'should be successful'
    end

    test 'nonexisting email' do
      data = {
        email: 'nonexistinguser@gmail.com'
      }
      context = ::LightService::Context.make(data)
      result = FindsUser.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, ::Errors::InvalidEmail.message
      assert_equal result.error_code, ::Errors::InvalidEmail.code
    end
  end
end
