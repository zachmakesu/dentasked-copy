require 'test_helper'

module Actions
  class FindsOrCreatesFbUserTest < ActiveSupport::TestCase
    setup do
      @email = "rommel@gorated.com"
      @uid = "451596928374951"
      @first_name = "Boy"
      @last_name =  "Bagsiks"
    end

    test "new user from unique email" do
      data = {
        email: @email,
        uid: @uid,
        first_name: @first_name,
        last_name: @last_name
      }
      context = ::LightService::Context.make(data)
      assert_difference 'User.count', 1 do
        VCR.use_cassette 'facebook/avatar' do
          result = FindsOrCreatesFbUser.execute(context)
          assert result.success?, 'should be successful'
        end
      end
    end

    test "find user from existing uid" do
      data = {
        email: @email,
        uid: identities(:patient_identity).uid,
        first_name: @first_name,
        last_name: @last_name
      }
      context = ::LightService::Context.make(data)
      assert_difference 'User.count', 0 do
        VCR.use_cassette 'facebook/avatar/existing_uid' do
          result = FindsOrCreatesFbUser.execute(context)
          assert result.success?, 'should be successful'
          assert_equal result.user, users(:patient)
        end
      end
    end

    test "temporary facebook email gets updated" do
      email = 'kevincc@gorated.ph'
      data = {
        email: email,
        uid: identities(:temporary_patient_identity).uid,
        first_name: @first_name,
        last_name: @last_name
      }
      context = ::LightService::Context.make(data)
      assert_difference 'User.count', 0 do
        VCR.use_cassette 'facebook/avatar/temporary_email' do
          result = FindsOrCreatesFbUser.execute(context)
          assert result.success?, 'should be successful'
          assert_equal result.user.reload.email, email
        end
      end
    end
  end
end
