require 'test_helper'

module Actions
  class ValidatesFbTokenTest < ActiveSupport::TestCase
    setup do
      @token = File.read("#{Rails.root}/test/support/config/valid_fb_access_token").chomp
    end

    test "valid token passed" do
      data = {
        token: @token
      }
      context = ::LightService::Context.make(data)
      VCR.use_cassette "facebook/valid_token" do
        result = ValidatesFbToken.execute(context)
        assert result.success?, 'should be successful'
        assert_equal result.email, "rommel@gorated.com"
        assert_equal result.uid, "451596928374951"
        assert_equal result.first_name, "Boy"
        assert_equal result.last_name, "Bagsiks"
      end
    end

    test "invalid token passed" do
      data = {
        token: "invalid"
      }
      context = ::LightService::Context.make(data)
      VCR.use_cassette "facebook/invalid_token" do
        result = ValidatesFbToken.execute(context)
        assert result.failure?, 'should fail'
      end
    end
  end
end
