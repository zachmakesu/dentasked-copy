require 'test_helper'

module Actions
  class ValidatesSignatureTest < ActiveSupport::TestCase
    setup do
      @params = {
        access_token: "PatientAccessToken"
      }
      @path = "/path"
      @signature = HMAC.signature_from(@path, @params)
    end

    test "valid signature passed" do
      data = {
        path: @path,
        params: @params,
        signature: @signature
      }
      context = ::LightService::Context.make(data)
      result =  ValidatesSignature.execute(context)
      assert result.success?, 'should be successful'
    end

    test "invalid signature passed" do
      data = {
        path: @path,
        params: @params,
        signature: "InvalidHMACSignature"
      }
      context = ::LightService::Context.make(data)
      result =  ValidatesSignature.execute(context)
      assert result.failure?, 'should fail'
      assert_equal result.message, 'Invalid signature'
    end
  end
end
