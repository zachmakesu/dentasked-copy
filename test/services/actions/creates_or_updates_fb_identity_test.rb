require 'test_helper'

module Actions
  class CreatesOrUpdatesFbIdentityTest < ActiveSupport::TestCase
    setup do
      @uid = "451596928374951"
      @token = File.read("#{Rails.root}/test/support/config/valid_fb_access_token").chomp
    end

    test "new identity for new user" do
      data = {
        uid: @uid,
        token: @token,
        user: users(:no_mobile_patient)
      }
      context = ::LightService::Context.make(data)
      assert_difference 'Identity.count', 1 do
        result = CreatesOrUpdatesFbIdentity.execute(context)
        assert result.success?, 'should be successful'
      end
    end

    test "update identity for existing user" do
      data = {
        uid: @uid,
        token: @token,
        user: users(:patient)
      }
      context = ::LightService::Context.make(data)
      assert_difference 'Identity.count', 0 do
        result = CreatesOrUpdatesFbIdentity.execute(context)
        assert result.success?, 'should be successful'
        assert_equal users(:patient).identities.facebook.oauth_token , @token
      end
    end
  end
end
