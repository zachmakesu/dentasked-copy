require 'test_helper'

module Actions
  class GeneratesAPIKeyTest < ActiveSupport::TestCase
    setup do
      @user = users(:patient)
    end

    test "creating api key for user" do
      data = {
        user: @user
      }
      assert_difference 'APIKey.count', 1 do
        context = ::LightService::Context.make(data)
        result = GeneratesAPIKey.execute(context)
        assert result.success?, 'should be successful'
      end
    end
  end
end
