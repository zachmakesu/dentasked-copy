require 'test_helper'

class AuthenticateFbUserTest < ActiveSupport::TestCase
  setup do
    @token = File.read("#{Rails.root}/test/support/config/valid_fb_access_token").chomp
  end

  test "valid token" do
    sign_in_count = 0
    VCR.use_cassette "facebook/authenticate_fb_user" do
      assert_difference ['User.count', 'Identity.count', 'UserRole.count', 'APIKey.count'], 1 do
        service = AuthenticateFbUser.call(
          token: @token
        )
        assert service.success?, "should be successful"
        assert_equal service.user.email, 'rommel@gorated.com'
        sign_in_count = service.user.sign_in_count
      end
    end
    VCR.use_cassette "facebook/authenticate_fb_user" do
      assert_difference ['User.count', 'Identity.count', 'UserRole.count'], 0 do
        service = AuthenticateFbUser.call(
          token: @token
        )
        assert_equal service.user.sign_in_count, sign_in_count + 1, 'should increase sign in count'
      end
    end
  end

  test "invalid token" do
    VCR.use_cassette "facebook/authenticate_invalid_fb_user" do
      assert_difference ['User.count', 'Identity.count', 'UserRole.count', 'APIKey.count'], 0 do
        service = AuthenticateFbUser.call(
          token: "invalidToken"
        )
        assert service.failure?, "should be successful"
        assert_equal service.message, "Facebook access token is invalid"
      end
    end
  end
end
