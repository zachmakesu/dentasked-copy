require 'test_helper'

class AuthenticateAPIUserTest < ActiveSupport::TestCase
  setup do
    @patient  = users(:patient)
    @email    = @patient.email
    @password = default_api_password
    @role     = 'Patient'
  end

  test "valid credentials" do
    @patient.activations.last.activate!
    sign_in_count = 0
    assert_difference ['APIKey.count'], 1 do
      service = AuthenticateAPIUser.call(
        email:    @email,
        password: @password,
        role:     @role
      )
      assert service.success?, 'should be successful'
      assert_equal service.user.sign_in_count, sign_in_count + 1, 'should increase sign in count'
    end
  end
  test "invalid credentials" do
    @patient.activations.last.activate!
    sign_in_count = 0
    assert_difference ['APIKey.count'], 0 do
      service = AuthenticateAPIUser.call(
        email:    @email,
        password: 'InvalidPassword',
        role:     @role
      )
      assert service.failure?, 'should fail'
      refute_equal service.user.sign_in_count, sign_in_count + 1, 'should not increase sign in count'
      assert_equal service.message, ::Errors::InvalidPassword.message
      assert_equal service.error_code, ::Errors::InvalidPassword.code
    end
  end
end
