require "test_helper"

class InviteContactsTest < ActiveSupport::TestCase
  setup do
    @inviter = patientify(users(:patient))
    @names = %w(
      Lauren
      Reina
    )
    @numbers = %w(
      09191234567
      09209876543
    )
    @service = InviteContacts.new(names: @names, numbers: @numbers, inviter: @inviter)
  end
  test "passing equal number of names and numbers" do
    assert @service.valid?, "should be valid"
  end
  test "passing nonequal number of names and numbers" do
    service = InviteContacts.new(names: @names, numbers: [], inviter: @inviter)
    refute service.valid?, "should not be valid"
  end
  test "should have a response when called" do
    @service.()
    assert_equal @service.response, { message: "Successfully invited #{@names.count} #{"contact".pluralize(@names.count)}" }
  end
end
