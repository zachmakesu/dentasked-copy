require 'test_helper'

class AuthenticateRequestTest < ActiveSupport::TestCase
  setup do
    @user = users(:patient)
    @token = "PatientAccessToken"
    @params = {
      access_token: @token
    }
    @path = "/users"
    signature = HMAC.signature_from(@path, @params)
    authorization_header = "Dentasked #{@user.uid}:#{signature}"
    @service = AuthenticateRequest.call(
      authorization_header: authorization_header,
      path: @path,
      params: @params
    )
  end

  test "valid authorization header" do
    assert @service.success?, "should be successful"
  end

  test "empty authorization header" do
    service = AuthenticateRequest.call(authorization_header: "")
    assert service.failure?, "should fail"
    assert_equal service.message, "Missing authorization header"
  end

  test "invalid signature in authorization header" do
    authorization_header = "Dentasked #{@user.uid}:SIG"
    service = AuthenticateRequest.call(
      authorization_header: authorization_header,
      params: @params
    )
    assert service.failure?, "should fail"
    assert_equal service.message, "Invalid signature"
  end
end
