require 'test_helper'

class AppointmentTest < ActiveSupport::TestCase
  setup do
    @patient = patientify(users(:patient))
    @appointment = appointments(:confirmed_appointment)
  end
  test 'create appointment' do
    appointment = @patient.appointments.build
    appointment.service = services(:dental_cleaning)
    schedule = 24.hours.from_now
    appointment.schedule = schedule
    appointment.note = "This is le note"
    appointment.clinic_assignment = clinic_assignments(:assigned_dentist_prestige)
    assert appointment.save, 'should be valid'
    refute_nil appointment.status, 'should have assigned default status'
    assert_equal appointment.schedule, schedule, 'should have assigned schedule'
    assert_equal 1, appointment.histories.count, 'should create histories'
  end

  test '#reschedule(new_schedule)' do
    confirmed_appointment = appointments(:confirmed_appointment)
    new_schedule = "20/11/2015 01:22 PM"

    new_schedule_datetime = DateTime.strptime(new_schedule, '%d/%m/%Y %I:%M %p').change(offset: '+8')
    confirmed_appointment.reschedule new_schedule
    assert_equal confirmed_appointment.schedule, new_schedule_datetime, 'should change appointment schedule to new schedule'
    refute_empty confirmed_appointment.reschedules, 'should have reschedules'
  end

  test '#rescheduled?' do
    appointment = appointments(:rescheduled_appointment)
    assert appointment.rescheduled?, 'should return true for rescheduled appointments'
  end

  test "by earliest schedule appointment scope" do
    earliest_schedule = 1.day.from_now
    (3..7).each do |x|
      appointment = @appointment.dup
      appointment.schedule = x.days.from_now
      appointment.code = Appointment.generate_code
      appointment.save
      appointment.confirmed!
      # Reschedule an appointment to be the earliest
      if x == 5
        appointment.reschedule earliest_schedule
      end
    end
    appointments = @patient.appointments
    earliest_appointment = appointments.by_earliest.first
    assert_equal earliest_schedule.iso8601, earliest_appointment.schedule.iso8601, "should sort schedules accordingly even if rescheduled"
  end

  test "by latest schedule appointment scope" do
    latest_schedule = 100.days.from_now
    (1..5).each do |x|
      appointment = @appointment.dup
      appointment.schedule = x.days.from_now
      appointment.code = Appointment.generate_code
      appointment.save
      appointment.confirmed!
      # Reschedule an appointment to be the latest
      if x == 4
        appointment.reschedule latest_schedule
      end
    end
    appointments = @patient.appointments
    latest_appointment = appointments.by_latest.first
    assert_equal latest_schedule.iso8601, latest_appointment.schedule.iso8601, "should sort schedules accordingly even if rescheduled"
  end

  test "alternatives for appointment is not empty" do
    assert_not_empty @appointment.alternatives
  end

  test "alternatives don't include same appointment's dentist" do
    refute_includes @appointment.alternatives, @appointment.dentist
  end

  test "alternatives should be unique" do
    assert_equal @appointment.alternatives.count, @appointment.alternatives.uniq.count
  end

  test "bookings for previous dates" do
    appointment = Appointment.new
    appointment.schedule = DateTime.current - 2.days
    appointment.valid?
    assert_equal appointment.errors[:schedule].to_sentence, 'should not be in the past'
  end

  test "bookings for today" do
    appointment = Appointment.new
    appointment.schedule = DateTime.current.beginning_of_day + 3.hours
    appointment.valid?
    assert_equal appointment.errors[:schedule].to_sentence, 'should not be today'
  end

  test "offline bookings for today" do
    appointment = Appointment.new
    appointment.offline = true
    appointment.schedule = DateTime.current
    appointment.valid?
    assert appointment.errors[:schedule].empty?, 'should not have schedule error'
  end

  test "generate code without provided platform" do
    code = Appointment.generate_code
    assert code.include?('LEGACY')
  end

  test "generate code with provided platform" do
    code = Appointment.generate_code(platform: 'ios')
    assert code.include?('IOS')
  end

  test "appointment code should be unique" do
    appointment = Appointment.new
    appointment.code = appointments(:completed_appointment).code
    refute appointment.valid?
    assert_includes appointment.errors.full_messages, 'Code has already been taken'
  end

  # current available times suggestion starts with the schedule start time
  # and ends at 7pm of the same date
  test "appointment available times" do
    appointment = appointments(:cancelled_appointment)
    # 5 since 3-4, 4-5, 5-6, 6-7, 7-8
    assert_equal 5, appointment.available_times.count
    assert_equal "2017-10-23T15:00:00+08:00", appointment.available_times.first
    assert_equal "2017-10-23T19:00:00+08:00", appointment.available_times.last
  end
end
