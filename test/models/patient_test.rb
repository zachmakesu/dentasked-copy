require "test_helper"

class PatientTest < ActiveSupport::TestCase
  test "create valid patient" do
    user = Patient.new email: "new@user.com", password: default_api_password
    assert user.valid?, "valid with email and password"
    assert_difference "UserRole.count", 1 do
      assert user.save, "should create if valid"
      assert user.patient?, "should assign patient role"
      refute user.preference.nil?, "should have preference reference"
    end
  end

  test "create duplicate patient" do
    user = Patient.new email: users(:patient).email, password: default_api_password
    refute user.valid?, "invalid with same credentials as :dentist fixture"
  end

  test "create patient without email" do
    user = Patient.new email: ""
    refute user.valid?, "invalid without email"
  end

  test "create dentist with duplicate profile" do
    user = Patient.new email: "new@user.com", password: default_api_password
    assert user.valid?, "valid if duplicate profile_id but different profile_type"
  end

  test "Patient.search" do
    assert_not_empty Patient.search
    assert_not_empty Patient.search("Kevin")
    Patient.search.each do |patient|
      assert patient.patient?
    end
    assert_empty Patient.search("nonexisting user")
  end

  test "Patient#can_favorite(resource" do
    patient = patientify(users(:patient))
    nonfavorite_clinic = clinics(:adajar)

    assert patient.can_favorite?(nonfavorite_clinic)
  end

  test "references to favorite dentists" do
    patient = patientify(users(:no_mobile_patient))
    assert_difference "Favorite.count", 1 do
      patient.favorite_dentists << dentistify(users(:dentist))
    end
    refute_equal patient.reload.favorite_dentists.length, 0
    assert_instance_of Dentist, patient.reload.favorite_dentists.first
  end
end
