require 'test_helper'

class IdentityTest < ActiveSupport::TestCase

  setup do
    @identity = identities(:patient_identity)
  end

  test "should have valid provider, uid and oath_token" do
    assert_not_nil @identity.uid, 'should have uid'
    assert_not_nil @identity.oauth_token, 'should have oauth_token'
    assert_not_nil @identity.provider, 'should have provider'
    assert_equal 'facebook', @identity.provider, 'should have facebook as provider'
  end
end
