require "test_helper"

class FavoriteTest < ActiveSupport::TestCase
  setup do
    @patient = patientify(users(:patient))
  end
  test "should have expected validators" do
    favorite = Favorite.new
    refute favorite.valid?, "should not have valid favorite"
  end

  test "should save favorite clinic" do
    favorite = Favorite.new(patient: @patient,
                            favorited: clinics(:adajar))
    assert favorite.valid?, "should have valid favorite"
    assert favorite.save, "should have saved valid favorite"
  end

  test "should save favorite dentist" do
    favorite = Favorite.new(patient: @patient,
                            favorited: users(:assigned_dentist))
    assert favorite.valid?, "should have valid favorite"
    assert favorite.save, "should have saved valid favorite"
  end

  test "should not save without patient" do
    favorite = Favorite.new(favorited: clinics(:prestige))
    refute favorite.valid?, "should not have valid favorite without patient"
    refute favorite.save, "should not save without patient"
  end
end
