require 'test_helper'

class APIKeyTest < ActiveSupport::TestCase
  setup do
    @valid_token = 'ThisIsAValidAPITokenOfValid32<Len'
  end

  test 'create api key without access token' do
    key = APIKey.new
    key.access_token = ''
    refute key.valid?, 'invalid without access_token'
  end

  test 'create api key with valid length' do

  end

  test 'create valid api key' do
    key = APIKey.new
    key.access_token = @valid_token
    key.save
    assert_not_equal @valid_token, key.access_token, 'access_token accessor should be encrypted after setting'
    assert_not_nil key.encrypted_access_token, 'access token should be encrypted'
    assert_not_nil key.expires_at, 'access token should have expiration'
  end

  test 'compare encrypted_access_token and access_token param' do
    assert APIKey.respond_to?(:secure_compare), 'should respond to .secure_compare'
    key = APIKey.new
    key.access_token = @valid_token
    key.save
    assert APIKey.secure_compare(@valid_token, key.encrypted_access_token), 'should successfully compare plain test token with stored encrypted access token'
  end

  test 'scope by validity' do
    assert APIKey.respond_to?(:valid), 'should respond_to .valid scope'
    key = APIKey.new
    key.access_token = @valid_token
    key.save
    assert APIKey.valid.include?(key), 'should included recently created valid key'
    key.update_attribute :expires_at, 14.days.ago
    refute APIKey.valid.include?(key), 'should included recently created valid key'
  end

end
