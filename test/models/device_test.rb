require 'test_helper'

class DeviceTest < ActiveSupport::TestCase
  setup do
    @valid_device = Device.new
    @valid_device.token = 'token'
    @valid_device.name = 'new_device'
    @valid_device.platform = 'ios'
  end
  test 'create device with invalid platform' do
    @valid_device.platform = nil
    refute @valid_device.valid?, 'should have non-nil platform'
    @valid_device.platform = 'android'
    refute @valid_device.valid?, 'should have valid platform'
  end
  test 'create device without name' do
    @valid_device.name = nil
    refute @valid_device.valid?, 'should have a name identifier'
  end
  test 'create device without token' do
    @valid_device.token = nil
    refute @valid_device.valid?, 'should have a token'
    @valid_device.token = ''
    refute @valid_device.valid?, 'should have a non-blank token'
  end
  test 'create device with duplicate token' do
    @valid_device.token = devices(:iphone_5).token
    refute @valid_device.valid?, 'should have a unique token'
  end
  test 'create device without associated user' do
    @valid_device.user_id = nil
    refute @valid_device.valid?, 'should have associated user for device'
    @valid_device.owner = users(:patient)
    @valid_device.save
    assert_equal @valid_device.owner, users(:patient), 'should reference user owner'
  end

  test '#register_to(user)' do
    assert_difference 'Device.count', 0, 'cannot be registered' do
      @valid_device.token = devices(:iphone_5).token
      assert @valid_device.register_to(users(:dentist)), 'should always be able to register a device even if token is a duplicate'
      assert_empty Device.where(id: devices(:iphone_5).id), 'should be destroyed since duplicate device'
    end
  end
end
