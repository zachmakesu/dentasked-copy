require "test_helper"

class SpecialtyTest < ActiveSupport::TestCase
  test "create valid specialty" do
    specialty = Specialty.new
    specialty.name = "Sample"
    assert specialty.valid?, "should create valid specialty"
  end

  test "create specialty without name" do
    specialty = Specialty.new
    specialty.name = ""
    refute specialty.valid?, "should not create specialty without name"
  end

  test "create specialty with duplicate name" do
    specialty = Specialty.new
    specialty.name = specialties(:endodontics).name
    refute specialty.valid?, "should not create specialty with duplicate name"
  end
end
