require 'test_helper'

class PartnerSignupTest < ActiveSupport::TestCase
  setup do
    @admin = users(:admin)
    @partner_signup = partner_signups(:partner_signup)
    @dentist_is_secretary = partner_signups(:pending)
  end
  test "dentist reference if approved" do
    VCR.use_cassette "partners/draft" do
      PartnerSignupHandler.approve!(@partner_signup, @admin)
      dentist = Dentist.find_by(email: @partner_signup.dentist_object.fetch("email"))
      assert_equal @partner_signup.dentist, dentist
    end
  end
  test "dentist reference if pending" do
    VCR.use_cassette "partners/draft" do
      assert_nil @partner_signup.dentist
    end
  end
  test "secretary reference if approved" do
    VCR.use_cassette "partners/draft" do
      PartnerSignupHandler.approve!(@partner_signup, @admin)
      secretary = Secretary.find_by(email: @partner_signup.secretary_object.fetch("email"))
      assert_equal @partner_signup.secretary, secretary
    end
  end
  test "secretary reference if pending" do
    VCR.use_cassette "partners/draft" do
      assert_nil @partner_signup.secretary
    end
  end
  test "#dentist_is_secretary" do
    refute @partner_signup.dentist_is_secretary
    assert @dentist_is_secretary.dentist_is_secretary
  end
end
