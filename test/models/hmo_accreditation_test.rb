require 'test_helper'

class HMOAccreditationTest < ActiveSupport::TestCase
  setup do
    @dentist = users(:dentist)
    @clinic = clinics(:prestige)
    @hmo = hmos(:intellicare)
  end

  test 'should create new hmo and dentist association' do
    accreditation = HMOAccreditation.new(hmo: @hmo,
                                         accredited: @dentist,
                                         accredited_type: 'Dentist')
    assert accreditation.valid?, 'should have valid accreditation'
    assert accreditation.save, 'should save accrediation'
  end
end
