require 'test_helper'

class ServiceAssignmentTest < ActiveSupport::TestCase
  setup do
    @dentist = users(:dentist)
    @clinic = clinics(:prestige)
    @service = services(:dental_cleaning)
  end

  test 'should create new service and dentist association' do
    assignment = ServiceAssignment.new(service: @service,
                                         assigned: @dentist,
                                         assigned_type: 'User')
    assert assignment.valid?, 'should have valid assignment'
    assert assignment.save, 'should save accreditation'
  end
end
