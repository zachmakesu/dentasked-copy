require 'test_helper'

class SymptomTest < ActiveSupport::TestCase
  test 'create symptom without name' do
    symptom = Symptom.new
    symptom.name = ""
    refute symptom.valid?, 'should not create symptom without name'
  end
  
  test 'create symptom with duplicate name' do
    symptom = Symptom.new
    symptom.name = symptoms(:sensitive_teeth).name
    refute symptom.valid?, 'should not create symptom with duplicate name'
  end
end
