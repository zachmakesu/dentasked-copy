require 'test_helper'

class AssignmentScheduleTest < ActiveSupport::TestCase
  setup do
    @assignment = clinic_assignments(:assigned_dentist_prestige)
    @schedule = @assignment.schedules.build
    @schedule.day = 0
    @schedule.start_time = "01:00 PM".to_time.utc
    @schedule.end_time = "05:00 PM".to_time.utc
  end
  test 'create assignment with invalid day' do
    @schedule.day = 7
    refute @schedule.valid?, 'should not be greater than 6'
    @schedule.day = -1
    refute @schedule.valid?, 'should not be less than 0'
    @schedule.day = 3
    assert @schedule.valid?, 'should be between 0-6'
  end

  test 'create assignment with start_time after end_time' do
    @schedule.start_time = "06:00 PM".to_time.utc
    @schedule.end_time = "05:00 PM".to_time.utc
    refute @schedule.valid?, 'start_time should be before end_time'
  end

end
