require "test_helper"

class UserRoleTest < ActiveSupport::TestCase
  test "role can only be assigned once" do
    user_role = UserRole.new
    user_role.user  = users(:admin)
    user_role.role = roles(:admin)

    refute user_role.valid?, "should be invalid"
  end
end
