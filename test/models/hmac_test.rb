require 'test_helper'

class HMACTest < ActiveSupport::TestCase
  setup do
    @hmac = HMAC.new('endpoint', {'hello' => 'world'})
  end
  test 'create hmac object' do
    assert_not_nil @hmac.endpoint, 'should have .endpoint'
    assert_not_nil @hmac.params, 'should have .params'
  end

  test 'sign initialized hmac object' do
    assert_not_nil @hmac.digest
  end

  test 'generate signature from endpoint, params and secret' do
    assert_not_nil HMAC.signature_from('/test/endpoint', {})
  end

  test 'securely compare signatures' do
    sig = HMAC.signature_from('endpoint', { 'hello' => 'world' })
    assert_equal @hmac.digest, sig, '.digest should equal #signature_from'
    assert HMAC.secure_compare(@hmac.digest, sig), '#secure_compare should return secure comparison between signatures'
  end

  test 'compare signatures for different versions of api' do
    sig_v1 = HMAC.signature_from('endpoint', { 'hello' => 'world' })
    sig_v2 = HMAC.signature_from('endpoint', { 'hello' => 'world' }, 2)
    refute_equal sig_v1, sig_v2, 'signature should not be equal for different api versions'

  end
end
