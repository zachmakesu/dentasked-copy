require 'test_helper'

class PatientRecordTest < ActiveSupport::TestCase
  # attr-encrypted related tests
  test "#mobile_number_secret_key" do
    assert_equal PatientRecord.new.mobile_number_secret_key, ENV["MOBILE_NUMBER_SECRET_KEY"], "return MOBILE_NUMBER_SECRET_KEY"
  end

  test "#mobile_number returns plain text value" do
    number = "09191234567"
    patient_records(:patient_record).mobile_number = number
    patient_records(:patient_record).save
    assert_equal patient_records(:patient_record).mobile_number, "+639191234567"
    assert_not_empty patient_records(:patient_record).encrypted_mobile_number, "encrypted mobile number is stored"
  end

  test "#mobile_number should be unique" do
    number = "09191234567"
    patient_records(:patient_record).mobile_number = number
    patient_records(:patient_record).save

    new_patient = patient_records(:patient_record).dup
    new_patient.email = "new@email.com"
    new_patient.mobile_number = number
    refute new_patient.valid?, "invalid due to duplicate number"
  end
end
