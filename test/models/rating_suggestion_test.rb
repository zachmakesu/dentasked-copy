require 'test_helper'

class RatingSuggestionTest < ActiveSupport::TestCase
  setup do
    @suggestion = rating_suggestions(:awesome)
    @admin = users(:admin)
  end

  test "rating suggestion uniqueness" do
    suggestion = RatingSuggestion.new
    suggestion.user = @admin
    suggestion.name = @suggestion.name
    refute suggestion.valid?, "should be validated"
  end

  test "rating suggestion user reference" do
    suggestion = RatingSuggestion.new
    suggestion.user = users(:patient)
    suggestion.name = "Nice la"
    refute suggestion.valid?, "should be an admin"
  end

  test "rating suggestion admin reference" do
    suggestion = RatingSuggestion.new
    suggestion.user = @admin
    suggestion.name = "Nice la"
    suggestion.save
    assert_includes @admin.rating_suggestions, suggestion, "should reference suggestion"
  end

  test "enable a rating suggestion" do
    disabled_suggestion = rating_suggestions(:disabled)
    disabled_suggestion.enable
    assert disabled_suggestion.enabled?, "should update enable property"
  end

  test "disable a rating suggestion" do
    enabled_suggestion = rating_suggestions(:enabled)
    enabled_suggestion.disable
    refute enabled_suggestion.enabled?, "should update enable property"
  end
end
