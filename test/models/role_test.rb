require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  setup do
    @patient_role = roles(:patient)
  end

  test "case insensitive uniqueness of roles" do
    new_role = Role.new
    new_role.name = @patient_role.name.upcase
    refute new_role.valid?, "should have unique role name regardless of case"
  end

  test "downcased name is stored" do
    new_role = Role.new
    new_role.name = "UPPERCASE_ROLE"
    new_role.save
    assert_equal new_role.name, "uppercase_role", "should be downcased during validation"
  end

  test "presence validation for role name" do
    new_role = Role.new
    refute new_role.valid?, "should have a name"
  end

  test "uniqueness of roles" do
    new_role = Role.new
    new_role.name = @patient_role.name
    refute new_role.valid?, "should have unique role name"
  end

end
