require "test_helper"

class SpecializationTest < ActiveSupport::TestCase
  setup do
    @dentist = dentistify(users(:dentist))
    @specialty = specialties(:endodontics)
  end

  test "should create new specialty and dentist association" do
    specialization = Specialization.new(dentist: @dentist,
                                        specialty: @specialty)
    assert specialization.valid?, "should have valid specialization"
    assert specialization.save, "should save specialization"
  end

  test "should not reassign same specialization to same doctor" do
    dentist = dentistify(users(:assigned_dentist))
    specialty = specialties(:periodontics)
    specialization = Specialization.new(dentist: dentist,
                                        specialty: specialty)
    assert_not specialization.valid?, "should have not valid specialization"
    assert_not specialization.save, "should not save specialization"
  end
end
