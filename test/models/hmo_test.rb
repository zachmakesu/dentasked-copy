require 'test_helper'

class HMOTest < ActiveSupport::TestCase

  test 'should have expected validators' do
    hmo = HMO.new
    assert_not hmo.valid? 'should not have valid HMO'
    assert_equal [:name], hmo.errors.keys, 'should have expected error keys'
  end

  test 'should not save without name' do
    hmo = HMO.new(description: 'this is a description')
    assert_not hmo.valid? 'should not have valid HMO without name'
    assert_not hmo.save, 'should not save wihout name'
  end

  test 'should save hmo' do
    hmo = HMO.new(name: 'LegitCare', description: 'Legit care here!')
    assert hmo.valid?, 'should have valid booking'
    assert hmo.save, 'should have saved valid booking'
  end

end
