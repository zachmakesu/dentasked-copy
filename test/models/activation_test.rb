require 'test_helper'

class ActivationTest < ActiveSupport::TestCase
  test 'autogeneration of activation code' do
    activation = users(:patient).activations.create
    assert activation.valid?, 'should result in a valid record'
    assert_not_nil activation.code, 'should have a generated code'
  end

  test 'default expiration on create' do
    activation = users(:patient).activations.create
    assert activation.expires_at > activation.created_at, 'should be greater than date of creation'
  end

  test 'duplicate activation code value' do
    activation = users(:patient).activations.new
    activation.code = activations(:patient).code
    activation.save
    refute activation.valid?, 'should be invalid'
  end

  test 'activation code should be numeric' do
    activation = users(:patient).activations.create
    activation.code = "xxxxxx"
    refute activation.valid?
    assert_not_empty activation.errors[:code]
  end

  test 'legacy activation code can still be activated' do
    activation = activations(:legacy)
    assert activation.valid?
    assert activation.activate!
    assert activation.activated?
    assert_not_nil activation.activated_at
  end

  test '#expired?' do
    activation = users(:patient).activations.create
    refute activation.expired?, "should return false since it's not expired"
  end

  test '#activated?' do
    activation = users(:patient).activations.create
    refute activation.activated?, "should return false since it's not yet activated"
  end

  test '#activate!' do
    activation = users(:patient).activations.create
    activation.activate!
    assert_not_nil activation.activated_at, "should assign activated_at value"
  end
end
