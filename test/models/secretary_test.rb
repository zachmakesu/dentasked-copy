require "test_helper"

class SecretaryTest < ActiveSupport::TestCase
  setup do
    @dentist = dentistify(users(:dentist))
    @secretary = secretarify(users(:secretary))
  end
  test "create valid secretary" do
    user = Secretary.new email: "new@secretary.com", password: "itshappening"
    assert user.valid?, "valid with email and password"
    assert_difference "UserRole.count", 1 do
      assert user.save, "should create if valid"
      assert user.secretary?, "should assign secretary role"
    end
  end

  test "create duplicate secretary" do
    user = Secretary.new email: users(:secretary).email, password: "itshappening"
    refute user.valid?, "invalid with same credentials as :secretary fixture"
  end

  test "create secretary without email" do
    user = Secretary.new email: ""
    refute user.valid?, "invalid without email"
  end

  test "#add_dentist(dentist)" do
    assert @secretary.add_dentist(@dentist), "should successfully add dentist"
    assert_includes @secretary.dentists, @dentist, "should include added dentist"
    refute @secretary.add_dentist(@dentist), "should not allow adding same dentist"
  end
end
