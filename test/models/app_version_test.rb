require 'test_helper'

class AppVersionTest < ActiveSupport::TestCase
  test "get latest android patient" do
    latest_app = app_versions(:android_patient)
    assert_equal latest_app, AppVersion.latest(platform: "android", category: "patient")
  end
end
