require "test_helper"

class RatingTest < ActiveSupport::TestCase
  setup do
    @rating = Rating.new
    @rating.rating = nil
    @rating.rater = users(:patient)
    @rating.ratee = users(:dentist)
    @rating.context = appointments(:confirmed_appointment)
  end

  test "create rating without rating" do
    refute @rating.valid?, "should be invalid without actual rating"
  end

  test "rating which is negative value" do
    @rating.rating = -1
    refute @rating.valid?, "should be invalid if rating is negative"
  end

  test "rating greater than 5" do
    @rating.rating = 6
    refute @rating.valid?, "should be invalid"
  end

  test "rating greater than 0 or and less than or equal to 5" do
    @rating.rating = 3
    assert @rating.valid?, "should be valid"
  end

  test "rating associations between patient and dentist" do
    @rating.rating = 3
    assert @rating.save, "should save if valid"
    assert_includes users(:patient).sent_ratings, @rating, "patient should associate sent rating"
    assert_includes users(:dentist).received_ratings, @rating, "dentist should associate received rating"
    assert_includes users(:patient).ratees, users(:dentist), "patient should include rated dentist to #ratees"
    assert_includes users(:dentist).raters, users(:patient), "dentist should include patient who rated them to #raters"
  end

  test "rating association with context" do
    @rating.rating = 3
    assert @rating.save, "should save if valid"
    assert_includes appointments(:confirmed_appointment).ratings, @rating, "appointment context should recognize ratings"
  end
end
