require 'test_helper'

class ServiceTest < ActiveSupport::TestCase
  test 'create service with duplicate name' do
    service = Service.new
    service.name = services(:dental_cleaning).name
    refute service.valid?, 'invalid if duplicate name'
  end

  test 'create service without name' do
    service = Service.new
    service.name = ""
    service.duration = 60.minutes
    refute service.valid?, 'should not create service without name'
  end

  test 'create service without duration' do
    service = Service.new
    service.name = "General Checkup"
    service.duration = nil
    refute service.valid?, 'should not create service without duration'
  end

  test 'alphabetical scope' do
    services = Service.alphabetical
    assert_equal services.first.name, 'Check up'
    assert_equal services.last.name, 'Whitening'
  end
end
