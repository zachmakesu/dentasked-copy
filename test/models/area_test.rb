require 'test_helper'

class AreaTest < ActiveSupport::TestCase
  test 'create invalid area' do
    area = Area.new
    area.name = ""
    refute area.valid?, 'should be invalid without name'
    area.name = areas(:pasig).name
    refute area.valid?, 'should be unique'
  end

  test 'area should be geocoded after create' do
    VCR.use_cassette 'areas/pasig' do
      area = Area.new
      area.name = "Sample Area"
      area.base_address = "Manggahan, Pasig City"
      area.save
      assert_not_nil area.lat, 'should have geocoded lat'
      assert_not_nil area.lng, 'should have geocoded lng'
    end
  end

  test 'Area.search' do
    assert_not_empty Area.search
    assert_not_empty Area.search('Pasig')
    assert_empty Area.search('mars')
  end

end
