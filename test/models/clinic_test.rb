require "test_helper"

class ClinicTest < ActiveSupport::TestCase
  test "create clinic without name" do
    VCR.use_cassette "clinics/gorated" do
      clinic = Clinic.new
      clinic.name = ""
      clinic.address = "Unit 20, iPark Center, 401 Amang Rodriguez Avenue, Manggahan, Pasig City"
      refute clinic.valid?, "should be invalid without name"
    end
  end

  test "create clinic without address" do
    clinic = Clinic.new
    clinic.name = "Sample"
    clinic.address = ""
    refute clinic.valid?, "should be invalid without address"
  end

  test "create clinic with duplicate name" do
    VCR.use_cassette "clinics/prestige" do
      clinic = Clinic.new clinics(:prestige).attributes
      refute clinic.valid?, "should be invalid with duplicate name"
    end
  end

  test "create clinic without city" do
    clinic = clinics(:prestige)
    clinic.city_id = 0
    refute clinic.valid?, "should be invalid without city"
  end

  test "un/assign dentist to clinic" do
    clinic = clinics(:prestige)
    dentist = dentistify(users(:dentist))
    clinic.assign dentist
    assert clinic.dentists.include?(dentist), "should add dentist to clinic\"s list of dentists"
    clinic.unassign dentist
    refute clinic.dentists.include?(dentist), "should remove dentist to clinic\"s list of dentists"
  end

  test "assign dentist to same clinic" do
    dentist = users(:assigned_dentist)
    clinic = clinics(:prestige)
    refute clinic.assign(dentist).persisted?, "should not be able to assign dentist to same clinic"
  end

  test "unassign dentist from clinic he\"s not assigned to" do
    dentist = users(:assigned_dentist)
    clinic = clinics(:adajar)
    refute clinic.unassign(dentist), "should not be able to unassign dentist to a clinic he\"s not assigned to"
  end

  test "clinic should be geocoded after create" do
    VCR.use_cassette "clinics/gorated" do
      clinic = Clinic.new
      clinic.name = "Sample Clinic"
      clinic.address = "Unit 20, iPark Center, 401 Amang Rodriguez Avenue, Manggahan, Pasig City"
      clinic.save
      assert_not_nil clinic.lat, "should have geocoded lat"
      assert_not_nil clinic.lng, "should have geocoded lng"
    end
  end
end
