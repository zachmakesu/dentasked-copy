require "test_helper"

class DentistProfileTest < ActiveSupport::TestCase
  test "should have auto-generated activation code" do
    profile = DentistProfile.new
    assert profile.valid?, "valid with auto-generated activation code"
    assert_not_nil profile.activation_code, "has auto-generated activation_code"
  end

  test "should have unique activation code" do
    profile = DentistProfile.new
    profile.activation_code = dentist_profiles(:dentist_profile).activation_code
    refute profile.valid?, "invalid without unique activation code"
  end

  test 'activation activation_code should be numeric' do
    profile = DentistProfile.new
    profile.activation_code = "xxxxxx"
    refute profile.valid?
    assert_not_empty profile.errors[:activation_code]
  end

  # attr-encrypted related tests
  test "#license_number_secret_key" do
    assert_equal DentistProfile.new.license_number_secret_key, ENV["LICENSE_NUMBER_SECRET_KEY"], "return LICENSE_NUMBER_SECRET_KEY"
  end

  test "#license_number returns plain text value" do
    number = "007"
    dentist_profiles(:dentist_profile).license_number = number
    dentist_profiles(:dentist_profile).save
    assert_equal dentist_profiles(:dentist_profile).license_number, number
    assert_not_empty dentist_profiles(:dentist_profile).encrypted_license_number, "encrypted license number is stored"
  end
end
