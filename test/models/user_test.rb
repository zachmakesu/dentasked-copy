require "test_helper"

class UserTest < ActiveSupport::TestCase
  setup do
    @admin = adminify(users(:admin))
    @dentist = dentistify(users(:dentist))
    @patient = patientify(users(:patient))
    @secretary = secretarify(users(:secretary))
  end

  test "created user should have a profile" do
    refute @patient.profile.nil?, "should have associated profile"
  end

  test "User#patient?" do
    assert @patient.patient?, "@patient should be a patient?"
  end

  test "User#dentist?" do
    assert @dentist.dentist?, "@dentist should be a dentist?"
  end

  test "User#admin?" do
    assert @admin.admin?, "@admin should be an admin?"
    refute @patient.admin?, "@patient should not be an admin?"
  end

  test "User.search" do
    assert_not_empty User.search
    assert_not_empty User.search("Kevin")
    assert_empty User.search("nonexisting user")
  end

  test "User#report(user)" do
    assert_difference "Report.count", 1, "should be able to report other users" do
      @dentist.report!(@patient)
      assert_includes @dentist.reported_users, @patient, "should include recently reported patient to reported users"
    end
    assert_difference "Report.count", 0, "should not be able to report self" do
      @dentist.report!(@dentist)
    end
  end

  test "User#can_report?(user)" do
    assert @secretary.can_report?(@patient), "should be able to report new user"
    @secretary.report!(@patient)
    refute @secretary.can_report?(@patient), "should not be able to report same user multiple times for same day"
  end

  test "User report relationships" do
    assert_includes @patient.reported_users, @dentist, "should include reported dentist"
    assert_includes @dentist.reporters, @patient, "should include reporter patient"
  end

  test "User#valid_role?(role)" do
    assert @patient.valid_role?("Patient"), "should return role validity based on actual role"
    refute @patient.valid_role?("Secretary"), "should return role validity based on actual role"
  end

  test "User#disable!" do
    @patient.disable!
    assert @patient.disabled_at, "should correctly set disabled_at on disable call"
  end

  test "User#disabled?" do
    @patient.disable!
    assert @patient.disabled?, "should correctly return disable status"
  end

  test "User#enable!" do
    @patient.disable!
    @patient.enable!
    refute @patient.disabled_at, "should correctly set disabled_at on disable call"
  end

  test "User#enabled?" do
    @patient.enable!
    assert @patient.enabled?, "should correctly return enable status"
  end

  test "User#agree_to_terms" do
    @patient.agree_to_terms
    refute_nil @patient.terms_agreed_at, "should correctly set when user agreed to terms"
  end

  test "User#agreed_to_terms?" do
    @patient.agree_to_terms
    assert @patient.agreed_to_terms?, "should correctly return if user agreed to terms"
  end

  test "User#birthdate should accurately compute age" do
    almost_adult = @patient
    almost_adult.birthdate = (18.years.ago.utc + 1.day).to_s
    almost_adult.save
    assert_equal almost_adult.age_string, "17 years old", "should be 17 years old"
  end

  test "user birthdate should be parsed reliably" do
    patient = @patient.dup
    birthdate = "08/14/1992"
    patient.birthdate = birthdate
    patient.save
    assert_equal patient.formatted_birthdate, birthdate
  end

  # attr-encrypted related tests
  test "User#birthdate_secret_key" do
    assert_equal User.new.birthdate_secret_key, ENV["BIRTHDATE_SECRET_KEY"], "return BIRTHDATE_SECRET_KEY"
  end

  test "User#birthdate returns plain text value" do
    birthdate = "2017-02-24"
    @patient.birthdate = birthdate
    @patient.save
    assert_equal @patient.birthdate, birthdate
    assert_not_empty @patient.encrypted_birthdate, "encrypted birthdate is stored"
  end

  test "User#mobile_number_secret_key" do
    assert_equal User.new.mobile_number_secret_key, ENV["MOBILE_NUMBER_SECRET_KEY"], "return MOBILE_NUMBER_SECRET_KEY"
  end

  test "User#mobile_number returns plain text value" do
    number = "+639191234567"
    @patient.mobile_number = number
    @patient.save
    assert_equal @patient.mobile_number, number
    assert_not_empty @patient.encrypted_mobile_number, "encrypted mobile number is stored"
  end

  test "User#mobile_number uniqueness" do
    mobile_number = "09191234567"
    users(:patient).update(mobile_number: mobile_number)
    new_patient = Patient.new(email: 'testuser@gmail.com', password: '1234please')
    new_patient.mobile_number = mobile_number
    refute new_patient.valid?, "should have unique mobile number"
  end

  test "User#mobile_number PH number format" do
    mobile_number = "091912345679"
    patient = users(:patient)
    refute patient.update(mobile_number: mobile_number), 'should be a valid PH mobile number'
  end

  # Password requirement validations
  test "password complexity validation for weak passwords" do
    patient = @patient
    weak_passwords = %w(
    a
    blacksheepwall
    GREEDISGOOD
    {}[],.<>;:‘“?/|\`~!@#$%^&*()_-+=
    12345678
    password
      letmein
    )
    weak_passwords.each do |password|
      patient.password = password
      refute patient.valid?, "should not accept #{password} as password"
    end
  end

  test "password complexity validation for 'strong' passwords" do
    patient = @patient
    passwords = %w(
      PaSsWoRd
      jelly22fi$h
      $tar|warz
      m@nn3qu1nZ
      3Lp$y22r00
      iZd3@dp30pL3
      {k}[],.<>;:‘“?/|\`~!@#$%^&*()_-+=
    )
    passwords.each do |password|
      patient.password = password
      assert patient.valid?, "should accept #{password} as password"
      patient.save
      assert patient.valid_password?(password), "should correctly compare updated password"
    end
  end

  # No idea how to test this in isolation
  # So we're testing this inside the User model
  # Roleable Concern tests
  test "admins scope" do
    assert_includes User.admins, users(:admin), "should include admin user"
    refute_includes User.admins, users(:patient), "should not include patient user"
  end

  test "patients scope" do
    assert_includes User.patients, users(:patient), "should include patient user"
    refute_includes User.patients, users(:admin), "should not include admin user"
  end

  test "dentists scope" do
    assert_includes User.dentists, users(:dentist), "should include dentist user"
    refute_includes User.dentists, users(:patient), "should not include patient user"
  end

  test "secretaries scope" do
    assert_includes User.secretaries, users(:secretary), "should include secretary user"
    refute_includes User.secretaries, users(:patient), "should not include patient user"
  end

  test "granting patient role to a basic user" do
    basic_user = users(:basic)
    assert_difference "UserRole.count", 1 do
      basic_user.make_patient!
    end
    assert basic_user.patient?, "should be classified as patient"
  end

  test "granting patient role to a patient" do
    patient_user = @patient
    assert_difference "UserRole.count", 0 do
      patient_user.make_patient!
    end
  end

  test "revoking patient role from a patient" do
    patient_user = @patient
    assert_difference "UserRole.count", -1 do
      patient_user.revoke_patient!
    end
  end

  test "granting dentist role to a basic user" do
    basic_user = users(:basic)
    assert_difference "UserRole.count", 1 do
      basic_user.make_dentist!
    end
    assert basic_user.dentist?, "should be classified as dentist"
  end

  test "granting dentist role to a dentist" do
    dentist_user = @dentist
    assert_difference "UserRole.count", 0 do
      dentist_user.make_dentist!
    end
  end

  test "revoking dentist role from a dentist" do
    dentist_user = @dentist
    assert_difference "UserRole.count", -1 do
      dentist_user.revoke_dentist!
    end
  end

  test "granting secretary role to a basic user" do
    basic_user = users(:basic)
    assert_difference "UserRole.count", 1 do
      basic_user.make_secretary!
    end
    assert basic_user.secretary?, "should be classified as secretary"
  end

  test "granting secretary role to a secretary" do
    secretary_user = @secretary
    assert_difference "UserRole.count", 0 do
      secretary_user.make_secretary!
    end
  end

  test "revoking secretary role from a secretary" do
    secretary_user = @secretary
    assert_difference "UserRole.count", -1 do
      secretary_user.revoke_secretary!
    end
  end

  test "granting admin role to a basic user" do
    basic_user = users(:basic)
    assert_difference "UserRole.count", 1 do
      basic_user.make_admin!
    end
    assert basic_user.admin?, "should be classified as admin"
  end

  test "granting admin role to a admin" do
    admin_user = @admin
    assert_difference "UserRole.count", 0 do
      admin_user.make_admin!
    end
  end

  test "revoking admin role from a admin" do
    admin_user = @admin
    assert_difference "UserRole.count", -1 do
      admin_user.revoke_admin!
    end
  end

  test "basic? method for basic user" do
    assert users(:basic).basic?, 'should be a basic user if no roles'
  end

  test "User#linked_mobile_number? with linked mobile" do
    patient = users(:no_mobile_patient)
    patient.update_attribute(:mobile_number, "639191234567")
    patient.activations.last.activate!
    assert patient.linked_mobile_number?, "should return true if activated and has mobile number"
  end

  test "User#linked_mobile_number? with linked mobile but not activated" do
    patient = users(:no_mobile_patient)
    patient.update_attribute(:mobile_number, "639191234567")
    refute patient.linked_mobile_number?, "should return false if not  activated but has mobile number"
  end

  test "User#linked_mobile_number? without activation or mobile number" do
    patient = users(:no_mobile_patient)
    refute patient.linked_mobile_number?, "should return false if no activation and mobile number"
  end

  test "temporary facebook email should not send an email to" do
    user = users(:no_mobile_patient)
    user.email = '123@facebook.com'
    assert user.has_temporary_email?
  end

  test "temporary facebook email should send an email to" do
    user = users(:no_mobile_patient)
    user.email = 'user@dentasked.abc'
    refute user.has_temporary_email?
  end
end
