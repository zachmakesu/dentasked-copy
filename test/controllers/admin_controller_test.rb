require 'test_helper'

class AdminControllerTest < ActionController::TestCase

  test "should get index" do
    sign_in users(:admin)
    get :index
    assert_response :redirect
  end

end
