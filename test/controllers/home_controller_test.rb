require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  setup do
    @android_ua = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19"
    @ios_ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A5297c Safari/602.1"
  end
  test "get funfacts" do
    get :funfacts
    assert_response :success
  end

  test "get custom successful password change page" do
    get :successful_password_change
    assert_response :success
  end

  test "get old ios download url" do
    AppSetting.ios_link = nil
    @request.user_agent = @ios_ua
    get :ios
    assert_redirected_to ENV.fetch("IOS_PATIENT_DOWNLOAD_URL")
  end

  test "get download url with android device" do
    AppSetting.android_link = nil
    @request.user_agent = @android_ua
    get :download
    assert_redirected_to ENV.fetch("ANDROID_PATIENT_DOWNLOAD_URL")
  end

  test "get marketing download url with android device" do
    marketing_url = "https://google.com"
    AppSetting.android_link = marketing_url
    @request.user_agent = @android_ua
    get :download
    assert_redirected_to marketing_url
  end

  test "get download url with ios device" do
    AppSetting.ios_link = nil
    @request.user_agent = @ios_ua
    get :download
    assert_redirected_to ENV.fetch("IOS_PATIENT_DOWNLOAD_URL")
  end

  test "get marketing download url with ios device" do
    marketing_url = "https://itunes.com"
    AppSetting.ios_link = marketing_url
    @request.user_agent = @ios_ua
    get :download
    assert_redirected_to marketing_url
  end
end
