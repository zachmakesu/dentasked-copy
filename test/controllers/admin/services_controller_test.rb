require 'test_helper'

class Admin::ServicesControllerTest < ActionController::TestCase
  setup do
    @dental_service = services(:dental_cleaning)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:services)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dental_service" do
    assert_difference('Service.count') do
      post :create, service: { name: 'Test', duration: 60 }
    end

    assert_redirected_to admin_services_path
  end

  test "should show dental_service" do
    get :show, id: @dental_service
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dental_service
    assert_response :success
  end

  test "should update dental_service" do
    patch :update, id: @dental_service, service: { name: 'Test', duration: 70 }
    assert_redirected_to admin_services_path
  end

  test "should destroy dental_service" do
    assert_difference('Service.count', -1) do
      delete :destroy, id: @dental_service
    end

    assert_redirected_to admin_services_path
  end
end
