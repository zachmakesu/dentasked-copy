require "test_helper"

class Admin::DentistsControllerTest < ActionController::TestCase
  setup do
    @dentist = dentistify(users(:dentist))
    @assigned_dentist = dentistify(users(:assigned_dentist))
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dentists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dentist" do
    assert_difference("Dentist.count") do
      post :create, dentist: {
        email: "d.canlas07@gmail.com"
      }
      assert_redirected_to admin_dentists_path
      assert_equal "Dentist was successfully created.", flash[:notice]
    end
  end

  test "should create invalid dentist" do
    post :create, dentist: {
      email: ""
    }
    assert_template "new"
  end

  test "should show dentist" do
    get :show, id: @dentist
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dentist
    assert_response :success
  end

  test "should update dentist" do
    put :update, id: @dentist.id, dentist: {
      email: "d.canlas08@gmail.com",
      password: "Password1235",
      password_confirmation: "Password1235"
    }
    assert_not_nil assigns(:dentist)
    assert_equal "Dentist was successfully updated.", flash[:notice]
    assert_redirected_to admin_dentists_path
    assert_equal @dentist.profile, assigns(:dentist).profile
  end

  test "should update invalid dentist" do
    put :update, id: @dentist.id, dentist: {
      email: ""
    }
    assert_not_nil assigns(:dentist)
    assert_template "edit"
  end

  test "should destroy dentist" do
    assert_difference("Dentist.count", -1) do
      delete :destroy, id: @dentist
    end
    assert_equal "Dentist was successfully destroyed.", flash[:notice]
    assert_redirected_to admin_dentists_path
  end

  test "should assign dentist" do
    assert_difference("ClinicAssignment.count") do
      post :assignment, id: @dentist, clinic_assignment: { clinic_id: clinics(:prestige).id }
    end
  end

  test "should unassign dentist" do
    assert_difference("ClinicAssignment.count", -1) do
      post :unassignment, id: @assigned_dentist, clinic_assignment: { clinic_id: clinics(:prestige).id }
    end
  end

  test "search dentist" do
    query = "Edward"
    get :index, q: query
    assert_response :success
    assert_not_nil assigns(:dentists)

    assert_equal Dentist.search(query).count, assigns(:dentists).count

    assigns(:dentists).each do |user|
      assert_includes "#{user.first_name} #{user.last_name}", query
    end
  end
end
