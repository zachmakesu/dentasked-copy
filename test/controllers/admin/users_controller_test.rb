require "test_helper"

class Admin::UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:patient)
    login users(:admin)
  end

  test "get index" do
    get :index
    assert_response :success
    assert_template "admin/users/index"
    assert_includes @response.body, @user.name
    assert_includes @response.body, @user.roles.first.name
  end

  test "search user" do
    get :index, q: "Elliot"
    assert_response :success
    assert_not_nil assigns(:users)

    assigns(:users).each do |user|
      assert_includes "#{user.first_name} #{user.last_name}", "Elliot"
    end
  end

  test "should show admin_user" do
    get :show, id: @user
    assert_response :success
  end

  test "get edit" do
    get :edit, id: @user.id
    assert_response :success
    assert_includes @response.body, @user.first_name
  end
end
