require 'test_helper'

class Admin::FAQsControllerTest < ActionController::TestCase
  setup do
    @faq = faqs(:two)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:faqs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create faq" do
    assert_difference('FAQ.count') do
      post :create, faq: { question: 'What is a dentist?',
                           answer: 'An expert in dental health and cosmetics',
                           app_type: 'Patient' }
    end

    assert_redirected_to admin_faqs_path(type: 'Patient')
  end

  test "should get edit" do
    get :edit, id: @faq
    assert_response :success
  end

  test "should update faq" do
    patch :update, id: @faq, faq: { question: 'What is a patient?',
                                    answer: 'The one who the dentists treats',
                                    app_type: 'Secretary' }
    assert_redirected_to admin_faqs_path(type: @faq.app_type)
  end

  test "should destroy faq" do
    assert_difference('FAQ.count', -1) do
      delete :destroy, id: @faq
    end

    assert_redirected_to admin_faqs_path(type: @faq.app_type)
  end
end
