require 'test_helper'

class Admin::SpecialtiesControllerTest < ActionController::TestCase
  setup do
    @specialty = specialties(:endodontics)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:specialties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create specialty" do
    assert_difference('Specialty.count') do
      post :create, specialty: { name: 'Specialty'  }
    end

    assert_redirected_to admin_specialties_path
  end

  test "should show specialty" do
    get :show, id: @specialty
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @specialty
    assert_response :success
  end

  test "should update specialty" do
    patch :update, id: @specialty, specialty: { name: 'Specialty'  }
    assert_redirected_to admin_specialties_path
  end

  test "should destroy specialty" do
    assert_difference('Specialty.count', -1) do
      delete :destroy, id: @specialty
    end

    assert_redirected_to admin_specialties_path
  end
end
