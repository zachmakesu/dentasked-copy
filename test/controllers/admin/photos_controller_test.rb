require 'test_helper'

class Admin::PhotosControllerTest < ActionController::TestCase

  setup do
    @photo = photos(:clinic)
    login users(:admin)
  end

  test "should destroy photo" do
    assert_difference('Photo.count', -1) do
      delete :destroy, id: @photo
    end
  end

end
