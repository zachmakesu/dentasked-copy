require 'test_helper'

class Admin::RatingsControllerTest < ActionController::TestCase
  setup do
    @rating = ratings(:one)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ratings)
  end

  test "should show rating" do
    get :show, id: @rating
    assert_response :success
  end

end
