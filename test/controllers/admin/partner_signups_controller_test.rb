require "test_helper"
include Devise::TestHelpers

class Admin::PartnerSignupsControllerTest < ActionController::TestCase
  setup do
    @admin = adminify(users(:admin))
    @partner_signup = partner_signups(:pending)
    login @admin
  end

  test "get index" do
    VCR.use_cassette "clinics/draft" do
      get :index
      assert_response :success
      assert_template "admin/partner_signups/index"
    end
  end

  test "approving a partner_signup" do
    VCR.use_cassette "partners/approve" do
      patch :update, id: @partner_signup.id, status: "approved"
      assert_response :redirect
      refute @partner_signup.reload.pending?, "should not be pending anymore"
      assert @partner_signup.reload.approved?, "should approve partner sign up"
    end
  end

  test "declining a partner_signup" do
    patch :update, id: @partner_signup.id
    assert_response :redirect
    assert @partner_signup.reload.declined?, "should decline partner sign up"
    assert @partner_signup.reviewer, @admin
  end
end
