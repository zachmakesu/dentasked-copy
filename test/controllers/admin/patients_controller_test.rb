require 'test_helper'

class Admin::PatientsControllerTest < ActionController::TestCase
  setup do
    @patient = users(:patient)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patients)
  end

  test "should show patient" do
    get :show, id: @patient
    assert_response :success
  end
end
