require 'test_helper'

class Admin::SymptomsControllerTest < ActionController::TestCase
  setup do
    @symptom = symptoms(:sensitive_teeth)
    login users(:admin)
  end
  test 'get admin symptoms index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:symptoms)
  end

  test 'get admin symptoms new' do
    get :new
    assert_response :success
    assert_not_nil assigns(:symptom)
  end

  test 'post valid admin symptom' do
    assert_difference('Symptom.count') do
      post :create, symptom: { name: "Valid" }
    end
    assert_redirected_to admin_symptoms_path
    assert_equal 'Symptom was successfully created.', flash[:notice]
  end

  test 'post invalid admin symptom' do
    post :create, symptom: { name: "" }
    assert_template 'new'
  end

  test 'get admin symptoms edit' do
    get :edit, id: @symptom
    assert_response :success
    assert_not_nil assigns(:symptom)
  end

  test 'put invalid admin symptom' do
    put :update, id: @symptom.id, symptom: { name: "" }
    assert_not_nil assigns(:symptom)
    assert_template 'edit'
  end

  test 'put valid admin symptom' do
    put :update, id: @symptom.id, symptom: { name: 'Valid' }
    assert_not_nil assigns(:symptom)
    assert_redirected_to edit_admin_symptom_path(@symptom)
    assert_equal 'Symptom was successfully updated.', flash[:notice]
  end

  test 'destroy admin symptom' do
    assert_difference 'Symptom.count', -1 do
      delete :destroy, id: @symptom.id
    end
    assert_redirected_to admin_symptoms_path
  end
end
