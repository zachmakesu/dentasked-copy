require "test_helper"
include Devise::TestHelpers

class Admin::RatingSuggestionsControllerTest < ActionController::TestCase
  setup do
    login users(:admin)
    @awesome = rating_suggestions(:awesome)
    @suggestion = rating_suggestions(:enabled)
  end

  test "get index" do
    get :index
    assert_response :success
    assert_template "admin/rating_suggestions/index"
    assert_includes @response.body, @suggestion.name
    assert_includes @response.body, @suggestion.user.name
  end

  test "get new" do
    get :new
    assert_response :success
  end

  test "post new with valid params" do
    assert_difference "RatingSuggestion.count", 1 do
      post :create, rating_suggestion: { name: "Rating", enabled: true }
      assert_response :redirect
    end
  end

  test "post new with invalid params" do
    post :create, rating_suggestion: { name: @suggestion.name, enabled: true }
    assert_response :success
    assert_template "admin/rating_suggestions/new"
  end

  test "get edit" do
    get :edit, id: @suggestion.id
    assert_response :success
    assert_includes @response.body, @suggestion.name
  end

  test "put update with invalid params" do
    patch :update, id: @suggestion.id, rating_suggestion: { name: @awesome.name }
    assert_response :success
    assert_template "admin/rating_suggestions/edit"
  end

  test "put update with valid params" do
    new_suggestion = "New"
    patch :update, id: @suggestion.id, rating_suggestion: { name: new_suggestion, enabled: true }
    assert_response :redirect
  end

  test "destroy rating suggestion" do
    assert_difference "RatingSuggestion.count", -1 do
      delete :destroy, id: @suggestion.id
      assert_response :redirect
    end
  end
end
