require "test_helper"
include Devise::TestHelpers

class Admin::DraftsControllerTest < ActionController::TestCase
  setup do
    @admin = users(:admin)
    @draft = drafts(:pending)
    login @admin
  end

  test "get index" do
    get :index
    assert_response :success
    assert_template "admin/drafts/index"
    assert_includes @response.body, @draft.classification
    assert_includes @response.body, @draft.owner.name
  end

  test "approving a draft" do
    patch :update, id: @draft.id, status: "approved"
    assert_response :redirect
    assert @draft.reload.approved?, "should change status of draft"
    assert @draft.reviewer, @admin
  end

  test "declining a draft" do
    patch :update, id: @draft.id
    assert_response :redirect
    assert @draft.reload.declined?, "should change status of draft"
    assert @draft.reviewer, @admin
  end
end
