require 'test_helper'

class Admin::AppointmentsControllerTest < ActionController::TestCase
  setup do
    @appointment = appointments(:pending_appointment)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:appointments)
  end

  test "should show appointment" do
    get :show, id: @appointment
    assert_response :success
  end
end
