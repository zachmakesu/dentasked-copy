require 'test_helper'

class Admin::FeedbacksControllerTest < ActionController::TestCase
  setup do
    @feedback = feedbacks(:one)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:feedbacks)
  end

  test "should show feedback" do
    get :show, id: @feedback
    assert_response :success
  end
end
