require 'test_helper'

class Admin::HMOsControllerTest < ActionController::TestCase
  setup do
    @hmo = hmos(:medicare)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hmos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hmo" do
    assert_difference('HMO.count') do
      post :create, hmo: { name: 'Bupa International',
                           description: 'International Healthcare' }
    end

    assert_redirected_to admin_hmo_path(assigns(:hmo))
  end

  test "should show hmo" do
    get :show, id: @hmo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hmo
    assert_response :success
  end

  test "should update hmo" do
    patch :update, id: @hmo, hmo: { name: 'Medicare Plus' }
    assert_redirected_to admin_hmo_path(assigns(:hmo))
  end

  test "should destroy hmo" do
    assert_difference('HMO.count', -1) do
      delete :destroy, id: @hmo
    end

    assert_redirected_to admin_hmos_path
  end
end
