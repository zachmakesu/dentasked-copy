require 'test_helper'

class Admin::ClinicsControllerTest < ActionController::TestCase
  setup do
    @clinic = clinics(:prestige)
    @city = cities(:test_city)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:clinics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create clinic" do
    VCR.use_cassette 'clinics/gorated' do
      assert_difference('Clinic.count') do
        post :create, clinic: { name: 'Gorated', address: 'Unit 20, iPark Center, 401 Amang Rodriguez Avenue, Manggahan, Pasig City', city_id: @city.id }
      end

      assert_redirected_to admin_clinics_path
    end
  end

  test "should get edit" do
    get :edit, id: @clinic
    assert_response :success
  end

  test "should update clinic" do
    VCR.use_cassette 'clinics/gorated' do
      patch :update, id: @clinic, clinic: { name: 'Gorated', address: 'Unit 20, iPark Center, 401 Amang Rodriguez Avenue, Manggahan, Pasig City' }
      assert_redirected_to admin_clinics_path
    end
  end

  test "should destroy clinic" do
    assert_difference('Clinic.count', -1) do
      delete :destroy, id: @clinic
    end

    assert_redirected_to admin_clinics_path
  end
end
