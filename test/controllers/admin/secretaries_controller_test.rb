require 'test_helper'

class Admin::SecretariesControllerTest < ActionController::TestCase
  setup do
    @secretary = users(:secretary)
    login users(:admin)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:secretaries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create secretary" do
    assert_difference('Secretary.count') do
      post :create, secretary: { email: 'd.canlas07@gmail.com' }
    end

    assert_redirected_to admin_secretaries_path
  end

  test "should show secretary" do
    get :show, id: @secretary
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @secretary
    assert_response :success
  end

  test "should update secretary" do
    patch :update, id: @secretary, secretary: { first_name: 'Dean',
                                                last_name: 'Canlas' }
    assert_redirected_to admin_secretaries_path
  end

  test "should destroy secretary" do
    assert_difference('Secretary.count', -1) do
      delete :destroy, id: @secretary
    end

    assert_redirected_to admin_secretaries_path
  end
end
