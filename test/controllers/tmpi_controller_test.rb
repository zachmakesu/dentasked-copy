require 'test_helper'

class TMPIControllerTest < ActionController::TestCase
  setup do
    @request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(
      ENV.fetch("TMPI_CMS_USERNAME"),
      ENV.fetch("TMPI_CMS_PASSWORD")
    )
  end
  test "viewing all appointments" do
    get :index
    assert_response :success
  end
  test "confirming an appointment" do
    assert_difference 'TMPILog.count', 1 do
      appointment = appointments(:pending_appointment)
      post :confirm, { id: appointment.id }
      assert_response :redirect
      assert  appointment.reload.confirmed?, 'should confirm appointment'

      log = TMPILog.last
      assert_equal log.status, 'confirmed'
      assert_equal log.username, ENV.fetch("TMPI_CMS_USERNAME")
      assert_equal log.appointment_id, appointment.id
    end
  end
  test "decline an appointment" do
    assert_difference 'TMPILog.count', 1 do
      appointment = appointments(:pending_appointment)
      appointment = appointments(:pending_appointment)
      post :decline, { id: appointment.id }
      assert_response :redirect
      assert  appointment.reload.declined?, 'should decline appointment'

      log = TMPILog.last
      assert_equal log.status, 'declined'
      assert_equal log.username, ENV.fetch("TMPI_CMS_USERNAME")
      assert_equal log.appointment_id, appointment.id
    end
  end
end
