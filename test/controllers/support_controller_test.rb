require 'test_helper'

class SupportControllerTest < ActionController::TestCase
  test "should get index" do
    get :index, { type: 'Patient', uid: users(:patient).uid }
    assert_response :success
  end
end
