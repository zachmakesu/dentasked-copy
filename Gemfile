source "https://rubygems.org"

gem "active_type", ">= 0.3.2"
gem "attr_encrypted", "~> 3.0.0"
gem "autoprefixer-rails", ">= 5.0.0.1"
gem "aws-sdk"
gem "bcrypt", "~> 3.1.7"
gem "bitly"
gem "bootsnap", require: false
gem "browser"
gem "chartkick"
gem "ci_reporter_minitest"
gem "coffee-rails", "~> 4.1.0"
gem "cropper-rails"
gem "devise", ">= 3.4.0"
gem "dotenv-rails", ">= 2.0.0"
gem "draper"
gem "ejs"
gem "exception_notification"
gem "fcm"
gem "font-ionicons-rails"
gem "foundation-rails"
gem "geocoder"
gem "google-api-client"
gem "google_places"
gem "grape"
gem "grape-jbuilder"
gem "grape-kaminari"
gem "groupdate"
gem "houston"
gem "jquery-rails"
gem "jquery-turbolinks"
gem "kaminari"
gem "koala"
gem "light-service"
gem "lograge"
gem "mail", ">= 2.6.3"
gem "marco-polo"
gem "omniauth-facebook"
gem "omniauth-google-oauth2"
gem "paperclip", "~> 4.3"
gem "pg", "~> 0.18"
gem "phonelib"
gem "pusher"
gem "pwdcalc", git: "https://github.com/trimentor/pwdcalc.git"
gem "rack-attack"
gem "rails", "4.2.1"
gem "rails-settings-cached"
gem "ransack", github: "activerecord-hackery/ransack"
gem "redis-rails"
gem "rest-client", "~> 1.8"
gem "sass-rails", "~> 5.0"
gem "secure_headers", ">= 2.1.0"
gem "sidekiq"
gem "sinatra", ">= 1.3.0", require: false
gem "slack-notifier"
gem "tabs_on_rails"
gem "turbolinks", ">= 2.5.2"
gem "validates_timeliness", "~> 3.0"
gem "whenever", require: false

group :production, :staging do
  gem "unicorn"
end

group :development do
  gem "airbrussh", require: false
  gem "annotate", ">= 2.5.0"
  gem "awesome_print"
  gem "better_errors"
  gem "binding_of_caller"
  gem "brakeman", require: false
  gem "bundler-audit", require: false
  gem "capistrano", "~> 3.4.0", require: false
  gem "capistrano-bundler", require: false
  gem "capistrano-db-tasks", require: false
  gem "capistrano-mb", ">= 0.22.2", require: false
  gem "capistrano-rails", require: false
  gem "foreman"
  gem "guard", ">= 2.2.2", require: false
  gem "guard-livereload", require: false
  gem "guard-minitest", require: false
  gem "letter_opener"
  gem "overcommit", require: false
  gem "quiet_assets"
  gem "rack-livereload"
  gem "rb-fsevent", require: false
  gem "spring"
  gem "sshkit", "~> 1.7.1", require: false
  gem "terminal-notifier-guard", require: false
  gem "thin", require: false
  gem "xray-rails", ">= 0.1.16"
end

group :test do
  gem "capybara"
  gem "connection_pool"
  gem "launchy"
  gem "minitest-reporters"
  gem "mocha"
  gem "poltergeist"
  gem "shoulda"
  gem "terminal-notifier"
  gem "test_after_commit"
  gem "vcr"
  gem "webmock"
end

group :development, :test do
  gem "flay", "~> 2.8.0", require: false
  gem "rubocop", "~> 0.49.1", require: false
  gem "rubocop-gitlab-security", "~> 0.0.6", require: false
  gem "pronto"
  gem "pronto-eslint", require: false
  gem "pronto-flay", require: false
  gem "pronto-rubocop", require: false
  gem "pronto-scss", require: false
  gem "scss_lint", "~> 0.54.0", require: false
end
