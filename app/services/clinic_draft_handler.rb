ClinicDraftHandlerError = Class.new(StandardError)

class ClinicDraftHandler
  VALID_MESSAGE = %(Clinic draft is valid)
  attr_reader :object, :params
  def initialize(params={}, opts={})
    @params = params.with_indifferent_access
    @object = Clinic.new(modified_params)
  end

  def validate
    object.valid?
  end

  def errors
    object.errors.full_messages.join('\n')
  end

  private

  def modified_params
    params.tap do |hash|
      logo = hash.fetch(:logo){ Hash.new }
      hash[:logo] = build_attachment(logo)
    end
  end

  def build_attachment(image)
    return nil unless image.present?
    attachment =  {
      :filename => image[:filename],
      :type     => image[:type],
      :headers  => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end
end
