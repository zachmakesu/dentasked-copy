module Actions
  class ValidatesAuthorizationHeader
    extend ::LightService::Action
    expects :authorization_header
    promises :uid, :signature

    executed do |ctx|
      auth = ctx.authorization_header
      if auth.blank?
        ctx.fail!("Missing authorization header")
        next ctx
      end
      if /\ADentasked ([\w]+):([\w\+\=]+)\z/ =~ auth
        ctx.uid = $1
        ctx.signature = $2
      else
        ctx.fail!("Invalid authorization header")
        next ctx
      end
    end
  end
end
