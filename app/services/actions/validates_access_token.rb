module Actions
  class ValidatesAccessToken
    extend ::LightService::Action
    expects :uid, :token
    promises :invalid_token

    executed do |ctx|
      ctx.invalid_token = true
      if user = User.find_by(uid: ctx.uid)
        if token = user.api_keys.find {|a| APIKey.secure_compare(ctx.token, a.encrypted_access_token) }
          if token.expired?
            ctx.fail!(::Errors::ExpiredToken.message, error_code: ::Errors::ExpiredToken.code)
            next ctx
          else
            ctx.invalid_token = false
          end
        else
          ctx.fail!(::Errors::InvalidToken.message, error_code: ::Errors::InvalidToken.code)
          next ctx
        end
      else
        ctx.fail!(::Errors::InvalidToken.message, error_code: ::Errors::InvalidToken.code)
        next ctx
      end
    end
  end
end
