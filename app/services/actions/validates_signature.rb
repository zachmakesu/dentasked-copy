module Actions
  class ValidatesSignature
    extend ::LightService::Action
    expects :path, :params, :signature

    executed do |ctx|
      expected_signature = HMAC.signature_from(ctx.path, ctx.params)
      unless Rails.env.production?
        Rails.logger.debug "#{ctx.path} expected: #{expected_signature}; actual: #{ctx.signature}"
        Rails.logger.debug "params: #{ctx.params}"
      end
      unless HMAC.secure_compare(expected_signature, ctx.signature)
        ctx.fail!("Invalid signature")
        next ctx
      end
    end
  end
end
