module Actions
  class AssignsFbUserRole
    extend ::LightService::Action
    expects :role, :user

    executed do |ctx|
      if ctx.role == 'dentist'
        ctx.user.make_dentist!
      elsif ctx.role == 'secretary'
        ctx.user.make_secretary!
      else
        ctx.user.make_patient!
      end
    end
  end
end
