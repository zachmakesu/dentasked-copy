module Actions
  class AuthenticatesUser
    extend ::LightService::Action
    expects :user, :password, :role
    promises :user

    executed do |ctx|
      if ctx.user.disabled?
        ctx.fail!(::Errors::DisabledAccount.message, error_code: ::Errors::DisabledAccount.code)
        next ctx
      elsif !ctx.user.valid_password?(ctx.password) || !ctx.user.valid_role?(ctx.role)
        ctx.fail!(::Errors::InvalidPassword.message, error_code: ::Errors::InvalidPassword.code)
        next ctx
      elsif !ctx.user.activated?
        ctx.fail!(::Errors::InactiveAccount.message, error_code: ::Errors::InactiveAccount.code)
        next ctx
      else
        ctx.user.increment!(:sign_in_count)
      end
    end
  end
end
