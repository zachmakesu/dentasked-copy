module Actions
  class CreatesOrUpdatesFbIdentity
    extend ::LightService::Action
    expects :uid, :user, :token

    executed do |ctx|
      if identity = ctx.user.identities.facebook
        identity.update({
          oauth_token:  ctx.token,
          oauth_expires_at:  (DateTime.now.utc + 30.days)
        })
      else
        ctx.user.identities.create!({
          uid:  ctx.uid,
          provider:  'facebook',
          oauth_token:  ctx.token,
          oauth_expires_at:  (DateTime.now.utc + 30.days)
        })
      end
    end
  end
end
