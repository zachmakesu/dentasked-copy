module Actions
  class FindsUser
    extend ::LightService::Action
    expects :email
    promises :user

    executed do |ctx|
      if user = User.find_by(email: ctx.email)
        ctx.user = user
      else
        ctx.user = User.new
        ctx.fail!(::Errors::InvalidEmail.message, error_code: ::Errors::InvalidEmail.code)
        next ctx
      end
    end
  end
end
