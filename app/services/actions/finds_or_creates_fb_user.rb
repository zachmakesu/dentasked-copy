module Actions
  class FindsOrCreatesFbUser
    extend ::LightService::Action
    expects :email, :first_name, :last_name, :uid
    promises :user

    executed do |ctx|
      email = ctx.email.present? ? ctx.email : "#{ctx.uid}@facebook.com"
      identity = Identity.find_by(uid: ctx.uid, provider: 'facebook')
      if user = identity.try(:user) || User.find_by(email: email)
        user.increment!(:sign_in_count)
        if user.has_temporary_email?
          user.skip_reconfirmation!
          user.update(email: email)
        end
        unless user.avatar.present?
          user.update(avatar: avatar(ctx: ctx))
        end
        ctx.user = user
      else
        user = User.new(
          first_name: ctx.first_name,
          last_name: ctx.last_name,
          email: email,
          password: Devise.friendly_token[0,20],
          avatar: avatar(ctx: ctx)
        )
        user.skip_confirmation!
        if user.save
          ctx.user = user
        else
          ctx.fail!("Cannot create user account for FB user")
        end
      end
    end

    def self.avatar(ctx:)
      User.process_uri("http://graph.facebook.com/#{ctx.uid}/picture?type=large")
    end
  end
end
