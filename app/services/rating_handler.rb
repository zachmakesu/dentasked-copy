class RatingHandler
  class MissingRatingError < StandardError; end
  FAILED_RATING_MESSAGE = "Failed to submit rating, please try again!".freeze
  attr_reader :context, :rating

  def initialize(context, current_user, params = {})
    @context = context
    @current_user = current_user
    @params = params
  end

  def call
    @rating = Rating.new(
      context: @context,
      description: @params[:comment],
      ratee: ratee,
      rater: rater,
      rating: @params[:rating]

    )
    @rating.save
  end

  private

  def ratee
    @context.clinic_assignment.clinic
  end

  def rater
    @current_user
  end
end
