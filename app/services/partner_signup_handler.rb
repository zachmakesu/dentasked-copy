PartnerSignupHandlerError = Class.new(StandardError)
class PartnerSignupHandler
  ERROR_MESSAGE = "Please provide valid partner signup information"
  SUCCESS_MESSAGE = "Successfully submitted Partner Signup application"
  attr_reader :clinic_draft, :secretary_draft, :dentist_draft
  attr_reader :clinic_params, :secretary_params, :dentist_params

  def initialize(clinic_params={}, secretary_params={}, dentist_params={})
    @clinic_params = clinic_params
    @secretary_params = secretary_params
    @dentist_params = dentist_params
    @clinic_draft = ClinicDraftHandler.new(@clinic_params)
    @secretary_draft = SecretaryDraftHandler.new(@secretary_params)
    @dentist_draft = DentistDraftHandler.new(@dentist_params)
  end

  def validate
    clinic_draft.validate && secretary_draft.validate && dentist_draft.validate
  end

  def create
    return false unless validate
    clinic_object     = clinic_params.except(:logo)
    secretary_object  = secretary_params.except(:avatar)
    dentist_object    = dentist_params.except(:avatar)
    partner_signup_params = {
      clinic_object: clinic_object,
      secretary_object: secretary_object,
      dentist_object: dentist_object
    }

    partner_signup_params[:clinic_logo] = clinic.logo if clinic.logo.present?
    partner_signup_params[:secretary_avatar] = secretary.avatar if secretary.avatar.present?
    partner_signup_params[:dentist_avatar] = dentist.avatar if dentist.avatar.present?

    PartnerSignup.create(partner_signup_params)
  end

  def self.send_verification_code(partner_signup)
    if secretary = partner_signup.secretary
      activation = secretary.reload.activations.last
      SMS::ActivationSender.perform_async(activation.id)
    end
  end

  def self.approve!(partner_signup, current_user)
    handler = PartnerSignupHandler.new(partner_signup.clinic_object,
                                       partner_signup.secretary_object,
                                       partner_signup.dentist_object)
    if handler.send(:approve_partner_signup)
      ActiveRecord::Base.transaction do
        partner_signup.approved!
        partner_signup.update(reviewer: current_user)
      end
    end
  end

  def self.decline!(partner_signup, current_user)
    ActiveRecord::Base.transaction do
      partner_signup.declined!
      partner_signup.update(reviewer: current_user)
    end
  end

  def errors
    validate ? "" : ERROR_MESSAGE
  end

  private

  def approve_partner_signup
    return false unless validate
    new_secretary = nil
    ActiveRecord::Base.transaction do
      clinic.save
      dentist.save
      new_secretary = if dentist_is_secretary
                        dentist.make_secretary!
                        dentist.becomes(Secretary)
                      else
                        secretary.save
                        secretary
                      end
    end
    if dentist.persisted?
      clinic.assign(dentist)
      new_secretary.add_dentist(dentist) if new_secretary.persisted?
    end
  end

  def dentist_is_secretary
    secretary_draft.object.email == dentist_draft.object.email
  end

  def clinic
    clinic_draft.object
  end

  def dentist
    dentist_draft.object
  end

  def secretary
    secretary_draft.object
  end
end
