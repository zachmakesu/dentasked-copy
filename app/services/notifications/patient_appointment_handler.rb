Notifications::PatientAppointmentHandlerError = Class.new(StandardError)

class Notifications::PatientAppointmentHandler
  NOTIFICATION_TYPE = 2

  def initialize(appointment)
    @appointment = appointment
    @dentist_name = appointment.dentist.name_with_salutation
    @date = appointment.schedule.strftime("%a %b %e, %Y at %I:%M %p")
    @old_date = appointment.read_attribute(:schedule).strftime("%a %b %e, %Y at %I:%M %p")
  end

  def deliver
    IOSHandler.new(recipient, alert, payload, client).deliver
    AndroidHandler.new(recipient, alert, payload, fcm_key).deliver
  end

  private

  def alert
    if @appointment.confirmed?
      if @appointment.rescheduled?
        "Your appointment #{@dentist_name} has been rescheduled from #{@old_date} to #{@date}."
      else
        "Your appointment with #{@dentist_name} is confirmed."
      end
    elsif @appointment.cancelled?
      "#{@dentist_name} cancelled your appointment on #{@date}"
    elsif @appointment.failed? || @appointment.declined?
      "Sorry, #{@dentist_name} is currently not available. Here are our other options."
    elsif @appointment.complete?
      "Your appointment with #{@dentist_name} is done."
    end
  end

  def client
    ConsumerAPN.new
  end

  def fcm_key
    ENV["CONSUMER_FCM_KEY"]
  end

  def payload
    if @appointment.confirmed?
      if @appointment.rescheduled?
        confirmed_and_rescheduled_payload
      else
        confirmed_payload
      end
    elsif @appointment.cancelled?
      cancelled_payload
    elsif @appointment.failed? || @appointment.declined?
      failed_or_declined_payload
    elsif @appointment.complete?
      complete_payload
    end
  end

  def recipient
    @appointment.patient
  end

  def confirmed_and_rescheduled_payload
    empty_payload
  end

  def confirmed_payload
    {
      id: @appointment.id,
      name: @dentist_name,
      avatar_url: URI.join(ActionController::Base.asset_host.to_s, @appointment.dentist.avatar.url).to_s,
      schedule: @date,
      status_id: @appointment[:status]
    }
  end

  def cancelled_payload
    {
      id: @appointment.id,
      status_id: @appointment[:status]
    }
  end

  def failed_or_declined_payload
    required_data = {
      id: @appointment.id,
      name: @dentist_name,
      avatar_url: URI.join(ActionController::Base.asset_host.to_s, @appointment.dentist.avatar.url).to_s,
      schedule: @appointment.schedule.iso8601,
      service_id: @appointment.service.id,
      clinic_assignment_id: @appointment.clinic_assignment_id,
      status_id: @appointment[:status]
    }
    unless @appointment.hmo.nil?
      required_data['hmo_id'] = @appointment.hmo.id
    end
    required_data
  end

  def complete_payload
    empty_payload
  end

  def empty_payload
    { none: nil }
  end
end
