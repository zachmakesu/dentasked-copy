class Notifications::DayReminderHandler
  NOTIFICATION_TYPE = 5

  def initialize(appointment)
    @appointment = appointment
  end

  def deliver
    IOSHandler.new(recipient, alert, payload, client).deliver
    AndroidHandler.new(recipient, alert, payload, fcm_key).deliver
  end

  def valid?
    schedule_1_day_before = @appointment.schedule - 24.hours
    (schedule_1_day_before) > DateTime.now
  end

  private

  def alert
    date = @appointment.schedule.strftime("%d %b %Y, %l:%M %p")
    "Reminder: Don't forget: #{@appointment.service.name} with #{@appointment.dentist.name_with_salutation}, Tomorrow, #{date}"
  end

  def client
    ConsumerAPN.new
  end

  def fcm_key
    ENV["CONSUMER_FCM_KEY"]
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      appointment_id: @appointment.id
    }
  end

  def recipient
    @appointment.patient
  end
end
