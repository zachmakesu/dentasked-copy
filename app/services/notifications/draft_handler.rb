Notifications::DraftHandlerError = Class.new(StandardError)

class Notifications::DraftHandler
  NOTIFICATION_TYPE = 1

  def initialize(draft)
    @draft = draft
  end

  def deliver
    IOSHandler.new(recipient, alert, payload, client).deliver
    AndroidHandler.new(recipient, alert, payload, fcm_key).deliver
  end

  private

  def alert
    "Your recent Dentist info. update request was #{@draft.status} by Dentasked Admin."
  end

  def client
    PartnerAPN.new
  end

  def fcm_key
    ENV["PARTNER_FCM_KEY"]
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      owner_uid: recipient.uid
    }
  end

  def recipient
    @draft.owner
  end
end
