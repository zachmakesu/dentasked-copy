Notifications::RatingHandlerError = Class.new(StandardError)

class Notifications::RatingHandler
  NOTIFICATION_TYPE = 4

  def initialize(appointment)
    @appointment = appointment
  end

  def deliver
    IOSHandler.new(recipient, alert, payload, client).deliver
    AndroidHandler.new(recipient, alert, payload, fcm_key).deliver
  end

  private

  def alert
    "Thank you for booking with DentaSked. Please take a moment to leave a rating to help improve our services!"
  end

  def client
    ConsumerAPN.new
  end

  def fcm_key
    ENV["CONSUMER_FCM_KEY"]
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      appointment_id: @appointment.id,
      ratee_name: @appointment.clinic_assignment.clinic.name
    }
  end

  def recipient
    @appointment.patient
  end
end
