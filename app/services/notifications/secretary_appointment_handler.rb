Notifications::SecretaryAppointmentHandlerError = Class.new(StandardError)

class Notifications::SecretaryAppointmentHandler
  NOTIFICATION_TYPE = 3

  def initialize(appointment)
    @appointment = appointment
    @patient_name = appointment.patient.name
    @dentist_name = appointment.dentist.name_with_salutation
    @date = appointment.schedule.strftime("%a %b %e, %Y at %I:%M %p")
    @old_date = appointment.read_attribute(:schedule).strftime("%a %b %e, %Y at %I:%M %p")
  end

  def deliver
    recipient.each do |rec|
      IOSHandler.new(rec, alert, payload, client).deliver
      AndroidHandler.new(rec, alert, payload, fcm_key).deliver
    end
  end

  private

  def alert
    if @appointment.cancelled?
      "#{@patient_name} cancelled their appointment request for #{@dentist_name} on #{@date}"
    else
      "#{@patient_name} made an appointment with #{@dentist_name}"
    end
  end

  def client
    PartnerAPN.new
  end

  def fcm_key
    ENV["PARTNER_FCM_KEY"]
  end

  def payload
    if @appointment.cancelled?
      cancelled_payload
    else
      new_appointment_payload
    end
  end

  def recipient
    @appointment.dentist.secretaries
  end

  def new_appointment_payload
    data = {
      id: @appointment.id,
      avatar_url: URI.join(ActionController::Base.asset_host.to_s, @appointment.dentist.avatar.url).to_s,
      schedule: @appointment.schedule.iso8601,
      service_id: @appointment.service.id,
      clinic_assignment_id: @appointment.clinic_assignment_id,
      status_id: @appointment[:status]
    }
    unless @appointment.hmo.nil?
      data[:hmo_id] = @appointment.hmo.id
    end
    data
  end

  def cancelled_payload
    {
      id: @appointment.id,
      status_id: @appointment[:status]
    }
  end
end
