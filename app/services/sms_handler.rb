class SMSHandler
  COUNTRY_CODE = %(PH).freeze
  attr_reader :message, :number, :client

  def initialize(notification:)
    @message = notification.message
    @number = notification.recipient
    @client = if valid_dynamic_client
                dynamic_client.new(message: message, number: formatted_number)
              else
                notification.client.new(message: message, number: formatted_number)
              end
  end

  def call
    return unless Phonelib.valid_for_country?(number, COUNTRY_CODE)
    self.tap do 
      client.()
      after_action
    end
  end

  private

  def formatted_number
    Phonelib.parse(number, COUNTRY_CODE).e164
  end

  def after_action
    SMSLog.create!(
      message: message,
      recipient: formatted_number,
      client: client.name.to_sym,
      status: client.success? ? :success : :failure
    )
  end

  def valid_dynamic_client
    SMSLog.clients.keys.include?(AppSetting.sms_client)
  end

  def dynamic_client
    preferred_client = AppSetting.sms_client
    provider = if preferred_client == "sns"
                 preferred_client.upcase
               else
                 preferred_client.titleize
               end
    "SMS::#{provider}Client".constantize
  end
end
