SecretaryDraftHandlerError = Class.new(StandardError)
class SecretaryDraftHandler
  VALID_MESSAGE = %(Secretary draft is valid)
  attr_reader :object, :params
  def initialize(params={}, opts={})
    @params = params.with_indifferent_access
    @object = Secretary.new(modified_params)
  end

  def validate
    object.valid?
  end

  def errors
    object.errors.full_messages.join('\n')
  end

  private

  def modified_params
    params.tap do |hash|
      avatar = hash.fetch(:avatar){ Hash.new }
      hash[:avatar] = build_attachment(avatar)
    end
  end

  def build_attachment(image)
    return nil unless image.present?
    attachment =  {
      :filename => image[:filename],
      :type     => image[:type],
      :headers  => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end
end
