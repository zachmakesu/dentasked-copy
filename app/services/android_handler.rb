AndroidHandlerError = Class.new(StandardError)

class AndroidHandler
  def initialize(recipient, alert, payload, key = nil, logger = Rails.logger)
    payload[:alert] = alert
    @fcm            = FCM.new(key)
    @recipient      = recipient
    @payload        = payload
    @logger         = logger
  end

  def deliver
    return true if devices.empty? || testing?
    response = @fcm.send registration_ids, options
    after_action_for response
    @logger.debug(response) unless Rails.env.production?
    self
  end

  private

  def devices
    @recipient.devices.android.active
  end

  def registration_ids
    devices.pluck(:token)
  end

  def options
    {
      data: @payload
    }
  end

  def after_action_for(response)
    disable response.fetch(:not_registered_ids) { [] }
  end

  def disable(not_registered_ids = [])
    not_registered_ids.each do |token|
      if device = Device.find_by(token: token)
        device.update(enabled: false)
      end
    end
  end

  def testing?
    Rails.env.test?
  end
end
