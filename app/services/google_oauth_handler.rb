require 'google/api_client'

class GoogleOauthHandler
  attr_reader :auth_client

  def initialize(user, auth_code=nil, scope="https://www.googleapis.com/auth/calendar", config="#{Rails.root}/config/client_secrets.json")
    @user = user
    @config = config
    @auth_code = auth_code
    @auth_client = client_secrets.to_authorization
    @auth_client.update!(
      scope: scope,
      redirect_uri: 'urn:ietf:wg:oauth:2.0:oob'
    )
  end

  def call
    auth_client.code = @auth_code
    @raw_response = begin
                      auth_client.fetch_access_token!
                    rescue ArgumentError, Signet::AuthorizationError => e
                      destroy_google_identity
                      { message: e.message }
                    end
    update_google_identity if success?
  end

  def response
    OpenStruct.new(
      access_token: @raw_response.fetch("access_token"){ nil },
      refresh_token: @raw_response.fetch("refresh_token"){ nil},
      message: @raw_response.fetch("message"){ "Successfully fetched info" }
    )
  end

  def success?
    !!response.refresh_token
  end

  private

  def client_secrets
    Google::APIClient::ClientSecrets.load(@config)
  end

  def update_google_identity

    if identity = @user.identities.where(provider: 'google_oauth2').first
      identity.update(
        oauth_token: response.access_token,
        oauth_refresh_token: response.refresh_token,
        oauth_expires_at: 1.hour.from_now
      )
    else
      @user.identities.create(
        uid: @user.uid,
        provider: 'google_oauth2',
        oauth_token: response.access_token,
        oauth_refresh_token: response.refresh_token,
        oauth_expires_at: 1.hour.from_now
      )
    end
  end

  def destroy_google_identity
    @user.identities.google.try(:destroy)
  end
end
