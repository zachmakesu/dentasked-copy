class SMS::BaseClient
  attr_reader :message, :number, :response

  def initialize(message:, number:)
    @message = message
    @number = number
  end

  def call
    raise NotImplementedError
  end

  def name
    raise NotImplementedError
  end

  def success?
    false
  end

  SMSLog.clients.keys.each do |client|
    define_method "#{client}?" do
      name == client.to_s
    end
  end
end

