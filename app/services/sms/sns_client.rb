class SMS::SNSClient < SMS::BaseClient
  def name
    'sns'
  end

  def call
    @response = client.publish(payload)
  end

  def success?
    # TODO: Is there a way to check aws sns delivery status?
    true
  end

  private

  def client
    Aws::SNS::Client.new(
      access_key_id: ENV.fetch('AWS_SNS_ACCESS_KEY'),
      secret_access_key: ENV.fetch('AWS_SNS_SECRET_KEY'),
      region: ENV.fetch('AWS_SNS_REGION')
    )
  end

  def payload
    {
      phone_number: number,
      message: message
    }
  end
end
