class SMS::TwilioClient < SMS::BaseClient
  def name
    'twilio'
  end

  def call
    return
  end

  def success?
    false
  end

  private

  def client
    raise NotImplementedError
  end
end
