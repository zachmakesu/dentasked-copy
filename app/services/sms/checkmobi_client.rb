class SMS::CheckmobiClient < SMS::BaseClient
  ENDPOINT = %(https://api.checkmobi.com/v1/sms/send).freeze

  def name
    'checkmobi'
  end

  def call
    @response = client.post do |req|
      req.headers['Authorization'] = ENV.fetch("CHECKMOBI_SECRET")
      req.headers['Content-Type'] = 'application/json'
      req.body = payload.to_json
    end
  end

  def success?
    response.success?
  end

  private

  def client
    Faraday.new(url: ENDPOINT)
  end

  def payload
    {
      "to": number,
      "text": message,
      "platform":"web"
    }
  end
end
