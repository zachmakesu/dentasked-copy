module SMS
  module Notification
    class PartnerBookingRequest < Base
      attr_reader :appointment, :mobile_number, :resend
      def initialize(appointment_id:)
        @appointment = ::Appointment.find_by(id: appointment_id)
      end

      def message
        <<-EOS.strip_heredoc
          Hello #{secretary.first_name}, You received an appointment request from #{patient.name}

          Log in to your Dentasked Partner's App Account to view the booking details and confirm it.
        EOS
      end

      def recipient
        secretary.mobile_number
      end

      def valid?
        true
      end

      def client
        SMS::CheckmobiClient
      end

      private

      def secretary
        appointment.clinic_assignment.dentist.secretaries.first
      end

      def patient
        appointment.patient
      end
    end
  end
end
