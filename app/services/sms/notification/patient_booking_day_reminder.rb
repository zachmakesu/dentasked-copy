module SMS
  module Notification
    class PatientBookingDayReminder < Base
      attr_reader :appointment, :mobile_number, :resend
      def initialize(appointment_id:)
        @appointment = ::Appointment.find_by(id: appointment_id)
      end

      def message
        <<-EOS.strip_heredoc
          DentaSked Reminder: Hi, #{patient.first_name}! Don't forget, you have a
          #{appointment.service.name} with #{appointment.dentist.name_with_salutation} Tomorrow at #{appointment.formatted_time}
        EOS
      end

      def recipient
        patient.mobile_number
      end

      def valid?
        patient.mobile_number.present? &&
          DateTime.now.utc <= (appointment.schedule.utc - 1.day)
      end

      def client
        SMS::CheckmobiClient
      end

      private

      def patient
        appointment.patient
      end
    end
  end
end
