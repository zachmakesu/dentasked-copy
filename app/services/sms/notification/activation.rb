module SMS
  module Notification
    class Activation < Base
      attr_reader :activation, :mobile_number, :resend
      def initialize(activation_id:, mobile_number:nil, resend:false)
        @activation = ::Activation.find_by(id: activation_id)
        @mobile_number = mobile_number || activation.user.mobile_number
        @resend = resend
      end

      def message
        "Your Dentasked Activation Code: #{activation.code}"
      end

      def recipient
        mobile_number
      end

      def valid?
        activation && !activation.expired? && !activation.user.admin?
      end

      def client
        if resend
          # TODO: Replace with AWS SNS since it's originally planned to be
          # used for resending activation codes, for now use checkmobi for both
          SMS::CheckmobiClient
        else
          SMS::CheckmobiClient
        end
      end
    end
  end
end
