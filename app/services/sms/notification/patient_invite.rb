module SMS
  module Notification
    class PatientInvite < Base
      include Rails.application.routes.url_helpers
      attr_reader :name, :number, :inviter
      def initialize(name:, number:, inviter:)
        @name = name
        @number = number
        @inviter = inviter
      end

      def message
        if inviter_is_secretary? && single_clinic_assignment?
          clinic = inviter.clinics.first
          "#{clinic.name} invites you to book your next appointment using the Dentasked app. Download Dentasked here #{download_link}"
        else
          "Hi #{name}, You have been invited by #{inviter.name} to book your next dental appointment using the DentaSked App. Download DentaSked here. #{download_link}"
        end
      end

      def recipient
        number
      end

      def valid?
        Phonelib.valid_for_country?(number, 'PH')
      end

      def client
        SMS::CheckmobiClient
      end

      private def inviter_is_secretary?
        inviter.is_a?(Secretary)
      end

      private def single_clinic_assignment?
        inviter.clinics.count == 1
      end

      private def download_link
        ENV.fetch("APP_DOWNLOAD_URL")
      end
    end
  end
end
