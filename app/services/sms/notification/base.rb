module SMS
  module Notification
    class Base
      def valid?
        raise NotImplementedError
      end

      def message
        raise NotImplementedError
      end

      def recipient
        raise NotImplementedError
      end

      def client
        raise NotImplementedError
      end
    end
  end
end
