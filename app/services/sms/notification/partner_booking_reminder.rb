module SMS
  module Notification
    class PartnerBookingReminder < Base
      attr_reader :appointment, :mobile_number, :resend
      def initialize(appointment_id:)
        @appointment = ::Appointment.find_by(id: appointment_id)
      end

      def message
        <<-EOS.strip_heredoc
          An appointment request from #{patient.name} is about to expire soon. Respond to the request by logging in to your DentaSked Partner's app.
        EOS
      end

      def recipient
        secretary.mobile_number
      end

      def valid?
        appointment.pending?
      end

      def client
        SMS::CheckmobiClient
      end

      private

      def secretary
        appointment.clinic_assignment.dentist.secretaries.first
      end

      def patient
        appointment.patient
      end
    end
  end
end
