module SMS
  module Notification
    class ResetPasswordInstructions < Base
      include Rails.application.routes.url_helpers
      attr_reader :user, :token
      def initialize(user:, token:)
        @user = user
        @token = token
      end

      def message
        <<-EOS.strip_heredoc
        Someone has requested a link to change your password. You can do this through the link below. If you didn't request this, please ignore this message. Your password won't change until you access the link below and create a new one. Don't forget to share the app to your friends! If you have comments on how we can improve the service, feel free to email us at dentaskedph@gmail.com

        Change my password: #{reset_password_url}
        EOS
      end

      def recipient
        user.mobile_number
      end

      def valid?
        user.mobile_number.present?
      end

      def client
        SMS::CheckmobiClient
      end

      private def reset_password_url
        url = edit_user_password_url(reset_password_token: token)
        bitly = Bitly.client.shorten(url)
        bitly.short_url
      end
    end
  end
end
