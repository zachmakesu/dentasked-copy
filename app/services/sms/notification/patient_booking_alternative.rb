module SMS
  module Notification
    class PatientBookingAlternative < Base
      attr_reader :appointment, :mobile_number
      def initialize(appointment_id:)
        @appointment = ::Appointment.find_by(id: appointment_id)
      end

      def message
        intro = <<~EOS.strip_heredoc
        Sorry, #{appointment.dentist.name_with_salutation} is currently not available. Here are your options:

        #{alternatives}

        EOS
        intro + dynamic_link_url
      end

      def recipient
        patient.mobile_number
      end

      def valid?
        patient.mobile_number.present? && (appointment.failed? || appointment.declined?) && !appointment.alternatives.empty?
      end

      def client
        SMS::CheckmobiClient
      end

      private
      def patient
        appointment.patient
      end

      def alternatives
        appointment.alternatives.map{|d| "- #{d.name_with_salutation}"}.join("\n")
      end

      def dynamic_link_url
        link_params = <<-EOS.strip_heredoc
        service_id=#{appointment.service_id}&schedule=#{appointment.schedule}
        EOS
        link_domain = ENV.fetch("FIREBASE_DYNAMIC_LINK_DOMAIN")
        base_link = ENV.fetch("DYNAMIC_LINK_BASE")
        apn = ENV.fetch("DYNAMIC_LINK_APN")
        dfl = ENV.fetch("DYNAMIC_LINK_DFL")
        encoded_link = CGI.escape(base_link+link_params)
        dynamic_link = link_domain + encoded_link + apn + dfl
        bitly_dynamic = Bitly.client.shorten(dynamic_link)
        bitly_dynamic.short_url
      end

    end
  end
end
