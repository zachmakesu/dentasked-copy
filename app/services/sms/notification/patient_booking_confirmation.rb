module SMS
  module Notification
    class PatientBookingConfirmation < Base
      attr_reader :appointment
      def initialize(appointment_id:)
        @appointment = ::Appointment.find_by(id: appointment_id)
      end

      def message
        app_message
      end

      def recipient
        patient.mobile_number
      end

      def valid?
        patient.mobile_number.present?
      end

      def client
        SMS::CheckmobiClient
      end

      private

      def app_message
        <<-EOS.strip_heredoc
        Your appointment with #{appointment.dentist.name_with_salutation} is confirmed.

        #{appointment.clinic_assignment.clinic.name}
        #{appointment.service.name}
        #{appointment.formatted_schedule}
        EOS
      end

      def patient
        appointment.patient
      end
    end
  end
end
