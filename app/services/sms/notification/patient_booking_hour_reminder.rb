module SMS
  module Notification
    class PatientBookingHourReminder < Base
      attr_reader :appointment, :mobile_number, :resend
      def initialize(appointment_id:)
        @appointment = ::Appointment.find_by(id: appointment_id)
      end

      def message
        <<-EOS.strip_heredoc
          DentaSked Reminder: Don't forget, you have an appointment later:
          #{appointment.service.name} with #{appointment.dentist.name_with_salutation} at #{appointment.formatted_time}
        EOS
      end

      def recipient
        patient.mobile_number
      end

      def valid?
        patient.mobile_number.present? &&
          DateTime.now.utc <= (appointment.schedule.utc - 3.hours)
      end

      def client
        SMS::CheckmobiClient
      end

      private

      def secretary
        appointment.clinic_assignment.dentist.secretaries.first
      end

      def patient
        appointment.patient
      end
    end
  end
end
