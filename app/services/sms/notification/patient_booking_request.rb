module SMS
  module Notification
    class PatientBookingRequest < Base
      attr_reader :appointment, :mobile_number, :resend, :walkin
      def initialize(appointment_id:, walkin:false)
        @appointment = ::Appointment.find_by(id: appointment_id)
        @walkin = walkin
      end

      def message
        if walkin
          walkin_message
        else
          app_message
        end
      end

      def recipient
        patient.mobile_number
      end

      def valid?
        patient.mobile_number.present?
      end

      def client
        SMS::CheckmobiClient
      end

      private

      def app_message
        <<-EOS.strip_heredoc
          Hi #{appointment.patient.first_name}, Thanks for booking through Dentasked. Here are your appointment request details:

          #{appointment.clinic_assignment.clinic.name}
          #{appointment.service.name}
          #{appointment.dentist.name_with_salutation}
          #{appointment.formatted_schedule}

          We'll notify you within 48 hours before the dentist can confirm the schedule.
        EOS
      end

      def walkin_message
        <<-EOS.strip_heredoc
          Hi #{appointment.patient.first_name}, Here are your appointment request details:

          #{appointment.clinic_assignment.clinic.name}
          #{appointment.service.name}
          #{appointment.dentist.name_with_salutation}
          #{appointment.formatted_schedule}

          Scheduling and keeping track of your dentist appointments just got easier. Just download the Dentasked app here #{download_link}
        EOS
      end

      def patient
        appointment.patient
      end

      def download_link
        ENV.fetch("APP_DOWNLOAD_URL")
      end
    end
  end
end
