class InviteContacts
  attr_reader :names, :numbers, :inviter, :error, :response
  def initialize names:, numbers:, inviter:
    @names = names
    @numbers = numbers
    @inviter = inviter
  end

  def valid?
    validations = [:equal_names_and_numbers?]
    return unless validations.all?{|v| self.send(v) }
    true
  end

  def call
    names.each_with_index do |name, i|
      number = numbers[i]
      SMS::PatientInviteSender.perform_async(inviter.id, { mobile_number: number, contact_name: name })
    end
    @response = successful_response
  end

  private

  def equal_names_and_numbers?
    (names.count == numbers.count).tap do |equal|
      @error = "Invalid number of invites" unless equal
    end
  end

  def successful_response
    {
      message: "Successfully invited #{names.count} #{"contact".pluralize(names.count)}"
    }
  end
end
