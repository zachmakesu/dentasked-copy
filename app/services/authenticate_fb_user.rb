class AuthenticateFbUser
  extend LightService::Organizer

  def self.call(token: "", role: "")
    with(
      token: token,
      role: role
    ).reduce(
      Actions::ValidatesFbToken,
      Actions::FindsOrCreatesFbUser,
      Actions::CreatesOrUpdatesFbIdentity,
      Actions::AssignsFbUserRole,
      Actions::GeneratesAPIKey
    )
  end
end
