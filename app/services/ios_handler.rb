class IOSHandler
  attr_reader :client, :errors
  def initialize(recipient, alert, payload, client, logger=Rails.logger)
    @recipient  = recipient
    @alert      = alert
    @payload    = payload
    @client     = client
    @errors     = []
    @logger     = logger
  end

  def deliver
    return true if devices.empty? || testing?
    send_notifications
    self
  end

  private

  def devices
    @recipient.devices.ios.active
  end

  def send_notifications
    devices.each do |device|
      notification = create_notification(device)
      client.push notification
      after_action_for notification
    end
  end

  def create_notification device
    notification = Houston::Notification.new(device: device.token)
    notification.alert = @alert
    notification.custom_data = @payload
    notification
  end

  def after_action_for notification
    error = notification.error
    if error
      @logger.error(error)
      @errors << error
      # must be an expired/invalid device so we should disable it
      device.update(enabled: false)
    else
      @logger.debug(notification.inspect) unless Rails.env.production?
    end
  end

  def testing?
    Rails.env.test?
  end
end
