DraftHandlerError = Class.new(StandardError)

class DraftHandler

  attr_accessor :response

  def initialize user, params, opts={}
    @user     = user
    @dentist  = Dentist.find_by(uid: params[:uid])
    @object   = params
    @options  = opts
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      @draft = @user.drafts.build(draft_options)
      @draft.photos.build(avatar_options) if @object.key?(:avatar)
      @draft.save
      GoogleAnalyticsApi.new.event('Info Update Request', '@draft.status', 'Status', @user.try(:id))
    end

    create_response(@draft.errors.full_messages)
    self
  end

  def self.decline! draft, user, opts={}
    @user   = user
    @draft  = draft
    @status = "declined"
    @response = Hash.new

    @draft.update(reviewer_options)

    create_response(@draft.errors.full_messages)
  end

  def self.approve! draft, user, opts={}
    @user     = user
    @draft    = draft
    @dentist  = draft.dentist
    @object   = draft.object
    @status   = "approved"
    @response = Hash.new

    self.apply_draft!
  end

  private
  def create_response(errors=nil)
    filtered_errors = errors.flatten.uniq.compact
    if filtered_errors.blank?
      response[:success] = true
      response[:details] = @dentist
    else
      response[:success] = false
      response[:details] = filtered_errors.join(', ')
    end
  end

  def draft_options
    {
      classification: @options[:classification],
      object:         @object.except(:avatar),
      dentist_id:     @dentist.id
    }
  end

  def avatar_options
    {
      name: "avatar",
      image: build_attachment(@object[:avatar])
    }
  end

  def self.apply_draft!
    ActiveRecord::Base.transaction do
      @profile      = @dentist.profile
      @services     = @dentist.service_assignments
      @specialties  = @dentist.specializations
      @hmos         = @dentist.hmo_accreditations

      # Destroy deselected hmos
      deselected_hmos = @hmos - HMOAccreditation.where(hmo_id: @object[:hmos].to_s.split(";").map(&:to_i))
      deselected_hmos.each(&:destroy)

      # Destroy deselected services
      deselected_services = @services - ServiceAssignment.where(service_id: @object[:services].to_s.split(";").map(&:to_i))
      deselected_services.each(&:destroy)

      # Destroy deselected specialties
      deselected_specializations = @specialties - Specialization.where(specialty_id: @object[:specialties].to_s.split(";").map(&:to_i))
      deselected_specializations.each(&:destroy)

      @dentist.assign_attributes(dentist_options)
      @profile.assign_attributes(dentist_profile_options)
      @services.build(service_options)                          if @object.key?(:services)
      @specialties.build(specialty_options)                     if @object.key?(:specialties)
      @hmos.build(hmo_options)                                  if @object.key?(:hmos)

      @dentist.save
      @profile.save
      @services.each(&:save)
      @specialties.each(&:save)
      @hmos.each(&:save)

      @draft.update(reviewer_options)

      if @object.key?(:schedules)
        schedules     = Base64.decode64(@object[:schedules]).split('|')
        schedule_ids  = []
        schedules.each do |schedule|
          sched = schedule.split(';')

          @schedule             = AssignmentSchedule.find_or_initialize_by(clinic_assignment_id: @object[:clinic_assignment_id], day: sched[0])
          @schedule.start_time  = DateTime.parse(sched[1])
          @schedule.end_time    = DateTime.parse(sched[2])
          @schedule.save

          schedule_ids << @schedule.id
        end
        AssignmentSchedule.where('clinic_assignment_id = ? AND id NOT IN (?)',@object[:clinic_assignment_id], schedule_ids).destroy_all
      end
    end

    errors = []
    errors << @dentist.errors.full_messages                     if @dentist.present?
    errors << @profile.errors.full_messages                     if @profile.present?
    errors << @services.flat_map{|e| e.errors.full_messages}    if @services.present?
    errors << @specialties.flat_map{|e| e.errors.full_messages} if @specialties.present?
    errors << @hmos.flat_map{|e| e.errors.full_messages}        if @hmos.present?
    errors << @schedule.errors.full_messages                    if @schedule.present?
    errors << @draft.errors.full_messages                       if @draft.present?

    create_response(errors)
  end

  def self.dentist_options
    {
      avatar: @draft.photos.find_by(name: "avatar").try(:image)
    }.compact
  end

  def self.dentist_profile_options
    {
      fee:            @object[:fee],
      experience:     @object[:experience],
      license_number: @object[:license_number]
    }.compact
  end

  def self.service_options
    (@object[:services].to_s.split(';').map(&:to_i) - @services.pluck(:service_id)).map do |id|
      { service_id: id, assigned_type: 'User', assigned_id: @dentist.id }
    end
  end

  def self.specialty_options
    (@object[:specialties].to_s.split(';').map(&:to_i) - @specialties.pluck(:specialty_id)).map do |id|
      { specialty_id: id, dentist_id: @dentist.id }
    end
  end

  def self.hmo_options
    (@object[:hmos].to_s.split(';').map(&:to_i) - @hmos.pluck(:hmo_id)).map do |id|
      { hmo_id: id, accredited_type: 'User', accredited_id: @dentist.id }
    end
  end

  def self.reviewer_options
    {
      status:       @status,
      reviewed_at:  DateTime.now,
      reviewer_id:  @user.id
    }
  end

  def self.create_response(errors=nil)
    filtered_errors = errors.flatten.uniq.compact
    if filtered_errors.blank?
      @response[:success] = true
      @response[:details] = @dentist
    else
      @response[:success] = false
      @response[:details] = filtered_errors.join(', ')
    end

    @response
  end

  def build_attachment(image)
    attachment =  {
      :filename => image[:filename],
      :type     => image[:type],
      :headers  => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end
end
