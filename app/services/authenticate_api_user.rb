class AuthenticateAPIUser
  extend LightService::Organizer
  def self.call(email: "", password: "", role: "")
    with(
      email:    email,
      password: password,
      role:     role
    ).reduce(
      Actions::FindsUser,
      Actions::AuthenticatesUser,
      Actions::GeneratesAPIKey
    )
  end
end
