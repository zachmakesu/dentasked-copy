DentistDraftHandlerError = Class.new(StandardError)
class DentistDraftHandler
  VALID_MESSAGE = %(Dentist draft is valid)
  attr_reader :object, :params
  def initialize(params, opts={})
    @params = params.with_indifferent_access
    @object = Dentist.new(modified_params)
    @object.build_profile(profile_params)
  end

  def validate
    object.valid?
  end

  def errors
    object.errors.full_messages.join('\n')
  end

  private

  def dentist_params
    {
      first_name: params[:first_name],
      last_name: params[:last_name],
      email: params[:email],
      mobile_number: params[:mobile_number],
      birthdate: params[:birthdate],
      service_ids: params[:service_ids],
      hmo_ids: params[:hmo_ids],
      specialty_ids: params[:specialization_ids],
      avatar: params[:avatar]
    }
  end

  def profile_params
    {
      experience: params[:experience],
      fee: params[:fee],
      license_number: params[:license_number]
    }
  end

  def dentist_profile
    DentistProfile.new(profile_params)
  end

  def modified_params
    dentist_params.tap do |hash|
      avatar = hash.fetch(:avatar){ Hash.new }
      hash[:avatar] = build_attachment(avatar)
    end
  end

  def build_attachment(image)
    return nil unless image.present?
    attachment =  {
      :filename => image[:filename],
      :type     => image[:type],
      :headers  => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end
end
