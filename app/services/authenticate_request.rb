class AuthenticateRequest
  extend LightService::Organizer

  def self.call(authorization_header: "", params: {}, path: "")
    with(
      authorization_header: authorization_header,
      params: params,
      path: path,
      token: params.fetch(:access_token) { "" }
    ).reduce(
      Actions::ValidatesAuthorizationHeader,
      Actions::ValidatesAccessToken,
      Actions::ValidatesSignature
    )
  end
end
