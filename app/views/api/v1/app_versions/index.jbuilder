json.app_version do
  json.version_code @app_version.version_code
  json.category @app_version.category
  json.platform @app_version.platform

  json.features @app_version.features do |feature|
    json.id feature.id
    json.title feature.title
    json.details feature.details
  end
end
