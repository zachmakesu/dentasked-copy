json.data do
  json.array! @clinics do |clinic|
    json.name clinic.name
    json.location do
      json.lat clinic.lat
      json.long clinic.lng
    end
    json.mobile_number clinic.mobile_number
    json.logo dynamic_logo_url(clinic) 
  end
end
