json.data do
  json.id @clinic.id
  json.name @clinic.name
  json.address @clinic.address
  json.parking_slot @clinic.parking_slot
  json.mobile_number @clinic.mobile_number
  json.logo dynamic_logo_url(@clinic)
  json.logo_url dynamic_logo_url(@clinic)
  json.price @clinic.price
  json.lat @clinic.lat
  json.lng @clinic.lng
  json.is_open @clinic.is_open?
  json.favorited current_user.favorite_clinics.include?(@clinic) if current_user.patient?
  json.city @clinic.city.try(:full_name)
  json.schedules do
    json.array! dynamic_schedules(@clinic) do |day|
      json.day day
    end
  end
  json.services do
    json.array! @clinic.services.alphabetical do |service|
      json.id service.id
      json.name service.name
    end
  end
  json.dentists do
    json.array! @clinic.dentists do |dentist|
      json.uid dentist.uid
      json.avatar_url dynamic_avatar_url(dentist)
      json.fee dentist.profile.fee
      json.experience dentist.profile.experience
      json.name dentist.name_with_salutation
      json.first_name dentist.first_name
      json.last_name dentist.last_name
      json.specialization dentist.specialization
      json.hmo_accredited dentist.hmos.present?
      json.hmos do
        json.array! dentist.hmos do |hmo|
          json.id hmo.id
          json.name hmo.name
        end
      end
    end
  end
  json.photos do
    json.array! @clinic.photos do |photo|
      json.id photo.id
      json.url URI.join(dynamic_assets_url, photo.image.url).to_s
    end
  end
end
