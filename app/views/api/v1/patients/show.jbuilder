json.data do
  json.uid @patient.uid
  json.id @patient.id
  json.name @patient.name
  json.first_name @patient.first_name
  json.last_name @patient.last_name
  json.email @patient.email
  json.mobile_number @patient.mobile_number
  json.birthdate @patient.birthdate if @patient.birthdate.present?
  json.formatted_birthdate @patient.formatted_birthdate
  json.age @patient.age_string
  json.hmo @patient.has_hmo? ? @patient.last_hmo_used.name : "No HMO"
  json.work @patient.work
  json.gender @patient.gender.capitalize
  json.avatar_url dynamic_avatar_url(@patient)
  json.appointment_count @patient.appointments.for_date(DateTime.now.to_s).confirmed.count
  json.appointments do
    json.array! @patient.appointments.for_patient_profile.by_latest do |appointment|
      json.schedule appointment.formatted_schedule
      json.service appointment.service.name
      json.status appointment.status
    end
  end
end
