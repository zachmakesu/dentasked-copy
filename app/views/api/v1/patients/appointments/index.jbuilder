json.data do
  json.array! @appointments do |appointment|
    json.id appointment.id
    json.schedule_date appointment.schedule.iso8601
    json.created_at_date appointment.created_at.iso8601
    json.service do
      json.id appointment.service.id
      json.name appointment.service.name
    end
    if hmo = appointment.hmo
      json.hmo do
        json.id hmo.id
        json.name hmo.name
      end
    end
    json.status_id appointment[:status]
    json.specialty appointment.specialty
    json.note appointment.note
    json.days appointment.in_days
    json.hours_remaining appointment.time_remaining
    json.dentist do
      json.uid appointment.clinic_assignment.dentist.uid
      json.first_name appointment.clinic_assignment.dentist.first_name
      json.last_name appointment.clinic_assignment.dentist.last_name
      json.name appointment.clinic_assignment.dentist.name_with_salutation
      json.avatar_url dynamic_avatar_url(appointment.clinic_assignment.dentist)
      json.specialization appointment.clinic_assignment.dentist.specialization
      json.assigned_clinic appointment.clinic_assignment.clinic.name
      json.city appointment.clinic_assignment.clinic.city.full_name
    end
  end
end
