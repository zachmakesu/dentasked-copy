json.data do
  json.array! @dentists do |dentist|
    json.id dentist.id
    json.uid dentist.uid
    json.name dentist.name_with_salutation
    json.first_name dentist.first_name
    json.last_name dentist.last_name
    json.email dentist.email
    json.mobile_number dentist.mobile_number
    json.birthdate dentist.birthdate if dentist.birthdate.present?
    json.avatar dynamic_avatar_url(dentist)
    json.has_avatar dentist.avatar.present?
    json.today_appointments_count dentist.today_confirmed_appointments.count
    json.tomorrow_appointments_count dentist.tomorrow_confirmed_appointments.count
    json.specialization dentist.specialization

    json.services dentist.services_description
    json.services_raw do
      json.array! dentist.services do |service|
        json.id service.id
      end
    end

    json.hmo dentist.hmo_description
    json.hmos_raw do
      json.array! dentist.hmos do |hmo|
        json.id hmo.id
      end
    end

    json.specialties dentist.specialty_description
    json.specialties_raw do
      json.array! dentist.specialties do |specialty|
        json.id specialty.id
      end
    end

    json.license_number dentist.profile.license_number_description
    json.license_number_raw dentist.profile.license_number
    json.experience dentist.profile.experience_description
    json.experience_raw dentist.profile.experience
    json.fee dentist.profile.fee_description
    json.fee_raw dentist.profile.fee.to_f

    json.clinics do
      json.array! dentist.clinics do |clinic|
        json.name clinic.name
        json.address clinic.address
        json.schedule "MWF 09:00 AM - 09:00 PM"
        json.lat clinic.lat
        json.lng clinic.lng
      end
    end
  end
end
