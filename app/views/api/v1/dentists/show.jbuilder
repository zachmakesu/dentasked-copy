json.data do
  json.uid @dentist.uid
  json.email @dentist.email
  json.name @dentist.name_with_salutation
  json.first_name @dentist.first_name
  json.last_name @dentist.last_name
  json.mobile_number @dentist.mobile_number
  json.birthdate @dentist.birthdate if @dentist.birthdate.present?
  json.avatar_url dynamic_avatar_url(@dentist)
  json.fee @dentist.profile.fee_description
  json.fee_value @dentist.profile.fee
  json.experience @dentist.profile.experience_description
  json.specialization @dentist.specialization
  json.license_number @dentist.profile.license_number
  json.hmo_accredited @dentist.hmos.present?
  json.favorited current_user.favorite_dentists.include?(@dentist) if current_user.patient?

  unless @dentist.clinics.empty?
    json.assigned_clinic @dentist.clinics.first.name
    json.city @dentist.clinics.first.city.full_name
  end

  json.today_appointments_count @dentist.today_confirmed_appointments.count
  json.tomorrow_appointments_count @dentist.tomorrow_confirmed_appointments.count
  if clinic_assignment = @dentist.clinic_assignments.first
    json.clinic_assignment_id clinic_assignment.id
    json.schedules clinic_assignment.schedules.each do |schedule|
      json.id schedule.id
      json.day_index schedule.day
      json.start_time schedule.start_time.iso8601
      json.end_time schedule.end_time.iso8601
    end
  end

  json.specialties do
    json.array! @dentist.specialties do |specialty|
      json.id specialty.id
      json.name specialty.name
    end
  end
  json.services do
    json.array! @dentist.services.alphabetical do |service|
      json.id service.id
      json.name service.name
    end
  end
  json.hmos do
    json.array! @dentist.hmos do |hmo|
      json.id hmo.id
      json.name hmo.name
    end
  end
  json.clinics do
    json.array! @dentist.clinics do |clinic|
      json.id clinic.id
      json.logo dynamic_logo_url(clinic)
      json.name clinic.name
      json.address clinic.address
      json.lat clinic.lat
      json.lng clinic.lng
      json.city clinic.city.full_name
      json.is_open clinic.is_open?
      json.schedules do
        json.array! dynamic_schedules(clinic) do |day|
          json.day day
        end
      end
      json.parking_slot clinic.parking_slot
      json.photos clinic.photos do |photo|
        json.id photo.id
        json.url URI.join(dynamic_assets_url, photo.image.url).to_s
      end
    end
  end
  json.photos do
    json.array! @dentist.photos do |photo|
      json.id photo.id
      json.name photo.name
      json.url URI.join(dynamic_assets_url, photo.image.url).to_s
    end
  end
end
