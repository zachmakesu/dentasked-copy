json.data do
  json.uid @patient.uid
  json.name @patient.name
  json.email @patient.email
  json.mobile_number @patient.mobile_number
  json.age @patient.age_string
  json.location "Philippines"
  json.avatar dynamic_avatar_url(@patient)
  json.appointments_today_count @patient.appointments.for_today.confirmed.count
  json.hmo  @patient.hmo.name
  json.previous_appointments do
    json.array! @patient.appointments.for_agenda.elapsed.by_latest do |appointment|
      json.id appointment.id
      json.schedule appointment.formatted_schedule
      json.service appointment.service.name
      json.status appointment.status
    end
  end
  if @patient.last_appointment
    json.last_appointment do
      json.id @patient.last_appointment.id
      json.schedule @patient.last_appointment.formatted_schedule
      json.service @patient.last_appointment.service.name
      json.status @patient.last_appointment.status
      json.specialty @patient.last_appointment.specialty
      json.hmo @patient.last_appointment.hmo
      json.note @patient.last_appointment.note
      json.dentist do
        json.name @patient.last_appointment.clinic_assignment.dentist.name_with_salutation
        json.avatar_url dynamic_avatar_url(@patient.last_appointment.clinic_assignment.dentist) 
      end
      json.clinic do
        json.name @patient.last_appointment.clinic_assignment.clinic.name
        json.address @patient.last_appointment.clinic_assignment.clinic.address
      end
    end
  end
end
