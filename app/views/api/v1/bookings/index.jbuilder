json.data do
  json.array! @bookings do |booking|
    json.schedule booking.schedule
    json.service booking.dental_service.name
    json.operations do
      json.array! booking.booking_operations do |op|
        json.status op.status
        json.address op.clinic.address
        json.lat op.clinic.lat
        json.lng op.clinic.lng
      end
    end
  end
end
