json.token @token
json.uid @user.uid
json.missing_mobile_number true unless @user.linked_mobile_number?
