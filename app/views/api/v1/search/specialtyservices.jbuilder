json.data do
  json.array! @sands do |sand|
    json.array! sand do |s|
      json.id s.id
      json.name s.name
      json.type s.class.name.downcase
    end
  end
end
