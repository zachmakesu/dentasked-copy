json.data do
  json.array! @keywords do |keyword|
    json.name keyword.name
    json.id keyword.is_a?(Dentist) ? keyword.uid : keyword.id.to_s
    json.type keyword.class.name.downcase
    description, image = if keyword.is_a?(Dentist)
                           [keyword.clinics.first.name, dynamic_avatar_url(keyword)]
                         elsif keyword.is_a?(Clinic)
                           [keyword.display_schedule, dynamic_logo_url(keyword)]
                         elsif keyword.is_a?(Service)
                           ["#{keyword.duration} Minutes", ""]
                         else
                           ["", ""]
                         end
    json.description description
    json.image image
  end
end
