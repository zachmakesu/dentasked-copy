json.data do
  json.dentists do
    json.array! @dentists do |dentist|
      json.first_name dentist.first_name
      json.last_name dentist.last_name
      json.uid dentist.uid
      json.avatar dynamic_avatar_url(dentist)
      json.avatar_url dynamic_avatar_url(dentist)
      json.fee dentist.profile.fee
      json.name dentist.name_with_salutation
      json.address dentist.clinics.first.address unless dentist.clinics.empty?
      json.mobile_number dentist.mobile_number
      json.specialization dentist.specialization
      json.clinic_assignment_id dentist.clinic_assignments.first.id unless dentist.clinic_assignments.empty?
      json.assigned_clinic dentist.clinics.first.name unless dentist.clinics.empty?
      json.city(dentist.clinics.first.city.name) unless dentist.clinics.empty? || dentist.clinics.first.city.nil?
      json.experience dentist.profile.experience_description
    end
  end
  json.clinics do
    json.array! @clinics do |clinic|
      json.id clinic.id
      json.logo dynamic_logo_url(clinic)
      json.name clinic.name
      json.lat clinic.lat
      json.lng clinic.lng
      json.address clinic.address
      json.city clinic.city.try(:full_name)
      json.operation_status clinic.operation_status
      json.mobile_number clinic.mobile_number
      json.is_open clinic.is_open?
      json.parking_slot clinic.parking_slot
      json.photos clinic.photos do |photo|
        json.id photo.id
        json.url URI.join(dynamic_assets_url, photo.image.url).to_s
      end
      json.schedules do
        json.array! dynamic_schedules(clinic) do |day|
          json.day day
        end
      end
    end
  end
end
