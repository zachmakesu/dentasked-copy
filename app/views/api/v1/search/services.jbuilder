json.data do
  json.array! @services do |service|
      json.id service.id
      json.name service.name
      json.duration service.duration
  end
end
