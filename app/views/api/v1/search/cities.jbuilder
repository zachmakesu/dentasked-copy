json.data do
  json.array! @grouped_cities do |region, cities|
    json.id     region.id
    json.name   region.name
    json.cities cities.sort_by(&:name) do |city|
      json.id     city.id
      json.name   city.name
    end
  end
end
