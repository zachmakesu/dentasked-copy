json.data do
  json.agreed_to_terms @secretary.agreed_to_terms?
  json.linked_google_account @secretary.linked_google_account?
  json.clinic do
    json.id @clinic.id
    json.name @clinic.name
    json.logo dynamic_logo_url(@clinic)
  end
  json.dentists do
    json.array! @secretary.dentists do |dentist|
      json.id dentist.uid
      json.uid dentist.uid
      json.name dentist.name_with_salutation
      json.avatar dynamic_avatar_url(dentist)
      # TODO: Remove redundant field for avatar and uid
      # to conform to ios model req
      json.avatar_url dynamic_avatar_url(dentist)
      json.specialization dentist.specialization
      json.color dentist.profile.color
      # TODO: Relate secretary and clinic
      json.clinic_assignment_id dentist.clinic_assignments.first.id if dentist.clinic_assignments.first
    end
  end
end
