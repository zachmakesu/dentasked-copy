json.data do
  json.name @patient.name
  json.uid @patient.uid
  json.avatar_url dynamic_avatar_url(@patient) 
  json.agreed_to_terms @patient.agreed_to_terms?
end
