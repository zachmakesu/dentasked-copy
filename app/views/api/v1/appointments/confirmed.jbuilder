json.data do
  json.array! @appointments do |date, appointments|
    json.date date
    json.count appointments.count
  end
end
