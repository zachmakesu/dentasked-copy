json.data do
  json.times do
    json.array! @times do |time|
      json.schedule time
    end
  end
  json.dates do
    json.array! @dates do |date|
      json.schedule date.iso8601
    end
  end
end
