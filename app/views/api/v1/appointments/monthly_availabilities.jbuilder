json.data do
  json.array! @start_date..@end_date do |date|
    json.date date.iso8601
    json.has_available Appointment.available_times(@dentist, date.iso8601)["data"].any?{ |schedule| schedule[:available] }
  end
end
