json.data do
  json.service @appointment.service.name
  json.schedule @appointment.schedule.iso8601
  json.clinic do
    json.name @clinic.name
    json.address @clinic.address
    json.parking_slots @clinic.parking_slot
    json.mobile_number @clinic.mobile_number
    json.logo_url dynamic_logo_url(@clinic)
    json.price @clinic.price
    json.dentists do
      json.array! @clinic.dentists do |dentist|
        json.name dentist.name
        json.photos do
          json.array! @clinic.photos do |photo|
            json.id photo.id
            json.photo_url URI.join(dynamic_assets_url, photo.image.url).to_s
          end
        end
      end
    end
    json.photos do
      json.array! @clinic.photos do |photo|
        json.id photo.id
        json.photo_url URI.join(dynamic_assets_url, photo.image.url).to_s
      end
    end
  end
end
