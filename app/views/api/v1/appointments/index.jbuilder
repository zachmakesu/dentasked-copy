json.data do
  json.array! @appointments do |appointment|
    json.id appointment.id
    json.has_schedule_conflict appointment.has_schedule_conflict?
    json.schedule appointment.formatted_schedule
    json.schedule_date appointment.schedule.iso8601
    json.created_at_date appointment.created_at.iso8601
    # TODO: Remove redundant keys for properties once app is finalized i.e. HMO
    # & service
    json.service appointment.service.name
    json.service_object do
      json.id appointment.service.id
      json.name appointment.service.name
    end
    json.hmo appointment.hmo ? appointment.hmo.name : ""
    if hmo = appointment.hmo
      json.hmo_object do
        json.id hmo.id
        json.name hmo.name
      end
    end
    json.status appointment.status
    json.status_id appointment[:status]
    json.specialty appointment.specialty
    json.note appointment.note
    json.days appointment.in_days
    json.hours_remaining appointment.time_remaining
    json.secretary do
      json.name appointment.clinic_assignment.dentist.secretaries.first.name if appointment.clinic_assignment.dentist.secretaries.present?
      json.mobile_number appointment.clinic_assignment.dentist.secretaries.first.mobile_number if appointment.clinic_assignment.dentist.secretaries.present?
    end
    json.dentist do
      json.name appointment.clinic_assignment.dentist.name_with_salutation
      json.avatar_url dynamic_avatar_url(appointment.clinic_assignment.dentist)
      json.color appointment.clinic_assignment.dentist.profile.color
    end
    json.clinic do
      json.name appointment.clinic_assignment.clinic.name
      json.address appointment.clinic_assignment.clinic.address
      json.city appointment.clinic_assignment.clinic.city.name
    end
    json.patient do
      json.uid appointment.patient.uid
      json.name appointment.patient.name
      json.avatar dynamic_avatar_url(appointment.patient)
      json.has_avatar appointment.patient.avatar.present?
      json.age appointment.patient.age_string
      json.location "Philippines"
      json.mobile_number appointment.patient.mobile_number
    end
  end
end
