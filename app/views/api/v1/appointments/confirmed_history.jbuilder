json.data do
  json.array! @appointments do |date, appointments|
    json.date date
    json.appointments appointments do |appointment|
      json.id appointment.id
      json.schedule_date appointment.schedule.iso8601
      json.created_at_date appointment.created_at.iso8601
      json.service do
        json.id appointment.service.id
        json.name appointment.service.name
      end
      if hmo = appointment.hmo
        json.hmo_object do
          json.id hmo.id
          json.name hmo.name
        end
      end
      json.status appointment.status
      json.status_id appointment[:status]
      json.specialty appointment.specialty
      json.note appointment.note
      json.clinic do
        json.name appointment.clinic_assignment.clinic.name
        json.address appointment.clinic_assignment.clinic.address
        json.city appointment.clinic_assignment.clinic.city.name
      end
      json.patient do
        json.uid appointment.patient.uid
        json.name appointment.patient.name
        json.avatar dynamic_avatar_url(appointment.patient)
        json.has_avatar appointment.patient.avatar.present?
        json.age appointment.patient.age_string
      end
    end

  end
end
