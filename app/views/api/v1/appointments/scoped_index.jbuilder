json.data do
  json.array! @appointments do |year, appointments|
    json.year year
    json.data appointments do |appointment|
      json.id appointment.id
      json.schedule appointment.formatted_schedule
      json.service appointment.service.name
      json.status appointment.status
      json.specialty appointment.specialty
      json.hmo appointment.hmo
      json.note appointment.note
      json.dentist do
        json.name appointment.clinic_assignment.dentist.name_with_salutation
        json.avatar_url dynamic_avatar_url(appointment.clinic_assignment.dentist)
      end
      json.clinic do
        json.name appointment.clinic_assignment.clinic.name
        json.address appointment.clinic_assignment.clinic.address
      end
      json.patient do
        json.name appointment.patient.name
        json.avatar dynamic_avatar_url(appointment.patient)
        json.age appointment.patient.age_string
        json.location "Philippines"
        json.mobile_number appointment.patient.mobile_number
      end
    end
  end
end
