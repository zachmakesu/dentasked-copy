json.data do
  json.id @appointment.id
  json.schedule @appointment.schedule_in_words
  json.service @appointment.service.name
  json.status @appointment.status
  json.status_id @appointment[:status]
  json.specialty @appointment.specialty
  json.hmo @appointment.hmo.present?
  json.note @appointment.show_note
  json.dentist do
    json.name @appointment.clinic_assignment.dentist.name_with_salutation
    json.mobile_number @appointment.clinic_assignment.dentist.mobile_number
    json.fee @appointment.clinic_assignment.dentist.profile.fee
    json.experience @appointment.clinic_assignment.dentist.profile.experience
    json.hmo @appointment.clinic_assignment.dentist.hmos.present?
    json.avatar_url dynamic_avatar_url(@appointment.clinic_assignment.dentist)
    json.specialty @appointment.clinic_assignment.dentist.specialties.first.name unless @appointment.clinic_assignment.dentist.specialties.empty?
  end
  json.clinic do
    json.name @appointment.clinic_assignment.clinic.name
    json.mobile_number @appointment.clinic_assignment.clinic.mobile_number
    json.address @appointment.clinic_assignment.clinic.address
    json.city(@appointment.clinic_assignment.clinic.city.name) unless @appointment.clinic_assignment.clinic.city.blank?
    json.lat @appointment.clinic_assignment.clinic.lat
    json.lng @appointment.clinic_assignment.clinic.lng
    json.hmo @appointment.clinic_assignment.clinic.hmos.present?
  end
  if @appointment.clinic_assignment.dentist.secretaries.present?
    json.secretary do
      json.name @appointment.clinic_assignment.dentist.secretaries.first.name
      json.mobile_number @appointment.clinic_assignment.dentist.secretaries.first.mobile_number
    end
  end
end
