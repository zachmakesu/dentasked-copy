json.data do
  json.array! @specialties do |specialty|
    json.id specialty.id
    json.name specialty.name
  end
end
