json.data do
  json.array! @rating_suggestions do |rating_suggestion|
    json.name rating_suggestion.name
  end
end
