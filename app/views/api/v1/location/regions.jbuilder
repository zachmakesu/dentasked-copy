json.data do
  json.array! @regions do |region|
    json.id region.id
    json.name region.name
  end
end
