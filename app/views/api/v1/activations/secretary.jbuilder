json.data do
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.email_address @user.email
  json.mobile_number @user.mobile_number
  json.birthdate @user.birthdate
end
