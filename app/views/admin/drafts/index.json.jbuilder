json.array!(@admin_drafts) do |admin_draft|
  json.extract! admin_draft, :id
  json.url admin_draft_url(admin_draft, format: :json)
end
