class NotificationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.daily_appointments.subject
  #
  def daily_appointments(dentist)
    @dentist = dentist
    @appointments = @dentist.today_confirmed_appointments

    mail to: "#{@dentist.email}", subject: "Your appointments for today"
  end

  def no_appointment(dentist)
    @dentist = dentist

    mail to: "#{@dentist.email}", subject: "No appointments for today"
  end
end
