class UserMailer < ApplicationMailer
  def welcome_email(user_id)
    @user = User.find(user_id)
    mail to: "#{@user.email}", subject: "Thank you for using DentaSked!"
  end

  def welcome_patient(user_id)
    @user = Patient.find(user_id)
    mail to: "#{@user.email}", subject: "Thank you for signing up, #{@user.first_name}!"
  end
end
