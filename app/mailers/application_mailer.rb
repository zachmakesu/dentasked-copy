class ApplicationMailer < ActionMailer::Base
  default from: "noreply@dentasked.com"
  layout 'mailer'
end
