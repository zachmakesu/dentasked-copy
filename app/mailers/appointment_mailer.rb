class AppointmentMailer < ApplicationMailer

  def request_appointment(appointment_id)
    @booking = Appointment.find(appointment_id)
    @patient = @booking.patient

    mail to: "#{@patient.email}", subject: "Thank you for using DentaSked!"
  end

end
