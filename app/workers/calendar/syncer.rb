require 'google/api_client'
require 'signet/oauth_2/client'

module Calendar
  class Syncer
    include Sidekiq::Worker

    def perform(id, user_id)
      @user = User.find(user_id)
      return unless @user.linked_google_account?

      @appointment = Appointment.find(id)
      @dentist = @appointment.clinic_assignment.dentist
      @user.identities.google.refresh_google_token if @user.identities.google.expired?

      client = Google::APIClient.new
      client.authorization.access_token = @user.identities.google.oauth_token
      calendar_api = client.discovered_api('calendar', 'v3')

      event = {
        'summary' => @appointment.service.name,
        'location' => @appointment.clinic_assignment.clinic.address,
        'description' => "#{@appointment.service.name} with #{@dentist.name_with_salutation}",
        'start' => {
          'dateTime' => @appointment.schedule.to_datetime.rfc3339,
          'timeZone' => 'Asia/Manila',
        },
        'end' => {
          'dateTime' => (@appointment.schedule + @appointment.service.duration.minutes).to_s.to_datetime.rfc3339,
          'timeZone' => 'Asia/Manila',
        }
      }

      if @user.secretary?
        event.merge!(
          'description' => "#{@appointment.service.name} with #{@appointment.patient.name}",
          'attendees' => [
            {'email' => @user.email },
            {'email' => @dentist.email }
          ]
        )
      end

      results = client.execute!(
        :api_method => calendar_api.events.insert,
        :parameters => {
          :calendarId => 'primary'},
          :body_object => event)
      event = results.data
      Rails.logger.info "Event created: #{event.htmlLink}"
    end

  end
end
