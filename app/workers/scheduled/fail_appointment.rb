module Scheduled
  class FailAppointment
    include Sidekiq::Worker

    def perform(id)
      @appointment = Appointment.find_by_id(id)
      if @appointment.present? && @appointment.pending?
        @appointment.failed! 
      end
    end
  end
end
