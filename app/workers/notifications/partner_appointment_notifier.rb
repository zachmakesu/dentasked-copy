module Notifications
  class PartnerAppointmentNotifier
    include Sidekiq::Worker

    def perform(id)
      @appointment = Appointment.find(id)
      Notifications::SecretaryAppointmentHandler.new(@appointment).deliver
    end
  end
end
