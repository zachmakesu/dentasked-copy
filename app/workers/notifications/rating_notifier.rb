module Notifications
  class RatingNotifier
    include Sidekiq::Worker
    def perform(id)
      @appointment = Appointment.find(id)
      Notifications::RatingHandler.new(@appointment).deliver
    end
  end
end
