module Notifications
  class DayReminderNotifier
    include Sidekiq::Worker

    def perform(id)
      @appointment = Appointment.find(id)
      handler = Notifications::DayReminderHandler.new(@appointment)
      handler.deliver if handler.valid?
    end
  end
end
