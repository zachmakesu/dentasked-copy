module Notifications
  class ConsumerAppointmentNotifier
    include Sidekiq::Worker

    def perform(id)
      @appointment = Appointment.find_by(id: id)
      return if @appointment.nil? || @appointment.offline?
      if @appointment.failed? || @appointment.declined?
        SMS::PatientBookingAlternativeSender.perform_async(id)
      end
      Notifications::PatientAppointmentHandler.new(@appointment).deliver
    end
  end
end
