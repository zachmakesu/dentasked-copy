module SMS
  class PatientBookingRequestSender
    include Sidekiq::Worker
    def perform(id, options={})
      walkin = options.fetch("walkin"){ false }
      notification = SMS::Notification::PatientBookingRequest.new(appointment_id: id, walkin: walkin)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
