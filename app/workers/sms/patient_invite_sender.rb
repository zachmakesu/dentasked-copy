module SMS
  class PatientInviteSender
    include Sidekiq::Worker
    def perform(current_user_id, options={})
      inviter = User.find(current_user_id)
      mobile_number = options.fetch("mobile_number"){ nil }
      contact_name = options.fetch("contact_name"){ nil }
      notification = SMS::Notification::PatientInvite.new(name: contact_name, number: mobile_number, inviter: inviter)
      sms_handler = SMSHandler.new(notification: notification)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
