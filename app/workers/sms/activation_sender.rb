module SMS
  class ActivationSender
    include Sidekiq::Worker
    def perform(id, options={})
      resend = options.fetch("resend"){ false }
      mobile_number = options.fetch("mobile_number"){ nil }
      notification = SMS::Notification::Activation.new(activation_id: id, mobile_number: mobile_number, resend: resend)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
