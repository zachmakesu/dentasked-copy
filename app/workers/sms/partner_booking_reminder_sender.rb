module SMS
  class PartnerBookingReminderSender
    include Sidekiq::Worker
    def perform(id, options={})
      notification = SMS::Notification::PartnerBookingReminder.new(appointment_id: id)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
