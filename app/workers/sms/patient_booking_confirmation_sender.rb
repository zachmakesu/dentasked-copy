module SMS
  class PatientBookingConfirmationSender
    include Sidekiq::Worker
    def perform(id, options={})
      notification = SMS::Notification::PatientBookingConfirmation.new(appointment_id: id)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
