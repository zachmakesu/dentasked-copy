module SMS
  class ResetPasswordInstructionsSender
    include Sidekiq::Worker
    def perform(user_id, token, options={})
      user = User.find(user_id)
      notification = SMS::Notification::ResetPasswordInstructions.new(user: user, token: token)
      sms_handler = SMSHandler.new(notification: notification)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
