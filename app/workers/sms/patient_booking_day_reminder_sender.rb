module SMS
  class PatientBookingDayReminderSender
    include Sidekiq::Worker
    def perform(id, options={})
      notification = SMS::Notification::PatientBookingDayReminder.new(appointment_id: id)
      if notification.valid?
        sms_handler = SMSHandler.new(notification: notification)
        sms_handler.()
      end
    end
  end
end
