module AdminNotifications
  class DraftNotifier
    include Sidekiq::Worker
    def perform(id)
      @draft = Draft.find_by(id: id)
      Notifications::DraftHandler.new(@draft).deliver
    end
  end
end
