class ScheduledJob < ActiveRecord::Base
  validates_presence_of :jid, :schedulable_id, :schedulable_type
  belongs_to :schedulable, polymorphic: true
end

# == Schema Information
#
# Table name: scheduled_jobs
#
#  created_at       :datetime         not null
#  id               :integer          not null, primary key
#  jid              :string           not null
#  schedulable_id   :integer
#  schedulable_type :string
#  updated_at       :datetime         not null
#
