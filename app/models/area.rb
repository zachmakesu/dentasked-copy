# == Schema Information
#
# Table name: areas
#
#  base_address :string
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  lat          :float
#  lng          :float
#  name         :string
#  updated_at   :datetime         not null
#

class Area < ActiveRecord::Base
  geocoded_by :base_address, latitude: :lat, longitude: :lng
  after_validation :geocode, if: ->(obj){ obj.base_address.present? && obj.base_address_changed? }
  validates_presence_of :name
  validates_uniqueness_of :name

  def self.search q = nil
    query = "%#{q}%"
    where('name ILIKE ?', query)
  end

end
