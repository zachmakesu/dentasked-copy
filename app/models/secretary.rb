# == Schema Information
#
# Table name: users
#
#  avatar_content_type        :string
#  avatar_file_name           :string
#  avatar_file_size           :integer
#  avatar_updated_at          :datetime
#  confirmation_sent_at       :datetime
#  confirmation_token         :string
#  confirmed_at               :datetime
#  created_at                 :datetime
#  current_sign_in_at         :datetime
#  current_sign_in_ip         :string
#  disabled_at                :datetime
#  email                      :string           default(""), not null
#  encrypted_birthdate        :string
#  encrypted_birthdate_iv     :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  encrypted_password         :string           default(""), not null
#  first_name                 :string
#  id                         :integer          not null, primary key
#  last_name                  :string
#  last_sign_in_at            :datetime
#  last_sign_in_ip            :string
#  reset_password_sent_at     :datetime
#  reset_password_token       :string
#  sign_in_count              :integer          default(0), not null
#  terms_agreed_at            :datetime
#  uid                        :string           default(""), not null
#  unconfirmed_email          :string
#  updated_at                 :datetime
#

class Secretary < User
  self.default_scope -> { secretaries }
  before_validation :generate_random_password, on: :create
  after_create :assign_secretary_role

  has_many :secretary_assignments
  has_many :dentists, through: :secretary_assignments
  has_many :processed_appointments, class_name: "AppointmentHistory", foreign_key: :processor_id
  has_many :drafts, foreign_key: :owner_id

  has_one :profile, class_name: "SecretaryProfile", foreign_key: :user_id, dependent: :destroy

  def add_dentist(dentist)
    return nil if dentists.include?(dentist)
    SecretaryAssignment.create!(dentist_id: dentist.id, secretary_id: id)
  end

  def clinics
    dentists.includes(:clinics).map(&:clinics).flatten.uniq
  end

  private

  def generate_random_password
    generated_password = Devise.friendly_token.first(8)
    self.password = generated_password
  end

  def assign_secretary_role
    make_secretary!
  end
end
