# == Schema Information
#
# Table name: hmo_accreditations
#
#  accredited_id   :integer
#  accredited_type :string
#  created_at      :datetime         not null
#  hmo_id          :integer
#  id              :integer          not null, primary key
#  updated_at      :datetime         not null
#

class HMOAccreditation < ActiveRecord::Base
  belongs_to :hmo
  belongs_to :accredited, polymorphic: true

  validates_uniqueness_of :hmo, scope: [:accredited_id, :accredited_type], message: 'has this accrediation already'
end
