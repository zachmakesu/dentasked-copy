class UserRole < ActiveRecord::Base
  after_create :ensure_role_specific_profile
  validates_uniqueness_of :user_id, scope: :role_id
  belongs_to :user
  belongs_to :role

  private

  def ensure_role_specific_profile
    role_profile = "#{role.name.titleize}Profile".constantize
    role_profile.find_or_create_by(user_id: user.id)
  end
end

# == Schema Information
#
# Table name: user_roles
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  role_id    :integer          not null
#  updated_at :datetime         not null
#  user_id    :integer          not null
#
