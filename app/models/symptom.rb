# == Schema Information
#
# Table name: symptoms
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

class Symptom < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  has_many :preferences, dependent: :destroy, class_name: 'BookingPreference'

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["Name","Description","created_at"]

      all.each do |symptom|
        csv << [symptom.name,symptom.description,symptom.created_at]
      end
    end
  end

end
