# == Schema Information
#
# Table name: appointments
#
#  clinic_assignment_id :integer          not null
#  code                 :string
#  created_at           :datetime         not null
#  hmo_id               :integer
#  id                   :integer          not null, primary key
#  note                 :text
#  offline              :boolean          default(FALSE)
#  patient_id           :integer
#  patient_record_id    :integer
#  schedule             :datetime         not null
#  service_id           :integer          not null
#  specialty_id         :integer
#  status               :integer          not null
#  updated_at           :datetime         not null
#

class Appointment < ActiveRecord::Base
  EXPIRATION = 48.hours
  before_validation :set_default_status, on: :create
  after_commit :scheduled_failure, on: :create, if: Proc.new { |record| !record.offline? }
  after_commit :send_consumer_appointment_notification, on: :update, if: Proc.new { |record| !record.offline? }
  after_commit :send_partner_appointment_notification, on: :create, if: Proc.new { |record| !record.offline? }
  after_commit :send_reminder_notification, on: :update, if: Proc.new { |record| !record.offline? }

  enum status: [ :pending, :confirmed, :declined, :cancelled, :noshow, :complete, :failed  ]
  belongs_to  :patient_user, foreign_key: :patient_id, class_name: 'User'
  belongs_to :patient
  belongs_to :clinic_assignment
  belongs_to :service
  belongs_to :hmo
  belongs_to :specialty

  has_many :ratings, as: :context, dependent: :destroy
  has_many :reschedules, dependent: :destroy
  has_many :histories, class_name: "AppointmentHistory", dependent: :destroy
  has_many :scheduled_jobs, as: :schedulable, dependent: :destroy

  validates :status, inclusion: { in: Appointment.statuses.keys }
  validates_presence_of :schedule, :service_id, :clinic_assignment_id
  validate :no_past_date_appointments
  validate :no_same_day_appointments
  validates :code, uniqueness: true, allow_blank: true

  scope :recent, -> { order(created_at: :desc) }
  scope :for_agenda, -> { where("status NOT IN (?)", [0, 2, 6]) }
  scope :new_appointments, -> { where(status: "pending") }
  scope :confirmed_appointments, -> { where("status = ? OR status = ?", 1, 5) }
  scope :failed_appointments, -> { where("status = ? OR status = ? OR status = ?", 2, 3, 4) }
  scope :expired_appointments, -> { where("status = ?", 6) }
  scope :for_patient, -> { where("status = ? OR status = ? OR status = ?", 1, 3, 6) }
  scope :for_patient_profile, -> { where("status = ? OR status = ? OR status = ?", 3, 4, 5) }
  scope :offline, -> { where("offline IS TRUE") }
  scope :online, -> { where("offline IS FALSE") }

  SCHEDULE_RANGE = { morning: (DateTime.now.beginning_of_day + 6.hours)..(DateTime.now.middle_of_day),
                     afternoon: (DateTime.now.middle_of_day)...(DateTime.now.middle_of_day + 6.hours),
                     evening: (DateTime.now.middle_of_day + 6.hours)..(DateTime.now.end_of_day - 4.hours)
  }

  def self.by_earliest
    sort_by_schedule('ASC')
  end

  def self.by_latest
    sort_by_schedule('DESC')
  end

  def self.elapsed
    joins("LEFT OUTER JOIN reschedules ON (appointments.id = reschedules.appointment_id AND reschedules.is_active IS TRUE)")
      .where("(reschedules.schedule < :today AND reschedules.is_active IS TRUE) OR appointments.schedule < :today", today: Date.today)
  end

  def self.for_date(datestring = DateTime.now.iso8601)
    date = DateTime.parse(datestring).change(offset: '+8')
    appointment_ids = where(schedule: (date.at_beginning_of_day.utc..date.at_end_of_day)).reject{|a|a.rescheduled?}.map{|a| a.id if a }
    reschedule_ids = Reschedule.active.where(schedule: (date.at_beginning_of_day.utc..date.at_end_of_day)).map{|r| r.appointment.id if r.appointment }
    where(id: (appointment_ids << reschedule_ids).flatten.uniq)
  end

  def self.in_month(datestring = DateTime.now.iso8601)
    date = DateTime.parse(datestring).change(offset: '+8')
    where(schedule: (date.beginning_of_month.utc..date.end_of_month))
  end

  def self.availability(datestring = DateTime.now.iso8601, service_id)
    service = service_id ? Service.find(service_id) : Service.first
    time = DateTime.parse(datestring)
    where(schedule: ((time - service.duration.minutes)..(time + service.duration.minutes)))
  end

  def self.preferred_time(freetime)
    where(schedule: SCHEDULE_RANGE[freetime.to_sym]) if SCHEDULE_RANGE.keys.include?(freetime.to_sym)
  end

  def self.for_today
    self.for_date(DateTime.now.iso8601)
  end

  def self.for_tomorrow
    self.for_date((DateTime.now + 1.day).iso8601)
  end

  def self.generate_code(platform: nil)
    uuid = SecureRandom.uuid
    validated_platform = Device::SUPPORTED_DEVICES.include?(platform.to_s.downcase) ? platform : 'legacy'
    "#{validated_platform}-#{uuid}".upcase
  end

  def patient
    patient_id.nil? ? PatientRecord.find(patient_record_id) : super
  end

  def show_note
    note.blank? ? "No note provided" : self.note
  end

  def expires_at
    created_at + Appointment::EXPIRATION
  end

  def available_times(date = self.schedule, start_time = self.schedule, end_time = self.schedule.change(hour: 19))
    interval = self.service.duration.minutes
    @appointments = Appointment.confirmed.for_date(date.to_s)

    unless date == self.schedule
      start_time = start_time.change(day: date.day, month: date.month, year: date.year)
      end_time = end_time.change(day: date.day, month: date.month, year: date.year)
    end

    @available_times = []
    (@available_times << (start_time += interval).iso8601) while start_time < end_time

    @available_times.select! do |time|
      next true if @appointments.empty?
      @appointments.any? do |a|
        !a.has_schedule_conflict? time.to_s
      end
    end

    @available_times.flatten.uniq
  end

  def self.available_times(dentist, date)
    # base DateTime schedule on +8 tz
    parsed_date = DateTime.parse(date).change(offset: '+8')
    clinic_assignments = dentist.clinic_assignments

    # default clinic hours if there are no schedules
    # 9   = 9AM
    # 16  = 4PM
    start_hour = 9
    end_hour = 16

    # check if dentist actually has a schedule for said date
    # currently blindly checks all dentist's schedule
    # TODO: use clinic_assignment reference instead
    if schedule_for_day = clinic_assignments.map(&:schedules).flatten.select{|d| d.day == parsed_date.wday }.first
      start_hour = schedule_for_day.start_time.hour
      end_hour = (schedule_for_day.end_time.hour - 1)
    end

    # filter appointments for this dentist only!
    # use clinic_assignment reference
    appointments = Appointment.where(clinic_assignment_id: clinic_assignments.map(&:id)).confirmed.for_date(date.to_s)
    {}.tap do |response|
      response['data'] = [].tap do |times|
        for i in start_hour..end_hour
          time = {}
          start_schedule = parsed_date.change(hour: i)
          end_schedule = parsed_date.change(hour: i+1)
          time[:schedule] = "#{start_schedule.strftime('%I:%M%p')} - #{end_schedule.strftime('%I:%M%p')}"
          time[:hour] = i
          # unavailable if conflict with existing appointments
          # or there are no schedules for said day!
          time[:available] = !!schedule_for_day && !parsed_date.today? && !appointments.any?{|a| a.schedule.hour >= i && (a.schedule.hour + 1) <= i+1}
          times << time
        end
      end
    end
  end

  def available_dates
    start_date = self.schedule + 1.day
    end_date = self.schedule + 2.days
    @available_dates = []
    while start_date < end_date do
      @available_dates << self.available_times(start_date)
      start_date += 1.day
    end

    @available_dates.flatten.uniq
  end

  def is_due?
    DateTime.now > self.schedule ? true : false
  end

  def count_days_before
    (self.schedule.to_date - DateTime.now.to_date).to_i unless self.is_due?
  end

  def in_days
    if self.confirmed? && !self.is_due?
      if self.count_days_before <= 0
        "Less than a day"
      else
        "In #{self.count_days_before} #{'day'.pluralize(self.count_days_before)}"
      end
    else
      "Past due"
    end
  end

  def time_diff
    seconds_diff = (Time.zone.now - (self.created_at.to_time + 24.hours)).to_i.abs

    @hours = seconds_diff / 3600
    seconds_diff -= @hours * 3600

    @minutes = seconds_diff / 60
    seconds_diff -= @minutes * 60

    @seconds = seconds_diff
  end

  def time_remaining
    time_diff
    if self.pending?
      @hours.zero? ? "#{@minutes} mins" : "#{@hours} hrs"
    end
  end

  def dentist
    self.clinic_assignment.dentist
  end

  def schedule
    reschedules.empty? ? self[:schedule] : reschedules.last.schedule
  end

  def reschedule(new_schedule)
    return false unless can_be_rescheduled?
    datetime = if new_schedule.is_a? String
                 DateTime.strptime(new_schedule, '%d/%m/%Y %I:%M %p').change(offset: '+8')
               else
                 new_schedule.to_datetime
               end
    ActiveRecord::Base.transaction do
      reschedules.each(&:deactivate)
      reschedules.create!(schedule: datetime, is_active: true)
      histories.create(status_from: self.status, status_changed_to: self.status, rescheduled: true)
    end
  end

  def rescheduled?
    !reschedules.empty?
  end

  def can_be_rescheduled?
    confirmed? || pending?
  end

  def formatted_time
    schedule.strftime("%I:%M %p")
  end

  def formatted_schedule
    schedule.strftime("%B %e, %Y - %l:%M %p")
  end

  def schedule_in_words
    schedule.strftime("%a, %b %e, %Y at %l:%M %p")
  end

  def has_schedule_conflict?(schedule = self.schedule)
    !clinic_assignment.appointments.for_date(schedule.to_s).confirmed.availability(schedule.to_s, self.service_id).count.zero?
  end

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << %w{ Patient Patient_Email Patient_Number Service Clinic Schedule Status Code Walk_In Created_At}
      find_each do |appointment|
        csv << ["#{appointment.patient.first_name} #{appointment.patient.last_name}", appointment.patient.email, appointment.patient.mobile_number, appointment.service.name, appointment.clinic_assignment.clinic.name, appointment.schedule, appointment.status, appointment.code.to_s, appointment.offline, appointment.created_at]
      end
    end
  end

  def alternatives(limit: 3)
    dentists = Dentist.service_filter(service_id)
    dentists = dentists.where.not(id: dentist.id)
    dentists = dentists.hmo_filter(hmo_id) if hmo_id.present?
    pending_appointments = Appointment.for_date(schedule.to_s).pending.availability(schedule.to_s, service_id).includes(clinic_assignment: :dentist)
    confirmed_appointments = Appointment.for_date(schedule.to_s).confirmed.availability(schedule.to_s, service_id).includes(clinic_assignment: :dentist)
    filtered_clinic_assignments = [confirmed_appointments.map(&:clinic_assignment_id), dentist.clinic_assignment_ids].flatten.uniq
    dentists << pending_appointments.where.not(clinic_assignment_id: filtered_clinic_assignments).map { |a| a.clinic_assignment.dentist }
    dentists.flatten.compact.uniq.take(limit)
  end

  private

  def set_default_status
    self.status = offline ? :confirmed : :pending
  end

  def send_consumer_appointment_notification
    if self.patient.instance_of? Patient
      Notifications::ConsumerAppointmentNotifier.perform_async(self.id) if self.confirmed? || self.failed? || self.declined? || self.complete?
      Notifications::RatingNotifier.perform_async(self.id) if self.complete?
    end
  end

  def send_partner_appointment_notification
    Notifications::PartnerAppointmentNotifier.perform_async(self.id) if self.pending?
  end

  def send_reminder_notification
    if self.confirmed? && (self.patient.instance_of? Patient)
      clear_existing_reminders

      # Send a notification 24 hours before the actual appointment schedule
      schedule_1_day_before = self.schedule - 24.hours
      if (schedule_1_day_before) > DateTime.now
        day_push_reminder = Notifications::DayReminderNotifier.perform_at(schedule_1_day_before, self.id)
        scheduled_jobs.create!(jid: day_push_reminder)

        day_sms_reminder = SMS::PatientBookingDayReminderSender.perform_at(schedule_1_day_before, self.id)
        scheduled_jobs.create!(jid: day_sms_reminder)
      end

      hour_sms_reminder = SMS::PatientBookingHourReminderSender.perform_at(self.schedule - 3.hours, self.id)
      scheduled_jobs.create!(jid: hour_sms_reminder)
    end
  end

  def scheduled_failure
    SMS::PartnerBookingReminderSender.perform_in(Appointment::EXPIRATION - 8.hours, self.id)
    Scheduled::FailAppointment.perform_in(Appointment::EXPIRATION, self.id)
  end

  def self.sort_by_schedule(order = "ASC")
    joins("LEFT OUTER JOIN reschedules r ON (appointments.id = r.appointment_id AND r.is_active IS TRUE)")
      .order("r.schedule, appointments.schedule #{order}")
  end

  def clear_existing_reminders
    scheduled_jobs.each do |job|
      j = Sidekiq::ScheduledSet.new.find_job(job.jid)
      j.delete if j.present?
    end
    scheduled_jobs.destroy_all
  end

  before_validation do
    self.histories.build(status_from: self.status_was, status_changed_to: self.status) if self.changed.include?("status")
  end

  def no_past_date_appointments
    if schedule.present? && (schedule.past? && !schedule.today?) && schedule_changed?
      errors.add(:schedule, 'should not be in the past')
    end
  end

  def no_same_day_appointments
    if !offline? && schedule.present? && schedule.today? && schedule_changed?
      errors.add(:schedule, 'should not be today')
    end
  end
end
