# == Schema Information
#
# Table name: dentist_profiles
#
#  activated_at                :datetime
#  activation_code             :string           not null
#  color                       :string           default("#2AA6A4"), not null
#  created_at                  :datetime         not null
#  encrypted_license_number    :string
#  encrypted_license_number_iv :string
#  experience                  :integer
#  fee                         :decimal(10, 2)
#  id                          :integer          not null, primary key
#  updated_at                  :datetime         not null
#  user_id                     :integer
#

class DentistProfile < ActiveRecord::Base
  attr_encrypted :license_number, key: proc {|dentist| dentist.license_number_secret_key}, marshal: true, allow_empty_value: true
  before_validation :generate_activation_code, on: :create
  belongs_to :user

  validates :activation_code, presence: true, uniqueness: true
  validate :numeric_activation_code

  def fee
    if self[:fee].nil?
      0
    else
      self[:fee]
    end
  end

  def experience
    if self[:experience].nil?
      0
    else
      self[:experience]
    end
  end

  def license_number_description
    if license_number.blank?
      "No license number provided"
    else
      license_number
    end
  end

  def fee_description
    if fee == 0
      "Free consultation fee"
    else
      "P%.2f" % [fee]
    end
  end

  def experience_description
    if experience == 0
      "No experience"
    else
      "#{experience} #{'year'.pluralize(experience)} experience"
    end
  end

  def license_number_secret_key
    ENV.fetch('LICENSE_NUMBER_SECRET_KEY')
  end

  private

  def generate_activation_code
    # https://stackoverflow.com/a/13340334
    if activation_code.blank?
      begin
        self.activation_code = 6.times.map{rand(10)}.join
      end while self.class.exists?(activation_code: activation_code)
    end
  end

  def numeric_activation_code
    if activation_code =~ /\D/
      errors.add(:activation_code, 'should only contain numbers')
    end
  end
end
