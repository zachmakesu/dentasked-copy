# == Schema Information
#
# Table name: drafts
#
#  classification :string
#  created_at     :datetime         not null
#  dentist_id     :integer
#  id             :integer          not null, primary key
#  object         :text
#  owner_id       :integer
#  reviewed_at    :datetime
#  reviewer_id    :integer
#  status         :integer          default(0)
#  updated_at     :datetime         not null
#

class Draft < ActiveRecord::Base
  serialize :object

  enum status: { pending: 0, approved: 1, declined: 2 }

  validates :object, :owner, :dentist, presence: true

  belongs_to :owner, class_name: "User"
  belongs_to :reviewer, class_name: "Admin"
  belongs_to :dentist

  has_many :photos, as: :imageable, dependent: :destroy

  def self.search q = nil
    query = "%#{q}%"
    joins(:owner).where('first_name ILIKE ? OR last_name ILIKE ?', query, query)
  end
end
