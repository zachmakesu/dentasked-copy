# == Schema Information
#
# Table name: secretary_assignments
#
#  created_at   :datetime         not null
#  dentist_id   :integer
#  id           :integer          not null, primary key
#  secretary_id :integer
#  updated_at   :datetime         not null
#

class SecretaryAssignment < ActiveRecord::Base
  # belongs_to :clinic_assignment
  belongs_to :dentist
  belongs_to :secretary

  validates_presence_of :secretary, :dentist
  validates_uniqueness_of :dentist, scope: :secretary, message: 'has already been assigned'
end
