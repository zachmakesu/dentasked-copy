# == Schema Information
#
# Table name: clinics
#
#  address           :string
#  city_id           :integer
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  landline          :string
#  landmarks         :string
#  lat               :float
#  lng               :float
#  logo_content_type :string
#  logo_file_name    :string
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#  mobile_number     :string
#  name              :string
#  parking_slot      :string
#  price             :string
#  updated_at        :datetime         not null
#

class Clinic < ActiveRecord::Base
  geocoded_by :address, latitude: :lat, longitude: :lng
  after_validation :geocode, if: ->(obj){ obj.address.present? && obj.address_changed? }

  has_attached_file :logo, :styles => { medium: '300x300#', thumb: '100x100#', small: '50x50#' }, default_url: 'icon.png'
  validates_attachment_content_type :logo, :content_type => /\Aimage\/(jpg|jpeg|png)\z/, :message => 'only .jpg, .jpeg, or .png images'
  before_post_process :rename_logo

  validates :name, uniqueness: true
  validates_presence_of :name, :address

  validate :city_existence
  belongs_to :city

  has_many :clinic_assignments, dependent: :destroy
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :dentists, through: :clinic_assignments
  has_many :secretary_assignments, through: :dentists
  has_many :favorites, as: :favorited, dependent: :destroy, class_name: 'Favorite'
  has_many :favorited_by, through: :favorites, source: :patient
  has_many :received_ratings, as: :ratee, class_name: 'Rating'
  has_many :raters, through: :received_ratings, source_type: 'Patient'

  has_many :hmo_accreditations, as: :accredited, dependent: :destroy
  has_many :hmos, through: :hmo_accreditations

  has_many :service_assignments, as: :assigned, dependent: :destroy
  has_many :services, through: :service_assignments

  def secretaries
    Secretary.includes(:roles, :activations, secretary_assignments: [:dentist]).where("users.id IN (?)", dentists.map(&:secretaries).flatten.uniq)
  end

  def activated_secretaries
    secretaries.where.not(activations: { activated_at: nil })
  end

  def parking_slot
    read_attribute(:parking_slot).blank? ? "Without parking" : read_attribute(:parking_slot)
  end

  def schedules
    clinic_assignments.map { |c| c.schedules }.flatten.uniq
  end

  def days
    schedules.sort_by(&:day).map { |s| s.day }.flatten.uniq
  end

  def schedule_data
    if schedules.present?
      @start_time = schedules.min_by(&:start_time).start_time
      @end_time = schedules.max_by(&:end_time).end_time
      @start_day = schedules.min_by(&:day)
      @end_day = schedules.max_by(&:day)
      @days = schedules.sort_by(&:day)
    end
  end

  def display_schedule
    if schedules.present?
      schedule_data
      "#{@start_day.abbr_day_name_upcased} - #{@end_day.abbr_day_name_upcased} #{@start_time.strftime('%l:%M %p')} -#{@end_time.strftime('%l:%M %p')}"
    else
      "No schedule provided"
    end
  end

  def schedule_per_day
    if schedules.present?
      schedule_data
      @days.map { |s| "#{s.abbr_day_name_upcased} #{s.start_time.strftime('%l:%M %p')} -#{s.end_time.strftime('%l:%M %p')}" }
    else
      []
    end
  end

  def is_open?
    return false if schedules.empty?
    if schedule_for_today = schedules.select{|s| s.day == DateTime.now.utc.wday}.first
      time = Time.now.utc + (8*60*60)
      open_time = schedule_for_today.start_time
      close_time = schedule_for_today.end_time
      # http://stackoverflow.com/a/28546184
      (((time.to_r / 60 / 60 / 24) % 1) >= ((open_time.to_r / 60 / 60 / 24) % 1)) && (((time.to_r / 60 / 60 / 24) % 1) <= ((close_time.to_r / 60 / 60 / 24) % 1))
    else
      false
    end
  end

  def is_schedule_continous?
    return false if days.count == 1
    (0..6).each_cons(days.count).any? { |d| d == days }
  end

  def operation_status
    if schedules.present?
      is_open? ? "OPEN TODAY" : "CLOSED"
    else
      "UNKNOWN"
    end
  end

  def self.search q = nil
    query = "%#{q}%"
    where('name ILIKE ?', query)
  end

  def assign(dentist)
    ClinicAssignment.create dentist_id: dentist.id, clinic_id: self.id
  end

  def unassign(dentist)
    assignment = ClinicAssignment.where(clinic_id: self.id, dentist_id: dentist.id).first
    return false unless assignment
    assignment.destroy
  end

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["Name","Mobile Number","Address","City","Province","Latitude","Longitude","created_at"]

      all.each do |clinic|
        csv << [clinic.name, clinic.mobile_number, clinic.address, clinic.city.try(:name), (clinic.city.province.name if clinic.city.province.present?), clinic.lat, clinic.lng, clinic.created_at]
      end
    end
  end

  private

  def rename_logo
    extension = File.extname(logo_file_name).downcase
    self.logo.instance_write :file_name, "#{SecureRandom.uuid}#{extension}"
  end

  def city_existence
    self.errors.add(:city, "must exist") unless City.exists?(id: city_id)
  end
end
