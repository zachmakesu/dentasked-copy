class AppVersionFeature < ActiveRecord::Base
  belongs_to :app_version
  validates :title, :details, presence: :true
end

# == Schema Information
#
# Table name: app_version_features
#
#  app_version_id :integer
#  created_at     :datetime         not null
#  details        :text
#  id             :integer          not null, primary key
#  title          :string
#  updated_at     :datetime         not null
#
