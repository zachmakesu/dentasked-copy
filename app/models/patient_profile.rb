# == Schema Information
#
# Table name: patient_profiles
#
#  created_at :datetime         not null
#  gender     :integer
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  user_id    :integer
#  work       :string
#

class PatientProfile < ActiveRecord::Base
  enum gender: [:male, :female]
  belongs_to :user
  validates :gender, inclusion: { in: PatientProfile.genders.keys }, allow_blank: true
end
