# == Schema Information
#
# Table name: users
#
#  avatar_content_type        :string
#  avatar_file_name           :string
#  avatar_file_size           :integer
#  avatar_updated_at          :datetime
#  confirmation_sent_at       :datetime
#  confirmation_token         :string
#  confirmed_at               :datetime
#  created_at                 :datetime
#  current_sign_in_at         :datetime
#  current_sign_in_ip         :string
#  disabled_at                :datetime
#  email                      :string           default(""), not null
#  encrypted_birthdate        :string
#  encrypted_birthdate_iv     :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  encrypted_password         :string           default(""), not null
#  first_name                 :string
#  id                         :integer          not null, primary key
#  last_name                  :string
#  last_sign_in_at            :datetime
#  last_sign_in_ip            :string
#  reset_password_sent_at     :datetime
#  reset_password_token       :string
#  sign_in_count              :integer          default(0), not null
#  terms_agreed_at            :datetime
#  uid                        :string           default(""), not null
#  unconfirmed_email          :string
#  updated_at                 :datetime
#

class Admin < User
  self.default_scope -> { admins }
  after_create :assign_admin_role
  has_many :assigned_drafts, class_name: "Draft", foreign_key: :reviewer_id
  has_one :profile, class_name: "AdminProfile", foreign_key: :user_id, dependent: :destroy

  private

  def assign_admin_role
    make_admin!
  end
end
