# == Schema Information
#
# Table name: provinces
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  psgc_code  :string           not null
#  region_id  :integer
#  updated_at :datetime         not null
#

class Province < ActiveRecord::Base
  belongs_to :region
  has_many :cities
end
