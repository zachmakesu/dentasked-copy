# == Schema Information
#
# Table name: users
#
#  avatar_content_type        :string
#  avatar_file_name           :string
#  avatar_file_size           :integer
#  avatar_updated_at          :datetime
#  confirmation_sent_at       :datetime
#  confirmation_token         :string
#  confirmed_at               :datetime
#  created_at                 :datetime
#  current_sign_in_at         :datetime
#  current_sign_in_ip         :string
#  disabled_at                :datetime
#  email                      :string           default(""), not null
#  encrypted_birthdate        :string
#  encrypted_birthdate_iv     :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  encrypted_password         :string           default(""), not null
#  first_name                 :string
#  id                         :integer          not null, primary key
#  last_name                  :string
#  last_sign_in_at            :datetime
#  last_sign_in_ip            :string
#  reset_password_sent_at     :datetime
#  reset_password_token       :string
#  sign_in_count              :integer          default(0), not null
#  terms_agreed_at            :datetime
#  uid                        :string           default(""), not null
#  unconfirmed_email          :string
#  updated_at                 :datetime
#

class User < ActiveRecord::Base
  DEFAULT_COUNTRY_CODE = %(PH).freeze
  include Roleable

  attr_encrypted :birthdate, key: proc {|user| user.birthdate_secret_key}, marshal: true, allow_empty_value: true
  attr_encrypted :mobile_number, key: proc {|user| user.mobile_number_secret_key}, marshal: true, allow_empty_value: true
  before_validation :format_mobile_number, :format_birthdate
  after_create :create_activation
  before_create :generate_uid

  devise :confirmable,
    :database_authenticatable,
    :recoverable,
    :timeoutable,
    :trackable,
    :validatable,
    :omniauthable,
    :omniauth_providers => [:facebook, :google_oauth2]

  has_attached_file :avatar,
    styles: { medium: '300x300#', thumb: '100x100#', small: '50x50#' },
    default_url: 'icon.png',
    url: "/system/users/:attachment/:id_partition/:style/:hash.:extension",
    hash_data: "users/:attachment/:id/:style/:updated_at"

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/(jpg|jpeg|png)\z/,
    :message => 'only .jpg, .jpeg, or .png images'
  before_post_process :rename_avatar

  has_many :activations, :dependent => :destroy
  has_many :api_keys, :dependent => :destroy
  has_many :devices, dependent: :destroy
  has_many :identities, dependent: :destroy
  has_many :photos, as: :imageable
  has_many :rating_suggestions, inverse_of: :user
  has_many :received_ratings, as: :ratee, class_name: 'Rating', dependent: :destroy
  has_many :sent_ratings, as: :rater, class_name: 'Rating', dependent: :destroy

  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles

  has_many :sent_reports,  class_name:  "Report",
    foreign_key: "reporter_id",
    dependent:   :destroy
  has_many :received_reports, class_name:  "Report",
    dependent:   :destroy,
    as: :reportable
  has_many :reported_users, through: :sent_reports,  source: :reportable, source_type: 'User'
  has_many :reporters, through: :received_reports, source: :reporter

  has_many :raters, through: :received_ratings, source_type: 'User'
  has_many :ratees, through: :sent_ratings, source_type: 'User'
  has_many :feedbacks

  validate :mobile_number_uniqueness, :mobile_number_validity, :password_complexity
  validates_uniqueness_of :uid, :email

  scope :active_accounts, -> { joins(:api_keys).select("users.*, count(api_keys.id) as apis_count").group('users.id') }

  def self.search q = nil
    query = "%#{q}%"
    where('first_name ILIKE ? OR last_name ILIKE ?', query, query)
  end

  def self.find_by_mobile_number(number = "")
    formatted_number = Phonelib.parse(number, "PH").e164
    User.find_each.find{ |u| u.mobile_number == formatted_number}
  end

  def delete_previous_api_keys
    if api_keys.count > 5
      api_keys.first.delete
    end
  end

  def self.find_by_google_token(access_token, profile = nil)
    begin
      data = access_token
      existing_user = Identity.find_by_uid(data['uid']).user
      unless existing_user.nil?
        unless existing_user.identity.nil?
          existing_user.identity.refresh_google_token
        else
          new_identity_for_user = existing_user.build_identity(
            uid: data['uid'],
            provider: data['provider'],
            oauth_token: data['credentials']['token'],
            oauth_expires_at: DateTime.now + data['credentials']['expires_at'].to_i.seconds,
            oauth_refresh_token: data['credentials']['refresh_token']
          )
          new_identity_for_user.save
        end
        existing_user
      else
        birthday = if data['extra']['raw_info']['birthday'].blank?
                     nil
                   else
                     Date.strptime(data['extra']['raw_info']['birthday'],'%Y-%m-%d')
                   end
        new_user = User.new(
          first_name: data['first_name'],
          last_name: data['last_name'],
          email: data['info']['email'],
          password: Devise.friendly_token[0,20],
          avatar: User.process_uri(data['info']['image']),
          gender: data['gender'],
          birthdate: birthday
        )
        new_user.build_identity({
          uid: data['uid'],
          provider: 'google_oauth2',
          oauth_token: data['credentials']['token'],
          oauth_refresh_token: data['credentials']['refresh_token'],
          oauth_expires_at: data['credentials']['expires_at']
        })
        new_user.save
        if profile.present?
          if profile == 'dentist'
            new_user.make_dentist!
          else
            new_user.make_patient!
          end
        end
        new_user
      end
    rescue Exception => e
      ExceptionNotifier.notify_exception(e)
      return e.message
    end
  end

  def activated?
    activations.any?(&:activated?)
  end

  def linked_mobile_number?
    activated? && mobile_number.present?
  end

  def can_report?(user)
    return false if self.id == user.id
    last_report = sent_reports.where(reportable_id: user.id, reportable_type: 'User').last
    if last_report
      (last_report.created_at + 1.day) <= DateTime.now
    else
      true
    end
  end

  def report!(user)
    return unless self.can_report?(user)
    user.received_reports.create!(reporter_id: self.id)
  end

  def name
    "#{first_name} #{last_name}".gsub(/\s+/, ' ').strip
  end

  def formatted_birthdate
    return "" if birthdate.nil?
    birthdate.to_date.strftime('%m/%d/%Y')
  end

  # http://stackoverflow.com/a/2357790
  def age
    return 0 if birthdate.nil?
    dob = birthdate.to_date
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end

  def age_string
    return "" if birthdate.nil?
    "#{age} #{'year'.pluralize(age)} old"
  end

  # Prevent non-admin from accessing the CMS
  def active_for_authentication?
    super && admin?
  end

  def inactive_message
    "Sorry, your account is not authorized to access Dentasked. Please contact an admin."
  end

  # User disable
  def disable!
    self.update(disabled_at: DateTime.now)
  end

  def disabled?
    !disabled_at.nil?
  end

  def enable!
    self.update(disabled_at: nil)
  end

  def enabled?
    disabled_at.nil?
  end

  def agree_to_terms
    return false if agreed_to_terms?
    self.update(terms_agreed_at: DateTime.now)
  end

  def agreed_to_terms?
    !terms_agreed_at.nil?
  end

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << if self.name == "Secretary"
               ["ID", "Clinics","Clinic Addresses","Clinic Cities","Clinic Provinces","Clinics Lat & Lng","Name","Email","Role","Activation Code", "Activated at","Mobile Number","created at"]
      else
        ["ID", "Name","Email","Role","Activation Code", "Activated at","Mobile Number","created at"]
      end

      find_each do |user|
        csv << if self.name == "Secretary"
                 clinics = Clinic.where(id: user.dentists.flat_map{|d| d.clinics.pluck(:id)}.uniq)
                 clinic_names = clinics.pluck(:name).join(",\n")
                 clinic_addresses = clinics.pluck(:address).join(",\n")
                 clinic_cities    = clinics.map{|c| c.city.try(:name)}.join(",\n")
                 clinic_provinces = clinics.includes(:city).map{|c| c.city.province.name if c.city.present? && c.city.province.present?}.join(",\n")
                 clinic_lat_lng = clinics.map{|c| ["lat: #{c.lat} / lng: #{c.lng}"]}.join(",\n")
                 [user.id, clinic_names,clinic_addresses,clinic_cities,clinic_provinces,clinic_lat_lng,user.name, user.email, user.role, user.activations.last.try(:code), user.activations.where.not(activated_at: nil).last.try(:activated_at), user.mobile_number, user.created_at]
        else
          [user.id, user.name, user.email, user.role, user.activations.last.try(:code), user.activations.where.not(activated_at: nil).last.try(:activated_at), user.mobile_number, user.created_at]
        end
      end
    end
  end

  def birthdate_secret_key
    ENV.fetch('BIRTHDATE_SECRET_KEY')
  end

  def mobile_number_secret_key
    ENV.fetch('MOBILE_NUMBER_SECRET_KEY')
  end

  def linked_google_account?
    return false if identities.where(provider: 'google_oauth2').empty?
    !identities.google.expired?
  end

  def role
    return "Basic" if basic?
    roles.map do  |role|
      role.name.titleize
    end.join('/')
  end

  def send_confirmation_notification?
    false
  end

  def has_temporary_email?
    !!(email =~ /\A\d+@facebook\.com\z/)
  end

  private

  def create_activation
    activations.create! unless admin?
  end

  def self.process_uri(uri)
    avatar_url = URI.parse(uri)
    avatar_url.scheme = 'https'
    avatar_url.to_s
  end

  def generate_uid
    self.uid = loop do
      random_token = SecureRandom.hex(16)
      break random_token unless self.class.exists?(uid: random_token)
    end
  end

  def rename_avatar
    extension = File.extname(avatar_file_name).downcase
    self.avatar.instance_write :file_name, "#{SecureRandom.uuid}#{extension}"
  end

  def password_complexity
    if password.present?
      has_lowercase = password =~ /[a-z]/
      has_uppercase = password =~ /[A-Z]/
      has_number = password =~ /[0-9]/
      has_special_char = password =~ /[{}\[\],.<>;:'"?\/|`~!@#$%^&*()_\-+="']/
      complexity_met = (
        (has_lowercase && has_uppercase) ||
        (has_lowercase && has_number) ||
        (has_lowercase && has_special_char) ||
        (has_uppercase && has_number) ||
        (has_uppercase && has_special_char) ||
        (has_number && has_special_char)
      )
      errors.add(:password, "complexity requirements not met") unless complexity_met
    end
  end

  def format_mobile_number
    country_code = DEFAULT_COUNTRY_CODE # TODO: Set to user specific code?
    valid_mobile_number = Phonelib.valid_for_country?(mobile_number, country_code)
    if valid_mobile_number
      self.mobile_number = Phonelib.parse(mobile_number, country_code).e164
    end
  end

  def format_birthdate
    if /\A([\d]{1,2})[-\/]([\d]{1,2})[-\/]([\d]{4})\z/ =~ birthdate.to_s
      m = $1
      d = $2
      y = $3
      if d.to_i > 12
        self.birthdate = "#{d}/#{m}/#{y}"
      end
    end
  end

  def mobile_number_uniqueness
    if mobile_number.present? && User.find_each.find{ |u| u.id != id && u.mobile_number == mobile_number }
      errors.add(:base, "Mobile number already in use")
    end
  end

  def mobile_number_validity
    country_code = DEFAULT_COUNTRY_CODE # TODO: Set to user specific code?
    valid_mobile_number = Phonelib.valid_for_country?(mobile_number, country_code)
    if mobile_number.present? && !valid_mobile_number
      errors.add(:base, "Mobile number invalid")
    end
  end
end
