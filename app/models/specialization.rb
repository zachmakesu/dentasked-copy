# == Schema Information
#
# Table name: specializations
#
#  created_at   :datetime         not null
#  dentist_id   :integer
#  id           :integer          not null, primary key
#  specialty_id :integer
#  updated_at   :datetime         not null
#

class Specialization < ActiveRecord::Base
  belongs_to :specialty
  belongs_to :dentist

  validates_uniqueness_of :specialty, scope: :dentist, message: 'has this specialty already'
end
