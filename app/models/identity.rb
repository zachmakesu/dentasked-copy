# == Schema Information
#
# Table name: identities
#
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  oauth_expires_at    :datetime
#  oauth_refresh_token :text
#  oauth_token         :text
#  provider            :string
#  uid                 :string
#  updated_at          :datetime         not null
#  user_id             :integer
#

class Identity < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :uid, :provider, :oauth_token
  validates_uniqueness_of :uid, scope: :provider

  scope :google, -> { find_by_provider('google_oauth2') }

  def self.facebook
    where(provider: 'facebook').first
  end

  def self.find_for_oauth(auth)
    find_or_create_by(uid: auth.uid, provider: auth.provider)
  end

  # http://stackoverflow.com/a/16783149
  def refresh_facebook_token
    # Checks the saved expiry time against the current time
    # Get the new token
    new_token = facebook_oauth.exchange_access_token_info(oauth_token)

    # Save the new token and its expiry over the old one
    self.oauth_token = new_token['access_token']
    self.oauth_expires_at = new_token['expires']
    save
  end

  def refresh_facebook_token_with(token)
    # Checks the saved expiry time against the current time
    # Get the new token
    new_token = facebook_oauth.exchange_access_token_info(token)

    # Save the new token and its expiry over the old one
    self.oauth_token = new_token['access_token']
    expiration_date = (Time.now + new_token['expires'].to_i)
    self.oauth_expires_at = expiration_date
    save
  end

  def refresh_google_token
    begin
      client = Google::APIClient.new(application_name: 'Dentasked for Partners')
      client.authorization.client_id = ENV['GOOGLE_CLIENT_ID']
      client.authorization.client_secret = ENV['GOOGLE_CLIENT_SECRET']
      client.authorization.grant_type = 'refresh_token'
      client.authorization.refresh_token = self.oauth_refresh_token
      new_token = client.authorization.fetch_access_token!

      self.oauth_token = new_token['access_token']
      self.oauth_expires_at = Time.now + new_token['expires_in'].to_i
      save
    rescue Exception => e
      ExceptionNotifier.notify_exception(e)
      return false
    end
  end

  def expired?
    DateTime.now > self.oauth_expires_at
  end

  # Connect to Facebook via Koala's oauth
  def facebook_oauth
    # Insert your own Facebook client ID and secret here
    @facebook_oauth ||= Koala::Facebook::OAuth.new(ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET'])
  end

end
