# == Schema Information
#
# Table name: users
#
#  avatar_content_type        :string
#  avatar_file_name           :string
#  avatar_file_size           :integer
#  avatar_updated_at          :datetime
#  confirmation_sent_at       :datetime
#  confirmation_token         :string
#  confirmed_at               :datetime
#  created_at                 :datetime
#  current_sign_in_at         :datetime
#  current_sign_in_ip         :string
#  disabled_at                :datetime
#  email                      :string           default(""), not null
#  encrypted_birthdate        :string
#  encrypted_birthdate_iv     :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  encrypted_password         :string           default(""), not null
#  first_name                 :string
#  id                         :integer          not null, primary key
#  last_name                  :string
#  last_sign_in_at            :datetime
#  last_sign_in_ip            :string
#  reset_password_sent_at     :datetime
#  reset_password_token       :string
#  sign_in_count              :integer          default(0), not null
#  terms_agreed_at            :datetime
#  uid                        :string           default(""), not null
#  unconfirmed_email          :string
#  updated_at                 :datetime
#

class Dentist < User
  self.default_scope -> { dentists }
  before_validation :generate_random_password
  before_create :assign_default_services
  after_create :assign_dentist_role

  has_many :clinic_assignments, dependent: :destroy
  has_many :clinics, through: :clinic_assignments
  has_many :appointments, through: :clinic_assignments, dependent: :destroy

  has_many :hmo_accreditations, as: :accredited, dependent: :destroy
  has_many :hmos, through: :hmo_accreditations

  has_many :service_assignments, as: :assigned, dependent: :destroy
  has_many :services, through: :service_assignments

  has_many :specializations, dependent: :destroy
  has_many :specialties, through: :specializations

  has_many :secretary_assignments, dependent: :destroy
  has_many :secretaries, through: :secretary_assignments

  has_many :favorites, as: :favorited, dependent: :destroy, class_name: 'Favorite'
  has_many :favorited_by, through: :favorites, source: :patient

  has_many :drafts, foreign_key: :dentist_id, dependent: :destroy

  has_one :profile, class_name: "DentistProfile", foreign_key: :user_id, dependent: :destroy

  def name_with_salutation
    "Dr. #{name}"
  end

  def specialization
    specialties.empty? ? "No specified specialization" : specialties.first.name
  end

  def self.hmo_filter(hmo_id)
    hmo = HMO.find(hmo_id)
    where(id: hmo.dentist_ids.uniq)
  end

  def self.service_filter(service_id)
    service = Service.find(service_id)
    where(id: service.dentist_ids.uniq)
  end

  def self.specialty_filter(specialty_id)
    specialty = Specialty.find(specialty_id)
    where(id: specialty.dentist_ids.uniq)
  end

  def activated?
    profile.activated_at?
  end

  def today_confirmed_appointments
    clinic_assignments.map{|c| c.appointments.for_today.confirmed.by_earliest}.flatten
  end

  def tomorrow_confirmed_appointments
    clinic_assignments.map{|c| c.appointments.for_tomorrow.confirmed.by_earliest}.flatten
  end

  def services_description
    if services.empty?
      'N/A'
    else
      services.alphabetical.map(&:name).join('\n')
    end
  end

  def hmo_description
    if hmos.empty?
      'N/A'
    else
      hmos.map(&:name).join(', ')
    end
  end

  def specialty_description
    if specialties.empty?
      'N/A'
    else
      specialties.map(&:name).join(', ')
    end
  end

  private

  def generate_random_password
    if self.password.blank?
      self.password = Devise.friendly_token.first(16)
    end
  end

  def assign_default_services
    if services.empty?
      services_name = ['check up', 'consultation'].map{|val| "%#{val}%"}
      default_services = Service.where('name ILIKE ANY (array[?])', services_name)
      services << default_services
    end
  end

  def assign_dentist_role
    make_dentist!
  end
end
