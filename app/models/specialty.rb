# == Schema Information
#
# Table name: specialties
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

class Specialty < ActiveRecord::Base
  validates :name, uniqueness: true, presence: true
  has_many :preferences, dependent: :destroy, class_name: 'BookingPreference'

  has_many :appointments
  has_many :specializations
  has_many :dentists, through: :specializations

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["Name","Description","created_at"]

      all.each do |specialty|
        csv << [specialty.name,specialty.description,specialty.created_at]
      end
    end
  end

end
