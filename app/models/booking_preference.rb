# == Schema Information
#
# Table name: booking_preferences
#
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  patient_id   :integer          not null
#  specialty_id :integer
#  symptom_id   :integer
#  updated_at   :datetime         not null
#

class BookingPreference < ActiveRecord::Base
  belongs_to :patient
  belongs_to :specialty
  belongs_to :symptom
end
