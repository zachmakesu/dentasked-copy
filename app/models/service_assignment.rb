# == Schema Information
#
# Table name: service_assignments
#
#  assigned_id   :integer
#  assigned_type :string
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  service_id    :integer
#  updated_at    :datetime         not null
#

class ServiceAssignment < ActiveRecord::Base
  belongs_to :service
  belongs_to :assigned, polymorphic: true

  validates_uniqueness_of :service, scope: [:assigned_id, :assigned_type], message: 'has this service already'
end
