# == Schema Information
#
# Table name: ratings
#
#  context_id   :integer          not null
#  context_type :string           not null
#  created_at   :datetime         not null
#  description  :text
#  id           :integer          not null, primary key
#  ratee_id     :integer          not null
#  ratee_type   :string           not null
#  rater_id     :integer          not null
#  rater_type   :string           not null
#  rating       :float            default(0.0)
#  updated_at   :datetime         not null
#

class Rating < ActiveRecord::Base
  belongs_to :ratee, polymorphic: true
  belongs_to :rater, polymorphic: true
  belongs_to :context, polymorphic: true

  validates_numericality_of :rating, greater_than_or_equal_to: 0, less_than_or_equal_to: 5
  validates_presence_of :ratee, :rater, :context
end
