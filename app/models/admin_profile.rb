# == Schema Information
#
# Table name: admin_profiles
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  user_id    :integer
#

class AdminProfile < ActiveRecord::Base
  belongs_to :user
end
