# == Schema Information
#
# Table name: cities
#
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  name        :string           not null
#  province_id :integer
#  psgc_code   :string           not null
#  updated_at  :datetime         not null
#

class City < ActiveRecord::Base
  belongs_to :province
  has_many :clinics

  def full_name
    if name.downcase.include?("city")
      name
    else
      "#{name.strip} City"
    end
  end
end
