module Roleable
  extend ActiveSupport::Concern

  included do
    scope :admins,        -> { joins(:roles).where(roles: { name: "admin" }) }
    scope :dentists,      -> { joins(:roles).where(roles: { name: "dentist" }) }
    scope :patients,      -> { joins(:roles).where(roles: { name: "patient" }) }
    scope :secretaries,   -> { joins(:roles).where(roles: { name: "secretary" }) }
  end

  ROLES = %w(
    Admin
    Dentist
    Patient
    Secretary
  ).freeze

  def valid_role?(role)
    return false unless ROLES.include?(role)
    has_role?(role.downcase)
  end

  def basic?
    roles.empty?
  end

  ROLES.map(&:downcase).each do |role|
    define_method "#{role}?".to_sym do
      has_role? role
    end

    define_method "make_#{role}!".to_sym do
      return true if has_role?(role)
      if role_object = Role.find_by(name: role)
        roles << role_object
      else
        false
      end
    end

    define_method "revoke_#{role}!".to_sym do
      return true unless has_role?(role)
      if role_object = Role.find_by(name: role)
        roles.destroy(role_object)
      else
        false
      end
    end
  end

  private

  def has_role?(role)
    roles.map(&:name).include?(role)
  end
end
