# == Schema Information
#
# Table name: favorites
#
#  created_at     :datetime         not null
#  favorited_id   :integer
#  favorited_type :string
#  id             :integer          not null, primary key
#  patient_id     :integer          not null
#  updated_at     :datetime         not null
#

class Favorite < ActiveRecord::Base
  belongs_to :patient
  belongs_to :favorited, polymorphic: :true

  validates_presence_of :patient, :favorited
  # A user can only favorite each item once, keeping in mind that items of different favorited_types may have the same ID
  validates_uniqueness_of :favorited_id, scope: [:patient_id, :favorited_type], message: 'has already been favorited'
end
