# == Schema Information
#
# Table name: faqs
#
#  answer     :text
#  app_type   :string
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  question   :string
#  updated_at :datetime         not null
#

class FAQ < ActiveRecord::Base
  TYPES = %w{
    Patient
    Secretary
  }

  validates :app_type, presence: true, inclusion: { in: TYPES }
  validates_presence_of :question, :answer 

  TYPES.each do |type|
    define_method "#{type.downcase}?".to_sym do
      self.app_type == type
    end
  end
end
