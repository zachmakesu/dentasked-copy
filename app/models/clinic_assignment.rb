# == Schema Information
#
# Table name: clinic_assignments
#
#  clinic_id  :integer          not null
#  created_at :datetime         not null
#  dentist_id :integer          not null
#  id         :integer          not null, primary key
#  is_active  :boolean          default(TRUE)
#  updated_at :datetime         not null
#

class ClinicAssignment < ActiveRecord::Base
  belongs_to :clinic
  belongs_to :dentist
  before_save :ensure_clinic_has_schedules, on: :create

  has_many :appointments, dependent: :destroy
  has_many :schedules, class_name: 'AssignmentSchedule', dependent: :destroy

  validates_uniqueness_of :dentist_id, scope: :clinic_id, message: 'has already been assigned'

  def create_default_schedules
    for i in 1..5
      unless schedules.where(day: i).first
        schedule_params = {
          day: i,
          start_time: Time.parse('09:00 AM'),
          end_time: Time.parse('05:00 PM')
        }
        if persisted?
          schedules.create! schedule_params
        else
          schedules.build schedule_params
        end
      end
    end
  end

  private
  def ensure_clinic_has_schedules
    self.create_default_schedules
  end
end
