# == Schema Information
#
# Table name: devices
#
#  created_at :datetime         not null
#  enabled    :boolean          default(TRUE), not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  platform   :string           not null
#  token      :string           not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Device < ActiveRecord::Base
  SUPPORTED_DEVICES = %w{ ios android }.freeze
  belongs_to :owner, class_name: 'User', foreign_key: :user_id
  validates_presence_of :name, :platform, :token, :user_id
  validates :platform, inclusion: { in: SUPPORTED_DEVICES }
  validates_uniqueness_of :token

  scope :android, -> { where(platform: "android") }
  scope :ios, -> { where(platform: "ios") }
  scope :active, -> { where(enabled: true) }

  def register_to(user)
    if existing_devices = Device.where(token: self.token)
      existing_devices.destroy_all
    end
    self.user_id = user.id
    save
  end
end
