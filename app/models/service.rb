# == Schema Information
#
# Table name: services
#
#  created_at  :datetime         not null
#  description :string
#  duration    :integer
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

class Service < ActiveRecord::Base
  has_many :appointments, dependent: :destroy
  has_many :service_assignments, dependent: :destroy
  has_many :dentists, through: :service_assignments, source: :assigned, source_type: 'User'
  has_many :clinics, through: :service_assignments, source: :assigned, source_type: 'Clinic'

  validates :name, uniqueness: true, presence: true
  validates :duration, presence: true

  def self.alphabetical(order: :asc)
    order(name: order)
  end

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["Name","Description","Duration","created_at"]

      all.each do |service|
        csv << [service.name,service.description,service.duration,service.created_at]
      end
    end
  end

end
