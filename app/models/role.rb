class Role < ActiveRecord::Base
  before_validation :downcase_name
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  private

  def downcase_name
    self.name &&= name.downcase
  end
end

# == Schema Information
#
# Table name: roles
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#
