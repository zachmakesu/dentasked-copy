# == Schema Information
#
# Table name: hmos
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

class HMO < ActiveRecord::Base
  has_one :logo, as: :imageable, class_name: 'Photo'
  has_many :appointments
  has_many :hmo_accreditations, dependent: :destroy
  has_many :dentists, through: :hmo_accreditations, source: :accredited, source_type: 'User'
  has_many :clinics, through: :hmo_accreditations, source: :accredited, source_type: 'Clinic'
  accepts_nested_attributes_for :logo

  validates :name, presence: true

  def self.search q = nil
    query = "%#{q}%"
    where('name ILIKE ?', query)
  end

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["Name","Description","created_at"]

      all.each do |hmo|
        csv << [hmo.name,hmo.description,hmo.created_at]
      end
    end
  end

end
