# == Schema Information
#
# Table name: activations
#
#  activated_at :datetime
#  code         :string           not null
#  created_at   :datetime         not null
#  expires_at   :datetime
#  id           :integer          not null, primary key
#  updated_at   :datetime         not null
#  user_id      :integer
#

class Activation < ActiveRecord::Base
  before_save :set_expires_at, on: :create
  before_validation :generate_activation_code, on: :create

  belongs_to :user

  validates :code, presence: true, uniqueness: true
  validate :numeric_activation_code

  def expired?
    DateTime.now > self.expires_at
  end

  def activated?
    self.activated_at.present?
  end

  def activate!
    self.update(activated_at: DateTime.now)
  end

  private

  def set_expires_at
    self.expires_at = DateTime.now + 3.days
  end

  def generate_activation_code
    # https://stackoverflow.com/a/13340334
    if code.blank?
      begin
        self.code = 6.times.map{rand(10)}.join
      end while self.class.exists?(code: code)
    end
  end

  def numeric_activation_code
    if code_changed? && code =~ /\D/
      errors.add(:code, 'should only contain numbers')
    end
  end
end
