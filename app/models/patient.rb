# == Schema Information
#
# Table name: users
#
#  avatar_content_type        :string
#  avatar_file_name           :string
#  avatar_file_size           :integer
#  avatar_updated_at          :datetime
#  confirmation_sent_at       :datetime
#  confirmation_token         :string
#  confirmed_at               :datetime
#  created_at                 :datetime
#  current_sign_in_at         :datetime
#  current_sign_in_ip         :string
#  disabled_at                :datetime
#  email                      :string           default(""), not null
#  encrypted_birthdate        :string
#  encrypted_birthdate_iv     :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  encrypted_password         :string           default(""), not null
#  first_name                 :string
#  id                         :integer          not null, primary key
#  last_name                  :string
#  last_sign_in_at            :datetime
#  last_sign_in_ip            :string
#  reset_password_sent_at     :datetime
#  reset_password_token       :string
#  sign_in_count              :integer          default(0), not null
#  terms_agreed_at            :datetime
#  uid                        :string           default(""), not null
#  unconfirmed_email          :string
#  updated_at                 :datetime
#

class Patient < User
  self.default_scope -> { patients }
  before_create :create_patient_preference
  after_create :assign_patient_role

  has_one :preference, class_name: 'BookingPreference', dependent: :destroy
  has_many :favorites
  has_many :favorite_clinics, through: :favorites, source: :favorited, source_type: "Clinic"
  has_many :favorite_dentists,
    through: :favorites, 
    source: :favorited,
    source_type: "User",
    class_name: "Dentist"
  has_many :appointments, dependent: :destroy
  has_one :profile, class_name: "PatientProfile", foreign_key: :user_id, dependent: :destroy

  def work
    profile.work.present? ? profile.work : "None"
  end

  def gender
    profile.gender.present? ? profile.gender : "None"
  end

  def last_appointment
    appointments.for_agenda.by_latest.last
  end

  def appointments_with_hmo
    appointments.confirmed.where(hmo: true)
  end

  def hmo
    has_hmo? ? last_hmo_used : HMO.new
  end

  def has_hmo?
    appointments_with_hmo.present?
  end

  def last_hmo_used
    appointments_with_hmo.last.hmo
  end

  def can_favorite?(resource)
    !favorites.include?(resource)
  end

  def name
    "#{first_name} #{last_name}".gsub(/\s+/, ' ').strip
  end

  private

  def create_patient_preference
    build_preference
  end

  def assign_patient_role
    make_patient!
  end
end
