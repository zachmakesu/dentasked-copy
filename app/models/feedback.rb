# == Schema Information
#
# Table name: feedbacks
#
#  created_at  :datetime         not null
#  description :text
#  id          :integer          not null, primary key
#  subject     :string
#  updated_at  :datetime         not null
#  user_id     :integer
#

class Feedback < ActiveRecord::Base
  belongs_to :user
  validates :user, presence: :true
  validates_presence_of :subject, :description
end
