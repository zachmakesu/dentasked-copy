class PartnerSignup < ActiveRecord::Base
  serialize :clinic_object
  serialize :secretary_object
  serialize :dentist_object

  belongs_to :reviewer, class_name: "User"

  enum status: { pending: 0, approved: 1, declined: 2 }

  validates :clinic_object, :secretary_object, :dentist_object, presence: true

  has_attached_file :clinic_logo, styles: { medium: "300x300#", thumb: "100x100#", small: "50x50#" }, default_url: "icon.png"
  validates_attachment_content_type :clinic_logo, content_type: %r{\Aimage\/(jpg|jpeg|png)\z}, message: "only .jpg, .jpeg, or .png images"
  has_attached_file :secretary_avatar, styles: { medium: "300x300#", thumb: "100x100#", small: "50x50#" }, default_url: "icon.png"
  validates_attachment_content_type :secretary_avatar, content_type: %r{\Aimage\/(jpg|jpeg|png)\z}, message: "only .jpg, .jpeg, or .png images"
  has_attached_file :dentist_avatar, styles: { medium: "300x300#", thumb: "100x100#", small: "50x50#" }, default_url: "icon.png"
  validates_attachment_content_type :dentist_avatar, content_type: %r{\Aimage\/(jpg|jpeg|png)\z}, message: "only .jpg, .jpeg, or .png images"
  before_post_process :rename_attachment

  def dentist
    Dentist.find_by(email: dentist_object.fetch("email"))
  end

  def secretary
    Secretary.find_by(email: secretary_object.fetch("email"))
  end

  def dentist_is_secretary
    dentist_object.fetch("email") == secretary_object.fetch("email")
  end

  private

  def rename_attachment
    if clinic_logo.present?
      clinic_logo_extension = File.extname(clinic_logo_file_name).downcase
      self.clinic_logo.instance_write :file_name, "#{SecureRandom.uuid}#{clinic_logo_extension}"
    end

    if secretary_avatar.present?
      secretary_avatar_extension = File.extname(secretary_avatar_file_name).downcase
      self.secretary_avatar.instance_write :file_name, "#{SecureRandom.uuid}#{secretary_avatar_extension}"
    end

    if dentist_avatar.present?
      dentist_avatar_extension = File.extname(dentist_avatar_file_name).downcase
      self.dentist_avatar.instance_write :file_name, "#{SecureRandom.uuid}#{dentist_avatar_extension}"
    end
  end
end

# == Schema Information
#
# Table name: partner_signups
#
#  clinic_logo_content_type      :string
#  clinic_logo_file_name         :string
#  clinic_logo_file_size         :integer
#  clinic_logo_updated_at        :datetime
#  clinic_object                 :text
#  created_at                    :datetime         not null
#  dentist_avatar_content_type   :string
#  dentist_avatar_file_name      :string
#  dentist_avatar_file_size      :integer
#  dentist_avatar_updated_at     :datetime
#  dentist_object                :text
#  id                            :integer          not null, primary key
#  reviewed_at                   :datetime
#  reviewer_id                   :integer
#  secretary_avatar_content_type :string
#  secretary_avatar_file_name    :string
#  secretary_avatar_file_size    :integer
#  secretary_avatar_updated_at   :datetime
#  secretary_object              :text
#  status                        :integer          default(0)
#  updated_at                    :datetime         not null
#
