# == Schema Information
#
# Table name: appointment_histories
#
#  appointment_id    :integer
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  processor_id      :integer
#  rescheduled       :boolean          default(FALSE)
#  status_changed_to :string
#  status_from       :string
#  updated_at        :datetime         not null
#

class AppointmentHistory < ActiveRecord::Base
  scope :pending, -> { where(status_changed_to: "pending") }
  scope :confirmed, -> { where(status_changed_to: "confirmed") }
  scope :cancelled, -> { where(status_changed_to: "cancelled") }
  scope :noshow, -> { where(status_changed_to: "noshow") }
  scope :complete, -> { where(status_changed_to: "complete") }
  scope :rescheduled, -> { where(rescheduled: true) }
  scope :offline, -> (value) { joins(:appointment).where("offline = ?", value) }

  belongs_to :appointment
  belongs_to :processor, class_name: "Secretary"

  def week
    self.created_at.strftime('%W %Y')
  end

  def month
    self.created_at.strftime('%B %Y')
  end

end
