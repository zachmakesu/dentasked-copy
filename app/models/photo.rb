# == Schema Information
#
# Table name: photos
#
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  name               :string
#  updated_at         :datetime         not null
#

class Photo < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  has_attached_file :image,
                    styles: { medium: "300x300>", thumb: "100x100>" },
                    default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image,
                                    content_type: %r{\Aimage\/(jpg|jpeg|png)\z},
                                    message: "only .jpg, .jpeg, or .png images"
  before_post_process :rename_image

  def self.upload(images)
    images.each do |image|
      Photo.create(image: image)
    end
  end

  private

  def rename_image
    extension = File.extname(image_file_name).downcase
    image.instance_write :file_name, "#{SecureRandom.uuid}#{extension}"
  end
end
