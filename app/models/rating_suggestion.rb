class RatingSuggestion < ActiveRecord::Base
  belongs_to :user, inverse_of: :rating_suggestions
  validates_uniqueness_of :name
  validate :created_by_admin

  scope :active, -> { where(enabled: true) }

  def enable
    update(enabled: true) unless enabled?
  end

  def disable
    update(enabled: false) if enabled?
  end

  private

  def created_by_admin
    errors.add(:user_id, "should be created by admin") unless user.admin?
  end
end

# == Schema Information
#
# Table name: rating_suggestions
#
#  created_at :datetime         not null
#  enabled    :boolean          default(TRUE)
#  id         :integer          not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#  user_id    :integer          not null
#
