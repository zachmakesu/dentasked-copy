class SMSLog < ActiveRecord::Base
  enum status: [ :success, :failure ]
  enum client: [ :checkmobi, :sns, :twilio ]
  validates_presence_of :recipient, :message, :client, :status
  before_create :censor_recipient

  private

  def censor_recipient
    r = self.recipient.dup
    censor_count = 4
    self.recipient = r.gsub(/\d{#{censor_count}}$/, 'X' * censor_count)
  end

end

# == Schema Information
#
# Table name: sms_logs
#
#  client     :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  message    :text
#  recipient  :string
#  status     :integer
#  updated_at :datetime         not null
#
