# == Schema Information
#
# Table name: reports
#
#  created_at      :datetime         not null
#  id              :integer          not null, primary key
#  reportable_id   :integer
#  reportable_type :string
#  reporter_id     :integer
#  updated_at      :datetime         not null
#

class Report < ActiveRecord::Base
  belongs_to :reportable, polymorphic: true
  belongs_to :reporter, class_name: 'User'
end
