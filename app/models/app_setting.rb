# RailsSettings Model
class AppSetting < RailsSettings::Base
  source Rails.root.join("config/app.yml")
  namespace Rails.env
end

# == Schema Information
#
# Table name: settings
#
#  created_at :datetime
#  id         :integer          not null, primary key
#  thing_id   :integer
#  thing_type :string(30)
#  updated_at :datetime
#  value      :text
#  var        :string           not null
#
