# == Schema Information
#
# Table name: regions
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  psgc_code  :string           not null
#  updated_at :datetime         not null
#

class Region < ActiveRecord::Base
  has_many :provinces
  has_many :cities, through: :provinces
end
