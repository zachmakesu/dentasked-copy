class AppVersion < ActiveRecord::Base
  enum platform: { android: 0, ios: 1}
  enum category: { patient: 0, secretary: 1}
  has_many :features, class_name: "AppVersionFeature", dependent: :destroy
  accepts_nested_attributes_for :features, reject_if:  ->(obj) { obj[:details].blank? || obj[:title].blank? }

  validates :version_code, :created_by, :platform, presence: true
  validates :version_code, uniqueness: { scope: [:platform, :category] }
  validates_associated :features

  def self.latest(platform:, category:)
    [platform.to_s, category.to_s].inject(self, :try)&.last
  end
end

# == Schema Information
#
# Table name: app_versions
#
#  category     :integer
#  created_at   :datetime         not null
#  created_by   :string           not null
#  id           :integer          not null, primary key
#  platform     :integer          not null
#  updated_at   :datetime         not null
#  version_code :string           not null
#
