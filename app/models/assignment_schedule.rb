# == Schema Information
#
# Table name: assignment_schedules
#
#  clinic_assignment_id :integer
#  created_at           :datetime         not null
#  day                  :integer          not null
#  end_time             :time             not null
#  id                   :integer          not null, primary key
#  is_active            :boolean          default(TRUE), not null
#  start_time           :time             not null
#  updated_at           :datetime         not null
#

class AssignmentSchedule < ActiveRecord::Base
  belongs_to :clinic_assignment
  validates :day, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 6 }
  validates_datetime :end_time, after: :start_time
  validates_datetime :start_time, before: :end_time
  validates_uniqueness_of :clinic_assignment_id, scope: :day, message: 'has already been assigned a schedule that day'

  def day_name
    Date::DAYNAMES[self.day]
  end

  def abbr_day_name_upcased
    Date::ABBR_DAYNAMES[self.day].upcase
  end
end
