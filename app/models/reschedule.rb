# == Schema Information
#
# Table name: reschedules
#
#  appointment_id :integer
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  is_active      :boolean          default(FALSE)
#  schedule       :datetime         not null
#  updated_at     :datetime         not null
#

class Reschedule < ActiveRecord::Base
  after_commit :send_reschedule_notification, on: :create

  scope :active, -> { where(is_active: true)  }
  scope :inactive, -> { where(is_active: false)  }
  belongs_to :appointment

  def deactivate
    self.update_attribute :is_active, false
  end

  def send_reschedule_notification
    Notifications::ConsumerAppointmentNotifier.perform_async(self.appointment.id) if self.appointment.confirmed?
  end
end
