# == Schema Information
#
# Table name: patient_records
#
#  avatar_content_type        :string
#  avatar_file_name           :string
#  avatar_file_size           :integer
#  avatar_updated_at          :datetime
#  created_at                 :datetime         not null
#  email                      :string
#  encrypted_birthdate        :string
#  encrypted_birthdate_iv     :string
#  encrypted_mobile_number    :string
#  encrypted_mobile_number_iv :string
#  first_name                 :string
#  id                         :integer          not null, primary key
#  last_name                  :string
#  uid                        :string
#  updated_at                 :datetime         not null
#

class PatientRecord < ActiveRecord::Base
  DEFAULT_COUNTRY_CODE = %(PH).freeze

  before_validation :format_mobile_number
  attr_encrypted :birthdate, key: proc {|patient_record| patient_record.birthdate_secret_key}, marshal: true, allow_empty_value: true
  attr_encrypted :mobile_number, key: proc {|patient_record| patient_record.mobile_number_secret_key}, marshal: true, allow_empty_value: true
  before_create :generate_uid

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  has_many :appointments
  has_attached_file :avatar, styles: { medium: '300x300#', thumb: '100x100#', small: '50x50#' },
    default_url: 'icon.png'

  validate :mobile_number_uniqueness, on: :create
  validates_presence_of :first_name, :last_name, :mobile_number
  # validates_uniqueness_of :mobile_number
  validates :email, uniqueness: true, if: 'email.present?'
  validates_format_of :email, with: VALID_EMAIL_REGEX, allow_blank: true
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/(jpg|jpeg|png)\z/,
    :message => 'only .jpg, .jpeg, or .png images'
  before_post_process :rename_avatar

  def self.find_by_mobile_number(number = "")
    formatted_number = Phonelib.parse(number, "PH").e164
    PatientRecord.find_each.find{ |u| u.mobile_number == formatted_number}
  end

  def name
    "#{first_name.strip} #{last_name.strip}"
  end

  def age_string
    return "" if birthdate.nil?
    "#{(DateTime.now.year - birthdate.year)} years old"
  end

  def last_appointment
    appointments.for_agenda.by_latest.last
  end

  def appointments_with_hmo
    appointments.confirmed.where(hmo: true)
  end

  def hmo
    has_hmo? ? last_hmo_used : HMO.new
  end

  def has_hmo?
    appointments_with_hmo.present?
  end

  def last_hmo_used
    appointments_with_hmo.last.hmo
  end

  def birthdate_secret_key
    ENV.fetch('BIRTHDATE_SECRET_KEY')
  end

  def mobile_number_secret_key
    ENV.fetch('MOBILE_NUMBER_SECRET_KEY')
  end

  private

  def mobile_number_uniqueness
    if mobile_number.present? && PatientRecord.find_each.find{ |p| p.id != id && p.mobile_number == mobile_number }
      errors.add(:base, "Mobile number already in use")
    end
  end

  def generate_uid
    self.uid = loop do
      random_token = SecureRandom.hex(16)
      break random_token unless self.class.exists?(uid: random_token)
    end
  end

  def rename_avatar
    extension = File.extname(avatar_file_name).downcase
    self.avatar.instance_write :file_name, "#{SecureRandom.uuid}#{extension}"
  end

  def format_mobile_number
    country_code = DEFAULT_COUNTRY_CODE # TODO: Set to user specific code?
    valid_mobile_number = Phonelib.valid_for_country?(mobile_number, country_code)
    if valid_mobile_number
      self.mobile_number = Phonelib.parse(mobile_number, country_code).e164
    end
  end
end
