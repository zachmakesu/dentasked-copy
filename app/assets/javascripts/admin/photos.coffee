# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  file_field = $("#encoded_file_field")
  thumbnail = $("#croppable-thumbnail")
  container = $("#croppable-container")
  thumbnail_image = $(".croppable-image-thumbnail")
  image = $("#croppable-image")

  $("#croppable-cancel").on "click", (e) ->
    e.preventDefault()
    hideCropper()

  $("#croppable-save").on "click", (e) ->
    e.preventDefault()
    hideCropper()
    cropAction()

  $("#croppable-crop").on "click", (e) ->
    e.preventDefault()
    showCropper()
    setupCropper()

  cropAction = ->
    url = image.cropper('getCroppedCanvas').toDataURL('image/jpeg')
    file_field.val(url)
    thumbnail_image.css("background-image", "url(#{url}")

  setupCropper = ->
    image.cropper
     aspectRatio: 1
     viewMode: 1

  showCropper = ->
    container.show()
    thumbnail.hide()

  hideCropper = ->
    thumbnail.show()
    container.hide()

