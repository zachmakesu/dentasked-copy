$(document).ready(function(){
  $('[data-tmpi-toggle]').hide();
  $('[data-tmpi-toggle-link]').on('click', function(){
    var el = $(this).parents("table.tmpi").find("[data-tmpi-toggle]");
    var link = $(this);
    if(el.is(":visible")) {
      el.hide();
      link.text("More");
    } else {
      el.show();
      link.text("Hide");
    }
  });
});
