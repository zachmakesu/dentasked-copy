// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready(function(){
    $('#add-more-features').on('click', function(e){
    e.preventDefault();
    var wrapper = $('#features-wrapper');
    var counter = $(wrapper).children('div').length;
    counter++;
    var template = JST['admin/app_version_features']({counter: counter})
    $(wrapper).append(template);
    $('*.remove-features-btn').on('click', function(e){
      e.stopPropagation();
      $(this).parent().remove();
    });
  });
});
