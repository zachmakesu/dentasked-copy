class AppVersionDecorator < Draper::Decorator
  delegate_all
  decorates_association :features

  def type
    "app_versions"
  end
end
