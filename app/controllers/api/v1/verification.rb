module API
  module V1
    class Verification < Grape::API
      resource :verification do
        desc "Mobile number verification for users who logged in with facebook"
        params do
          requires :uid, type: String, desc: "UID of user to be verified"
          requires :mobile_number, type: String, desc: "Mobile number of User to be verified in 639xxxxxxx format"
        end
        post "/link" do
          mobile_number = params[:mobile_number]
          existing_mobile_number = !!User.find_by_mobile_number(mobile_number)
          error!("Mobile number already in use", 400) if existing_mobile_number
          user = User.find_by!(uid: params[:uid])
          last_activation = user.activations.last
          if last_activation.expired?
            activation = user.activations.create!
            SMS::ActivationSender.perform_async(activation.id, mobile_number: mobile_number)
            { message: "New activation code sent!" }
          else
            SMS::ActivationSender.perform_async(last_activation.id, mobile_number: mobile_number)
            { message: "Activation code sent!" }
          end
        end

        desc "Mobile number verification"
        params do
          requires :code, type: String, desc: "Activation code received by the user via SMS"
        end
        post do
          activation = Activation.find_by(code: params[:code])
          error!("Invalid activation code, try again.", 400) if activation.nil?
          error!("Account already active. Please try logging in.", 400) if activation.activated?
          if activation.expired?
            error!("Expired activation code. Please request a new one.", 400)
          else
            #  Potentially vulnerable to user changing own mobile number to an
            #  existing user's mobile number
            # optional :uid, type: String, desc: "UID of user verifying mbile number"
            # given :uid do
            #   requires :mobile_number, type: String, desc: "Mobile number of User to be verified in 639xxxxxxx format"
            # end
            activation.activate!
            if params[:mobile_number].present?
              user = User.find_by!(uid: params[:uid])
              user.update(mobile_number: params[:mobile_number])
            end
            { message: "Verification successful! You can now log in." }
          end
        end
      end

      desc "Resend activation code to user via SMS"
      params do
        optional :uid, type: String, desc: "UID of user to be verified"
        requires :mobile_number,
          type: String,
          desc: "Mobile number of User to resend code to in 639xxxxxxx format",
          allow_blank: false
      end
      post "/resend" do
        mobile_number = params[:mobile_number]
        if user = User.find_by(uid: params[:uid]) || User.find_by_mobile_number(mobile_number)
          error!("User already activated", 400) if user.activated?
          activation = user.activations.create!
          SMS::ActivationSender.perform_async(activation.id, mobile_number: mobile_number, resend: true)
          { message: "New activation code sent!" }
        else
          error!("User specified was not found", 404)
        end
      end

      desc "Endpoint for secretary code verification"
      params do
        requires :code, type: String, desc: "Secretary registration code"
      end
      post "secretary/verification", jbuilder: "v1/activations/secretary" do
        activation = Activation.find_by_code(params[:code])
        if activation
          @user = activation.user
          error!("Secretary already active, please try logging in", 500) if @user.activated?
        else
          error!("Invalid activation code!", 500)
        end
      end
    end
  end
end
