module API
  module V1
    class Pusher < Grape::API
      resource :pusher do
        params do
          requires :channel_name, type: String, desc: 'Presence channel to subscribe to'
          requires :socket_id, type: String, desc: 'socket_id of user authenticating'
        end
        post 'auth' do
          if @current_user = current_user
            response = ::Pusher[params[:channel_name]].authenticate(params[:socket_id], {
              :user_id => @current_user.uid, # => required
              :user_info => { # => optional - for example
                :first_name => @current_user.first_name,
                :last_name => @current_user.last_name,
                :email => @current_user.email,
                :role => @current_user.role
              }
            })
            response
          else
            error!('Forbidden', 403)
          end
        end
      end
    end
  end
end
