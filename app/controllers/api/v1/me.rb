module API
  module V1
    class Me < Grape::API
      resources :me do
        desc 'Current patient\'s appointment history'
        get '/history', jbuilder: 'v1/bookings/index' do
          @bookings = current_user.bookings.recent
        end

        resources :devices do
          desc 'Current user\'s devices registration endpoint'
          params do
            requires :name, type: String, desc: 'Name identifier of device'
            requires :token, type: String, desc: 'Token of device'
            requires :platform, type: String, desc: 'Platform of device i.e. ios, android, etc'
          end
          post '/' do
            @device = Device.new(
              name: params[:name],
              token: params[:token],
              platform: params[:platform]
            )
            if @device.register_to(current_user)
              @device
            else
              error!(@device.errors.full_messages.join('\n'), 500)
            end
          end
        end

        desc "Fetch google oauth and refresh token using auth code"
        params do
          requires :auth_code, type: String, desc: 'Google auth code'
        end
        post '/google_oauth' do
          oauth_handler = GoogleOauthHandler.new(current_user, params[:auth_code])
          oauth_handler.call
          if oauth_handler.success?
            { message: "Google oauth credentials saved." }
          else
            error!(oauth_handler.response.message, 400)
          end
        end

        desc 'Save user\'s Google OAuth Token'
        params do
          requires :provider_uid, type: String, desc: 'OAuth Provider UID'
          requires :oauth_token, type: String, desc: 'OAuth Token from Google'
          optional :refresh_token, type: String, desc: 'Refresh token for current token'
          requires :expires_at, type: String, desc: 'OAuth Token expiration'
        end
        post '/google' do
          if current_user.identities.google.blank?
            @identity = current_user.identities.build(
              uid: params[:provider_uid],
              provider: 'google_oauth2',
              oauth_token: params[:oauth_token],
              oauth_refresh_token: params[:refresh_token],
              oauth_expires_at: params[:expires_at].to_datetime.in_time_zone
            )
            @identity.save
            { message: "OAuth Token Saved!" }
          else
            if current_user.identities.google.expired?
              @identity = current_user.identities.google.refresh_google_token
              { message: "OAuth Token Refreshed!" }
            else
              { message: "OAuth Token is still valid!" }
            end
          end
        end

      end

      resources :s do
        resources :me do
          desc 'Track when user agree to terms and conditions so we could show this just once'
          post '/agree_to_terms' do
            @secretary = current_user
            if @secretary.agree_to_terms
              { message: 'Successfully agreed to terms!' }
            else
              error!('You have already agreed to terms', 400)
            end
          end

          desc 'Returns logo, clinic name and dentists of currently logged in secretary'
          get '/', jbuilder: 'v1/me/secretary' do
            @dentist = current_user.dentists.first
            @secretary = current_user
            @clinic = @dentist.clinics.first
          end
        end
      end

      resources :p do
        resources :me do
          desc 'Track when user agree to terms and conditions so we could show this just once'
          post '/agree_to_terms' do
            @patient = current_user
            if @patient.agree_to_terms
              { message: 'Successfully agreed to terms!' }
            else
              error!('You have already agreed to terms', 400)
            end
          end

          desc 'Returns logo, clinic name and dentists of currently logged in patient'
          get '/', jbuilder: 'v1/me/patient' do
            @patient = current_user
          end

          desc 'Edit profile of currently signed in patient'
          params do
            optional :avatar, type: Rack::Multipart::UploadedFile, desc: 'Dentist\'s avatar'
            optional :first_name, type: String
            optional :last_name, type: String
            optional :email, type: String
            optional :mobile_number, type: String
            optional :birthdate, type: String
            optional :work, type: String
            optional :gender, type: Integer
          end
          post '/edit', jbuilder: 'v1/patients/show' do
            @patient = current_user
            if @patient.present?
              if params[:avatar].present?
                new_avatar = ActionDispatch::Http::UploadedFile.new(params[:avatar])
                @patient.avatar = new_avatar
              end
              @patient.profile.gender = params[:gender].to_f if params[:gender].present?
              @patient.profile.work = params[:work] if params[:work].present?
              @patient.birthdate = Date.strptime(params[:birthdate], '%m/%d/%Y') if params[:birthdate].present?
              @patient.attributes = { first_name: params[:first_name],
                                      last_name: params[:last_name],
                                      email: params[:email],
                                      mobile_number: params[:mobile_number]
              }
              @patient.skip_confirmation!
              unless @patient.save && @patient.profile.save
                error!(@patient.errors.full_messages.join('\n'), 500)
              else
                @patient.confirm
                @patient
              end
            else
              error!('Patient not found', 500)
            end
          end

          resources :favorites do
            desc 'Return list of favorite clinics and dentists'
            get jbuilder: 'v1/search/index' do
              @clinics = current_user.favorite_clinics
              @dentists = current_user.favorite_dentists
            end

            desc 'Add clinic or dentist to favorites'
            params do
              optional :clinic_id, type: Integer, desc: 'ID of Clinic'
              optional :dentist_uid, type: String, desc: 'UID of Dentist'
            end
            post '/new' do
              @to_favorite = if params[:clinic_id]
                               Clinic.find(params[:clinic_id])
                             else
                               Dentist.find_by!(uid: params[:dentist_uid])
                             end

              if current_user.can_favorite?(@to_favorite)
                @favorite = current_user.favorites.build(favorited: @to_favorite)
                unless @favorite.save
                  error!(@favorite.errors.full_messages.join('\n'), 500)
                else
                  { message: "#{@to_favorite.class.to_s} added to favorites"}
                end
              else
                { message: "#{@to_favorite.class.to_s} already marked as favorite"}
              end
            end

            desc 'Delete clinic or dentist to favorites'
            params do
              optional :clinic_id, type: Integer, desc: 'ID of Clinic'
              optional :dentist_uid, type: String, desc: 'UID of Dentist'
            end
            post '/delete' do
              @favorite = if params[:clinic_id].present?
                            current_user.favorites.where(favorited_id: params[:clinic_id],
                                                         favorited_type: 'Clinic').first
                          else
                            dentist = Dentist.find_by!(uid: params[:dentist_uid])
                            current_user.favorites.where(favorited_id: dentist.id,
                                                         favorited_type: 'User').first
                          end

              if @favorite.present?
                unless @favorite.delete
                  error!(@favorite.errors.full_messages.join('\n'), 500)
                else
                  { message: "#{@favorite.favorited_type} removed from favorites"}
                end
              else
                { message: "#{@favorite.favorited_type} not found"}
              end
            end
          end
        end
      end
    end
  end
end
