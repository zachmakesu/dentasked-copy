module API
  module V1
    class Location < Grape::API
      resources :location do
        desc 'Return list of regions'
        get '/regions', jbuilder: 'v1/location/regions' do
          @regions= ::Region.order(name: :asc)
        end
        desc 'Return list of regions'
        params do
          requires :region_id, type: Integer, desc: 'region_id of cities to return'
        end
        get '/cities', jbuilder: 'v1/location/cities' do
          @cities = ::Region.find(params[:region_id]).provinces.map(&:cities).flatten.sort_by(&:name)
        end
      end
    end
  end
end
