module API
  module V1
    class Clinics < Grape::API

      resources :clinics do
        desc 'Return list of Clinics'
        get jbuilder: 'v1/clinics/index' do
          @clinics = Clinic.all
        end

        desc 'Search endpoint for Clinics'
        params do
          optional :query, type: String, desc: 'Name clinic to be searched'
        end
        get '/search', jbuilder: 'v1/clinics/index' do
          @clinics = Clinic.search(params[:query])
        end

        desc 'Return nearby Clinics'
        params do
          optional :lat, type: Float, desc: 'Latitude value of Patient\'s location'
          optional :lng, type: Float, desc: 'Longitude value of Patient\'s location'
        end
        get '/nearby', jbuilder: 'v1/clinics/index' do
          if params[:lat].present? && params[:lng].present?
            @clinics = Clinic.near([params[:lat], params[:lng]], 10)
          else
            @clinics = Clinic.all
          end
        end
      end

      resources :clinic do

        route_param :id do
          desc 'Return clinic data'
          get jbuilder: 'v1/clinics/show' do
            @clinic = Clinic.find_by_id(params[:id])
            unless @clinic.blank?
              @clinic
            else
              {message: 'Clinic doesn\'t exist!' }
            end
          end
        end

      end

    end
  end
end
