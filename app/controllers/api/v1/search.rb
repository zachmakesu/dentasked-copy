module API
  module V1
    class Search < Grape::API
      include Grape::Kaminari
      resources :p do
        resources :search do
          desc 'Return all cities'
          get '/cities', jbuilder: 'v1/search/cities' do
            @grouped_cities = Clinic.find_each.map{|c| c.city}.compact.uniq.group_by{|c| c.province.region }
          end
          desc 'Return all spcialties and services'
          get '/specialtyservices', jbuilder: 'v1/search/specialtyservices' do
            services = Service.all
            specialties = Specialty.all
            @sands = [] << services << specialties
          end

          desc 'Return all services'
          get '/services', jbuilder: 'v1/search/services' do
            @services = Service.order(name: :asc)
          end

          desc 'Return search keywords'
          params do
            optional :city_id, type: String, desc: 'ID of City to search keywords from'
            optional :q, type: String, desc: 'Search query for keyword'
            optional :page, type: String, desc: 'Page number of result'
            optional :hmo_id, type: Integer, desc: 'HMO ID'
          end
          get '/keywords', jbuilder: 'v1/search/keywords' do
            # TODO: Move to own search service
            if params[:city_id]
              cities = City.where(id: params[:city_id].split(','))
              # If city_id id is present, it's likely we're looking for a clinic
              # so we base the dentist query on the clinic
              clinics = Dentist.joins(:clinic_assignments).ransack(clinics_name_or_clinics_services_name_or_first_name_or_last_name_or_specialties_name_cont: params[:q], clinics_city_id_in: cities.map(&:id), hmos_id_in: params[:hmo_id], m: 'and').result.page(params[:page]).map(&:clinics).flatten.uniq
              dentists = Dentist.joins(:clinic_assignments).ransack(first_name_or_last_name_or_clinics_name_cont: params[:q], clinics_city_id_in: cities.map(&:id), hmos_id_in: params[:hmo_id], m: 'and').result.page(params[:page]).flatten
            else
              dentists = Dentist.joins(:clinic_assignments).order(first_name: :asc, last_name: :asc).ransack(first_name_or_last_name_or_clinics_name_or_services_name_or_specialties_name_cont: params[:q], hmos_id_in: params[:hmo_id], m: 'and').result.page(params[:page]).uniq.flatten
              clinics = Clinic.order(name: :asc).ransack(name_or_services_name_cont: params[:q]).result.page(params[:page]).uniq.flatten
            end
            services = Service.order(name: :asc).ransack(name_cont: params[:q]).result.page(params[:page])
            symptoms = Symptom.order(name: :asc).ransack(name_cont: params[:q]).result.page(params[:page])
            specialties = Specialty.order(name: :asc).ransack(name_cont: params[:q]).result.page(params[:page])
            @keywords = []
            [clinics, services, symptoms, specialties, dentists].each do |type|
              @keywords << type.sort_by(&:name)
            end
            @keywords.flatten!
          end

          paginate per_page: 10
          desc 'Return search keywords for Universal Search'
          get '/universal', jbuilder: 'v1/search/index' do
            @clinics = paginate(Clinic.all)
            @dentists = paginate(Dentist.joins(:clinic_assignments).where(clinic_assignments: { id: ClinicAssignment.pluck(:id) }))
          end

          desc 'Return available dentists and clinics'
          params do
            requires :schedule, type: String, desc: 'Schedule chosen by Patient'
            requires :keyword_id, type: Integer, desc: 'Keyword of id to search for'
            requires :keyword_type, type: String, desc: 'Type of keyword being searched for'
            optional :hmo_id, type: Integer, desc: 'HMO ID'
            optional :freetime, type: String, desc: 'Approximate time of the day where Patient is free'
            optional :city_id, type: String, desc: 'ID of City to search keywords from'
            optional :city_name, type: String, desc: "Name of current user's city to search keywords from"
          end
          paginate per_page: 10
          get '/', jbuilder: 'v1/search/index' do
            cities = if params[:city_name].present?
                       City.ransack(name_cont: params[:city_name]).result
                     else
                       City.where(id: params[:city_id].to_s.split(','))
                     end
            @dentists = case params[:keyword_type]
                        when 'service'
                          Dentist.service_filter(params[:keyword_id])
                        when 'clinic'
                          clinic = Clinic.find(params[:keyword_id])
                          Dentist.where(id: clinic.dentist_ids)
                        when 'dentist'
                          Dentist.where(id: params[:keyword_id])
                        when 'symptom'
                          Dentist.all
                        when 'specialty'
                          Dentist.specialty_filter(params[:keyword_id])
                        else
                          error!('Invalid keyword type', 500)
                        end
            @dentists = @dentists.hmo_filter(params[:hmo_id]) if params[:hmo_id].present?
            service_id = params[:keyword_type] != 'Service' ? nil : params[:keyword_id]
            @appointments = Appointment.for_date(params[:schedule]).pending.confirmed.availability(params[:schedule], service_id)
            @appointments = @appointments.preferred_time(params[:freetime]) if params[:freetime].present?
            @dentists, clinics = if cities.empty?
                                   [
                                     @dentists << @appointments.includes(clinic_assignment: :dentist).map { |a| a.clinic_assignment.dentist },
                                     Clinic.where(id: @dentists.flatten.map{|d| d.clinics.pluck(:id)}.flatten.uniq)
                                   ]
                                 else
                                   @dentists = @dentists.reject{|d| (d.clinics.map(&:city_id).flatten & cities.map(&:id)).empty?}
                                   [
                                     @dentists << @appointments.includes(clinic_assignment: [:clinic, :dentist]).select{|a| cities.include?(a.clinic_assignment.clinic.city)}.map{ |a| a.clinic_assignment.dentist },
                                     Clinic.where(city_id: cities.map(&:id), id: @dentists.flatten.map{|d| d.clinics.pluck(:id)}.flatten.uniq)
                                   ]
                                 end
            @dentists = paginate(Dentist.where(id: @dentists.flatten.compact.map(&:id).flatten.uniq))
            @clinics = paginate(clinics)
          end
        end
      end

    end
  end
end
