module API
  module V1
    class AuthRequired < Grape::API
      helpers do
        def authenticate!
          service = AuthenticateRequest.call(
            authorization_header: headers['Authorization'],
            params: params,
            path: request.path
          )
          if service.invalid_token
            error_response = {
              code: service.error_code,
              error: service.message
            }
            error!(error_response, 401)
          elsif service.failure?
            error!(service.message, 401)
          end
        end

        def current_user
          if /\ADentasked ([\w]+):([\w\+\=]+)\z/ =~ headers['Authorization']
            uid = $1
            @current_user ||= begin
                                user = User.find_by!(uid: uid)
                                if user.patient?
                                  Patient.find(user.id)
                                elsif user.secretary?
                                  Secretary.find(user.id)
                                else
                                  nil
                                end
                              end
          end
        end
      end

      before do
        authenticate!
      end

      # Mount all endpoints that require authentication
      mount API::V1::Areas
      mount API::V1::Appointments
      mount API::V1::Clinics
      mount API::V1::Dentists
      mount API::V1::HMO
      mount API::V1::Invites
      mount API::V1::Me
      mount API::V1::Patients
      mount API::V1::Pusher
      mount API::V1::RatingSuggestions
      mount API::V1::Reports
      mount API::V1::Search
      mount API::V1::Specialties
      mount API::V1::Symptoms
      mount API::V1::Devices
    end
  end
end
