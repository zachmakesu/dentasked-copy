module API
  module V1
    class Reports < Grape::API
      resources :reports do
        params do
          requires :user_id, type: String, desc: 'UID of user to report'
        end
        desc 'Endpoint for reporting user'
        post '/' do
          user = User.find_by!(uid: params[:user_id])
          if user && current_user.can_report?(user)
            { notice: 'Successfully submitted report' }
          else
            error!('Failed to report user', 500)
          end
        end
      end
    end
  end
end
