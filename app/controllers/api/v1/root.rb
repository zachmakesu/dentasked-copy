module API
  module V1
    class Root < Grape::API
      version 'v1'
      format :json
      formatter :json, Grape::Formatter::Jbuilder

      helpers do
        def dynamic_assets_url
          ActionController::Base.asset_host.to_s
        end

        def missing_avatar_url
          ActionController::Base.helpers.asset_path('icon.png')
        end

        def missing_dentist_avatar_url
          ActionController::Base.helpers.asset_path('dentist-avatar.png')
        end

        def dynamic_avatar_url(user)
          unless user.avatar.present?
            if !user.is_a?(PatientRecord) || user.try(:dentist?)
              missing_dentist_avatar_url
            else
              missing_avatar_url
            end
          else
            URI.join(dynamic_assets_url, user.avatar.url).to_s 
          end
        end

        def dynamic_logo_url(clinic)
          clinic.logo.present? ? URI.join(dynamic_assets_url, clinic.logo.url).to_s : missing_avatar_url
        end

        def dynamic_schedules(clinic)
          return [] if clinic.schedules.blank?
          if clinic.is_schedule_continous?
            [clinic.display_schedule]
          else
            clinic.schedule_per_day
          end
        end
      end

      rescue_from ActiveRecord::RecordNotFound do |e|
        rack_response('{ "status": 404, "message": "Requested resource not found" }', 404)
      end

      rescue_from Grape::Exceptions::ValidationErrors do |e|
        error!({ messages: e.message }, 400)
      end

      rescue_from :all, backtrace: true do |e|
        ExceptionNotifier.notify_exception(e)
        Rails.logger.debug e unless Rails.env.production?
        error!({ error: 'Internal server error', backtrace: e.backtrace }, 500)
      end

      mount API::V1::AuthLess
      mount API::V1::AuthRequired
    end
  end
end
