module API
  module V1
    class HMO < Grape::API
      resources :hmo do
        desc 'Return list of hmo'
        get jbuilder: 'v1/hmo/index' do
          @hmo = ::HMO.order(name: :asc)
        end
      end
    end
  end
end
