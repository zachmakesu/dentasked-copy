module API
  module V1
    class RatingSuggestions < Grape::API
      resources :rating_suggestions do
        desc "Return list of rating suggestions"
        get jbuilder: "v1/rating_suggestions/index" do
          @rating_suggestions = ::RatingSuggestion.active.all
        end
      end
    end
  end
end
