module API
  module V1
    class Login < Grape::API
      resource :login do
        desc "Login endpoint"
        params do
          requires :email, type: String, desc: "Email of User"
          requires :password, type: String, desc: "Password of User"
          optional :type, type: String, desc: "Type of app this user is logging in from"
        end
        post jbuilder: "v1/login/success" do
          service = AuthenticateAPIUser.call(
            email:     params[:email],
            password:  params[:password],
            role:      params[:type]
          )
          if service.success?
            @user = service.user
            @token = service.access_token
          else
            error_response = {
              code: service.error_code,
              error: service.message
            }
            error_response.merge!(mobile_number: service.user.mobile_number) unless service.user.activated?
            error!(error_response, 401)
          end
        end

        desc "Login via Facebook"
        params do
          requires :token, type: String, desc: "User's Facebook OAuth Token"
          requires :profile, type: String, desc: "User's Profile Type"
        end
        post "/facebook", jbuilder: "v1/login/success" do
          service = AuthenticateFbUser.call(
            token: params[:token],
            role:  params[:profile]
          )
          if service.success?
            @user = service.user
            @token = service.access_token
          else
            error!(service.message, 400)
          end
        end
      end
    end
  end
end
