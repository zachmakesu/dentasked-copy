module API
  module V1
    class Appointments < Grape::API
      resources :appointments do
        desc 'endpoint for new appointments'
        params do
          requires :clinic_assignment_id, type: Integer, desc: 'ID of clinic assignment'
          requires :schedule, type: String, desc: 'Schedule of appointment in ISO8601 format'
          # http://stackoverflow.com/a/9038554
          requires :service_id, type: Integer, desc: 'ID of dental service', regexp: /^[1-9][0-9]*$/
          optional :note, type: String, desc: 'Note for dentist for appointment'
          optional :hmo_id, type: Integer, desc: 'ID of HMO chosen by patient'
          optional :specialty_id, type: Integer, desc: 'ID of Specialty chosen by patient'
        end
        post '/', jbuilder: 'v1/appointments/create' do
          browser = Browser.new(request.user_agent)
          code = Appointment.generate_code(platform: browser.platform.id)
          @appointment = current_user.appointments.build({
            clinic_assignment_id: params[:clinic_assignment_id],
            service_id: params[:service_id],
            specialty_id: params[:specialty_id],
            hmo_id: params[:hmo_id],
            schedule: params[:schedule],
            note: params[:note],
            code: code
          })
          unless @appointment.save
            error!(@appointment.errors.full_messages.join('\n'), 500)
          else
            @clinic = @appointment.clinic_assignment.clinic
            GoogleAnalyticsApi.new.event("Appointment", "Created", "Status", current_user.try(:id))
            GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Created", "Status", current_user.try(:id))

            GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.patient.name}", "Patient Name", current_user.try(:id))
            GoogleAnalyticsApi.new.event("Appointments", "#{@clinic.name}", "Clinic", current_user.try(:id))
            GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.schedule}", "Schedule", current_user.try(:id))

            SMS::PatientBookingRequestSender.perform_async(@appointment.id)
            SMS::PartnerBookingRequestSender.perform_async(@appointment.id)
            AppointmentMailer.request_appointment(@appointment.id).deliver_now unless @appointment.patient.has_temporary_email?
          end
        end

        desc 'Endpoint for returning agenda appointments for a particular dentist'
        params do
          requires :date, type: String, desc: 'Date query for appointments'
          requires :dentist_uid, type: String, desc: 'UID of dentist to query appointments for'
        end
        get '/', jbuilder: 'v1/appointments/index' do
          @dentist = Dentist.find_by!(uid: params[:dentist_uid])
          @appointments = @dentist.clinic_assignments.map{|c| c.appointments.for_date(params[:date]).for_agenda.by_earliest}.flatten
        end

        desc 'Endpoint for returning agenda appointments for all dentists of a secretary'
        params do
          requires :date, type: String, desc: 'Date query for appointments'
        end
        get '/all_dentists', jbuilder: 'v1/appointments/index' do
          clinic_assignments = ClinicAssignment.where(dentist_id: current_user.dentist_ids)
          @appointments = clinic_assignments.map{|c| c.appointments.for_date(params[:date]).for_agenda.by_earliest}.flatten.sort{|x, y| x.schedule <=> y.schedule }
        end

        desc 'Endpoint for returning confirmed appointments count of dentist grouped by date'
        params do
          requires :dentist_uid, type: String, desc: 'UID of dentist to query appointments for'
        end
        get '/confirmed', jbuilder: 'v1/appointments/confirmed' do
          @dentist = Dentist.find_by!(uid: params[:dentist_uid])
          @appointments = @dentist.clinic_assignments.map{|c| c.appointments.for_agenda.by_earliest }.flatten.group_by{|a| a.schedule.to_date }
        end

        desc 'Endpoint for returning confirmed appointments count of secretary\'s dentists grouped by date'
        get '/confirmed/all_dentists', jbuilder: 'v1/appointments/confirmed' do
          clinic_assignments = ClinicAssignment.where(dentist_id: current_user.dentist_ids)
          @appointments = clinic_assignments.map{|c| c.appointments.for_agenda.by_earliest }.flatten.group_by{|a| a.schedule.to_date }
        end
      end

      resources :appointment do
        route_param :id do
          desc 'Sync appointment to Google Calendar account'
          post '/sync' do
            @appointment = Appointment.find(params[:id])
            if @appointment.confirmed?
              # Calendar::Syncer.perform_async(@appointment.id, current_user.id)
              { message: "Appointment added to Google Calendar." }
            else
              error!("Cannot sync appointment with Google Calendar", 400)
            end
          end
        end
      end

      resources :p do
        resources :appointments do
          desc 'Lists all patient\'s appointments'
          get '/', jbuilder: 'v1/patients/appointments/index' do
            @appointments = current_user.appointments.by_latest
          end

          resources :availabilities do

            desc 'List all available appointment schedule availabilities for particular date'
            params do
              requires :date, type: String, desc: 'Date query for schedule availability in 2016-09-10 format'
              requires :dentist_uid, type: String, desc: 'UID of dentist to query schedule availability'
            end
            get '/' do
              @dentist = Dentist.find_by(uid: params[:dentist_uid])
              Appointment.available_times(@dentist, params[:date])
            end

            params do
              requires :date, type: String, desc: 'Date query for schedule availability in 2016-09-10 format'
              requires :dentist_uid, type: String, desc: 'UID of dentist to query schedule availability'
            end
            get '/monthly', jbuilder: 'v1/appointments/monthly_availabilities' do
              @dentist = Dentist.find_by!(uid: params[:dentist_uid])
              @appointments = @dentist.clinic_assignments.map{|c| c.appointments.in_month(params[:date]).by_earliest}.flatten
              @start_date = Date.parse(params[:date]).beginning_of_month
              @end_date = Date.parse(params[:date]).end_of_month
            end
          end
        end

        resources :appointment do
          route_param :id do
            desc 'Returns appointment data'
            get '/show', jbuilder: 'v1/appointments/show' do
              @appointment = current_user.appointments.find(params[:id])
            end

            desc 'Cancel appointment'
            post '/rate' do
              @appointment = current_user.appointments.find(params[:id])
              handler = RatingHandler.new(@appointment, current_user, params)
              if handler.call
                { messages: 'Successfully rated' }
              else
                error!(RatingHandler::FAILED_RATING_MESSAGE, 400)
              end
            end

            desc 'Available times for declined booking'
            get '/availabilities', jbuilder: 'v1/appointments/availabilities' do
              appointment = Appointment.find_by_id(params[:id])
              @times = appointment.available_times
              @dates = appointment.available_dates
            end

            desc 'Endpoint for cancelling pending appointments of patient'
            put '/cancel' do
              @appointment = current_user.appointments.find(params[:id])
              if @appointment.pending?
                @appointment.cancelled!
                GoogleAnalyticsApi.new.event("Appointment", "Cancelled", "Status", current_user.try(:id))
                GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Cancelled", "Status", current_user.try(:id))
                Notifications::PartnerAppointmentNotifier.perform_async(@appointment.id)
                { message: "Appointment successfully marked as cancelled" }
              else
                error!("Appointment cannot be marked as cancelled", 400)
              end
            end
          end
        end
      end

      resources :s do
        resources :appointments do
          params do
            requires :date, type: String, desc: 'Date query for appointments'
            requires :dentist_uid, type: String, desc: 'UID of dentist to query appointments for'
          end
          get '/monthly', jbuilder: 'v1/appointments/index' do
            @dentist = Dentist.find_by!(uid: params[:dentist_uid])
            @appointments = @dentist.clinic_assignments.map{|c| c.appointments.in_month(params[:date]).by_earliest}.flatten
          end

          desc 'Create patient record and walk-in appointment'
          params do
            requires :clinic_assignment_id, type: Integer, desc: 'ID of clinic assignment'
            requires :schedule, type: String, desc: 'Schedule of appointment in ISO8601 format'
            # http://stackoverflow.com/a/9038554
            requires :service_id, type: Integer, desc: 'ID of dental service', regexp: /^[1-9][0-9]*$/
            optional :note, type: String, desc: 'Note for dentist for appointment'
            optional :hmo_id, type: Integer, desc: 'ID of HMO chosen by patient'

            optional :patient_uid, type: String, desc: 'UID of existing patient for walk-in booking'
            optional :mobile_number, type: String, desc: 'Mobile number for new patient record'
            given :mobile_number do
              requires :first_name, type: String, desc: 'First name for new patient record'
              requires :last_name, type: String, desc: 'Last name for new patient record'
              optional :email, type: String, desc: 'Email for new patient record'
              optional :birthdate, type: Date, desc: 'Birthdate for new patient record'
              optional :avatar, type: Rack::Multipart::UploadedFile, desc: 'Patient Photo'
            end

            mutually_exclusive :patient_uid, :mobile_number

          end
          post '/new' do
            browser = Browser.new(request.user_agent)
            code = Appointment.generate_code(platform: browser.platform.id)
            if params[:patient_uid].present?
              if @existing_patient = Patient.find_by(uid: params[:patient_uid]) || PatientRecord.find_by(uid: params[:patient_uid])
                @appointment = @existing_patient.appointments.build({
                  clinic_assignment_id: params[:clinic_assignment_id],
                  service_id: params[:service_id],
                  hmo_id: params[:hmo_id],
                  schedule: params[:schedule],
                  note: params[:note],
                  offline: true,
                  code: code
                })
                unless @appointment.save
                  error!(@appointment.errors.full_messages.join('\n'), 500)
                else
                  GoogleAnalyticsApi.new.event("Appointment", "Created", "Status", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Created", "Status", current_user.try(:id))

                  @clinic = @appointment.clinic_assignment.clinic
                  GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.patient.name}", "Patient Name", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("Appointments", "#{@clinic.name}", "Clinic", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.schedule}", "Schedule", current_user.try(:id))

                  SMS::PatientBookingRequestSender.perform_async(@appointment.id, walkin: true)
                  { message: "You've successfully booked an appointment!" }
                end
              else
                error!("Patient does not exist", 404)
              end
            else
              if @patientrecord = PatientRecord.find_by_mobile_number(params[:mobile_number])
                @appointment = @patientrecord.appointments.build({
                  clinic_assignment_id: params[:clinic_assignment_id],
                  service_id: params[:service_id],
                  hmo_id: params[:hmo_id],
                  schedule: params[:schedule],
                  note: params[:note],
                  offline: true,
                  code: code
                })
                unless @appointment.save
                  error!(@appointment.errors.full_messages.join('\n'), 500)
                else
                  GoogleAnalyticsApi.new.event("Appointment", "Created", "Status", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Created", "Status", current_user.try(:id))

                  @clinic = @appointment.clinic_assignment.clinic
                  GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.patient.name}", "Patient Name", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("Appointments", "#{@clinic.name}", "Clinic", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.schedule}", "Schedule", current_user.try(:id))
                  SMS::PatientBookingRequestSender.perform_async(@appointment.id, walkin: true)
                  { message: "You've successfully booked an appointment!" }
                end
              else
                @patientrecord = PatientRecord.new({
                  first_name: params[:first_name],
                  last_name: params[:last_name],
                  email: params[:email],
                  mobile_number: params[:mobile_number]
                })
                @patientrecord.birthdate = params[:birthdate] if params[:birthdate].present?
                @patientrecord.avatar = ActionDispatch::Http::UploadedFile.new(params[:avatar]) if params[:avatar].present?
                unless @patientrecord.save
                  error!(@patientrecord.errors.full_messages.join('\n'), 500)
                else
                  @appointment = @patientrecord.appointments.build({
                    clinic_assignment_id: params[:clinic_assignment_id],
                    service_id: params[:service_id],
                    hmo_id: params[:hmo_id],
                    schedule: params[:schedule],
                    note: params[:note],
                    offline: true,
                    code: code
                  })
                  unless @appointment.save
                    error!(@appointment.errors.full_messages.join('\n'), 500)
                  else
                    GoogleAnalyticsApi.new.event("Appointment", "Created", "Status", current_user.try(:id))
                    GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Created", "Status", current_user.try(:id))

                    @clinic = @appointment.clinic_assignment.clinic
                    GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.patient.name}", "Patient Name", current_user.try(:id))
                    GoogleAnalyticsApi.new.event("Appointments", "#{@clinic.name}", "Clinic", current_user.try(:id))
                    GoogleAnalyticsApi.new.event("Appointments", "#{@appointment.schedule}", "Schedule", current_user.try(:id))
                    SMS::PatientBookingRequestSender.perform_async(@appointment.id, walkin: true)
                    { message: "You've successfully booked an appointment!" }
                  end
                end
              end
            end
          end

          resources :pendings do

            desc 'List all pending appointments for a particular dentist'
            params do
              requires :dentist_uid, type: String, desc: 'UID of dentist to query appointments for'
            end
            get '/', jbuilder: 'v1/appointments/index' do
              @dentist = Dentist.find_by!(uid: params[:dentist_uid])
              @appointments = @dentist.clinic_assignments.map{|c| c.appointments.pending.by_earliest}.flatten
            end

            desc 'List all pending appointments for all dentists of secretary'
            get '/all_dentists', jbuilder: 'v1/appointments/index' do
              clinic_assignments = ClinicAssignment.where(dentist_id: current_user.dentist_ids)
              @appointments = clinic_assignments.map{|c| c.appointments.pending.by_earliest}.flatten.sort{|x, y| x.schedule <=> y.schedule }
            end

          end

          route_param :id do
            put '/complete' do
              @appointment = Appointment.find_by_id(params[:id])
              if @appointment.confirmed?
                @appointment.complete!
                GoogleAnalyticsApi.new.event("Appointment", "Complete", "Status", current_user.try(:id))
                GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Complete", "Status", current_user.try(:id))
                { message: "Appointment successfully marked as done" }
              else
                error!("Appointment cannot be marked as done", 500)
              end
            end

            put '/cancel' do
              @appointment = Appointment.find_by_id(params[:id])
              if @appointment.confirmed?
                @appointment.cancelled!
                GoogleAnalyticsApi.new.event("Appointment", "Cancelled", "Status", current_user.try(:id))
                GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Cancelled", "Status", current_user.try(:id))
                Notifications::ConsumerAppointmentNotifier.perform_async(@appointment.id)
                { message: "Appointment successfully marked as cancelled" }
              else
                error!("Appointment cannot be marked as cancelled", 500)
              end
            end

            put '/noshow' do
              @appointment = Appointment.find_by_id(params[:id])
              if @appointment.confirmed?
                @appointment.noshow!
                GoogleAnalyticsApi.new.event("Appointment", "No Show", "Status", current_user.try(:id))
                GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "No Show", "Status", current_user.try(:id))
                { message: "Appointment successfully marked as no show" }
              else
                error!("Appointment cannot be marked as no show", 500)
              end
            end

            params do
              requires :new_schedule, type: String, desc: 'New schedule for this appointment'
            end
            put '/reschedule' do
              @appointment = Appointment.find_by_id(params[:id])
              if @appointment.reschedule(params[:new_schedule])
                GoogleAnalyticsApi.new.event("Appointment", "Rescheduled", "Status", current_user.try(:id))
                GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Rescheduled", "Status", current_user.try(:id))
                { message: "Appointment successfully rescheduled" }
              else
                error!("Appointment cannot be rescheduled", 500)
              end
            end

          end

          resources :pending do
            route_param :id do
              desc 'Confirm appointment'
              put '/confirm' do
                @appointment = Appointment.find_by_id(params[:id])
                unless @appointment.confirmed?
                  @appointment.confirmed!
                  @appointment.histories.last.update(processor_id: current_user.id)
                  SMS::PatientBookingConfirmationSender.perform_async(@appointment.id)
                  GoogleAnalyticsApi.new.event("Appointment", "Confirmed", "Status", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Confirmed", "Status", current_user.try(:id))
                  { message: "Appointment successfully confirmed."}
                else
                  { message: "Appointment has already been confirmed."}
                end
              end


              desc 'Ignore appointment'
              put '/ignore' do
                @appointment = Appointment.find_by_id(params[:id])
                unless @appointment.declined?
                  @appointment.declined!
                  @appointment.histories.last.update(processor_id: current_user.id)
                  GoogleAnalyticsApi.new.event("Appointment", "Ignored", "Status", current_user.try(:id))
                  GoogleAnalyticsApi.new.event("#{current_user.role} Appointment", "Ignored", "Status", current_user.try(:id))
                  { message: "Appointment ignored."}
                else
                  { message: "Appointment has already been ignored."}
                end
              end
            end
          end
        end
      end
    end
  end
end
