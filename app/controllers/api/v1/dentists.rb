module API
  module V1
    class Dentists < Grape::API
      include Grape::Kaminari
      resources :s do
        resources :dentists do
          desc 'Return list of Dentists with optional search parameter'
          params do
            optional :query, type: String, desc: 'Name of dentist being searched'
          end
          get jbuilder: 'v1/dentists/index' do
            @dentists = current_user.dentists.search(params[:query])
          end

          desc 'Return dentist UID before adding with activation code'
          params do
            optional :activation_code, type: String, desc: 'Activation code of dentist to add'
          end
          get '/uid' do
            dentist_profile = DentistProfile.find_by!(activation_code: params[:activation_code])
            unless current_user.dentist_ids.include?(dentist_profile.user.id)
              { uid: "#{dentist_profile.user.uid}" }
            else
              error!('Dentist already added', 500)
            end
          end
        end

        resources :dentist do
          route_param :uid do
            paginate per_page: 5
            desc 'Get dentist confirmed appointments history'
            get '/history', jbuilder: 'v1/appointments/confirmed_history' do
              @dentist = Dentist.find_by!(uid: params[:uid])
              @appointments =  paginate(Kaminari.paginate_array(@dentist.clinic_assignments.map{|c| c.appointments.confirmed_appointments.by_latest}.flatten.group_by{|a| a.schedule.to_date }.to_a))
            end

            desc 'Add new dentist to manage'
            post '/add' do
              dentist = Dentist.find_by!(uid: params[:uid])
              if current_user.add_dentist(dentist)
                { message: 'Dentist succesfully added'}
              else
                error!('Dentist already added', 400)
              end
            end

            desc 'Get dentist profile'
            get '/', jbuilder: 'v1/dentists/show' do
              @dentist = current_user.dentists.find_by!(uid: params[:uid])
            end

            desc 'Edit dentist profile and basic information'
            params do
              optional :avatar, type: Rack::Multipart::UploadedFile, desc: 'Dentist\'s avatar'
              optional :license_number, type: String, desc: 'Dentist\'s license number'
              optional :experience, type: String, desc: 'Dentist\'s years of experience'
              optional :fee, type: String, desc: 'Dentist\'s fee'
              optional :specialties, type: String, desc: 'Dentist\'s specialties'
              optional :hmos, type: String, desc: 'Dentist\'s HMO accreditations'
              optional :services, type: String, desc: 'Dentist\'s offered services'
              optional :schedules, type: String, desc: "Dentist's schedules in DAY;START_DATE;END_DATE|DAY... encoded in BASE64"
              optional :clinic_assignment_id, type: String, desc: "Dentist clinic assignment id"
            end
            post '/edit', jbuilder: 'v1/dentists/show' do
              handler = DraftHandler.new(current_user, params, classification: "Edit dentist profile and basic information").create
              if handler.response[:success]
                @dentist = handler.response[:details]
              else
                error!(handler.response[:details], 500)
              end
            end

            desc 'Delete dentist'
            delete '/delete' do
              @dentist = current_user.dentists.find_by!(uid: params[:uid])
              unless current_user.dentists.delete(@dentist)
                error!("Dentist failed to be deleted", 400)
              else
                { message: "Dentist deleted from secretary's list" }
              end
            end
          end
        end
      end

      resources :dentist do
        route_param :uid do
          desc 'Return Dentist data'
          get '/show', jbuilder: 'v1/dentists/show' do
            @dentist = Dentist.find_by!(uid: params[:uid])
          end
        end
      end
    end
  end
end
