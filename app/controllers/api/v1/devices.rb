module API
  module V1
    class Devices < Grape::API

      resources :devices do
        desc 'Create a new device'
        params do
          requires :token, :type => String, :desc => 'Device specific token'
          requires :platform, :type => String, :desc => 'Which platform this device runs on'
          requires :name, :type => String, :desc => 'Device name'
        end
        post '/'  do
          @device = Device.new(token: params[:token],
                               platform: params[:platform],
                               name: params[:name])
          unless @device.register_to(current_user)
            error!(@device.errors.full_messages.join('\n'), 500)
          end
        end
      end

    end
  end
end
