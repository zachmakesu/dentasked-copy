module API
  module V1
    class Areas < Grape::API
      resource :areas do
        params do
          optional :query, type: String, desc: "area to search for"
        end
        get '/' do
          Area.search params[:query]
        end
      end
    end
  end
end
