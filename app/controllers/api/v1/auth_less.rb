module API
  module V1
    class AuthLess < Grape::API
      mount API::V1::Location
      mount API::V1::Login
      mount API::V1::Partners
      mount API::V1::Registration
      mount API::V1::Verification
      mount API::V1::AppVersions
    end
  end
end
