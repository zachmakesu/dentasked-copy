module API
  module V1
    class Partners < Grape::API
      resources :partners do
        resource :clinic do
          desc "Validate clinic information for partner signup"
          post do
            handler = ClinicDraftHandler.new(params)
            error!(handler.errors, 400) unless handler.validate
            { message: ClinicDraftHandler::VALID_MESSAGE }
          end
        end
        resource :secretary do
          desc "Validate secretary information for partner signup"
          post do
            handler = SecretaryDraftHandler.new(params)
            error!(handler.errors, 400) unless handler.validate
            { message: SecretaryDraftHandler::VALID_MESSAGE }
          end
        end
        resource :dentist do
          desc "Validate dentist information for partner signup"
          post do
            handler = DentistDraftHandler.new(params)
            error!(handler.errors, 400) unless handler.validate
            { message: DentistDraftHandler::VALID_MESSAGE }
          end
        end
        resources :hmo do
          desc 'Return list of hmo'
          get jbuilder: 'v1/hmo/index' do
            @hmo = ::HMO.order(name: :asc)
          end
        end
        resources :services do
          desc 'Return list of service'
          get jbuilder: 'v1/partners/services' do
            @services = ::Service.order(name: :asc)
          end
        end
        resources :specialties do
          desc 'Return list of specialization'
          get jbuilder: 'v1/partners/specialties' do
            @specialties = ::Specialty.order(name: :asc)
          end
        end
        desc "Apply as DentaSked partner"
        post do
          clinic_params       = params.fetch(:clinic){ Hash.new }
          secretary_params    = params.fetch(:secretary){ Hash.new }
          dentist_params      = params.fetch(:dentist){ Hash.new }
          handler = PartnerSignupHandler.new(clinic_params, 
                                             secretary_params,
                                             dentist_params)
          if handler.create
            { message: PartnerSignupHandler::SUCCESS_MESSAGE }
          else
            error!(PartnerSignupHandler::ERROR_MESSAGE, 400)
          end
        end
      end
    end
  end
end
