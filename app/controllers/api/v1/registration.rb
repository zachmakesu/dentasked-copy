module API
  module V1
    class Registration < Grape::API
      desc "Endpoint for independent patient registration"
      params do
        requires :first_name, type: String, desc: "Patient's first name"
        requires :last_name, type: String, desc: "Patient's last name"
        requires :email, type: String, desc: "Patient's email"
        requires :password, type: String, desc: "Patient's password"
        requires :mobile_number, type: String, desc: "Patient's mobile number"
        optional :birthday, type: String, desc: "Patient's birthday"
      end
      post "/registration", jbuilder: "/v1/patients/show" do
        @patient = Patient.new(
          first_name: params[:first_name],
          last_name: params[:last_name],
          email: params[:email],
          mobile_number: params[:mobile_number],
          password: params[:password]
        )
        if params[:birthday].present?
          begin
            @patient.birthdate = Date.strptime(params[:birthday].to_s, "(%m) - (%d) - (%Y)").iso8601
          rescue ArgumentError => e
            ExceptionNotifier.notify_exception(e)
          end
        end
        @patient.skip_confirmation!
        if @patient.save
          @patient.confirm
          SMS::ActivationSender.perform_async(@patient.activations.last.id)
          GoogleAnalyticsApi.new.event("Patients", "Created", "Registration", @patient.try(:id))
          UserMailer.welcome_patient(@patient.id).deliver_now
          { message: "Successful registration!" }
        else
          error!(@patient.errors.full_messages.join("\n"), 500)
        end
      end

      desc "Endpoint for password recovery"
      params do
        requires :email, type: String, desc: "User's email"
      end
      post "/password_recovery" do
        if user = User.find_by(email: params[:email])
          token = user.send_reset_password_instructions
          SMS::ResetPasswordInstructionsSender.perform_async(user.id, token)
          { message: "Please check the reset password instructions sent to your email" }
        else
          error!("User with specified email not found", 404)
        end
      end

      desc "Endpoint for secretary registration"
      params do
        requires :code, type: String, desc: "Secretary registration code"
        requires :password, type: String, desc: "Secretary's updated password"
      end
      post "secretary/registration" do
        @activation = Activation.find_by!(code: params[:code])
        @user = @activation.user
        if @user.activated?
          error!("Cannot update password, secretary already active", 400)
        else
          if @user.update(password: params[:password])
            @activation.activate!
            GoogleAnalyticsApi.new.event("Secretaries", "Created", "Registration", @user.try(:id))
            { message: "Successfully updated user password" }
          else
            error!(@user.errors.full_messages.join("\n"), 500)
          end
        end
      end
    end
  end
end
