module API
  module V1
    class Patients < Grape::API
      resources :s do
        resources :patients do
          desc "Return list of Patients"
          params do
            optional :dentist_uid, type: String, desc: "UID of dentist to return patients of"
          end
          get jbuilder: "v1/patients/index" do
            if params[:dentist_uid].present?
              dentist = Dentist.find_by!(uid: params[:dentist_uid])
              clinic_assignments = ClinicAssignment.where(dentist_id: dentist.id)
            else
              dentist_ids = current_user.dentist_ids
              clinic_assignments = ClinicAssignment.where(dentist_id: dentist_ids)
            end
            appointments = clinic_assignments.map(&:appointments).flatten
            @patients = appointments.map(&:patient).uniq
          end
        end
        resources :patient do
          route_param :uid do
            desc "Show endpoint for Patient"
            get jbuilder: "v1/secretary/patient" do
              @patient = Patient.find_by!(uid: params[:uid])
            end
          end
        end
      end

      resources :patients do
        desc "Update patient booking preferences"
        params do
          requires :symptom_id, type: Integer, desc: "ID of Symptom"
          requires :specialty_id, type: Integer, desc: "ID of Specialty"
        end
        put "/preferences" do
          @patient = Patient.find_by!(uid: current_user.uid)
          if @patient.preference.update(symptom_id: params[:symptom_id], specialty_id: params[:specialty_id])
            { notice: "Successfully updated preferences" }
          else
            error!(@patient.preference.errors.full_messages.join("\n"), 500)
          end
        end
      end

      resources :patient do
        route_param :uid do
          desc "Show endpoint for Patient"
          get jbuilder: "v1/patients/show" do
            @patient = Patient.find_by!(uid: params[:uid])
          end
        end
      end
    end
  end
end
