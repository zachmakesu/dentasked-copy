module API
  module V1
    class Invites < Grape::API
      resources :p do
        resources :invites do
          params do 
            requires :mobile_numbers, type: Array, desc: "Array of mobile numbers"
            requires :contact_names, type: Array, desc: "Array of contact names"
          end
          post do
            mobile_numbers = params.fetch(:mobile_numbers){ [] }
            contact_names = params.fetch(:contact_names){ [] }
            service = InviteContacts.new(names: contact_names, numbers: mobile_numbers, inviter: current_user)
            if service.valid?
              service.()
              service.response
            else
              error!(service.error, 400)
            end
          end
        end
      end
    end
  end
end
