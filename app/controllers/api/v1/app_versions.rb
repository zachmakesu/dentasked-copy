module API
  module V1
    class AppVersions < Grape::API
      resources :app_versions do
        desc "Get latest app version based on platform and category"
        get "/:platform/:category/latest", jbuilder: 'v1/app_versions/index' do
          if @app_version = AppVersion.latest(platform: params[:platform], category: params[:category])
          else
            error!( { messages: "App Version not found" } , 400)
          end
        end
      end
    end
  end
end
