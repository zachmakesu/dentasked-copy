module API
  module V1
    class Symptoms < Grape::API
      resource :symptoms do
        desc "Return list of recent symptoms"
        get jbuilder: 'v1/symptoms/index' do
          @symptoms = Symptom.all
        end
      end
    end
  end
end
