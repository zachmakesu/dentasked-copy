module API
  module V1
    class Specialties < Grape::API
      resource :s do
        resources :specialties do
          get jbuilder: 'v1/specialties/index' do
            @specialties = Specialty.all
          end
        end
      end
    end
  end
end
