class TMPIController < ApplicationController
  http_basic_authenticate_with name: ENV.fetch("TMPI_CMS_USERNAME"), password: ENV.fetch("TMPI_CMS_PASSWORD")
  layout 'tmpi'
  def index
    @appointments = Appointment.includes(clinic_assignment: [:clinic, :dentist], patient: {}).new_appointments.group_by{|a| a.created_at.strftime("%m/%d/%Y")}
  end

  def confirm
    @appointment = Appointment.find(params[:id])
    @appointment.confirmed!
    create_tmpi_log(status: 'confirmed')
    redirect_to action: :index
  end

  def decline
    @appointment = Appointment.find(params[:id])
    @appointment.declined!
    create_tmpi_log(status: 'declined')
    redirect_to action: :index
  end

  private

  def http_basic_username
    ActionController::HttpAuthentication::Basic.user_name_and_password(request).first
  end

  def create_tmpi_log(status:)
    TMPILog.create(
      username: http_basic_username,
      status: status,
      appointment_id: @appointment.id
    )
  end
end
