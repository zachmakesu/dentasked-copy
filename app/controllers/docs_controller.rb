class DocsController < ApplicationController
  layout false
  http_basic_authenticate_with name: ENV.fetch("DOCS_USERNAME"), password: ENV.fetch("DOCS_PASSWORD")
  def index
  end
end
