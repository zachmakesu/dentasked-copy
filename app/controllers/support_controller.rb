class SupportController < ApplicationController
  layout :false
  before_action :set_user, :check_required_params

  def index
    @faqs = if check_required_params
              FAQ.where(app_type: params[:type])
            else
              nil
            end
    @feedback = @user.feedbacks.new
  end

  def feedback
    feedback_params = params.require(:feedback).permit(:subject, :description, :uid)
    @user = User.find_by!(uid: feedback_params[:uid])
    @feedback = @user.feedbacks.build(subject: feedback_params[:subject],
                                      description: feedback_params[:description])
    if @feedback.save
      redirect_to support_path(type: @user.role, uid: @user.uid)
    else
      redirect_to support_path(type: @user.role, uid: @user.uid)
    end
  end

  private

  def set_user
    @user = User.find_by!(uid: params[:uid])
  end

  def check_required_params
    unless params[:type].present? && ['Patient', 'Secretary'].include?(params[:type])
      redirect_to unauthenticated_root_url
    else
      true
    end
  end
end
