class HomeController < ApplicationController
  layout :false

  def funfacts
  end

  def successful_password_change
  end

  def ios
    ios_redirect
  end

  def download
    if browser.platform.android?
      android_redirect
    else
      ios_redirect
    end
  end

  private def android_redirect
    link = AppSetting.android_link || ENV.fetch("ANDROID_PATIENT_DOWNLOAD_URL")
    redirect_to link
  end

  private def ios_redirect
    link = AppSetting.ios_link || ENV.fetch("IOS_PATIENT_DOWNLOAD_URL")
    redirect_to link
  end
end
