class Admin::FAQsController < AdminController
  before_action :set_admin_faq, only: [:show, :edit, :update, :destroy]
  set_tab :faq
  # GET /admin/faqs
  # GET /admin/faqs.json
  def index
    set_tab "faq_#{params[:type].downcase}", :submenu unless params[:type].nil?
    @faqs = FAQ.where(app_type: params[:type]).order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /admin/faqs/new
  def new
    @faq = FAQ.new
  end

  # GET /admin/faqs/1/edit
  def edit
  end

  # POST /admin/faqs
  # POST /admin/faqs.json
  def create
    @faq = FAQ.new(faq_params)

    respond_to do |format|
      if @faq.save
        format.html { redirect_to admin_faqs_path(type: @faq.app_type), notice: 'FAQ was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /admin/faqs/1
  # PATCH/PUT /admin/faqs/1.json
  def update
    respond_to do |format|
      if @faq.update(faq_params)
        format.html { redirect_to admin_faqs_path(type: @faq.app_type), notice: 'FAQ was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /admin/faqs/1
  # DELETE /admin/faqs/1.json
  def destroy
    @faq.destroy
    respond_to do |format|
      format.html { redirect_to admin_faqs_path(type: @faq.app_type), notice: 'FAQ was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_faq
    @faq = FAQ.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def faq_params
    params.require(:faq).permit(:question, :answer, :app_type)
  end
end
