class Admin::RatingSuggestionsController < AdminController
  before_action :set_rating_suggestion, only: [:edit, :update, :destroy]
  set_tab :rating_suggestions, only: :index

  # GET /admin/rating_suggestions
  # GET /admin/rating_suggestions.json
  def index
    @rating_suggestions = RatingSuggestion.all.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
    end
  end

  def new
    @rating_suggestion = current_user.rating_suggestions.build
  end

  def create
    @rating_suggestion = current_user.rating_suggestions.build(rating_suggestion_params)
    if @rating_suggestion.save
      redirect_to admin_rating_suggestions_path, notice: "Successfully created rating suggestion"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @rating_suggestion.update(rating_suggestion_params)
      redirect_to admin_rating_suggestions_path, notice: "Successfully updated rating suggestion"
    else
      render :edit
    end
  end

  def destroy
    @rating_suggestion.destroy
    redirect_to admin_rating_suggestions_path, notice: "Successfully deleted rating suggestion"
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_rating_suggestion
    @rating_suggestion = RatingSuggestion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def rating_suggestion_params
    params.require(:rating_suggestion).permit(:name, :enabled)
  end
end
