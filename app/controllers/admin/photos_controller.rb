class Admin::PhotosController < AdminController

  def destroy
    @photo = Photo.find params[:id]
    if @photo.destroy
      respond_to do |format|
        format.all { render nothing: true }
      end
    end
  end

end
