class Admin::RallyExportsController < AdminController
  def registered_users
    respond_to do |format|
      format.csv do
        @users, template = if params.key?(:booking)
                             [activated_users_with_appointments, 'admin/rally_exports/registered_users_with_appointments']
                           else
                             [activated_users, 'admin/rally_exports/registered_users']
                           end
        render template: template
      end
    end
  end

  private

  def filtered_users
    # Manually filtered ids of test users used
    # TODO: Remove once data is coming from fb analytics
    [34, 56, 57, 59, 61, 63, 69, 1050, 2174, 2955, 2970, 2972, 2974, 2975, 2977, 2978, 2979, 2980]
  end

  def activated_users
    user_collection_from(Patient.includes(:activations).select{|p| p.activated? && !filtered_users.include?(p.id)})
  end

  def activated_users_with_appointments
    user_collection_from(activated_users.includes(:appointments).select{|u| u.appointments.online.complete.count >= 1})
  end

  def user_collection_from(users)
    Patient.where(id: users.map(&:id))
  end
end
