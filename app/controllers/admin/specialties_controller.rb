class Admin::SpecialtiesController < AdminController
  before_action :set_specialty, only: [:show, :edit, :update, :destroy]
  set_tab :specialties, only: :index
  # GET /admin/specialties
  def index
    @specialties = Specialty.all.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data Specialty.all.to_csv }
    end
  end

  # GET /admin/specialties/1
  def show
  end

  # GET /admin/specialties/new
  def new
    @specialty = Specialty.new
  end

  # GET /admin/specialties/1/edit
  def edit
  end

  # POST /admin/specialties
  def create
    @specialty = Specialty.new(specialty_params)

    if @specialty.save
      redirect_to admin_specialties_path, notice: 'Specialty was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /admin/specialties/1
  def update
    if @specialty.update(specialty_params)
      redirect_to admin_specialties_path, notice: 'Specialty was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /admin/specialties/1
  def destroy
    @specialty.destroy
    redirect_to admin_specialties_url, notice: 'Specialty was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_specialty
    @specialty = Specialty.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def specialty_params
    params.require(:specialty).permit(:name)
  end
end
