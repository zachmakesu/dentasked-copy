class Admin::RatingsController < AdminController
  before_action :set_rating, only: [:show, :edit, :update, :destroy]
  set_tab :ratings, only: :index

  # GET /admin/ratings
  # GET /admin/ratings.json
  def index
    @ratings = Rating.all.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /admin/ratings/1
  # GET /admin/ratings/1.json
  def show
    @rating = Rating.find(params[:id])
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_rating
    @rating = Rating.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def rating_params
    params.require(:rating).permit(:booking_id, :description, :ratee_id, :ratee_type, :rater_id, :rater_type, :rating)
  end
end
