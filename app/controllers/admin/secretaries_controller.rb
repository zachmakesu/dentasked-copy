class Admin::SecretariesController < AdminController
  before_action :set_secretary, only: [:show, :edit, :update, :destroy]
  set_tab :secretaries
  set_tab :secretaries_index, :submenu, only: :index
  set_tab :secretaries_appointments_graph, :submenu, only: :graph
  set_tab :secretaries_graph, :submenu, only: :secretaries_graph

  # GET /admin/secretaries
  # GET /admin/secretaries.json
  def index
    @secretaries = Secretary.includes(:activations).search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)

    @secretary_data = Secretary.includes(:activations, :dentists).all
    # TODO: Optimize this query for activated secretaries
    @secretary_data = Secretary.includes(:activations, :dentists).where(id: @secretary_data.select(&:activated?).map(&:id)) if params[:type] == "activated"

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data @secretary_data.to_csv }
    end
  end

  # GET /admin/secretaries/1
  # GET /admin/secretaries/1.json
  def show
  end

  # GET /admin/secretaries/new
  def new
    @secretary = Secretary.new
  end

  # GET /admin/secretaries/1/edit
  def edit
  end

  # POST /admin/secretaries
  # POST /admin/secretaries.json
  def create
    @secretary = Secretary.new(secretary_params)
    @secretary.skip_confirmation!
    if @secretary.save
      redirect_to admin_secretaries_url, notice: 'Secretary was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /admin/secretaries/1
  # PATCH/PUT /admin/secretaries/1.json
  def update
    @secretary.skip_reconfirmation!
    respond_to do |format|
      if @secretary.update(secretary_params)
        format.html { redirect_to admin_secretaries_url, notice: 'Secretary was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /admin/secretaries/1
  # DELETE /admin/secretaries/1.json
  def destroy
    @secretary.destroy
    respond_to do |format|
      format.html { redirect_to admin_secretaries_url, notice: 'Secretary was successfully destroyed.' }
    end
  end

  def graph
    @confirmed_appointments   = AppointmentHistory.confirmed.offline(true)
    @complete_appointments    = AppointmentHistory.complete.offline(true)
  end

  def secretaries_graph
    valid_user_ids = Secretary.active_accounts.pluck(:id)
    @secretaries = Secretary.all.where(id: valid_user_ids)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_secretary
    @secretary = Secretary.includes(:dentists).find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def secretary_params
    if params[:secretary][:password].blank?
      params[:secretary].delete(:password)
    end
    params.require(:secretary).permit(:first_name, :last_name, :email,
                                      :mobile_number, :birthdate, :password,
                                      :password_confirmation, :avatar, dentist_ids: [])
  end
end
