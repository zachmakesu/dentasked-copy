class Admin::DentistsController < AdminController
  before_action :set_dentist, except: [:index, :new, :create]
  set_tab :dentists, only: :index
  # GET /admin/dentists
  def index
    @dentists = Dentist.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)

    @dentists_data = Dentist.includes(:activations)

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data @dentists_data.to_csv }
    end
  end

  # GET /admin/dentists/1
  def show
  end

  # GET /admin/dentists/new
  def new
    @dentist = Dentist.new
  end

  # GET /admin/dentists/1/edit
  def edit
  end

  # POST /admin/dentists
  def create
    @dentist = Dentist.new(permitted_dentist_params)
    @dentist.skip_confirmation!
    if @dentist.save
      @dentist.photos.upload params[:images] if params[:images]
      redirect_to admin_dentists_url, notice: 'Dentist was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /admin/dentists/1
  def update
    @dentist.skip_reconfirmation!
    if @dentist.update(permitted_dentist_params)
      if params[:fee].present?  || params[:experience].present? || params[:color].present? || params[:license_number]
        profile = @dentist.profile
        profile.fee = params[:fee]
        profile.experience = params[:experience]
        profile.color = params[:color]
        profile.license_number = params[:license_number]
        profile.save
      end
      @dentist.photos.upload params[:images] if params[:images]
      redirect_to admin_dentists_url, notice: 'Dentist was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /admin/dentists/1
  def destroy
    @dentist.destroy
    redirect_to admin_dentists_url, notice: 'Dentist was successfully destroyed.'
  end

  # POST /admin/dentists/1/assignment
  def assignment
    assignment_params = params.require(:clinic_assignment).permit(:clinic_id, schedules_attributes: [:day, :start_time, :end_time])
    @clinic = Clinic.find(assignment_params[:clinic_id])
    @clinic_assignment = @dentist.clinic_assignments.find_by_clinic_id(@clinic)
    if @clinic_assignment.present?
      @schedule = @clinic_assignment.schedules.build(assignment_params[:schedules_attributes])
      if @schedule.save
        redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist was successfully assigned.'
      else
        redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist was successfully failed.'
      end
    elsif assignment_params[:schedules_attributes].present?
      new_assignment = @clinic.assign @dentist
      schedule = new_assignment.schedules.build(assignment_params[:schedules_attributes])
      if schedule.save
        redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist was successfully assigned.'
      else
        redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist assignment failed.'
      end
    else
      if @clinic.assign @dentist
        redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist was successfully assigned.'
      else
        redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist assignment failed.'
      end
    end
  end

  # POST /admin/dentists/1/unassignment
  def unassignment
    assignment_params = params.require(:clinic_assignment).permit(:clinic_id)
    @clinic = Clinic.find(assignment_params[:clinic_id])
    if @clinic.unassign @dentist
      redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist was successfully unassigned.'
    else
      redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist unassignment failed.'
    end
  end

  def delete_schedule
    schedule_params = params.require(:assignment_schedule).permit(:assignment_schedule_id)
    @clinic_assignment = ClinicAssignment.find(params[:clinic_assignment_id])
    @assignment = @clinic_assignment.schedules.find(schedule_params[:assignment_schedule_id])
    if @assignment.destroy
      redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist Clinic schedule deleted.'
    else
      redirect_to edit_admin_dentist_url(@dentist), notice: 'Dentist Clinic schedule deleted.'
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_dentist
    @dentist = Dentist.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def permitted_dentist_params
    params.require(:dentist).permit(:first_name, :last_name, :email,
                                    :mobile_number, :birthdate, :password,
                                    :password_confirmation, :avatar,
                                    service_ids: [], hmo_ids: [],
                                    specialty_ids: [])
  end
end
