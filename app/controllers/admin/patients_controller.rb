class Admin::PatientsController < AdminController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  set_tab :patients
  set_tab :patients_index, :submenu, only: :index
  set_tab :patients_appointments_graph, :submenu, only: :graph
  set_tab :patients_graph, :submenu, only: :patients_graph

  # GET /admin/patients
  def index
    @patients = Patient.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)

    @patients_data = Patient.includes(:activations)

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data @patients_data.to_csv }
    end
  end

  # GET /admin/patients/1
  def show
  end

  # GET /admin/patients
  def new
    @patient = Patient.new
  end

  def edit
  end

  def create
    @patient = Patient.new(patient_params)
    @patient.skip_confirmation!
    if @patient.save
      @patient.photos.upload params[:images] if params[:images]
      redirect_to admin_patients_url, notice: 'Patient was successfully created.'
    else
      render :new
    end
  end

  def update

    if params[:patient][:password].blank?
      params[:patient].delete(:password)
      params[:patient].delete(:password_confirmation)
    end

    if @patient.update(patient_params)
      @patient.photos.upload params[:images] if params[:images]
      redirect_to admin_patients_url, notice: 'Patient was successfully updated.'
    else
      render :edit
    end
  end

  def graph
    @created_appointments     = AppointmentHistory.offline(false)
    @confirmed_appointments   = AppointmentHistory.confirmed.offline(false)
    @complete_appointments    = AppointmentHistory.complete.offline(false)
  end

  def patients_graph
    valid_user_ids = Patient.active_accounts.pluck(:id)
    @patients = Patient.all.where(id: valid_user_ids)
  end

  private

  def set_patient
    @patient = Patient.find(params[:id])
  end

  def patient_params
    params.require(:patient).permit(:first_name, :last_name, :email,
                                    :mobile_number, :birthdate, :password,
                                    :password_confirmation, :avatar)
  end
end
