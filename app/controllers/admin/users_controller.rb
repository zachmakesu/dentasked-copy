class Admin::UsersController < AdminController
  before_action :set_user, only: [:show, :edit, :update, :toggle_status, :resend_code]
  set_tab :users
  set_tab :users_index, :submenu, only: :index
  set_tab :users_graph, :submenu, only: :graph

  # GET /admin/users
  def index
    @users = User.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)
    @users_data = User.includes(:activations, :devices)
    respond_to do |format|
      format.html
      format.js
      format.csv { send_data @users_data.to_csv }
    end
  end

  # GET /admin/users/1
  def show
  end

  # GET /admin/users/1/edit
  def edit
  end

  # PATCH/PUT /admin/users/1
  def update
    if @user.update(permitted_user_params)
      @user.photos.upload params[:images] if params[:images]
      redirect_to edit_admin_user_path(@user), notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  def graph
    valid_user_ids = User.active_accounts.pluck(:id)
    @app_users = User.all.where(id: valid_user_ids)
  end

  def toggle_status
    if @user.enabled?
      @user.disable!
    else
      @user.enable!
    end
    redirect_to :back
  end

  def resend_code
    redirect_to(edit_admin_user_path(@user), notice: "User is already activated") if @user.activated?
    unless @user.mobile_number.blank?
      activation = @user.activations.create!
      SMS::ActivationSender.perform_async(activation.id, mobile_number: @user.mobile_number)
      redirect_to(:back, notice: "New user activation code sent")
    else
      message = "Cannot resend activation code without mobile number"
      redirect_to(:back, alert: message)
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def permitted_user_params
    params.require(:user).permit(:first_name, :last_name, :email,
                                 :mobile_number, :birthdate, :password,
                                 :password_confirmation, :avatar, :role_ids => [])
  end
end
