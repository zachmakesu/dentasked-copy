class Admin::HMOsController < AdminController
  before_action :set_hmo, only: [:show, :edit, :update, :destroy]
  set_tab :hmos, only: :index

  # GET /admin/hmos
  # GET /admin/hmos.json
  def index
    @hmos = HMO.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data HMO.all.to_csv }
    end
  end

  # GET /admin/hmos/1
  # GET /admin/hmos/1.json
  def show
  end

  # GET /admin/hmos/new
  def new
    @hmo = HMO.new
    @logo = @hmo.build_logo
  end

  # GET /admin/hmos/1/edit
  def edit
  end

  # POST /admin/hmos
  # POST /admin/hmos.json
  def create
    @hmo = HMO.new(hmo_params)

    respond_to do |format|
      if @hmo.save
        format.html { redirect_to admin_hmo_url(@hmo), notice: 'HMO was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /admin/hmos/1
  # PATCH/PUT /admin/hmos/1.json
  def update
    respond_to do |format|
      if @hmo.update(hmo_params)
        format.html { redirect_to admin_hmo_url(@hmo), notice: 'HMO was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /admin/hmos/1
  # DELETE /admin/hmos/1.json
  def destroy
    @hmo.destroy
    respond_to do |format|
      format.html { redirect_to admin_hmos_url, notice: 'HMO was successfully destroyed.' }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_hmo
    @hmo = HMO.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def hmo_params
    params.require(:hmo).permit(:name, :description, logo_attributes: [:image])
  end
end
