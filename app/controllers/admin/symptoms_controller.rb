class Admin::SymptomsController < AdminController
  set_tab :symptoms, only: :index

  def index
    @symptoms = Symptom.all.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data Symptom.all.to_csv }
    end
  end

  def new
    @symptom = Symptom.new
  end

  def create
    @symptom = Symptom.new permitted_symptoms_param
    if @symptom.save
      redirect_to admin_symptoms_path, notice: 'Symptom was successfully created.'
    else
      render 'new'
    end
  end

  def edit
    @symptom = Symptom.find(params[:id])
  end

  def update
    @symptom = Symptom.find(params[:id])
    if @symptom.update permitted_symptoms_param
      redirect_to edit_admin_symptom_path(@symptom), notice: 'Symptom was successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    @symptom = Symptom.find(params[:id])
    @symptom.destroy
    redirect_to admin_symptoms_path, notice: 'Symptom was successfully deleted.'
  end

  private
  def permitted_symptoms_param
    params.require(:symptom).permit(:name)
  end
end
