class Admin::ClinicsController < AdminController
  before_action :set_clinic, only: [:show, :edit, :update, :destroy]
  set_tab :clinics
  set_tab :clinics_list, :submenu, except: %w(map)
  set_tab :clinics_map, :submenu, only: %w(map)
  # GET /admin/clinics
  def index
    @clinics = Clinic.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)

    @clinic_data = if params[:type] == "all"
                     Clinic.all
                   elsif params[:type] == "activated"
                     Clinic.where(id: Clinic.all.map{|c| c.id unless c.activated_secretaries.blank? }.compact)
                   end

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data @clinic_data.to_csv }
    end
  end

  # GET /admin/clinics/1
  def show
  end

  # GET /admin/clinics/new
  def new
    @clinic = Clinic.new
  end

  # GET /admin/clinics/1/ediActionController::Base.helpers.asset_path(t
  def edit
  end

  # POST /admin/clinics
  def create
    @clinic = Clinic.new(clinic_params)

    if @clinic.save
      @clinic.photos.upload params[:images] if params[:images]
      redirect_to admin_clinics_path, notice: 'Clinic was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /admin/clinics/1
  def update
    if @clinic.update(clinic_params)
      @clinic.photos.upload params[:images] if params[:images]
      redirect_to admin_clinics_path, notice: 'Clinic was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /admin/clinics/1
  def destroy
    @clinic.destroy
    redirect_to admin_clinics_path, notice: 'Clinic was successfully destroyed.'
  end

  def map
    @clinics = []
    Clinic.includes(clinic_assignments: [{dentist: [:roles, :secretary_assignments, :activations]}]).find_each do |c| 
      unless c.activated_secretaries.blank?
        @clinics << [c.name.to_s, c.mobile_number, c.address, c.lat, c.lng, (c.logo.blank? ? ActionController::Base.helpers.asset_path('Locator.png') : c.logo.url), c.dentists.map{|d| [d.avatar.url, d.name]}]
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_clinic
    @clinic = Clinic.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def clinic_params
    params.require(:clinic).permit(:name, :address, :logo, :lat, :lng, :city_id, :mobile_number, :parking_slot,  service_ids: [])
  end
end
