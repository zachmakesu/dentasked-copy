class Admin::CitiesController < ApplicationController
  before_action :set_admin_city, only: [:show, :edit, :update, :destroy]
  set_tab :location_editor
  layout 'admin'

  # GET /admin/cities
  # GET /admin/cities.json
  def index
    @regions = Region.all
    @provinces = Province.where(region_id: (params[:region] || @regions.first.id))
    @cities = City.where("province_id = ? AND name ILIKE ?",(params[:province] || @provinces.first.id), "%#{params[:q]}%").order(:name).page(params[:page]).per(10)
  end

  def provinces
    @provinces = Province.where(region_id: (params[:region] || @regions.first.id))

    respond_to do |format|
      format.js { render json: @provinces }
    end
  end

  # GET /admin/cities/1
  # GET /admin/cities/1.json
  def show
  end

  # GET /admin/cities/new
  def new
    @admin_city = City.new
  end

  # GET /admin/cities/1/edit
  def edit
  end

  # POST /admin/cities
  # POST /admin/cities.json
  def create
    @admin_city = City.new(admin_city_params)

    respond_to do |format|
      if @admin_city.save
        format.html { redirect_to @admin_city, notice: 'City was successfully created.' }
        format.json { render :show, status: :created, location: @admin_city }
      else
        format.html { render :new }
        format.json { render json: @admin_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/cities/1
  # PATCH/PUT /admin/cities/1.json
  def update
    respond_to do |format|
      old_city_name = @admin_city.name
      old_province_name = @admin_city.province.name
      if @admin_city.update(admin_city_params)
        format.html { redirect_to admin_cities_path, notice: "City #{old_city_name} with province of #{old_province_name}  was successfully updated to #{@admin_city.name}, province of #{@admin_city.province.name}." }
        format.json { render :show, status: :ok, location: @admin_city }
      else
        format.html { render :edit }
        format.json { render json: @admin_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/cities/1
  # DELETE /admin/cities/1.json
  def destroy
    @admin_city.destroy
    respond_to do |format|
      format.html { redirect_to admin_cities_url, notice: 'City was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_city
    @admin_city = City.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_city_params
    params.require(:admin_city).permit(:name, :psgc_code, :province_id)
  end
end
