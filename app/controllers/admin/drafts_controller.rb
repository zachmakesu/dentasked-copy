class Admin::DraftsController < ApplicationController
  before_action :set_admin_draft, only: [:show, :edit, :update, :destroy]
  set_tab :drafts
  layout 'admin'

  # GET /admin/drafts
  # GET /admin/drafts.json
  def index
    @admin_drafts = Draft.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /admin/drafts/1
  # GET /admin/drafts/1.json
  def show
  end

  # GET /admin/drafts/new
  def new
    @admin_draft = Admin::Draft.new
  end

  # GET /admin/drafts/1/edit
  def edit
  end

  # POST /admin/drafts
  # POST /admin/drafts.json
  def create
    @admin_draft = Admin::Draft.new(admin_draft_params)

    respond_to do |format|
      if @admin_draft.save
        format.html { redirect_to @admin_draft, notice: 'Draft was successfully created.' }
        format.json { render :show, status: :created, location: @admin_draft }
      else
        format.html { render :new }
        format.json { render json: @admin_draft.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/drafts/1
  # PATCH/PUT /admin/drafts/1.json
  def update
    response = if params[:status] == "approved"
                 DraftHandler.approve!(@admin_draft, current_user)
               else  
                 DraftHandler.decline!(@admin_draft, current_user)
               end

    respond_to do |format|
      if response[:success]
        AdminNotifications::DraftNotifier.perform_async(@admin_draft.id)
        format.html { redirect_to admin_drafts_path, notice: "Draft of secretary #{@admin_draft.owner.first_name} for Dentist #{@admin_draft.dentist.first_name} was #{@admin_draft.status}." }
        format.json { render :show, status: :ok, location: @admin_draft }
      else
        format.html { redirect_to admin_drafts_path, alert: response[:details] }
        format.json { render json: @admin_draft.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/drafts/1
  # DELETE /admin/drafts/1.json
  def destroy
    @admin_draft.destroy
    respond_to do |format|
      format.html { redirect_to admin_drafts_url, notice: 'Draft was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_draft
    @admin_draft = Draft.find_by(id: params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_draft_params
    params.require(:admin_draft).permit(:status, :reviewed_at, :reviewer_id)
  end
end
