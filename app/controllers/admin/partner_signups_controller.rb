class Admin::PartnerSignupsController < ApplicationController
  before_action :set_admin_partner_signup, only: [:update]
  set_tab :partner_signups
  layout 'admin'

  # GET /admin/partner_signups
  # GET /admin/partner_signups.json
  def index
    @admin_partner_signups = PartnerSignup.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # PATCH/PUT /admin/partner_signups/1
  # PATCH/PUT /admin/partner_signups/1.json
  def update
    response = if params[:status] == "approved"
                 PartnerSignupHandler.approve!(@admin_partner_signup, current_user)
                 PartnerSignupHandler.send_verification_code(@admin_partner_signup)
               else
                 PartnerSignupHandler.decline!(@admin_partner_signup, current_user)
               end

    respond_to do |format|
      if response
        format.html { redirect_to admin_partner_signups_path, notice: "Partner signup was #{@admin_partner_signup.status}." }
      else
        format.html { redirect_to admin_partner_signups_path, alert: "Partner signup failed to update" }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_partner_signup
    @admin_partner_signup = PartnerSignup.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_partner_signup_params
    params.require(:admin_partner_signup).permit(:status, :reviewed_at, :reviewer_id)
  end
end
