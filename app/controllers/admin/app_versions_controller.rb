class Admin::AppVersionsController < AdminController
  set_tab :admin_app_versions, only: %w(index, show, edit, update)
  before_filter :set_app_version, only: [:show, :edit, :update, :destroy]
  before_filter :set_platform_and_categories, only: [:index, :edit]

  def index
    @app_versions = AppVersion.ransack(created_by_or_version_code_cont: params[:q]).result
    @app_versions = @app_versions.order(created_at: :desc).page(params[:page]).per(10)
    @app_version = AppVersion.new
    @app_version.features.build
  end

  def create
    @app_version = AppVersion.new(app_version_params)

    if @app_version.save
      redirect_to admin_app_versions_path, notice: 'New App Version was successfully created.'
    else
      redirect_to admin_app_versions_path, alert: @app_version.errors.full_messages.to_sentence
    end
  end

  def show
  end

  def edit
  end

  def update
    if @app_version.update(app_version_params)
      redirect_to admin_app_versions_path, notice: 'App Version was successfully updated.'
    else
      redirect_to admin_app_versions_path, alert: @app_version.errors.full_messages.to_sentence
    end
  end

  def destroy
    @app_version.destroy
    redirect_to admin_app_versions_path, notice: 'App Version was successfully deleted.'
  end

  private

  def set_app_version
    redirect_to admin_app_versions_path, alert: "App Version Not Found!" unless @app_version = AppVersion.find(params[:id])
  end

  def set_platform_and_categories
    @version_platforms = AppVersion.platforms.keys
    @version_categories = AppVersion.categories.keys
  end

  def app_version_params
    params.require(:app_version).permit(:version_code, :created_by, :platform, :category, features_attributes: [:title, :details, :id])
  end
end
