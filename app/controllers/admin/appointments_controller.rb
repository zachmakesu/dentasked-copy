class Admin::AppointmentsController < AdminController
  set_tab :appointments
  set_tab :appointments_index, :submenu, only: :index
  set_tab :appointments_graph, :submenu, only: :graph
  # GET /admin/appointments
  def index
    @appointments = if params[:status] == 'new'
                      Appointment.new_appointments
                    elsif params[:status] == 'confirmed'
                      Appointment.confirmed_appointments
                    elsif params[:status] == 'failed'
                      Appointment.failed_appointments
                    elsif params[:status] == 'expired'
                      Appointment.expired_appointments
                    else
                      Appointment.new_appointments
                    end
    @appointments = @appointments.includes(clinic_assignment: :clinic).ransack(patient_user_first_name_or_patient_user_last_name_or_clinic_assignment_clinic_name_cont: params[:query]).result.by_latest
    @appointments = @appointments.page(params[:page]).per(10)
    respond_to do |format|
      format.html
      format.js
      format.csv { send_data Appointment.all.to_csv }
    end
  end

  # GET /admin/appointments/1
  def show
    @appointment = Appointment.find(params[:id])
  end

  def new
    @appointment = Appointment.new
  end

  def create
    permitted_params = params.require(:appointment).permit(:patient_id, :clinic_assignment_id, :service_id, :schedule, :status)
    render :new unless @patient = Patient.find(permitted_params[:patient_id])
    @appointment = @patient.appointments.build(permitted_params)
    if @appointment.save
      redirect_to admin_appointments_path, notice: 'Successfully created appointment!'
    else
      render :new
    end
  end

  def graph
    @pending_appointments     = AppointmentHistory.pending
    @confirmed_appointments   = AppointmentHistory.confirmed
    @cancelled_appointments   = AppointmentHistory.cancelled
    @noshow_appointments      = AppointmentHistory.noshow
    @complete_appointments    = AppointmentHistory.complete
    @rescheduled_appointments = AppointmentHistory.rescheduled
  end
end
