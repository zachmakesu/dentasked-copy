class Admin::SMSController < AdminController
  set_tab :sms

  def index
    unless request.xhr?
      @checkmobi = CheckmobiAccount.new.()
    end
    @sms = SMSLog.order(created_at: :desc).page(params[:page]).per(10)
  end

  def change
    if SMSLog.clients.keys.include?(params[:client])
      AppSetting.sms_client = params[:client]
    end
    redirect_to action: :index
  end
end
