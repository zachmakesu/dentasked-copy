class Admin::FeedbacksController < AdminController
  before_action :set_feedback, only: [:show, :edit, :update, :destroy]
  set_tab :feedbacks, only: :index
  # GET /admin/feedbacks
  # GET /admin/feedbacks.json
  def index
    @feedbacks = Feedback.all.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /admin/feedbacks/1
  # GET /admin/feedbacks/1.json
  def show
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_feedback
    @feedback = Feedback.find(params[:id])
  end
end
