class Admin::ServicesController < AdminController
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  set_tab :services, only: :index

  # GET /admin/services
  def index
    @services = Service.all.order(created_at: :desc).page(params[:page]).per(10)

    respond_to do |format|
      format.html
      format.js
      format.csv { send_data Service.all.to_csv }
    end 
  end

  # GET /admin/services/1
  def show
  end

  # GET /admin/services/new
  def new
    @service = Service.new
  end

  # GET /admin/services/1/edit
  def edit
  end

  # POST /admin/services
  def create
    @service = Service.new(service_params)

    if @service.save
      redirect_to admin_services_path, notice: 'Dental service was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /admin/services/1
  def update
    if @service.update(service_params)
      redirect_to admin_services_path, notice: 'Dental service was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /admin/services/1
  def destroy
    @service.destroy
    redirect_to admin_services_url, notice: 'Dental service was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_service
    @service = Service.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def service_params
    params.require(:service).permit(:name, :description, :duration)
  end
end
