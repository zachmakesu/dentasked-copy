module Admin::ClinicsHelper
  def clinic_choices
    Clinic.all.map{|c| [c.name, c.id] }
  end
end
