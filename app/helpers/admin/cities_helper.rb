module Admin::CitiesHelper
  def city_choices
    City.includes(:province).order(name: :asc).all.map{|c| ["#{c.name} (#{c.province.name})", c.id] }
  end
end
