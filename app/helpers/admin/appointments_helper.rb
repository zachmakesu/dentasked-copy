module Admin::AppointmentsHelper
  def patient_choices
    Patient.order(first_name: :asc).all.map {|c| [c.name, c.id] }
  end

  def clinic_assignment_choices
    ClinicAssignment.includes(:dentist, :clinic).all.map {|ca| ["#{ca.dentist.name} - #{ca.clinic.name}", ca.id]}
  end

  def service_choices
    Service.all.map {|s| [s.name, s.id]}
  end

  def default_date_format(datetime:)
    datetime.strftime("%m/%d/%Y")
  end

  def default_time_format(datetime:)
    datetime.strftime("%H:%M:%S")
  end

  def default_datetime_format(datetime:)
    date = default_date_format(datetime: datetime)
    time = default_time_format(datetime: datetime)
    "#{date}   #{time}"
  end

  # https://stackoverflow.com/a/19596579
  def time_diff_to(datetime:)
    start_time = Time.now
    end_time = datetime
    seconds_diff = (start_time - end_time).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
  end

  def appointment_admin_tab_for(status:, current_status:, custom_title: '')
    current_status = current_status.blank? ? 'new' : current_status
    anchor_title = custom_title.blank? ? status.titleize : custom_title
    class_name = current_status.casecmp(status).zero? ? 'tab-title active' : 'tab-title'
    content_tag :li, class: class_name do
      link_to anchor_title, admin_appointments_path(status: status)
    end
  end

  def symbolized_appointment_source(appointment:)
    appointment.offline ? '✔' : '✘'
  end
end
