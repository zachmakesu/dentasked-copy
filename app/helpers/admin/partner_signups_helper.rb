module Admin::PartnerSignupsHelper
  def display_partner_signup(object)
    key = obj[0]
    value = obj[1]
    split_value = value.split(";")
    obj_value = case key
                when "specialties"  then Specialty.where(id: split_value).pluck(:name).join(", ")
                when "services"     then Service.where(id: split_value).pluck(:name).join(", ")
                when "hmos"         then HMO.where(id: split_value).pluck(:name).join(", ")
                when "schedules"
                  decoded_schedule = Base64.decode64(value)
                  if decoded_schedule.blank?
                    "No schedule provided"
                  else
                    thead = content_tag :thead do
                      content_tag :tr do
                        concat content_tag(:th, "Day")
                        concat content_tag(:th, "Time")
                      end
                    end
                    tbody = content_tag :tbody do
                      decoded_schedule.split('|').collect { |sched|
                        split_schedule = sched.split(";")
                        content_tag :tr do
                          concat content_tag(:td, Date::DAYNAMES[split_schedule[0].to_i])
                          concat content_tag(:td, pm_am_format(split_schedule[1])+" - "+pm_am_format(split_schedule[2]))
                        end
                      }.join.html_safe
                    end
                    content_tag :table, thead.concat(tbody), class: "draft-schedules"
                  end
                else
                  value.blank? ? "N/A" : value
                end
    content_tag :p, %Q(
        <strong></strong>: 
        #{obj_value}
    ).html_safe
  end
end
