module Admin::DentistsHelper

  def format_schedule a
    "#{a.abbr_day_name_upcased}, #{a.start_time.strftime('%l %p')} - #{a.end_time.strftime('%l %p')}"
  end

end
