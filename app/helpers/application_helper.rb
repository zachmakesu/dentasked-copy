module ApplicationHelper
  def filtered_flash(flash)
    # https://github.com/plataformatec/devise/issues/1777
    flash.select do |_key, value|
      value.is_a?(String)
    end
  end
end
