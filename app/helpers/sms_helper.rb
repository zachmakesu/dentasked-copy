module SMSHelper
  def sms_status_label(sms)
    klass = "success label"
    text = "Success"
    if sms.failure?
      klass = "alert label"
      text = "Failed"
    end
    content_tag :span, text, class: klass
  end

  def sms_client_image(sms)
    src, dim = if sms.checkmobi?
                 ["checkmobi", "20px"]
               elsif sms.sns?
                 ["sns", "30px"]
               elsif sms.twilio?
                 ["twilio", "30px"]
               else
                 ["checkmobi", "20px"]
               end

    image_tag "#{src}.png", style: "height: #{dim};"
  end

  def sms_active_client(param)
    if AppSetting.sms_client == param
      content_tag :span, "active", class: "success label"
    end
  end

  def sms_clients
    SMSLog.clients.keys
  end
end
