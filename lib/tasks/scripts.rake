namespace :scripts do
  desc "Encrypt raw PII data for security compliance"
  task pii_encryption: :environment do
    MAPPINGS = {
      "User" => %w(
        birthdate
        mobile_number
      ),
        "PatientRecord" => %w(
        birthdate
        mobile_number
      ),
        "DentistProfile" => %w(
        license_number
      )
    }.freeze

    MAPPINGS.each do |klass, prop_arr|
      prop_arr.each do |prop|
        puts "encrypting #{klass} instance's #{prop}"
        klass.constantize.find_each do |instance|
          raw_value = instance.send("raw_#{prop}")
          instance.send("#{prop}=", raw_value)
          instance.save
          print '✓'
        end
        puts "\n"
      end
    end
  end
  desc "Hash paperclip filenames for security compliance"
  task paperclip_hashing: :environment do
    MAPPINGS = {
      "Clinic"        => "logo",
      "PatientRecord" => "avatar",
      "Photo"         => "image",
      "User"          => "avatar"
    }.freeze
    MAPPINGS.each do |klass, prop|
      klass.constantize.find_each do |instance|
        attachment = instance.send(prop.to_sym)
        filename = "tempfilename"
        (attachment.styles.keys + [:original]).each do |style|
          begin
            old_name = instance["#{prop}_file_name"]
            path = attachment.path(style)
            next if path.nil?
            extname = File.extname(path.to_s)
            new_name = "#{attachment.hash_key(style)}#{extname}"
            old_loc = "#{File.dirname(path.to_s)}/#{old_name}"
            new_loc = "#{File.dirname(path.to_s)}/#{new_name}"
            FileUtils.mv(old_loc, new_loc)
            puts "Moved to #{new_loc}"
            if style == :original
              filename = new_name
              instance.send("#{prop}_file_name=", filename)
              if ENV["update_filename"]
                instance.save
                puts "Updated filename to #{filename}"
              end
            end
          rescue
            puts "[ERROR] Failed to move #{old_loc}"
          end
        end
      end
    end
  end
  desc "Migrate user roles"
  task migrate_user_roles: :environment do
    User.find_each do |user|
      role = user.role.downcase
      command = "make_#{role}!"

      # Migrate existing profiles to reference user
      if profile = "#{role.titleize}Profile".constantize.find(user.profile_id)
        profile.update(user_id: user.id)
        puts "Reference user with role profile"
      end

      # Create UserRole record i.e. assign actual role
      if user.send(command)
        # Log information for user
        puts "Assigned user ##{user.id} with #{role} role!"
      end
    end
  end
  desc "Move role avatars saved onto their own directory into users"
  task :move_role_avatars => :environment do
    roles = %w(Dentist Patient Secretary).map(&:constantize)
    with_avatars = []
    roles.each do |role|
      puts "Processing #{role.to_s} avatars"
      role.find_each do |user|
        if user.avatar_file_name.present? && !with_avatars.include?(user.id)
          filename = Rails.root.join("public",
                                     "system",
                                     role.to_s.downcase.pluralize,
                                     "avatars",
                                     user.id.to_s.rjust(9, "0").scan(/.{3}/).join('/'),
                                     "original",
                                     user.avatar_file_name
                                    )
          if File.exist?(filename)
            puts "Re-saving avatar attachment #{user.id} - #{filename}"
            avatar = File.new filename
            user.avatar = avatar
            user.save
            # if there are multiple styles, you want to recreate them :
            user.avatar.reprocess! 
            avatar.close
            with_avatars << user.id
          else
            puts "Skipping avatar update for #{role} ##{user.id}"
          end
        else
          puts "Skipping avatar update for #{role} ##{user.id}"
        end
      end
    end
  end
end
