namespace :notifications do
  desc 'Sends daily appointments notification email to Dentists'
  task :send_daily_appointments_email => :environment do
    Dentist.all.each do |dentist|
      if dentist.today_confirmed_appointments.present?
        NotificationMailer.daily_appointments(dentist).deliver
      else
        NotificationMailer.no_appointment(dentist).deliver
      end
    end
  end
end
