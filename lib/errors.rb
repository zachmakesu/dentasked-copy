module Errors
  class AuthenticationError < StandardError; end

  class InvalidEmail < AuthenticationError
    def self.code
      100
    end

    def self.message
      "Invalid email, you must create an account first"
    end
  end

  class InvalidPassword < AuthenticationError
    def self.code
      101
    end

    def self.message
      "Incorrect password supplied"
    end
  end

  class DisabledAccount < AuthenticationError
    def self.code
      102
    end

    def self.message
      "Account is disabled, please contact an admin for more info"
    end
  end

  class InactiveAccount < AuthenticationError
    def self.code
      103
    end

    def self.message
      "Account is not yet activated, please activate your account"
    end
  end
  class ExpiredToken < AuthenticationError
    def self.code
      104
    end

    def self.message
      "Access Token is expired"
    end
  end
  class InvalidToken < AuthenticationError
    def self.code
      105
    end

    def self.message
      "Access Token is invalid"
    end
  end
end
