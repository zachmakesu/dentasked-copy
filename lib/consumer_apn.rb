class ConsumerAPN
  def initialize
    @client = if Rails.env.production?
                Houston::Client.production
              elsif Rails.env.staging?
                Houston::Client.production
              else
                Houston::Client.development
              end
    @client.certificate = File.read "#{Rails.root}/config/consumer_apn_#{Rails.env}.pem"
  end

  def push(notification)
    @client.push(notification)
    if notification.error
      Rails.logger.error "[ConsumerAPN Error] #{notification.error}" 
      ExceptionNotifier.notify_exception(e)
    else
      Rails.logger.debug(notification.inspect) unless Rails.env.production?
    end
  end
end
