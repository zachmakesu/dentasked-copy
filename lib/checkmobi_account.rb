class CheckmobiAccount
  include ActionView::Helpers::NumberHelper
  ENDPOINT = %(https://api.checkmobi.com/v1/my-account).freeze
  ERROR_DEFAULT = %(n/a).freeze
  attr_reader :email, :name, :balance

  def call
    self.tap do
      @response = client.get do |req|
        req.headers['Authorization'] = ENV.fetch("CHECKMOBI_SECRET")
      end
    end
  end

  def email
    parsed_response.fetch("email"){ ERROR_DEFAULT }
  end

  def name
    parsed_response.fetch("subscription"){ Hash.new }.fetch("name"){ ERROR_DEFAULT }
  end

  def balance
    raw_balance = parsed_response.fetch("credit_balance"){ 0 }
    number_to_currency(raw_balance)
  end

  private

  def parsed_response
    JSON.parse(@response.body)
  end

  def client
    Faraday.new(url: ENDPOINT)
  end
end
